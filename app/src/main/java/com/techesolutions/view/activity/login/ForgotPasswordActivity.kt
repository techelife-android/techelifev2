package com.techesolutions.view.activity.login

import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.viewmodel.ForgotPasswordViewModel
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject

/**
 * Created by Neelam on 23-12-2020.
 */
class ForgotPasswordActivity : AppCompatActivity(), View.OnClickListener {
    var clickGetCode: TextView? = null
    var verification_note: TextView? = null
    var heading: TextView? = null
    var userNameET: EditText? = null
    var back:ImageView?=null
    private var languageData: String? = null
    var jsonValidations: JSONObject?=null
    lateinit var forgotPasswordViewModel: ForgotPasswordViewModel
    private val progressDialog = CustomProgressDialog()
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_forgot_password)
        headerTitle.visibility = View.GONE
        forgotPasswordViewModel = ViewModelProvider(this).get(ForgotPasswordViewModel::class.java)

        clickGetCode = findViewById<View>(R.id.btGetCode) as TextView
        verification_note = findViewById<View>(R.id.verification_note) as TextView
        heading = findViewById<View>(R.id.heading) as TextView
        userNameET = findViewById<View>(R.id.etEmail) as EditText
        back=findViewById<View>(R.id.back) as ImageView
        back?.setVisibility(View.VISIBLE)
        clickGetCode?.setOnClickListener(this)
        back?.setOnClickListener(this)
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if(name.equals("forgotPassword")) {
                    val jsonObjectLabels = jsonObject.getJSONObject("labels")
                    verification_note?.text=jsonObjectLabels.getString("get_verification_code").toString()
                    heading?.text=jsonObjectLabels.getString("forgot_password_note").toString()
                    btGetCode?.text=jsonObjectLabels.getString("get_code").toString()
                    jsonValidations=jsonObjectLabels.getJSONObject("validations")
                    Log.v("DEBUG : ", jsonObjectLabels.toString())

                }
            }

        } catch (e: Exception) {
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btGetCode -> {
                if (Utils.isOnline(this@ForgotPasswordActivity)) {
                    setValidation()
                }
            }
            R.id.back -> {
                finish()
            }

        }
    }

    private fun setValidation() {
        if (TextUtils.isEmpty(userNameET!!.text.toString())) {
            if(jsonValidations!=null && jsonValidations!!.length()>0)
            onErrorListener(jsonValidations!!.getString("email_not_empty").toString())
            else
                onErrorListener(getString(R.string.user_notempty))

        } else {
            showLoading("Please wait")
            forgotPasswordViewModel.getCode(userNameET!!.text.toString(),"mobile")!!.observe(this, Observer { forgotPasswordResponse ->

                val success = forgotPasswordResponse.success;
                if(success==true) {
                    closeLoading()
                    val intent = Intent(this@ForgotPasswordActivity,
                        VerificationActivity::class.java)
                    intent.putExtra("hashcode",forgotPasswordResponse.data?.hashcode)
                    startActivity(intent)
                }
                else
                    closeLoading()
                    Toast.makeText(this,forgotPasswordResponse.message, Toast.LENGTH_SHORT).show()
            })

        }
    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

}