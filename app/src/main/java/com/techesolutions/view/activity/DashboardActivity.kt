package com.techesolutions.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.IntroView
import com.techesolutions.data.remote.model.dashboard.Vital
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.MySharedPreference
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.login.LoginActivity
import com.techesolutions.view.activity.vitaldevices.TecheWatchActivity
import com.techesolutions.view.adapter.VitalAdapter
import kotlinx.android.synthetic.main.layout_header_with_title.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Neelam on 24-12-2020.
 */

class DashboardActivity : AppCompatActivity(), View.OnClickListener {
    var recyclerView: RecyclerView? = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    var from: String?=null
    private val introView = IntroView()
    private var progressDialog = CustomProgressDialog()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_dashboard)
        Utils.statusBarSetup(this)
        processExtraData()

        //getting recyclerview from xml
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        val logout = findViewById(R.id.logout) as ImageView
        val playAll = findViewById(R.id.play) as ImageView

        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }
        headerTitle!!.text =  vitalUnitLabels!!.getString("my_vitals")
        if (userId != null && token != null) {
            getVitalList(userId, "Bearer $token", "mobile")
        }
        //adding a layoutmanager
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        logout.setOnClickListener(this)
        playAll.setOnClickListener(this)

    }
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)//must store the new intent unless getIntent() will return the old one
        Log.i("entered onNewIntent", "entered onNewIntent")
        processExtraData()
    }
    private fun processExtraData() {
        //Log.i("From onNewIntent")
        val i: Intent = getIntent()
        from = i.getStringExtra("from")
        //Log.i("from",from)
        if( from.equals("login")|| from.equals("timezone")) {
            showIntro()
        }
        if(from.equals("nextdashboard"))
        {
            logout.visibility=View.GONE
        }
        else{
            val shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
            val token: String? = shared.getString(Constants.KEY_TOKEN, null)
            val userId: String? = shared.getString(Constants.Login_User_ID, null)
            if (userId != null && token != null) {
                getVitalList(userId, "Bearer $token", "mobile")
            }
        }
    }
    private fun getVitalList(userId: String, token: String, apporigin: String) {
       // showLoading("Fetching data")
        val call = NetworkService.apiInterface.getVilatDeviceList(token, userId, apporigin)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                // closeLoading()
                Log.v("DEBUG : ", t.message.toString())

            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                //closeLoading()
                val stringResponse = response.body()?.string()
                if (stringResponse != null) {
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        val dlist = jsonObj.getJSONArray("data")
                        Log.i("dlist", "" + dlist)

                        val vitals = ArrayList<Vital>()
                        // val vital = jsonObjectLabels.getString("Vital").toString()
                        for (i in 0 until dlist.length()) {
                            val item = dlist.getJSONObject(i)
                            val data = item.getJSONObject("Data")
                            var takenFrom: String = ""
                            if (item!!.getString("Source").equals("device"))
                                takenFrom = vitalUnitLabels!!.getString("taken_from_LinkedDevice")
                            else {
                                takenFrom = vitalUnitLabels!!.getString("taken_from_Manualy")
                            }
                            if (item.getString("Vital").equals("Activities")) {
                                vitals.add(
                                    Vital(
                                        item.getString("Vital"),
                                        vitalNameLabels!!.getString("Activities"),
                                        vitalUnitLabels!!.getString("steps"),
                                        data!!.getString("Stepcount"),
                                        vitalUnitLabels!!.getString("Stepscalories"),
                                        data!!.getString("Stepscalories"),
                                        data!!.getString("DateandTime"),
                                        takenFrom,
                                        item.getString("DeviceType"),
                                        item.getString("DeviceID")
                                    )
                                )
                            } else if (item.getString("Vital").equals("Blood_Pressure")) {
                                vitals.add(
                                    Vital(
                                        item.getString("Vital"),
                                        vitalNameLabels!!.getString("Blood_Pressure"),
                                        vitalUnitLabels!!.getString("systolic"),
                                        data!!.getString("Systolic_bp"),
                                        vitalUnitLabels!!.getString("diastolic"),
                                        data!!.getString("Diastolic_bp"),
                                        data!!.getString("DateandTime"),
                                        takenFrom,
                                        item.getString("DeviceType"),
                                        item.getString("DeviceID")
                                    )
                                )
                            } else if (item.getString("Vital").equals("Temperature")) {
                                vitals.add(
                                    Vital(
                                        item.getString("Vital"),
                                        vitalNameLabels!!.getString("Temperature"),
                                        vitalUnitLabels!!.getString("Temperature_F"),
                                        data!!.getString("Temperature"),
                                        vitalUnitLabels!!.getString("Temperature_C"),
                                        data!!.getString("Temperature_Unit"),
                                        data!!.getString("DateandTime"),
                                        takenFrom,
                                        item.getString("DeviceType"),
                                        item.getString("DeviceID")
                                    )
                                )
                            } else if (item.getString("Vital").equals("Pulse_Oximetry")) {
                                vitals.add(
                                    Vital(
                                        item.getString("Vital"),
                                        vitalNameLabels!!.getString("Pulse_Oximetry"),
                                        vitalUnitLabels!!.getString("O2_Saturation"),
                                        data!!.getString("O2_Sat"),
                                        "",
                                        "",
                                        data!!.getString("DateandTime"),
                                        takenFrom,
                                        item.getString("DeviceType"),
                                        item.getString("DeviceID")
                                    )
                                )
                            } else if (item.getString("Vital").equals("Heart_Rate")) {
                                vitals.add(
                                    Vital(
                                        item.getString("Vital"),
                                        vitalNameLabels!!.getString("Heart_Rate"),
                                        vitalUnitLabels!!.getString("BPM"),
                                        data!!.getString("Heart_Rate_Beats"),
                                        "",
                                        "",
                                        data!!.getString("DateandTime"),
                                        takenFrom,
                                        item.getString("DeviceType"),
                                        item.getString("DeviceID")
                                    )
                                )
                            } else if (item.getString("Vital").equals("Weight")) {
                                vitals.add(
                                    Vital(
                                        item.getString("Vital"),
                                        vitalNameLabels!!.getString("Weight"),
                                        vitalUnitLabels!!.getString("Weight_In_Kgs"),
                                        data!!.getString("Weight"),
                                        vitalUnitLabels!!.getString("Weight_In_Lbs"),
                                        data!!.getString("Weight_Unit"),
                                        data!!.getString("DateandTime"),
                                        takenFrom,
                                        item.getString("DeviceType"),
                                        item.getString("DeviceID")
                                    )
                                )
                            } else if (item.getString("Vital").equals("Blood_Sugar")) {
                                vitals.add(
                                    Vital(
                                        item.getString("Vital"),
                                        vitalNameLabels!!.getString("Blood_Sugar"),
                                        vitalUnitLabels!!.getString("Sugar_Level"),
                                        data!!.getString("Sugar"),
                                        "",
                                        "",
                                        data!!.getString("DateandTime"),
                                        takenFrom,
                                        item.getString("DeviceType"),
                                        item.getString("DeviceID")
                                    )
                                )
                            }
                        }
                        //adding some dummy data to the list

                        val adapter = VitalAdapter(this@DashboardActivity, vitals)

                        //now adding the adapter to recyclerview
                        recyclerView?.adapter = adapter
                    } else {
                        Toast.makeText(
                            this@DashboardActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    //  closeLoading()
                    val intent = Intent(this@DashboardActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }

        })

    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.logout -> {
                MySharedPreference.setBooleanPreference(this, Constants.KEY_LOGGEDIN, false)
                MySharedPreference.setPreference(this, Constants.KEY_PASSWORD, "")
                MySharedPreference.setPreference(this, Constants.KEY_USERNAME, "")
                MySharedPreference.setPreference(this, Constants.KEY_TOKEN, "")
              /*  val sharedPreferences: SharedPreferences =
                    getSharedPreferences("MyPrefs", MODE_PRIVATE)
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.clear()
                editor.commit()*/
                val intent = Intent(this@DashboardActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
            R.id.play -> {
                val intent = Intent(this@DashboardActivity, TecheWatchActivity::class.java)
                intent.putExtra("title", vitalNameLabels!!.getString("Activities"))
                intent.putExtra("vitalName", vitalNameLabels!!.getString("Activities"))
                intent.putExtra("unitName1", vitalUnitLabels!!.getString("steps"))
                intent.putExtra("unitName2", vitalUnitLabels!!.getString("Stepscalories"))
                intent.putExtra("type", "onego")
                startActivity(intent)
                //finish()
            }
        }

    }

    private fun closeIntro() {
        if (introView != null && introView?.dialog.isShowing())
            introView?.dialog.dismiss()
    }

    private fun showIntro() {
        introView?.show(this)
        introView.dialog.findViewById<View>(R.id.cp_bg_view).setOnClickListener(View.OnClickListener { view ->
            closeIntro()
        })
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }
}

