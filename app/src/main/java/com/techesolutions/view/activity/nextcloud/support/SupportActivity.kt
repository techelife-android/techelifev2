package com.techesolutions.view.activity.nextcloud.support

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.fontedVews.edittext.ATecheRegularEditText
import com.techesolutions.data.remote.model.support.TicketSupportData
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.callbacks.ItemClickListener
import kotlinx.android.synthetic.main.activity_cemaras.*
import kotlinx.android.synthetic.main.activity_support.*
import kotlinx.android.synthetic.main.activity_support.noDataText
import kotlinx.android.synthetic.main.add_web_info_dialog.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SupportActivity : AppCompatActivity(), View.OnClickListener, ItemClickListener {
    private var progressDialog = CustomProgressDialog()
    var token: String? = null
    var username: String? = null
    var userId: String? = null
    var unique_id: String? = null
    var password: String? = null
    var recyclerView: RecyclerView? = null
    var supportAdapter: SupportAdapter? = null
    var ticketSupportData = ArrayList<TicketSupportData>()
    var format =
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    var formatDate =
        SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_support)
        Utils.setDimensions(this)

        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        token = shared.getString(Constants.KEY_TOKEN, null)
        username = shared.getString(Constants.KEY_NEXT_CLOUD_USER, null)
        password = shared.getString(Constants.KEY_UNIQUE, null)
        userId=shared.getString(Constants.Login_User_ID, null)
        unique_id = shared.getString(Constants.KEY_UNIQUE, null)
        setUpHeader()
        getTicketList()
        setListeners()
    }
    private fun setListeners() {
        // listening to search query text change
        tvSeacrh.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                supportAdapter!!.filter!!.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                if (supportAdapter != null) supportAdapter!!.filter!!.filter(query)
                return false
            }
        })
    }

    @SuppressLint("WrongConstant")
    open fun setUpHeader() {
        val headerTitleTV =
            findViewById<View>(R.id.headerTitle) as TextView
        headerTitleTV.text = "Tickets"
        addEvent.visibility = View.VISIBLE
        addEvent.setOnClickListener(this)
        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }
        rlRefresh.setOnClickListener(this)

        recyclerView = findViewById(R.id.recycler_view) as RecyclerView

        //adding a layoutmanager
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

    }

    override fun onClick(v: View?) {

        when (v?.id) {

            R.id.addEvent -> {
                showCreateTicketDialog(this@SupportActivity)
                  }

            R.id.rlRefresh -> {
              getTicketList()
            }
        }
    }
    private fun showCreateTicketDialog(context: SupportActivity) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.generate_ticket)

        val yesBtn = dialog.findViewById(R.id.save) as TextView
        val noBtn = dialog.findViewById(R.id.cancel) as TextView
        val etSubject = dialog.findViewById(R.id.etSubject) as ATecheRegularEditText
        val etDescription = dialog.findViewById(R.id.etDescription) as ATecheRegularEditText

        yesBtn.setOnClickListener {
            if (TextUtils.isEmpty(etSubject!!.text.toString())) {
                onErrorListener("Please enter Subject")
            }
            if (TextUtils.isEmpty(etDescription!!.text.toString())) {
                onErrorListener("Please enter description")
            }
            else {
                dialog.dismiss()
                addTicket(
                    etSubject!!.text.toString(),
                    etDescription!!.text.toString()
                )

            }
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }
    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun addTicket(subject: String, description: String) {

        showLoading("Please wait")
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.addTickets(
                "Bearer $token",
                "mobile",
                subject,
                description,
                "pending"
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        getTicketList()
                    } else {
                        Toast.makeText(
                            this@SupportActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }


            })
        }

    }
    private fun getTicketList() {

        if (Utils.isOnline(this)) {
            showLoading("Fetching data...")
            ticketSupportData!!.clear()
            val call = NetworkService.apiInterface.getTickets(
                "Bearer $token",
                "mobile"
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    if (response != null) {
                        val stringResponse = response.body()?.string()
                        val jsonObj = JSONObject(stringResponse)
                        val success = jsonObj!!.getString("success")
                        if (success != null && success.toString().equals("true")) {
                            setData( jsonObj)
                        } else {
                            Toast.makeText(
                                this@SupportActivity,
                                jsonObj!!.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    else{
                        Toast.makeText(this@SupportActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                    }
                }

            })

        }

    }

    private fun setData(jsonObj: JSONObject) {
        val jObj = jsonObj.getJSONObject("data")
        val dlist = jObj.getJSONArray("data")
        if(dlist.length()>0) {
            noDataText!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
            for (i in 0 until dlist.length()) {
                val data = dlist.getJSONObject(i)

                ticketSupportData!!.add(
                    TicketSupportData(
                        data!!.getInt("id"),
                        data!!.getString("ticket_no"),
                        data!!.getString("subject"),
                        data!!.getString("description"),
                        data!!.getString("status"),
                        data!!.getString("from_id"),
                        data!!.getString("to_id"),
                        data!!.getString("created_date"),
                        data!!.getString("updated_date"),
                        data!!.getString("from_name"),
                        data!!.getString("from_usertype"),
                        data!!.getString("to_name")
                    )
                )


                //adding some dummy data to the list

                supportAdapter = SupportAdapter(this@SupportActivity, ticketSupportData!!, this)
                //now adding the adapter to recyclerview
                recyclerView?.adapter = supportAdapter
            }

        }
        else{
            noDataText!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
        }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {
        /*val intent: Intent = Intent(this@SupportActivity, CommentsActivity::class.java)
        startActivity(intent)*/
    }

}