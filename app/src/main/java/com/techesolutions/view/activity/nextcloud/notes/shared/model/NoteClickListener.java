package com.techesolutions.view.activity.nextcloud.notes.shared.model;

import android.view.View;

import org.jetbrains.annotations.Nullable;

public interface NoteClickListener {
    void onNoteClick(int position);

    void onNoteDeleteClick(int position);

    boolean onNoteLongClick(int position, View v);

}