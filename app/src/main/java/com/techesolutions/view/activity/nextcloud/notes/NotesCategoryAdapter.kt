package com.techesolutions.view.activity.nextcloud.notes

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.utils.Constants

class NotesCategoryAdapter(
    context: Context,
    val notesList: List<NavigationAdapter.CategoryNavigationItem>?
) : RecyclerView.Adapter<NotesCategoryAdapter.ViewHolder>() {

    var activity:Context = context
    //var noteClickListener=noteclick
    /*  val now = Date()
      var dateValue = formatDate.format(now)
  */
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesCategoryAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.note_folder_items, parent, false)
        return ViewHolder(v, activity)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NotesCategoryAdapter.ViewHolder, position: Int) {
        notesList?.get(position)?.let { holder.bindItems(it, activity, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return notesList?.size!!
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(notes: NavigationAdapter.CategoryNavigationItem, context: Context, position: Int) {
            val textViewName = itemView.findViewById(R.id.title) as TextView
            val content = itemView.findViewById(R.id.content) as TextView
            val image = itemView.findViewById(R.id.activity_icon) as ImageView
            val delete = itemView.findViewById(R.id.delete) as ImageView
            delete!!.visibility=View.GONE
            val date = itemView.findViewById(R.id.date) as TextView
            if(notes.label.equals(""))
            {
                textViewName.text = "Uncategorized"
            }
            else {
                textViewName.text = notes.label
            }
            if(notes.count!! > 1) {
                content.text = "" + notes.count + " Files"
            }
            else{
                content.text = "" + notes.count + " File"
            }
            image.setImageResource(R.drawable.folder)
            itemView?.setOnClickListener {
                val intent = Intent(context, NotesActivity::class.java)
                intent.putExtra("category",notes.label)
                context.startActivity(intent)
            }

        }

    }

    fun getItem(notePosition: Int): NavigationAdapter.CategoryNavigationItem? {
        return notesList!!.get(notePosition)
    }

//    fun select(position: Int?): Boolean {
//        return !selected.contains(position) && selected.(position)
//    }
}