package com.techesolutions.view.activity.nextcloud.files.contacts;

import android.annotation.SuppressLint
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.common.utils.Log_OC
import com.owncloud.android.lib.resources.files.DownloadFileRemoteOperation
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils.getRootDirPath
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import ezvcard.Ezvcard
import kotlinx.android.synthetic.main.activity_documents.*
import kotlinx.android.synthetic.main.activity_pdf_view.*
import kotlinx.android.synthetic.main.layout_header.*
import org.mozilla.universalchardet.ReaderFactory
import java.io.*
import java.lang.ref.WeakReference
import java.util.*

class FileContactActivity : AppCompatActivity(), OnRemoteOperationListener,
    OnDatatransferProgressListener, NoteClickListener {
    val EXTRA_STREAM_URL = "STREAM_URL"
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var filename: String? = null
    private var file: String? = null
    var recyclerView: RecyclerView? = null
    var adapter: ContactAdapter? = null
    private var progressDialog = CustomProgressDialog()
    lateinit var dbContacts: ArrayList<Contacts>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
        headerTitle.text = "Contacts"
        showLoading("Please wait...")
        mHandler = Handler()
        val serverUri = Uri.parse(getString(R.string.server_base_url))
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                getString(R.string.username),
                getString(R.string.password_)
            )
        )

        val extras = intent.extras
        file = extras!!.getString(EXTRA_STREAM_URL)
        val downFolder = File(getRootDirPath(baseContext))
        val downloadedFile = File(downFolder, file)
        /*if (downloadedFile.exists()) {
            Log.i("Contacts downloaded", downloadedFile.absolutePath)
            readVCF(downloadedFile)
        } else {*/

            if (file != null) {
                startDownload(file!!)
            }
       // }
        back.setOnClickListener { finish() }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    @SuppressLint("WrongConstant")
    private fun readVCF(downloadedFile: File) {
        dbContacts = ArrayList<Contacts>()
        dbContacts!!.clear()
        try {
            val vcards = Ezvcard.parse(downloadedFile).all()
            Log.i("Contacts", vcards.toString())

            for (vcard in vcards) {
                Log.i("Name: ", vcard.formattedName.value)
                var name=""
                var mobile=""
                var mobType=""
                var memberType="Friend"
                if(!vcard.formattedName.value.isEmpty())
                {
                    name= vcard.formattedName.value
                }
                if(!vcard.telephoneNumbers.isEmpty()){
                    mobType= vcard.telephoneNumbers[0].types.toString()
                }
                if(!vcard.telephoneNumbers.isEmpty()){
                    mobile=vcard.telephoneNumbers[0].text
                }

                if(!vcard.relations.isEmpty()){
                    memberType=vcard.relations[0].text
                }
                dbContacts!!.add(
                    Contacts(
                        name,
                        mobType,
                        mobile,
                        memberType
                    )
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
         Log.i("Contacts", dbContacts.toString())
         closeLoading()
          recyclerView = findViewById(R.id.recycler_view) as RecyclerView
          recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
          adapter = ContactAdapter(this@FileContactActivity, dbContacts, this)

          //now adding the adapter to recyclerview
          recyclerView?.adapter = adapter
        // File file = new File( p.getRemotePath());
    }

    private fun startDownload(remotePath: String) {
        Log.i("Contacts", "downloading")
        val downFolder = File(getRootDirPath(baseContext))
        downFolder.mkdir()
        val downloadOperation = DownloadFileRemoteOperation(remotePath, downFolder.absolutePath)
        downloadOperation.addDatatransferProgressListener(this)
        downloadOperation.execute(mClient, this, mHandler)
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {
        if (!result!!.isSuccess()) {
            Toast.makeText(this, R.string.todo_operation_finished_in_fail, Toast.LENGTH_SHORT)
                .show()
            Log.e("PdfViewActivity", result.getLogMessage(), result.getException())
        } else {
            /* Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                 .show()*/
            val downFolder = File(getRootDirPath(baseContext))
            val downloadedFile = File(downFolder, file)
            downloadedFile?.let {
                readVCF(
                    it
                )
            }
        }
    }

    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileAbsoluteName: String?
    ) {
        /*val percentage =
            if (totalToTransfer > 0) totalTransferredSoFar * 100 / totalToTransfer else 0
        mHandler!!.post {
            val progressView: TextView? = null
            if (progressBar != null) {
                progressBar.progress = percentage.toInt()
            }
        }
        progressBar.visibility= View.VISIBLE*/
    }

    override fun onNoteClick(position: Int) {
        TODO("Not yet implemented")
    }

    override fun onNoteDeleteClick(position: Int) {
        TODO("Not yet implemented")
    }

    override fun onNoteLongClick(position: Int, v: View?): Boolean {
        TODO("Not yet implemented")
    }
}

private class TextLoadAsyncTask(
    private val textViewReference: WeakReference<TextView>,
    private val progressViewReference: WeakReference<FrameLayout>
) : AsyncTask<Any?, Void?, StringWriter>() {
    override fun onPreExecute() {
        // not used at the moment
    }

    override fun doInBackground(vararg params: Any?): StringWriter? {
        require(params.size == PARAMS_LENGTH) {
            ("The parameter to " + TextLoadAsyncTask::class.java.name
                    + " must be (1) the file location")
        }
        val location = params[0] as String
        Log.i("location", location)
        var scanner: Scanner? = null
        val source = StringWriter()
        val bufferedWriter = BufferedWriter(source)
        var reader: Reader? = null
        try {
            val file = File(location)
            reader = ReaderFactory.createReaderFromFile(file)
            scanner = Scanner(reader)
            while (scanner.hasNextLine()) {
                bufferedWriter.append(scanner.nextLine())
                if (scanner.hasNextLine()) {
                    bufferedWriter.append("\n")
                }
            }
            bufferedWriter.close()
            val exc = scanner.ioException()
            if (exc != null) {
                throw exc
            }
        } catch (e: IOException) {
            Log_OC.e("TAG", e.message, e)
            //finish()
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: IOException) {
                    Log_OC.e("TAG", e.message, e)
                    // finish()
                }
            }
            scanner?.close()
        }
        return source
    }

    override fun onPostExecute(stringWriter: StringWriter) {
        val textView = textViewReference.get()
        if (textView != null) {
            var originalText = stringWriter.toString()
            Log.i("originalText", originalText)

            //setText(textView, originalText, getFile(), requireActivity())
            textView.text = originalText
            textView.visibility = View.VISIBLE
        }
        val progress = progressViewReference.get()
        if (progress != null) {
            progress.visibility = View.GONE
        }
    }

    companion object {
        private const val PARAMS_LENGTH = 1
    }
}
