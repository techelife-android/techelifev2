package com.techesolutions.data.remote.model.devices

data class DeviceData(
    val iotassign_id: Int,
    val isactive: Int,
    val skuno: String,
    val sku_category: String,
    val skuname: String,
    val description: String,
    val skutypeid: String,
    val deviceid: String,
    val room_range: String,
    val room_label: String,
    val door_range: String,
    val door_label: String,
    val name: String,
    val firstname: String,
    val surname: String,
    val id: String,
    val created: String,
    val buyingskuid: String,
    val addeddateW: String
)
/*

"skutypeid": 1,
"photodocid": "",
"deviceid": "deviceno92199",
"room_range": "ROOM1",
"room_label": "Hall",
"door_range": "",
"door_label": "",
"name": "",
"firstname": "mahi",
"surname": "sharma",
"id": 99,
"created": "2021-09-06 07:22:13",
"buyingskuid": 47,
"addeddateW": "09-06-2021 03:22"*/
