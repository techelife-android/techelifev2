package com.techesolutions.view.activity.nextcloud.contacts;
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.utils.Constants
import com.techesolutions.utils.MySharedPreference
import kotlinx.android.synthetic.main.actvity_contact_settings.*
import kotlinx.android.synthetic.main.actvity_file_settings.*
import kotlinx.android.synthetic.main.actvity_file_settings.photoSwitch
import kotlinx.android.synthetic.main.layout_header.*
import java.util.*


class  ContactSettingsActivity : AppCompatActivity() {
    private var uploadEnable: Boolean? = null
    private var autoSaveEnable: Boolean? = null
    var shared: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actvity_contact_settings)
                headerTitle!!.text = "Contact Settings"
        shared = getSharedPreferences("TechePrefs", MODE_PRIVATE)
        uploadEnable = shared!!.getBoolean(Constants.KEY_CONTACT_UPLOAD, false)
        autoSaveEnable = shared!!.getBoolean(Constants.KEY_CONTACT_AUTO_SAVE, false)
        //videoEnable = true
        autoUploadSwitch.isChecked= uploadEnable as Boolean
        autoSaveSwitch.isChecked= autoSaveEnable as Boolean
        back.setOnClickListener { finish() }
        autoUploadSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            Log.i("isChecked", "" + isChecked)
            MySharedPreference.setBooleanPreference(this, Constants.KEY_CONTACT_UPLOAD, isChecked)
        })
        autoSaveSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            Log.i("isChecked", "" + isChecked)
            MySharedPreference.setBooleanPreference(this, Constants.KEY_CONTACT_AUTO_SAVE, isChecked)
        })

    }

}
