package com.techesolutions.view.callbacks;

import android.view.View;

public interface UserInfoClickListener {
   void onEditClick(int position, String type, String mode);
   void onDeleteClick(int position);
   void onViewUpdate(View v1,View v2, int position);
}
