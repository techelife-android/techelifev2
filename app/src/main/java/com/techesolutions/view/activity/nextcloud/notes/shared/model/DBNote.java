package com.techesolutions.view.activity.nextcloud.notes.shared.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.util.Calendar;

/**
 * DBNote represents a single note from the local SQLite database with all attributes.
 * It extends CloudNote with attributes required for local data management.
 */
public class DBNote extends CloudNote implements Item, Serializable {

    private final long id;
    private final long accountId;
    private final long remoteId;
    private DBStatus status;
    private String excerpt;
    private String title;
    private String category;
    private String content;
    private int scrollY;

//    public DBNote(long id, long remoteId, long modified, String title, String content, boolean favorite, String category, String etag, DBStatus status, long accountId, String excerpt, int scrollY) {
//        super(remoteId, modified, title, content, favorite, category, etag);
//        this.id = id;
//        this.excerpt = excerpt;
//        this.status = status;
//        this.accountId = accountId;
//        this.scrollY = scrollY;
//    }

    public DBNote(long id, long remoteId, long modified, String title, String content, boolean favorite, String category, String etag, DBStatus localEdited, long accountId, String excerpt, int scrollY) {
        super(remoteId, modified, title, content, favorite, category, etag);
        this.id = id;
        this.remoteId = remoteId;
        this.title = title;
        this.excerpt = excerpt;
        this.category = category;
        this.content = content;
        this.status = status;
        this.accountId = accountId;
        this.scrollY = scrollY;
    }

    public long getId() {
        return id;
    }

    public long getAccountId() {
        return accountId;
    }

    public DBStatus getStatus() {
        return status;
    }

    public void setStatus(DBStatus status) {
        this.status = status;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        super.setContent(content);
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        super.setContent(title);
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        super.setContent(category);
    }

    @Override
    public boolean isSection() {
        return false;
    }

    @NonNull
    @Override
    public String toString() {
        return "DBNote{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", remoteId=" + remoteId +
                ", status=" + status +
                ", excerpt='" + excerpt + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

    public int getScrollY() {
        return scrollY;
    }

    public void setScrollY(int scrollY) {
        this.scrollY = scrollY;
    }
}
