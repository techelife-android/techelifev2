package com.techesolutions.view.activity.nextcloud

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.techesolutions.view.activity.nextcloud.files.FileFolderData

@Database(entities = [FileFolderData::class], version = 1)
abstract class NextcloudDatabase : RoomDatabase() {

    abstract fun filesFolderDao(): FileFolderDao

    companion object {
        private var instance: NextcloudDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): NextcloudDatabase {
            if(instance == null)
                instance = Room.databaseBuilder(ctx.applicationContext, NextcloudDatabase::class.java,
                    "teche_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build()

            return instance!!

        }

        private val roomCallback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                populateDatabase(instance!!)
            }
        }

        private fun populateDatabase(db: NextcloudDatabase) {
            val noteDao = db.filesFolderDao()
        }
    }



}