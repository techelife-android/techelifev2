package com.peng.ppscalelibrary.BleManager.Interface;


import com.peng.ppscalelibrary.BleManager.Model.BleDeviceModel;
import com.peng.ppscalelibrary.BleManager.Model.LFPeopleGeneral;

public interface BleDataProtocoInterface {

    /*过程数据*/
    void progressData(LFPeopleGeneral bodyDataModel);

    /*锁定数据*/
    void lockedData(LFPeopleGeneral bodyDataModel, BleDeviceModel deviceModel, boolean isHeartRating);

    /*历史数据*/
    void historyData(boolean isEnd, LFPeopleGeneral bodyDataModel, String date);

    /*当前的设备信息*/
    void deviceInfo(BleDeviceModel deviceModel);
}
