package com.techesolutions.services.ble

import java.util.*

object Const {
    /**
     * Pulse Oximeter
     */
    val PO_UUID_SERVICE_DATA =
        UUID.fromString("49535343-fe7d-4ae5-8fa9-9fafd205e455")
    val PO_UUID_CHARACTER_RECEIVE =
        UUID.fromString("49535343-1e4d-4bd9-ba61-23c647249616")
    val PO_UUID_MODIFY_BT_NAME =
        UUID.fromString("00005343-0000-1000-8000-00805F9B34FB")

    /**
     * Blood Pressure
     * I/System.out: -3
     * I/System.out: -3
     * I/System.out: -4
     * I/System.out: 118  ( Systolic )   if this index is 0 that means it is still measuring.
     * I/System.out: 79   ( Diastolic )
     * I/System.out: 66   ( Pulse rate )
     * I/System.out: 13
     * I/System.out: 10
     *
     *
     */
    val BP_UUID_SERVICE_DATA =
        UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb")
    val BP_UUID_CHARACTER_RECEIVE =
        UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb")
    val BP_UUID_CHARACTER_WRITE =
        UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb")

    /**
     * Thermometer
     * I/System.out: -6
     * I/System.out: 16
     * I/System.out: 8
     * I/System.out: 81
     * I/System.out: 1
     * I/System.out: 110  (110 + 256 = 366 means 36.6 is the temperature)
     * I/System.out: 1
     * I/System.out: -28
     * I/System.out: 0
     * I/System.out: 0
     * I/System.out: 0
     * I/System.out: -91
     * I/System.out: -1
     *
     * Temp value index is = 5
     */
    val T_UUID_SERVICE_DATA =
        UUID.fromString("0000fe18-0000-1000-8000-00805f9b34fb")
    val T_UUID_CHARACTER_RECEIVE =
        UUID.fromString("0000fe10-0000-1000-8000-00805f9b34fb")

    /**
     * Unique config uuid
     */
    val UUID_CLIENT_CHARACTER_CONFIG =
        UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
}
