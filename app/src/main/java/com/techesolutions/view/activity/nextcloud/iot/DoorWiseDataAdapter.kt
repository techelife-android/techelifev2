package com.techesolutions.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.iot.IOTDoorsReadings
import com.techesolutions.data.remote.model.nextclouddashboard.CameraItems
import com.techesolutions.utils.Utils
import java.util.*

class DoorWiseDataAdapter(
    context:Context,
    val  userList: ArrayList<IOTDoorsReadings>
) : RecyclerView.Adapter<DoorWiseDataAdapter.ViewHolder>() {
    var activity:Context = context

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoorWiseDataAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.door_iot_readings_item, parent, false)
        return ViewHolder(v, activity)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: DoorWiseDataAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position],activity)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(data: IOTDoorsReadings, context: Context) {

            val title = itemView.findViewById(R.id.title) as TextView
            val date  = itemView.findViewById(R.id.date) as TextView
            val time  = itemView.findViewById(R.id.time) as TextView
            val status  = itemView.findViewById(R.id.status) as TextView
            title.text = data.labelDoor
           /* var timedate: String= Utils.convertEventDateFormat(
                ""+data.entry_time,
                context
            )*/
          /*  String[] time = timedate!!.split("at")*/
            date.text = Utils.convertIOTDoorDateFormat(
                ""+data.entry_timeW,
                context
            )
            time.text =  Utils.convertIOTDoorTimeFormat(
                ""+data.entry_timeW,
                context
            )
            if(data.door_status==0)
            {
                status.text = "Status - Closed"
            }
            else{
                status.text =  "Status - Open"
            }

        }
    }

}