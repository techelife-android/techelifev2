package com.techesolutions.utils

import android.content.Context
import android.util.AttributeSet
import android.widget.Button

/**
 * Created by Neelam on 19-12-2020.
 */
class RegularFontButton : androidx.appcompat.widget.AppCompatButton {
    constructor(context: Context) : super(context) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        if (Utils.aTecheRegularFont != null) {
            typeface = Utils.aTecheRegularFont
        } else {
            Utils.setDimensions(context)
            typeface = Utils.aTecheRegularFont
            println("Oxigen ligh tnot loaded")
        }
    }
}
