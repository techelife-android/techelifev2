package com.techesolutions.view.activity.nextcloud.events

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.techesolutions.utils.NotificationUtils

class ReminderBroadcast : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val _notificationUtils =
            NotificationUtils(context)
        val _builder = _notificationUtils.setNotification(
            intent!!.getStringExtra("title"), intent!!.getStringExtra("description")
        )
        _notificationUtils.manager.notify(101, _builder.build())
    }
}