package com.techesolutions.view.activity.vitaldevices

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.dashboard.POVital
import com.techesolutions.services.NetworkService
import com.techesolutions.services.ble.BleController
import com.techesolutions.services.ble.Const
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.DashboardActivity
import kotlinx.android.synthetic.main.activity_device_controls.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import android.content.Context

class TechePOActivity : AppCompatActivity(), BleController.StateListener , View.OnClickListener {
    var unit1: String? = null
    var unit2: String? = null
    var keyUnit3 = ""
    var title: String? = null
    var token: String? = null
    var userId: String? = null
    var vitalName: String? = null
    var subVitalName: String? = null
    var vitalNameLabels: JSONObject? = null
    var vitalUnitLabels: JSONObject? = null
    private var dataValue1: String? = null
    private var dataValue3: String? = null
    private var dateValue: String? = null
    private var timeValue: String? = null
    private var type: String? = null

    private var macAddress = ""
    private var mBleControl: BleController? = null
    lateinit var vitals: ArrayList<POVital>
    var isSend=false
    val tmpBtChecker: MutableList<BluetoothDevice> = mutableListOf()

    var formatterDate =
        SimpleDateFormat("MMM dd, yyyy | HH:mm")
    var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var formatTime = SimpleDateFormat("HH:mm:ss")
    private val progressDialog = CustomProgressDialog()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_controls)
        overridePendingTransition(R.anim.act_pull_in_right, R.anim.act_push_out_left)
        Utils.setDimensions(this)
        val i = intent
        title = i.getStringExtra("title")
        headerTitle!!.text = title
        title = i.getStringExtra("title")
        vitalName = i.getStringExtra("vitalName")
        subVitalName = i.getStringExtra("subVitalName")
        unit1 = i.getStringExtra("unitName1")
        unit2 = i.getStringExtra("unitName2")
        type = i.getStringExtra("type")
        headerTitle!!.text=title
        deviceImage.setImageResource(R.drawable.ic_po)
        var shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        token = shared.getString(Constants.KEY_TOKEN, null)
        userId = shared.getString(Constants.Login_User_ID, null)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

//        val deviceModel = i.getStringExtra("deviceModel")
//        val brand = i.getStringExtra("brand")
        // init BLE
        mBleControl = BleController(this) //BleController.getDefaultBleController(this);
        mBleControl!!.init(Const.PO_UUID_SERVICE_DATA, Const.PO_UUID_CHARACTER_RECEIVE, null)
        mBleControl!!.enableBtAdapter()
        mBleControl!!.bindService(this)
        ok.text=vitalNameLabels!!.getString("btn_Ok_Completed")
        addManuallyTV!!.text=vitalNameLabels!!.getString("addmanually")
        sendResultsBtnID!!.text=vitalNameLabels!!.getString("taptoretryconnection")
        textViewAllergyHeading!!.text=vitalNameLabels!!.getString("devicename")
        textViewTypeHeading!!.text=vitalNameLabels!!.getString("macaddress")
        connectedHeading!!.text=vitalNameLabels!!.getString("notconnected")
        //batteryHeading.text=vitalNameLabels!!.getString("notconnected")
        doneBT.setOnClickListener(this)
        sendBT.setOnClickListener(this)
        back.setOnClickListener(this)
        retryConnection.setOnClickListener(this)
        addManually.setOnClickListener(this)
        skip.setOnClickListener(this)

        // Start Scanning
        startScanning()
        //retryConnection.setOnClickListener(t)
    }

    private fun startScanning() {
        // init views
        tmpBtChecker.clear()
        connectedHeading!!.text=vitalNameLabels!!.getString("notconnected")
        connectedHeading!!.setTextColor(resources.getColor(R.color.peachy_pink))
        macAddressHeaderTV!!.text="-"
        readingsLayout!!.visibility = View.GONE
        doneBT!!.visibility = View.GONE
        sendBT!!.visibility = View.GONE
        retryConnection!!.visibility = View.GONE
        getBP!!.visibility = View.GONE
        showLoading("Scanning...")
        Handler().postDelayed(
            { mBleControl!!.scanLeDevice(true) },
            2000
        ) // delay in milliseconds (1000)
    }

var isfound: Boolean=false
    override fun onFoundDevice(device: BluetoothDevice?) {
        if (device != null && device.name != null && device.name
                .equals("BerryMed", ignoreCase = true) && !isfound
        ) {
            isfound=true
            closeLoading()
            showLoading("Connecting...")
            mBleControl!!.stopScan()
            macAddress = device.address
            macAddressHeaderTV!!.text = macAddress
            deviceNameHeaderTV!!.text="BerryMed"
            mBleControl!!.connect(device)
            return
        }
    }

    override fun onConnected() {
        closeLoading()
        runOnUiThread {
            connectedHeading!!.text=vitalNameLabels!!.getString("connected")
            connectedHeading!!.setTextColor(resources.getColor(R.color.colorPrimary))
            retryConnection!!.visibility = View.GONE
        }
    }

    override fun onDisconnected() {
        displayRetry()
    }

    override fun onReceiveData(dat: ByteArray?) {
        println("-----------")
        if (dat != null) {
            for (i in dat.indices) {
                println(dat[i])
            }
        }
        if (!dat!![4].equals(127)) {
            closeLoading()
            val now = Date()
            val measurementDate = formatterDate.format(now)
            dateValue = formatDate.format(now)
            timeValue = formatTime.format(now)
            val spO2Value = dat[4].toInt()
            val pulseValue = dat[3].toInt()
            keyUnit3 = "Heart_Rate_Beats"
            dataValue1 = spO2Value.toString()
            dataValue3 = pulseValue.toString()
            displayResults(
                measurementDate, "O2 saturation", spO2Value.toString(), unit1,
                "", "", "",
                "Pulse", pulseValue.toString(), "Beats/min"
            )
            vitals = java.util.ArrayList<POVital>()
            vitals.clear()
            vitals.add(
                POVital(
                    spO2Value.toString(),
                    pulseValue.toString(),
                    dateValue!!
                )
            )
          /*  try
            {
                datas = JsonArray()
                val `object` = JsonObject()
                `object`.addProperty("O2_Sat", spO2Value.toString())
                `object`.addProperty("DateandTime", dateValue!!)
                datas!!.add(`object`)
                *//*val req = JsonObject()
                req.addProperty("logTime", "")
                req.addProperty("datas", Gson().toJson(datas))*//*
            }
            catch (e:Exception) {
                e.printStackTrace()
            }*/

        }
    }

    override fun onServicesDiscovered() {
        mBleControl!!.initNotification()
        showLoading("Waiting for result...")
    }

    override fun onScanStop(timeout: Boolean) {
        if (timeout) {
            displayRetry()
        } else {
            // device found, scan stopped forcefully
        }
    }

    override fun onResume() {
        super.onResume()
        mBleControl!!.registerBtReceiver(this)
    }

    override fun onPause() {
        super.onPause()
        mBleControl!!.unregisterBtReceiver(this)
    }

    override fun onDestroy() {
//        mBleControl.disconnect();
        mBleControl!!.unbindService(this)
        super.onDestroy()
    }

//    @OnClick(R.id.getBP)
//    fun startMeasurement() {
//        getBP!!.visibility = View.GONE
//        showLoading("Measuring...")
//        val command = byteArrayOf(
//            0xFD.toByte(), 0xFD.toByte(), 0xFA.toByte(),
//            0x05.toByte(), 0x0D.toByte(), 0x0A.toByte()
//        )
//        mBleControl!!.writeCharacteristics(command)
//    }

    private fun displayResults(
        date: String,
        unit1Name: String,
        unit1Value: String,
        unit1Units: String?
        ,
        unit2Name: String,
        unit2Value: String,
        unit2Units: String
        ,
        unit3Name: String,
        unit3Value: String,
        unit3Units: String
    ) {
        if(!isSend)
        sendBT!!.visibility = View.VISIBLE
        runOnUiThread {
            addManually!!.visibility = View.GONE
            readingsLayout!!.visibility = View.VISIBLE
            readingDateTV!!.text = date
            unitOneName!!.text = unit1Name
            valueUnitOne!!.text = unit1Value
            valueUnitOneName!!.text = unit1Units
            if (unit2Name.length == 0) {
                unitTwoLayout!!.visibility = View.GONE
            } else {
                unitTwoName!!.text = unit2Name
                valueUnitTwo!!.text = unit2Value
                valueUnitTwoName!!.text = unit2Units
            }
            if (unit3Name.length == 0) {
                unitThreeLayout!!.visibility = View.GONE
            } else {
                unitThreeName!!.text = unit3Name
                valueUnitThree!!.text = unit3Value
                valueUnitThreeName!!.text = unit3Units
            }
        }
    }

    private fun displayRetry() {
        try {
            closeLoading()
            runOnUiThread {
                   if(type!=null && type.equals("onego"))
            {
                skip!!.visibility = View.VISIBLE
            }
            else {
                       addManually!!.visibility = View.VISIBLE
                   }
                readingsLayout!!.visibility = View.GONE
                doneBT!!.visibility = View.GONE
                sendBT!!.visibility = View.GONE
                retryConnection!!.visibility = View.VISIBLE
                connectedHeading!!.text=vitalNameLabels!!.getString("notconnected")
                connectedHeading!!.setTextColor(resources.getColor(R.color.peachy_pink))
                macAddressHeaderTV!!.text="-"
                getBP!!.visibility = View.GONE
                tmpBtChecker.clear()
                //                    if(!fromStop) {
                //                        Utils.showCustomToast("Can't find " + deviceModel + ". " +
                //                                "Please make sure its connected and bluetooth is turned on!", TecheBPActivity.this);
                //                    }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    private fun showNextDialog(
            title: String,
            context: Context
    ) {
        var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.LANGUAGE_API_DATA, "")
        var vitalNameLabels: JSONObject? = null
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.next_dialog)
        val titleTv = dialog.findViewById(R.id.title) as TextView
        titleTv.text =  "$title reading sent. Next vital is Weight."
        val yesBtn = dialog.findViewById(R.id.next) as TextView
        val noBtn = dialog.findViewById(R.id.goToHome) as TextView
        yesBtn.setOnClickListener {
            val intent = Intent(this@TechePOActivity, TecheWSActivity::class.java)
            intent.putExtra("title", vitalNameLabels!!.getString("Weight"))
            intent.putExtra("vitalName", "Weight")
            intent.putExtra("unitName1", vitalUnitLabels!!.getString("Weight_In_Kgs"))
            intent.putExtra("unitName2", vitalUnitLabels!!.getString("Weight_In_Lbs"))
            intent.putExtra("type","onego")
            startActivity(intent)
            finish()
        }
        noBtn.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this@TechePOActivity, DashboardActivity::class.java)
            intent.putExtra("from","thankyou")
            startActivity(intent)
            finish()
        }
        dialog.show()

    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
                    progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.doneBT -> {
                if(type!=null && type.equals("onego"))
                {
                    showNextDialog(title!!,this@TechePOActivity)

                }
                else {
                    val intent = Intent(this@TechePOActivity, ThankyouActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            R.id.addManually -> {
                showDialog(title!!,vitalName!!,unit1!!,unit2!!,this@TechePOActivity,progressDialog)
            }
            R.id.sendBT -> {
              sendDeviceData()
            }
            R.id.back -> {
                finish()
            }
            R.id.retryConnection -> {
                startScanning()
            }
            R.id.skip -> {
                val intent = Intent(this@TechePOActivity, TecheWSActivity::class.java)
                intent.putExtra("title", vitalNameLabels!!.getString("Weight"))
                intent.putExtra("vitalName", "Weight")
                intent.putExtra("unitName1", vitalUnitLabels!!.getString("Weight_In_Kgs"))
                intent.putExtra("unitName2", vitalUnitLabels!!.getString("Weight_In_Lbs"))
                intent.putExtra("type","onego")
                startActivity(intent)
                finish()
            }
        }
    }

//    @OnClick(R.id.addManually)
//    fun showAddManual() {
//        val dialogfragment = AddManualVitalDialogFragment(
//            this, this, "Pulse Oximeter", 0,
//            true, "SpO2", "", keyUnit1, keyUnit2,
//            3, 3, false, "", keyUnit3, 3, patientId, true
//        )
//        val bundle = Bundle()
//        dialogfragment.setArguments(bundle)
//        dialogfragment.show(this.supportFragmentManager, "Add Manual")
//    }
//
private fun showDialog(
    title: String,
    vitalName: String,
    unit1: String,
    unit2: String,
    context: Context,
    progressDialog: CustomProgressDialog
) {
    var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.LANGUAGE_API_DATA, "")
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    try {
        val jsonObject = JSONObject(languageData)
        val jsonArray = jsonObject.optJSONArray("config")
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val name = jsonObject.optString("name")
            if (name.equals("dashboard")) {
                vitalUnitLabels = jsonObject.getJSONObject("labels")
            }
            if (name.equals("vitals")) {
                vitalNameLabels = jsonObject.getJSONObject("labels")
            }
        }
    } catch (e: Exception) {
    }

    val dialog = Dialog(context)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setCancelable(false)
    dialog.setContentView(R.layout.add_vital_dialog)
    val body = dialog.findViewById(R.id.topHeader) as TextView
    val dateText = dialog.findViewById(R.id.dateText) as TextView
    val timeText = dialog.findViewById(R.id.timeText) as TextView
    val cancel = dialog.findViewById(R.id.cancel) as TextView
    val save = dialog.findViewById(R.id.save) as TextView
    val etDate = dialog.findViewById(R.id.etDate) as EditText
    val etTime = dialog.findViewById(R.id.etTime) as EditText
    val entryUnit1 = dialog.findViewById(R.id.entryUnit1) as TextView
    val entryUnit2 = dialog.findViewById(R.id.entryUnit2) as TextView
    val etEntryUnit2 = dialog.findViewById(R.id.etEntryUnit2) as EditText
    val etEntryUnit1 = dialog.findViewById(R.id.etEntryUnit1) as EditText
    val blood_sugar_RadioBT = dialog.findViewById(R.id.blood_sugar_RadioBT) as LinearLayout
    if(subVitalName!=null)
    {
        body.text = vitalNameLabels!!.getString(subVitalName)
    }
    else{
        body.text = title
    }
    save.text=vitalNameLabels!!.getString("save")!!
    cancel.text=vitalNameLabels!!.getString("cancel")!!
    dateText.text=vitalNameLabels!!.getString("date")!!
    timeText.text=vitalNameLabels!!.getString("time")!!
        entryUnit1.text = unit1
    if(unit2.length>0) {
        entryUnit2.text = unit2
    }
    else{
        etEntryUnit2.visibility=View.GONE
        entryUnit2.visibility=View.GONE
    }
    if(vitalName.equals("Blood_Sugar"))
    {
        blood_sugar_RadioBT.visibility=View.VISIBLE
    }
    val cal = Calendar.getInstance()
    val y = cal.get(Calendar.YEAR)
    val m = cal.get(Calendar.MONTH)
    val d = cal.get(Calendar.DAY_OF_MONTH)
    val date: String =
            Utils.getDisplayedDateFormat(d, m, y,context)

    etDate.setText(date)
    val hh = cal.get(Calendar.HOUR_OF_DAY)
    var mm = cal.get(Calendar.MINUTE)
    if(mm.toString().length==2)
    {
        etTime.setText("" + hh + ":" + mm)
        // mm=Integer.parseInt("0$mm")
    }
    else {
        etTime.setText("" + hh + ":" + "0$mm")
    }
    etDate.setOnClickListener(View.OnClickListener {
        val cal = Calendar.getInstance()
        val y = cal.get(Calendar.YEAR)
        val m = cal.get(Calendar.MONTH)
        val d = cal.get(Calendar.DAY_OF_MONTH)
        val datepickerdialog: DatePickerDialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            val date: String = Utils.getDisplayedDateFormat(dayOfMonth, monthOfYear, year,context)
            etDate.setText(date)
        }, y, m, d)

        datepickerdialog.show()
    })
    etTime.setOnClickListener {
        val c: Calendar = Calendar.getInstance()
        val hh = c.get(Calendar.HOUR_OF_DAY)
        val mm = c.get(Calendar.MINUTE)
        val timePickerDialog: TimePickerDialog = TimePickerDialog(
            context,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                etTime.setText("" + hourOfDay + ":" + minute);
            },
            hh,
            mm,
            true
        )
        timePickerDialog.show()
    }

    val yesBtn = dialog.findViewById(R.id.save) as TextView
    val noBtn = dialog.findViewById(R.id.cancel) as TextView
    yesBtn.setOnClickListener {
        var data1=etEntryUnit1.text.toString()
        var data2=etEntryUnit2.text.toString()
        if(etDate.text.toString().length<1)
        {
            Toast.makeText(context,"Please select date",Toast.LENGTH_SHORT).show()
        }
        else if(etTime.text.toString().length<1)
        {
            Toast.makeText(context,"Please select time",Toast.LENGTH_SHORT).show()
        }
        else if(data1.length<1)
        {
            Toast.makeText(context,"Please enter value",Toast.LENGTH_SHORT).show()
        }
        else if(data1.length>1 ) {
            syncManually(
                vitalName,
                context,
                progressDialog,
                data1,
                "",
                dialog
            )
        }
    }
    noBtn.setOnClickListener { dialog.dismiss() }
    dialog.show()

}

    private fun syncManually(
        vitalName: String,
        context: Context,
        progressDialog: CustomProgressDialog,
        data1: String,
        data2: String,
        dialog: Dialog
    ) {
        var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val now = Date()
        var dateValue = formatDate.format(now)
        var token=context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
            Constants.KEY_TOKEN, null)!!
        if (Utils.isOnline(context)) {
            showLoading(context,"Syncing data...",progressDialog)
            val params: MutableMap<String, String> =
                HashMap()
            params["Device_ID"] = vitalName!!
            params["Login_User_ID"] = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                Constants.Login_User_ID, null)!!

            params["Comment"] ="Helllo"
            params["Source"] ="Manual"
            params["apporigin"] ="mobile"
            params["Device_Type"] =vitalName!!
            if(subVitalName!=null && subVitalName.equals("Heart_Rate"))
            {
                params["Vital"] =subVitalName!!
                params["Data[" + 0 + "]"+"[Heart_Rate_Beats]"] =data1
            }
            else {
                params["Vital"] =vitalName!!
                params["Data[" + 0 + "]" + "[O2_Sat]"] = data1
            }
            params["Data[" + 0 + "]"+"[DateandTime]"] = dateValue

            val call = NetworkService.apiInterface.postVitalData("Bearer $token", params )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading(context,progressDialog)
                        dialog.dismiss()
                        val intent = Intent(this@TechePOActivity, DashboardActivity::class.java)
                        intent.putExtra("from","bp")
                        startActivity(intent)
                        finish()
                    } else {
                        closeLoading(context,progressDialog)
                        Toast.makeText(
                            context,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        }
    }

    private fun closeLoading(
        context: Context,
        progressDialog: CustomProgressDialog
    ) {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(
        context: Context,
        msg: String,
        progressDialog: CustomProgressDialog
    ) {
        progressDialog?.show(context, msg)
    }

    private fun sendDeviceData() {
        if (Utils.isOnline(this)) {
            showLoading("Syncing data...")
            val params: MutableMap<String, String> =
                HashMap()
            params["Device_ID"] = vitalName!!
            params["Login_User_ID"] = userId!!
            params["Vital"] =vitalName!!
            params["Comment"] ="Helllo"
            params["Source"] ="Device"
            params["apporigin"] ="mobile"
            params["Device_Type"] =vitalName!!
            params["Data[" + 0 + "]"+"[O2_Sat]"] = vitals.get(0).data1val
            params["Data[" + 0 + "]"+"[DateandTime]"] = vitals.get(0).dateandtime
            params["VitalHeartRate[0][DateandTime]"]=  vitals.get(0).dateandtime
            params["VitalHeartRate[0][Heart_Rate_Beats]"] = vitals.get(0).data2val
          /*  for (i in vitals.indices) {
                val data: POVital = vitals.get(i)
                params["O2_Sat"] = data.data1val
                params["Heart_Rate_Beats"] = data.data2val
                params["DateandTime"] = data.dateandtime
        }*/
Log.i("params",params.toString())
         val call = NetworkService.apiInterface.postVitalData("Bearer $token", params )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        isSend=true
                        sendBT!!.visibility = View.GONE
                        doneBT!!.visibility = View.VISIBLE
                    } else {
                        closeLoading()
                        sendBT!!.visibility = View.VISIBLE
                        doneBT!!.visibility = View.GONE
                        Toast.makeText(
                            this@TechePOActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        }
    }
}