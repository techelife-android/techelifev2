package com.peng.ppscalelibrary.BleManager.Model;

import com.holtek.libHTBodyfat.HTPeopleGeneral;

public class BleBodyDataModel extends HTPeopleGeneral {
    public double weightKg;
    public BleBodyDataModel(double weightKg, double heightCm, int sex, int age, int impedance) {
        super(weightKg, heightCm, sex, age, impedance);
        this.getBodyfatParameters();
        this.weightKg = this.htWeightKg;
    }




}
