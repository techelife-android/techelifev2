package com.techesolutions.view.activity.nextcloud

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.IntroView
import com.techesolutions.data.remote.model.nextclouddashboard.NextCloudDataModel
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.LanguageSelectionActivity
import com.techesolutions.view.activity.SplashActivity
import com.techesolutions.view.adapter.NextCloudDashboardAdapter
import kotlinx.android.synthetic.main.layout_header_with_title.*
import org.json.JSONArray
import org.json.JSONObject


/**
 * Created by Neelam on 24-12-2020.
 */

class NextCloudDashboardActivity : AppCompatActivity(), View.OnClickListener {
    var recyclerView: RecyclerView? = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    var dataModel: ArrayList<NextCloudDataModel>? =null
    var from: String?=null
    private val introView = IntroView()
    private var progressDialog = CustomProgressDialog()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_nextcloud_dashboard)
        Utils.statusBarSetup(this)
        processExtraData()

        //getting recyclerview from xml
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        val logout = findViewById(R.id.logout) as ImageView
        val playAll = findViewById(R.id.play) as ImageView
        playAll!!.visibility=View.GONE
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)

        headerTitle!!.text =  "Home"

        val adapter = NextCloudDashboardAdapter(this@NextCloudDashboardActivity, dataModel)

        //now adding the adapter to recyclerview
        recyclerView?.adapter = adapter
        val layoutManager = GridLayoutManager(this, 2)
        recyclerView!!.layoutManager = layoutManager

        logout.setOnClickListener(this)

    }
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)//must store the new intent unless getIntent() will return the old one
        Log.i("entered onNewIntent", "entered onNewIntent")
        processExtraData()
    }
    private fun processExtraData() {
        //Log.i("From onNewIntent")
        val i: Intent = getIntent()
        from = i.getStringExtra("from")

            val shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
            val token: String? = shared.getString(Constants.KEY_TOKEN, null)
            val userId: String? = shared.getString(Constants.Login_User_ID, null)
        val nextcloudData = shared.getString(Constants.KEY_NEXT_CLOUD_FEATURES, null)
        var dlist = JSONArray(nextcloudData)
        dataModel= ArrayList<NextCloudDataModel>()
        for (i in 0 until dlist.length()) {
            val item = dlist.getJSONObject(i)
            val name = item.optString("name")
            val label = item.optString("label")
            val allowed = item.optString("enable")
           if(allowed.equals("1")) {
                dataModel!!.add(NextCloudDataModel(name, label, allowed))
           }
        }

    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.logout -> {

                /* MySharedPreference.setBooleanPreference(this, Constants.KEY_LOGGEDIN, false)
                MySharedPreference.setPreference(this, Constants.KEY_PASSWORD, "")
                MySharedPreference.setPreference(this, Constants.KEY_USERNAME, "")
                MySharedPreference.setPreference(this, Constants.KEY_TOKEN, "")*/
                //deleteAppData()

                val sharedPreferences: SharedPreferences =
                    getSharedPreferences("TechePrefs", MODE_PRIVATE)
                val editor: SharedPreferences.Editor = sharedPreferences.edit()
                editor.clear()
               /* editor.remove(Constants.KEY_LOGGEDIN);
                editor.remove(Constants.KEY_PASSWORD);
                editor.remove(Constants.KEY_USERNAME);
                editor.remove(Constants.KEY_TOKEN);*/
                editor.commit();
               // editor.commit()
                val intent = Intent(
                    this@NextCloudDashboardActivity,
                    SplashActivity::class.java
                )
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                //finish()

            }
        }

    }

    private fun deleteAppData() {
        try {
            // clearing app data
            val packageName = applicationContext.packageName
            val runtime = Runtime.getRuntime()
            runtime.exec("pm clear $packageName")

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun closeIntro() {
        if (introView != null && introView?.dialog.isShowing())
            introView?.dialog.dismiss()
    }

    private fun showIntro() {
        introView?.show(this)
        introView.dialog.findViewById<View>(R.id.cp_bg_view).setOnClickListener(View.OnClickListener { view ->
            closeIntro()
        })
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }
}

