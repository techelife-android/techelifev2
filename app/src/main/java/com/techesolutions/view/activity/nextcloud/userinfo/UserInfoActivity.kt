package com.techesolutions.view.activity.nextcloud.userinfo

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.fontedVews.edittext.ATecheRegularEditText
import com.techesolutions.data.remote.model.userinfo.CardInfoData
import com.techesolutions.data.remote.model.userinfo.WebInfoData
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.adapter.UserCardInfoAdapter
import com.techesolutions.view.adapter.UserWebInfoAdapter
import com.techesolutions.view.callbacks.UserInfoClickListener
import kotlinx.android.synthetic.main.activity_cemaras.noDataText
import kotlinx.android.synthetic.main.activity_user_info.*
import kotlinx.android.synthetic.main.add_web_info_dialog.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class UserInfoActivity : AppCompatActivity(), View.OnClickListener, UserInfoClickListener {
    private var progressDialog = CustomProgressDialog()
    var token: String? = null
    var username: String? = null
    var userId: String? = null
    var unique_id: String? = null
    var password: String? = null
    var type: String? = null
    var recyclerView: RecyclerView? = null
    var webadapter: UserWebInfoAdapter? = null
    var cardadapter: UserCardInfoAdapter? = null
    var webdata = ArrayList<WebInfoData>()
    var cardData = ArrayList<CardInfoData>()
    var format =
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    var formatDate =
        SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_user_info)
        Utils.setDimensions(this)

        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        token = shared.getString(Constants.KEY_TOKEN, null)
        username = shared.getString(Constants.KEY_NEXT_CLOUD_USER, null)
        password = shared.getString(Constants.KEY_UNIQUE, null)
        userId=shared.getString(Constants.Login_User_ID, null)
        unique_id = shared.getString(Constants.KEY_UNIQUE, null)
        setUpHeader()
        type = "web"
        webData(type!!)
        //showLockScreen()
    }

    private fun showLockScreen() {
        val dialog = Dialog(this@UserInfoActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.lock_screen)
        val yesBtn = dialog.findViewById(R.id.save) as TextView
        val cancel = dialog.findViewById(R.id.cancel) as TextView
        val etPassword = dialog.findViewById(R.id.etPassword) as ATecheRegularEditText
        yesBtn.setOnClickListener {
            if (TextUtils.isEmpty(etPassword!!.text.toString())) {
                onErrorListener("Please enter password")
            }
            else {
                if(etPassword !!.text.toString().equals(password)) {
                    dialog.dismiss()
                }
                else{
                    onErrorListener("Wrong Password")
                }
            }
        }
        cancel.setOnClickListener {
            finish()
        }
        dialog.show()
    }

    @SuppressLint("WrongConstant")
    open fun setUpHeader() {
        val headerTitleTV =
            findViewById<View>(R.id.headerTitle) as TextView
        headerTitleTV.text = username
        addEvent.visibility = View.VISIBLE
        addEvent.setOnClickListener(this)
        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }

        webView?.setOnClickListener(this)
        cardView?.setOnClickListener(this)


        recyclerView = findViewById(R.id.recycler_view) as RecyclerView

        //adding a layoutmanager
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

    }

    override fun onClick(v: View?) {

        when (v?.id) {

            R.id.webView -> {
                webView.setBackgroundResource(R.drawable.vital_card_selected)
                cardView.setBackgroundResource(R.drawable.vital_card_bd)
                webView.setTextColor(ContextCompat.getColor(this@UserInfoActivity, R.color.white));
                cardView.setTextColor(
                    ContextCompat.getColor(
                        this@UserInfoActivity,
                        R.color.blueyGrey
                    )
                );
                webView.setTypeface(Typeface.DEFAULT_BOLD);
                cardView.setTypeface(Typeface.DEFAULT);
                type = "web"
                webData("web")
            }
            R.id.cardView -> {
                webView.setBackgroundResource(R.drawable.vital_card_bd)
                cardView.setBackgroundResource(R.drawable.vital_card_selected)

                webView.setTextColor(
                    ContextCompat.getColor(
                        this@UserInfoActivity,
                        R.color.blueyGrey
                    )
                );
                cardView.setTextColor(ContextCompat.getColor(this@UserInfoActivity, R.color.white));

                webView.setTypeface(Typeface.DEFAULT);
                cardView.setTypeface(Typeface.DEFAULT_BOLD);
                type = "credit_debit"
                webData("credit_debit")
            }
            R.id.addEvent -> {
                if(type!!.equals("web")) {
                    showAddUserWebInfoDialog(this@UserInfoActivity,0,"","add")
                }
                else{
                    showAddUserCardInfoDialog(this@UserInfoActivity,0,"","add")
                }
            }

        }
    }
    private fun showAddUserWebInfoDialog(context: UserInfoActivity, position: Int, infotype: String,mode: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.add_web_info_dialog)
        val body = dialog.findViewById(R.id.topHeader) as TextView

        val yesBtn = dialog.findViewById(R.id.save) as TextView
        val noBtn = dialog.findViewById(R.id.cancel) as TextView
        val etType = dialog.findViewById(R.id.etType) as ATecheRegularEditText
        val etUsername = dialog.findViewById(R.id.etUsername) as ATecheRegularEditText
        val etPassword = dialog.findViewById(R.id.etPassword) as ATecheRegularEditText
        val etName = dialog.findViewById(R.id.etName) as ATecheRegularEditText
        val etUrl = dialog.findViewById(R.id.etUrl) as ATecheRegularEditText
        if(mode.equals("add"))
        {
            body.text = type
            etType.setText(type)
        }
        else{
            body.text = infotype
            etType.setText(infotype)
            etUsername.setText(webdata.get(position).username)
            etPassword.setText(webdata.get(position).password)
            etName.setText(webdata.get(position).name)
            etUrl.setText(webdata.get(position).url)
        }
        yesBtn.setOnClickListener {
            if (TextUtils.isEmpty(etName!!.text.toString())) {
                onErrorListener("Please enter Name")
            }
          /*  if (TextUtils.isEmpty(etUrl!!.text.toString())) {
                onErrorListener("Please enter url")
            }*/
            if (TextUtils.isEmpty(etUsername!!.text.toString())) {
                onErrorListener("Please enter Username")
            }
            if (TextUtils.isEmpty(etPassword!!.text.toString())) {
                onErrorListener("Please enter password")
            }
            else {
                dialog.dismiss()
                if(mode.equals("add")) {
                    addUserWebInfo(
                        etName!!.text.toString(),
                        etUsername!!.text.toString(),
                        etPassword!!.text.toString(),
                        etUrl!!.text.toString()
                    )
                }
                else{
                    editUserWebInfo(
                        webdata.get(position).id,
                        etName!!.text.toString(),
                        etUsername!!.text.toString(),
                        etPassword!!.text.toString(),
                        etUrl!!.text.toString()
                    )
                }
            }
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }
    private fun showAddUserCardInfoDialog(context: UserInfoActivity, position: Int, infotype: String,mode: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.add_card_dialog)
        val body = dialog.findViewById(R.id.topHeader) as TextView

        val yesBtn = dialog.findViewById(R.id.save) as TextView
        val noBtn = dialog.findViewById(R.id.cancel) as TextView
        val etType = dialog.findViewById(R.id.etType) as ATecheRegularEditText
        val CardNumber = dialog.findViewById(R.id.etCardNumber) as ATecheRegularEditText
        val etCardHolderName = dialog.findViewById(R.id.etCardHolderName) as ATecheRegularEditText
        val etCardNickName = dialog.findViewById(R.id.etCardNickName) as ATecheRegularEditText
        val etExpiryDate = dialog.findViewById(R.id.etExpiryDate) as ATecheRegularEditText
        if(mode.equals("add"))
        {
            body.text = type
            etType.setText(type)
        }
        else{
            body.text = infotype
            etType.setText(type)
            CardNumber.setText(cardData.get(position).card_no)
            etCardHolderName.setText(cardData.get(position).name_on_card)
            etExpiryDate.setText(cardData.get(position).expiry)
        }
        etExpiryDate.setOnClickListener(View.OnClickListener {
            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)


            val datepickerdialog: DatePickerDialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                // etDate.setText("" + dayOfMonth + "/" +MONTH[monthOfYear] + "/" + year)
                val date: String =
                    Utils.getDisplayedDateFormat(dayOfMonth, monthOfYear, year,context)

                etExpiryDate.setText(date)
            }, y, m, d)

            datepickerdialog.show()
        })
        yesBtn.setOnClickListener {
            if (TextUtils.isEmpty(CardNumber!!.text.toString())) {
                onErrorListener("Please enter Card number")
            }
              if (TextUtils.isEmpty(etCardHolderName!!.text.toString())) {
                  onErrorListener("Please enter Card holder name")
              }
            if (TextUtils.isEmpty(etExpiryDate!!.text.toString())) {
                onErrorListener("Please select expiry date")
            }

            else {
                dialog.dismiss()
                if(mode.equals("add")) {
                    addUserCardInfo(
                        etCardHolderName!!.text.toString(),
                        CardNumber!!.text.toString(),
                        etExpiryDate!!.text.toString()
                    )
                }
                else{
                    editUserCardInfo(
                        cardData.get(position).id,
                        etCardHolderName!!.text.toString(),
                        CardNumber!!.text.toString(),
                        etExpiryDate!!.text.toString()
                    )
                }
            }
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }
    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun addUserWebInfo(name: String, username: String, password: String, url: String) {

        showLoading("Please wait")
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.addUserWebInfo(
                "Bearer $token",
                "mobile",
                type!!,
                unique_id!!,
                username,
                password,
                name,
                url
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        Toast.makeText(this@UserInfoActivity, "Added", Toast.LENGTH_SHORT)
                            .show()
                        webData(type!!)
                    } else {
                        Toast.makeText(
                            this@UserInfoActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }


            })
        }

    }
    private fun addUserCardInfo(name: String, cardnumber: String, expirydate: String) {

        showLoading("Please wait")
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.addUserCardInfo(
                "Bearer $token",
                "mobile",
                type!!,
                unique_id!!,
                name,
                cardnumber,
                expirydate
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        Toast.makeText(this@UserInfoActivity, "Added", Toast.LENGTH_SHORT)
                            .show()
                        webData(type!!)
                    } else {
                        Toast.makeText(
                            this@UserInfoActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }


            })
        }

    }

    private fun editUserWebInfo(id: String,name: String, username: String, password: String, url: String) {

        showLoading("Please wait")
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.editUserWebInfo(
                id,
                "Bearer $token",
                "mobile",
                type!!,
                unique_id!!,
                username,
                password,
                name,
                url
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        Toast.makeText(this@UserInfoActivity, "Updated", Toast.LENGTH_SHORT)
                            .show()
                        webData(type!!)
                    } else {
                        Toast.makeText(
                            this@UserInfoActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }


            })
        }

    }

    private fun editUserCardInfo(id: String,name: String, cardnumber: String, expirydate: String) {

        showLoading("Please wait")

        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.editUserCardInfo(
                id,
                "Bearer $token",
                "mobile",
                type!!,
                unique_id!!,
                name,
                cardnumber,
                expirydate
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        Toast.makeText(this@UserInfoActivity, "Updated", Toast.LENGTH_SHORT)
                            .show()
                        webData(type!!)
                    } else {
                        Toast.makeText(
                            this@UserInfoActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }


            })
        }

    }
    private fun webData(type: String) {

        if (Utils.isOnline(this)) {
            showLoading("Fetching data...")
            webdata!!.clear()
            cardData!!.clear()

            val call = NetworkService.apiInterface.getUserWebInfo(
                "Bearer $token",
                "mobile",
                type,
                "",
                unique_id!!,
                "1",
                "50",
                ""
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    if (response != null) {
                        val stringResponse = response.body()?.string()
                        val jsonObj = JSONObject(stringResponse)
                        val success = jsonObj!!.getString("success")
                        if (success != null && success.toString().equals("true")) {
                            setData(type, jsonObj)
                        } else {
                            Toast.makeText(
                                this@UserInfoActivity,
                                jsonObj!!.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    else{
                        Toast.makeText(this@UserInfoActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                    }
                }

            })

        }

    }

    private fun setData(type: String, jsonObj: JSONObject) {
        val jObj = jsonObj.getJSONObject("data")
        val dlist = jObj.getJSONArray("data")
        // webdata = ArrayList<WebInfoData>()
        // cardData = ArrayList<CardInfoData>()
        if(dlist.length()>0) {
            noDataText!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
        if (type.equals("web")) {
            for (i in 0 until dlist.length()) {
                val data = dlist.getJSONObject(i)

                webdata!!.add(
                    WebInfoData(
                        data!!.getString("id"),
                        data!!.getString("username"),
                        data!!.getString("password"),
                        data!!.getString("user_id"),
                        data!!.getString("created"),
                        data!!.getString("updated"),
                        data!!.getString("type"),
                        data!!.getString("name"),
                        data!!.getString("url")
                    )
                )


            }
            //adding some dummy data to the list
            if(webdata.size>0) {
                noDataText!!.visibility = View.GONE
                recyclerView!!.visibility = View.VISIBLE
                webadapter = UserWebInfoAdapter(this@UserInfoActivity, webdata!!, this)
                //now adding the adapter to recyclerview
                recyclerView?.adapter = webadapter
            }
            else{
                noDataText!!.visibility = View.VISIBLE
                recyclerView!!.visibility = View.GONE
            }

        } else {
            for (i in 0 until dlist.length()) {
                val data = dlist.getJSONObject(i)
                cardData!!.add(
                    CardInfoData(
                        data!!.getString("id"),
                        data!!.getString("user_id"),
                        data!!.getString("created"),
                        data!!.getString("updated"),
                        data!!.getString("type"),
                        data!!.getString("name_on_card"),
                        data!!.getString("card_no"),
                        data!!.getString("expiry"),
                        data!!.getString("cvv")
                    )
                )
            }
            if(cardData.size>0) {
                noDataText!!.visibility = View.GONE
                recyclerView!!.visibility = View.VISIBLE

            cardadapter = UserCardInfoAdapter(this@UserInfoActivity, cardData!!, this)

            //now adding the adapter to recyclerview
            recyclerView?.adapter = cardadapter
            }
            else{
                noDataText!!.visibility = View.VISIBLE
                recyclerView!!.visibility = View.GONE
            }
        }
        }
        else{
            noDataText!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
        }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onEditClick(position: Int,type: String, mode: String) {
        if(type.equals("web")) {
            showAddUserWebInfoDialog(this@UserInfoActivity,position,type,mode)
        }
        else{
            showAddUserCardInfoDialog(this@UserInfoActivity,position,type,mode)
        }
    }

    override fun onDeleteClick(position: Int) {
        showDeleteDialog(position)
    }

    override fun onViewUpdate(v: View?,cardnumberTv: View?, position: Int) {
       v!!.setBackgroundResource(R.drawable.ic_open_lock)
       // cardnumberTv!!.text=cardData.get(position).card_no
        val cardnumberTv = findViewById(R.id.etCardNumber) as TextView
        cardnumberTv.setText(cardData.get(position).card_no)
    }

    private fun showDeleteDialog(position: Int) {
        val factory = LayoutInflater.from(this)
        val deleteDialogView = factory.inflate(R.layout.delete_dialog, null)
        val deleteDialog = AlertDialog.Builder(this).create()
        deleteDialog.setView(deleteDialogView)
        deleteDialogView.findViewById<View>(R.id.yes).setOnClickListener { //your business logic
            deleteDialog.dismiss()
            if (type!!.equals("web")) {
                val webdata: WebInfoData = webdata!!.get(position)
                startDeletion(webdata.id)
            } else {
                val carddata: CardInfoData = cardData!!.get(position)
                startDeletion(carddata.id)
            }

        }
        deleteDialogView.findViewById<View>(R.id.no).setOnClickListener { deleteDialog.dismiss() }
        deleteDialog.show()
    }

    private fun startDeletion(infoId: String) {
        if (Utils.isOnline(this)) {
            showLoading("Fetching data...")

            val call = NetworkService.apiInterface.deleteUserInfo(
                infoId,
                "Bearer $token",
                "mobile",
                unique_id!!
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        Toast.makeText(
                            this@UserInfoActivity,
                            "Deleted.",
                            Toast.LENGTH_SHORT
                        ).show()
                        webData(type!!)
                    } else {
                        Toast.makeText(
                            this@UserInfoActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        }

    }

}