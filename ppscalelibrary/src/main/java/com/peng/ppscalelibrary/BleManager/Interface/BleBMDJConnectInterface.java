package com.peng.ppscalelibrary.BleManager.Interface;

public interface BleBMDJConnectInterface {
    /*连接成功*/
    void connectSuccess();

    /*连接失败*/
    void connectFailed();
}
