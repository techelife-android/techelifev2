package com.techesolutions.view.activity.nextcloud.files.preview;
import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.common.utils.Log_OC
import com.owncloud.android.lib.resources.files.DownloadFileRemoteOperation
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils.getRootDirPath
import kotlinx.android.synthetic.main.activity_documents.*
import kotlinx.android.synthetic.main.activity_pdf_view.*
import kotlinx.android.synthetic.main.layout_header.*
import org.mozilla.universalchardet.ReaderFactory
import java.io.*
import java.lang.ref.WeakReference
import java.util.*

class  PreviewDocumentActivity : AppCompatActivity(), OnRemoteOperationListener,
    OnDatatransferProgressListener {
    val EXTRA_STREAM_URL = "STREAM_URL"
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var filename: String?=null
    private var file: String?=null
    private var textLoadAsyncTask: TextLoadAsyncTask? = null
    private val progressDialog: CustomProgressDialog? = CustomProgressDialog()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_documents)
        mHandler = Handler()

        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        val user: String?=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", url + ", " + user + ", " + pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
                pass!!
            )
        )
        val extras = intent.extras
        file = extras!!.getString(EXTRA_STREAM_URL)
       // filename=file!!.replace("/", "")
        if (file!!.endsWith("/")) {
            filename = file!!.substring(0, file!!.lastIndexOf("/"))
            val files: Array<String> = file!!.split("/".toRegex()).toTypedArray()
            val f = files[files.size - 1]
            Log.i("TAG", "files: $f")
            // Log.i("TAG",subFolder.replaceAll("/",""));
            (findViewById<View>(R.id.headerTitle) as TextView).text = f
        } else {
            val file: String =
                file!!.substring(file!!.lastIndexOf("/") + 1, file!!.length)
            filename = file
        }
        val downFolder = File(getRootDirPath(baseContext))
        val downloadedFile = File(downFolder, file)
        if(downloadedFile.exists())
        {
            //showPdfFromFile(downloadedFile)
            loadAndShowTextPreview(downloadedFile)
        }
        else {

            if (file != null) {
                startDownload(file!!)
            }
        }
        headerTitle!!.text = filename
        back.setOnClickListener { finish() }
        findViewById<View>(R.id.refreshFiles).visibility = View.VISIBLE
        findViewById<View>(R.id.refreshFiles).setOnClickListener {
            if (file != null) {
                startDownload(file!!)
            }
        }
    }

    private fun loadAndShowTextPreview(file: File) {
        Log.i("filepath", file.absolutePath)
        textLoadAsyncTask = TextLoadAsyncTask(
            WeakReference<TextView>(text_preview),
            WeakReference<FrameLayout>(empty_list_progress)
        )

        //val downloadedFile = File(downFolder, file)
        textLoadAsyncTask!!.execute(file.absolutePath)    }

    private fun startDownload(remotePath: String) {
        showLoading("wait...")
        val downFolder = File(getRootDirPath(baseContext))
        downFolder.mkdir()
        val downloadOperation = DownloadFileRemoteOperation(remotePath, downFolder.absolutePath)
        downloadOperation.addDatatransferProgressListener(this)
        downloadOperation.execute(mClient, this, mHandler)
    }
    private fun showLoading(msg: String) {
        progressDialog!!.show(this, msg)
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog.dialog.isShowing) progressDialog.dialog.dismiss()
    }
    private fun showPdfFromFile(file: File) {
        pdfView.fromFile(file)
            .password(null)
            .defaultPage(0)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .onPageError { page, _ ->
                Toast.makeText(
                    this@PreviewDocumentActivity,
                    "Error at page: $page", Toast.LENGTH_LONG
                ).show()
            }
            .load()
    }

  override fun onRemoteOperationFinish(
      operation: RemoteOperation<*>?,
      result: RemoteOperationResult<*>?
  ) {
         closeLoading()
        if (!result!!.isSuccess()) {
            Toast.makeText(this, R.string.todo_operation_finished_in_fail, Toast.LENGTH_SHORT)
                .show()
            Log.e("PdfViewActivity", result.getLogMessage(), result.getException())
        } else {
           /* Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                .show()*/
            val downFolder = File(getRootDirPath(baseContext))
            val downloadedFile = File(downFolder, file)
            downloadedFile?.let { loadAndShowTextPreview(it) }
        }
    }

    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileAbsoluteName: String?
    ) {
        /*val percentage =
            if (totalToTransfer > 0) totalTransferredSoFar * 100 / totalToTransfer else 0
        mHandler!!.post {
            val progressView: TextView? = null
            if (progressBar != null) {
                progressBar.progress = percentage.toInt()
            }
        }
        progressBar.visibility= View.VISIBLE*/
    }
}

private class TextLoadAsyncTask(
    private val textViewReference: WeakReference<TextView>,
    private val progressViewReference: WeakReference<FrameLayout>
) : AsyncTask<Any?, Void?, StringWriter>() {
    override fun onPreExecute() {
        // not used at the moment
    }

     override fun doInBackground(vararg params: Any?): StringWriter? {
        require(params.size == PARAMS_LENGTH) {
            ("The parameter to " + TextLoadAsyncTask::class.java.name
                    + " must be (1) the file location")
        }
        val location = params[0] as String
         Log.i("location", location)
        var scanner: Scanner? = null
        val source = StringWriter()
        val bufferedWriter = BufferedWriter(source)
        var reader: Reader? = null
        try {
            val file = File(location)
            reader = ReaderFactory.createReaderFromFile(file)
            scanner = Scanner(reader)
            while (scanner.hasNextLine()) {
                bufferedWriter.append(scanner.nextLine())
                if (scanner.hasNextLine()) {
                    bufferedWriter.append("\n")
                }
            }
            bufferedWriter.close()
            val exc = scanner.ioException()
            if (exc != null) {
                throw exc
            }
        } catch (e: IOException) {
            Log_OC.e("TAG", e.message, e)
            //finish()
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: IOException) {
                    Log_OC.e("TAG", e.message, e)
                   // finish()
                }
            }
            scanner?.close()
        }
        return source
    }

    override fun onPostExecute(stringWriter: StringWriter) {
        val textView = textViewReference.get()
        if (textView != null) {
            var originalText = stringWriter.toString()
            Log.i("originalText", originalText)

            //setText(textView, originalText, getFile(), requireActivity())
            textView.text=originalText
            textView.visibility = View.VISIBLE
        }
        val progress = progressViewReference.get()
        if (progress != null) {
            progress.visibility = View.GONE
        }
    }

    companion object {
        private const val PARAMS_LENGTH = 1
    }

}
