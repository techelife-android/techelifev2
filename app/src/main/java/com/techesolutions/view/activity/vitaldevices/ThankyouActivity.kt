package com.techesolutions.view.activity.vitaldevices

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Utils
import kotlinx.android.synthetic.main.activity_thankyou.*
import org.json.JSONObject
import android.content.Context
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.DashboardActivity

class ThankyouActivity : AppCompatActivity() {
    var vitalNameLabels: JSONObject? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_thankyou)
        Utils.setDimensions(this)
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }
        thanksText!!.text=vitalNameLabels!!.getString("thanks_msg")
        recordedText!!.text=vitalNameLabels!!.getString("vitalrecorded")
        back!!.text=vitalNameLabels!!.getString("backtohome")
        back.setOnClickListener {
            val intent = Intent(this@ThankyouActivity, DashboardActivity::class.java)
            intent.putExtra("from","thankyou")
            startActivity(intent)
            finish() }
    }

}