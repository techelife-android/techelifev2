package com.techesolutions.view.adapter

import android.app.Dialog
import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.userinfo.WebInfoData
import com.techesolutions.view.activity.nextcloud.userinfo.UserInfoActivity
import com.techesolutions.view.callbacks.ItemClickListener
import com.techesolutions.view.callbacks.UserInfoClickListener

class UserWebInfoAdapter(
    context:Context,
    val userList: ArrayList<WebInfoData>,
    private var itemclick: UserInfoActivity
) : RecyclerView.Adapter<UserWebInfoAdapter.ViewHolder>() {
    var activity:Context = context
    var infoClickListener=itemclick
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserWebInfoAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.user_web_info_items, parent, false)
        return ViewHolder(v,activity,infoClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: UserWebInfoAdapter.ViewHolder, position: Int) {
        userList.get(position)?.let { holder.bindItems(it, activity, infoClickListener, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,context: Context, infoClickListener: UserInfoClickListener) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(vital: WebInfoData,context: Context, infoClickListener: UserInfoClickListener, position: Int) {
            val name  = itemView.findViewById(R.id.etName) as TextView
            val url  = itemView.findViewById(R.id.etUrl) as TextView
            val username  = itemView.findViewById(R.id.etUsername) as TextView
            val password  = itemView.findViewById(R.id.etPassword) as TextView
            name.text = vital.name
            url.text = vital.url
            username.text = "********"
            password.text = "********"
            username.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            password.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            val lock = itemView.findViewById(R.id.lock) as ImageView
            val rllock = itemView.findViewById(R.id.rllock) as RelativeLayout
            val menu = itemView.findViewById(R.id.rlmenu) as RelativeLayout
            menu.setOnCreateContextMenuListener({ menu, v, menuInfo ->
                val item = menu.add("Edit")
                item.setOnMenuItemClickListener({ i ->
                    infoClickListener.onEditClick(position,vital.type,"edit")
                    true // Signifies you have consumed this event, so propogation can stop.
                })

                val itemRemove = menu.add("Delete")
                itemRemove.setOnMenuItemClickListener({ i ->
                    infoClickListener.onDeleteClick(position)
                    true
                })
            })
            menu.setOnClickListener({ view -> view.showContextMenu() })
           // rllock.setOnClickListener(this)
            rllock.setOnClickListener {
                lock.setBackgroundResource(R.drawable.ic_open_lock)
                username.text = vital.username
                password.text = vital.password
                username.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f)
                password.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13f)
            }
        }
        /*override fun onClick(v: View?) {
            if(position == adapterPosition) {
                v!!.setBackgroundResource(R.drawable.ic_open_lock)
            } else {
                v!!.setBackgroundResource(R.drawable.ic_close_lock)
            }
        }*/
        private fun showDialog(title: String, context: Context) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.add_web_info_dialog)
            val body = dialog.findViewById(R.id.topHeader) as TextView
            body.text = title
            val yesBtn = dialog.findViewById(R.id.save) as TextView
            val noBtn = dialog.findViewById(R.id.cancel) as TextView
            yesBtn.setOnClickListener {
                dialog.dismiss()
            }
            noBtn.setOnClickListener { dialog.dismiss() }
            dialog.show()

        }
    }
}