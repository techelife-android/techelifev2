package com.techesolutions.data.remote.repository.login.cameras

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.techesolutions.services.NetworkService
import com.techesolutions.data.remote.model.nextclouddashboard.MasterModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Author: Neelam Upadhyay
 * Created: 18/08/2021
 */
object MasterDataRepository {
    val masterResponse = MutableLiveData<MasterModel>()

    fun getRoomDeviceData( token: String, apporigin: String, type: String): MutableLiveData<MasterModel> {

        val call = NetworkService.apiInterface.getRoomAndDeviceList_(token,apporigin,type)

        call.enqueue(object: Callback<MasterModel> {
            override fun onFailure(call: Call<MasterModel>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<MasterModel>,
                response: Response<MasterModel>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()
                masterResponse.postValue(data)
            }

        })

        return masterResponse
    }

}