package com.techesolutions.view.activity.nextcloud.files

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.*
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.FilePath.getPath
import com.techesolutions.utils.MySharedPreference
import com.techesolutions.view.activity.nextcloud.FileFolderDao
import com.techesolutions.view.activity.nextcloud.NextcloudDatabase
import com.techesolutions.view.activity.nextcloud.files.FilesActivity
import com.techesolutions.view.activity.nextcloud.files.contacts.FileContactActivity
import com.techesolutions.view.activity.nextcloud.files.filesync.AlarmReceiver
import com.techesolutions.view.activity.nextcloud.files.filesync.FileSettingsActivity
import com.techesolutions.view.activity.nextcloud.files.filesync.MediaPushService
import com.techesolutions.view.activity.nextcloud.files.preview.*
import com.techesolutions.view.callbacks.FileClickListener
import java.io.File
import java.io.Serializable
import java.util.*

class FilesActivity() : Activity(), OnRemoteOperationListener, OnDatatransferProgressListener,
    FileClickListener {
    var list: ListView? = null
    var files: List<RemoteFile>? = null
    private var fileUri: Uri? = null
    private var filePath: String? = null
    private var path: String? = null
    private var folderpath: String? = null
    private var type: String? = null
    private var instantUploadFolder: String? = null
    private var photoEnable: Boolean? = null
    private var videoEnable: Boolean? = null
    private var mHandler: Handler? = null
    private var spinner: Spinner? = null
    private var mClient: OwnCloudClient? = null
    private var mFilesAdapter: FileAdapter? = null
    private val progressDialog: CustomProgressDialog? = CustomProgressDialog()
    var folders: MutableList<RemoteFile>? = null
    var shared: SharedPreferences? = null
    private var fDao: FileFolderDao?= null
    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actvity_file)
        shared = getSharedPreferences("TechePrefs", MODE_PRIVATE)
        showLoading("Please wait...")
        (findViewById<View>(R.id.headerTitle) as TextView).text = "Files & Folders"
        mHandler = Handler()
        folders = ArrayList()
        val shared = getSharedPreferences("TechePrefs", MODE_PRIVATE)
        val url = shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        val user = shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass = shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", "$url, $user, $pass")
        var serverUri: Uri? = null
        if (url!!.endsWith("/")) {
            serverUri = Uri.parse(url.substring(0, url.length - 1))
        } else {
            serverUri = Uri.parse(url)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, false)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user,
                pass
            )
        )
        list = (findViewById<View>(R.id.list_view) as ListView)
        mFilesAdapter = FileAdapter(this, R.layout.file_folder_items, this)
        list!!.adapter = mFilesAdapter

        //database integration

       //  val database = NextcloudDatabase.getInstance(application)
       // fDao = database.filesFolderDao()
           //val allNotes = fDao!!.getAllFiles()

        startRefresh()
        findViewById<View>(R.id.upload).visibility = View.VISIBLE
        findViewById<View>(R.id.upload).setOnClickListener { showUploadDialog() }
        findViewById<View>(R.id.settings).visibility = View.VISIBLE
        findViewById<View>(R.id.refreshFiles).visibility = View.VISIBLE
        findViewById<View>(R.id.settings).setOnClickListener {
            val intent = Intent(this@FilesActivity, FileSettingsActivity::class.java)
            val args = Bundle()
            args.putSerializable("folderList", folders as Serializable?)
            intent.putExtra("BUNDLE", args)
            // intent.putExtra("folderList", (Parcelable) folders);
            startActivity(intent)
            //showUploadDialog();
        }
        findViewById<View>(R.id.refreshFiles).setOnClickListener {
            startRefresh()
        }
        findViewById<View>(R.id.back).setOnClickListener { finish() }
    }

    private fun showLoading(msg: String) {
        progressDialog!!.show(this, msg)
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog.dialog.isShowing) progressDialog.dialog.dismiss()
    }

    public override fun onDestroy() {
        super.onDestroy()
    }

    private fun startRefresh() {
        if (progressDialog != null && progressDialog.dialog.isShowing) {
            progressDialog.dialog.dismiss()
        }
        showLoading("Please wait...")
        val refreshOperation = ReadFolderRemoteOperation(FileUtils.PATH_SEPARATOR)
        refreshOperation.execute(mClient, this, mHandler)
    }

    private fun startUpload(fileUri: String?, type: String?) {
        val fileToUpload = File(fileUri)
        val remotePath = folderpath + fileToUpload.name
        val mimeType = type

        // Get the last modification date of the file from the file system
        val timeStampLong = System.currentTimeMillis() / 1000
        val timeStamp = timeStampLong.toString()
        val uploadOperation =
            UploadFileRemoteOperation(fileToUpload.absolutePath, remotePath, mimeType, timeStamp)
        uploadOperation.addDataTransferProgressListener(this) //addDatatransferProgressListener(this);
        uploadOperation.execute(mClient, this, mHandler)
    }

    private fun startRemoteDeletion(path: String, fileid: String) {
        showLoading("Deleting file")
        val removeOperation = RemoveFileRemoteOperation(path)
        removeOperation.execute(mClient, this, mHandler)
        instantUploadFolder = shared!!.getString(Constants.KEY_REMOTE_FOLDER, "")

            if (instantUploadFolder!!.equals(path)) {
                MySharedPreference.setPreference(
                    this@FilesActivity, Constants.KEY_REMOTE_FOLDER,
                    "/"
                )
            }
    }

    private fun startDownload(remotePath: String) {
        val downFolder = File(getRootDirPath(baseContext))
        downFolder.mkdir()
        val downloadOperation = DownloadFileRemoteOperation(remotePath, downFolder.absolutePath)
        downloadOperation.addDatatransferProgressListener(this)
        downloadOperation.execute(mClient, this, mHandler)
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>
    ) {
        if (!result.isSuccess) {
            Log.e(LOG_TAG, result.logMessage, result.exception)
        } else if (operation is ReadFolderRemoteOperation) {
            onSuccessfulRefresh(operation, result)
        } else if (operation is UploadFileRemoteOperation) {
            onSuccessfulUpload(operation, result)
        }
        else if (operation is RemoveFileRemoteOperation) {
            closeLoading()
            startRefresh()
        }
        else {
            closeLoading()
            startRefresh()
        }
    }

    private fun onSuccessfulRefresh(
        operation: ReadFolderRemoteOperation,
        result: RemoteOperationResult<*>
    ) {
        mFilesAdapter!!.clear()
        val files: MutableList<RemoteFile> = ArrayList()
        for (obj: Any in result.data) {
            files.add(obj as RemoteFile)
            if (obj.remotePath.endsWith("/")) {
                folders!!.add(obj)
                //Log.i("folder name", ((OCFile) obj).remotePath);
            }
        }
        if (files != null) {
            val it: Iterator<RemoteFile> = files.iterator()
            while (it.hasNext()) {
                //fDao!!.insert(it.)
                mFilesAdapter!!.add(it.next())
            }
            mFilesAdapter!!.remove(mFilesAdapter!!.getItem(0))
        }
        closeLoading()
        mFilesAdapter!!.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        photoEnable = shared!!.getBoolean(Constants.KEY_PHOTO_UPLOAD, false)
        videoEnable = shared!!.getBoolean(Constants.KEY_VIDEO_UPLOAD, false)
        instantUploadFolder = shared!!.getString(Constants.KEY_REMOTE_FOLDER, "")
        startService(Intent(applicationContext, MediaPushService::class.java))
    }

    private fun onSuccessfulUpload(
        operation: UploadFileRemoteOperation,
        result: RemoteOperationResult<*>
    ) {
        startRefresh()
    }

    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileName: String
    ) {
        val percentage =
            (if (totalToTransfer > 0) totalTransferredSoFar * 100 / totalToTransfer else 0)
        val upload = fileName.contains(getString(R.string.upload_folder_path))
        Log.d(LOG_TAG, "progressRate $percentage")
        mHandler!!.post {
            val progressView: TextView? = null
            if (upload) {
                // progressView = findViewById(R.id.upload_progress);
            } else {
                //progressView = findViewById(R.id.download_progress);
            }
            if (progressView != null) {
                progressView.text = java.lang.Long.toString(percentage) + "%"
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        when (requestCode) {
            PICKFILE_RESULT_CODE -> if (resultCode == -1) {
                fileUri = data.data
                filePath = fileUri!!.path
                val cR = contentResolver
                val mime = MimeTypeMap.getSingleton()
                //String type = mime.getExtensionFromMimeType(cR.getType(fileUri));
                type = cR.getType((fileUri)!!)

                //tvItemPath.setText(filePath);
                Log.i("data", type)
                path = getPath(baseContext, (fileUri)!!)
                Log.i("filepath ", path)
                // String docFilePath = getFileNameByUri(getActivity(), fileuri);
                // System.out.println("ConnectToDoctorFragment.onActivityResult " + docFilePath);
                //filePathToUpload = path;
                startUpload(path, type)
            }
        }
    }

    override fun onPositionClick(position: Int) {
        val p = mFilesAdapter!!.getItem(position)
        Log.i("mime type", p!!.mimeType)
        if (p.mimeType.equals("DIR", ignoreCase = true)) {
            val intent = Intent(this, SubFilesActivity::class.java)
            intent.putExtra("path", p.remotePath)
            startActivity(intent)
        } else if (p.mimeType.equals(
                "video/mp4",
                ignoreCase = true
            ) || p.mimeType.equals(
                "video/mpeg",
                ignoreCase = true
            ) || p.mimeType.equals("image/gif", ignoreCase = true)|| p.remotePath.contains(".mov", ignoreCase = true)
        ) {
            //startDownload(p.remotePath);
            val previewIntent = Intent(this, VideoPreviewActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            // previewIntent.putExtra("EXTRA_FILE", (Parcelable) p);
            previewIntent.putExtra(EXTRA_START_POSITION, 0)
            previewIntent.putExtra(EXTRA_AUTOPLAY, true)
            startActivity(previewIntent)
        } else if (p.mimeType.equals(
                "image/png",
                ignoreCase = true
            ) || p.mimeType.equals(
                "image/jpeg",
                ignoreCase = true
            ) || p.mimeType.equals(
                "image/jpg",
                ignoreCase = true
            ) || p.mimeType.equals(
                "image/heic",
                ignoreCase = true
            ) || p.mimeType.equals("image/heif", ignoreCase = true)
        ) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PreviewImageActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        } else if (p.mimeType.equals("application/pdf", ignoreCase = true)) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PdfViewActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.equals("text/vcard", ignoreCase = true)) {
            val previewIntent = Intent(this, FileContactActivity::class.java)
            previewIntent.putExtra(SubFilesActivity.EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(SubFilesActivity.FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.equals("text/markdown", ignoreCase = true)||p.mimeType.equals("text/plain", ignoreCase = true)) {
            val previewIntent = Intent(this, PreviewDocumentActivity::class.java)
            previewIntent.putExtra(SubFilesActivity.EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(SubFilesActivity.FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        /*else if (p.mimeType.equals(
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                ignoreCase = true
            ) || p.mimeType.equals("application/vnd.oasis.opendocument.text", ignoreCase = true)
        ) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        } else if (p.mimeType.equals(
                "application/xhtml+xml",
                ignoreCase = true
            ) || p.mimeType.equals(
                "application/xml",
                ignoreCase = true
            ) || p.mimeType.equals(
                "text/xml",
                ignoreCase = true
            ) || p.mimeType.equals(
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                ignoreCase = true
            ) || p.mimeType.equals(
                "text/csv",
                ignoreCase = true
            ) || p.mimeType.equals("application/vnd.ms-excel", ignoreCase = true)
        ) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.contains(".zip") || p.mimeType.contains(".rar")) {
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.contains(".ppt") || p.mimeType.contains(".pptx")) {
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }

        else if (p.mimeType.equals("application/x-rar-compressed",ignoreCase = true)||p.mimeType.equals("application/zip",ignoreCase = true)|| p.remotePath.contains(".zip") || p.remotePath.contains(".rar")|| p.remotePath.contains(".ai",ignoreCase = true)|| p.remotePath.contains(".psd",ignoreCase = true)|| p.remotePath.contains(".svg",ignoreCase = true)) {
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(FilesActivity.EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FilesActivity.FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation", ignoreCase = true)|| p.remotePath.contains(".ppt") || p.remotePath.contains(".pptx")) {
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(FilesActivity.EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FilesActivity.FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }*/
            else { // startDownload(p.remotePath);
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
    }

    override fun onDeleteClick(position: Int) {
        showDeleteDialog(position)
    }

    private fun showDeleteDialog(position: Int) {
        val factory = LayoutInflater.from(this)
        val deleteDialogView = factory.inflate(R.layout.delete_dialog, null)
        val deleteDialog = AlertDialog.Builder(this).create()
        deleteDialog.setView(deleteDialogView)
        deleteDialogView.findViewById<View>(R.id.yes).setOnClickListener(
            View.OnClickListener { //your business logic
                deleteDialog.dismiss()
                val p = mFilesAdapter!!.getItem(position)
                val path = p!!.remotePath
                val fileid = p.remoteId
                startRemoteDeletion(path, fileid)
            })
        deleteDialogView.findViewById<View>(R.id.no)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    deleteDialog.dismiss()
                }
            })
        deleteDialog.show()
    }

    private fun showUploadDialog() {
        val factory = LayoutInflater.from(this)
        val uploadDialogView = factory.inflate(R.layout.upload_file_dialog, null)
        val uploadDialog = AlertDialog.Builder(this).create()
        uploadDialog.setView(uploadDialogView)
        uploadDialogView.findViewById<View>(R.id.llSpinner).visibility = View.GONE
        spinner = uploadDialogView.findViewById(R.id.spinner)
        val categoryAdapter = SelectUplaodFolderAdapter(this, (folders)!!)
        spinner!!.setAdapter(categoryAdapter)
        spinner!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>?, arg1: View,
                position: Int, arg3: Long
            ) {
                folderpath = folders!![position].remotePath
                //folderpath = spinner.getSelectedItem().toString();
                Log.i("Path", "" + folderpath)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        )
        /*  spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                path=folders.get(position).remotePath;
                Log.i("Path",path);
            }
        });*/
        /* for (int i=0; i<folders.size(); i++) {
            if (folders.get(i).remotePath.equals(profileTimezone)) {
                Log.i("profileTimezone",""+i)
                position = i
            }
        }
        Log.i("profileTimezone",""+position)

        spinner!!.setSelection(position)*/uploadDialogView.findViewById<View>(R.id.browse)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
//                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
//                chooseFile.setType("*/*");
//                chooseFile = Intent.createChooser(chooseFile, "Choose a file");
//                startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
//                Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, PICKFILE_RESULT_CODE);
                    val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
                    galleryIntent.type = "*/*"
                    startActivityForResult(galleryIntent, PICKFILE_RESULT_CODE)
                }
            })
        uploadDialogView.findViewById<View>(R.id.yes)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    //your business logic
                    uploadDialog.dismiss()
                    startUpload(path, type)
                }
            })
        uploadDialogView.findViewById<View>(R.id.no)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    uploadDialog.dismiss()
                }
            })
        uploadDialog.show()
    }

    private object AlarmUtils {
        var alarmManager: AlarmManager? = null
        var pendingIntent: PendingIntent? = null
        fun setAlarm(applicationContext: Context, time: Long) {
            val intent = Intent(applicationContext, AlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(applicationContext, 0, intent, 0)
            alarmManager = applicationContext.getSystemService(ALARM_SERVICE) as AlarmManager

            //set the alarm repeat one day
            alarmManager!!.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                time, AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent
            )
        }
    }

    companion object {
        private val LOG_TAG = FilesActivity::class.java.canonicalName
        val PICKFILE_RESULT_CODE = 1
        val EXTRA_STREAM_URL = "STREAM_URL"
        val FILE_ID = "FILE_ID"

        /** Key to receive a flag signaling if the video should be started immediately  */
        val EXTRA_AUTOPLAY = "AUTOPLAY"

        /** Key to receive the position of the playback where the video should be put at start  */
        val EXTRA_START_POSITION = "START_POSITION"
        @JvmStatic
        fun getRootDirPath(context: Context): String {
            if ((Environment.MEDIA_MOUNTED == Environment.getExternalStorageState())) {
                val file = ContextCompat.getExternalFilesDirs(context.applicationContext, null)[0]
                return file.absolutePath
            } else {
                return context.applicationContext.filesDir.absolutePath
            }
        }
    }
}