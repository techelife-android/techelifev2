package com.techesolutions.view.activity.nextcloud.events

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import java.util.*

class CalenderAdapter(
    context: Context,
    val eventsList: ArrayList<Events>,
    private var noteclick: CalenderActivity
) : RecyclerView.Adapter<CalenderAdapter.ViewHolder>() {
    var activity:Context = context
    var eventClickListener=noteclick
  /*  val now = Date()
    var dateValue = formatDate.format(now)
*/
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalenderAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.event_items, parent, false)
        return ViewHolder(v, activity, eventClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CalenderAdapter.ViewHolder, position: Int) {
        eventsList.get(position)?.let { holder.bindItems(it, activity, eventClickListener, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return eventsList?.size!!
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context,
        eventClickListener: NoteClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            events: Events,
            context: Context,
            eventClickListener: NoteClickListener,
            position: Int
        ) {
            val textViewName = itemView.findViewById(R.id.title) as TextView
            val content = itemView.findViewById(R.id.content) as TextView
           // val image = itemView.findViewById(R.id.activity_icon) as TextView
            val menu = itemView.findViewById(R.id.rlmenu) as RelativeLayout
            var date = itemView.findViewById(R.id.time) as TextView
            var dateTime = itemView.findViewById(R.id.dateTime) as TextView
            var alert = itemView.findViewById(R.id.alert) as ImageView
            if(events.timer.length>1)
            {
                alert.visibility=View.VISIBLE
            }
            else{
                alert.visibility=View.GONE
            }
            if(events.title!=null)
                textViewName.text = events.title

            content.text=events.description
          /*  var sdate=events.stratTime.split("T")
            var edate=events.endTime.split("T")*/

           // date.text = sdate!![0]
            var stime: String =
                Utils.convertEventTimeFormat(events.stratTime, context)

            var etime: String =
                Utils.convertEventTimeFormat(events.endTime, context)


            var eventDate: String =
                Utils.convertEventDateFormat(events.stratTime, context)
            date.text = stime+"-"+etime+" on "+eventDate
           // dateTime.text = eventDate
            //date.text = ""+miliSec
           // image.setImageResource(R.drawable.notes)
//            itemView?.setOnClickListener {
//                noteClickListener.onNoteLongClick(position, itemView)
//                noteClickListener.onNoteClick(position)
//            }
            itemView.setOnLongClickListener {
                val position = layoutPosition
               // println("LongClick: $p")
               // noteClickListener.onNoteLongClick(position, itemView)

                true // returning true instead of false, works for me
            }

            itemView.setOnClickListener {
                val position = layoutPosition
                val events: Events = events
               // noteClickListener.onNoteClick(position)

                // Toast.makeText(context, "Recycle Click$events  ", Toast.LENGTH_SHORT).show()
            }
            menu.setOnCreateContextMenuListener({ menu, v, menuInfo ->
                val item = menu.add("Edit")
                item.setOnMenuItemClickListener({ i ->
                    eventClickListener.onNoteClick(position)
                    true // Signifies you have consumed this event, so propogation can stop.
                })
                val anotherItem = menu.add("Mark as important")
                anotherItem.setOnMenuItemClickListener({ i ->
                    // doOtherWorkOnItemClick()
                    true
                })
                val itemRemove = menu.add("Remove")
                itemRemove.setOnMenuItemClickListener({ i ->
                    eventClickListener.onNoteDeleteClick(position)
                    true
                })
            })
            menu.setOnClickListener({ view -> view.showContextMenu() })
        }

    }

    fun getItem(notePosition: Int): Events? {
        return eventsList.get(notePosition)
    }

}