package com.techesolutions.customControls.fontedVews.edittext

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.techesolutions.utils.Utils
/**
 * Created by Neelam on 20-12-2020.
 */
class ATecheMediumEditText : AppCompatEditText {
    constructor(context: Context) : super(context) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        if (Utils.aTecheMediumFont != null) {
            typeface = Utils.aTecheMediumFont
        } else {
            Utils.setDimensions(context)
            typeface = Utils.aTecheMediumFont
            println("Oxigen ligh tnot loaded")
        }
    }
}
