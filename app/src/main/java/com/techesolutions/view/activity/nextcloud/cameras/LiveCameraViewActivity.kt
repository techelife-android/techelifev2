package com.techesolutions.view.activity.nextcloud.cameras;

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.nextcloud.notes.NavigationAdapter
import kotlinx.android.synthetic.main.activity_vital_details.*
import kotlinx.android.synthetic.main.camera_navigation.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LiveCameraViewActivity : AppCompatActivity() , View.OnClickListener{
    var mStreamUri: String? = null
    var id: String? = null
    var camera: String? = null
    private val progressDialog = CustomProgressDialog()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.camera_navigation)
        val extras = intent.extras
        mStreamUri = extras!!.getString("camera_url")
        id = extras!!.getString("id")
        camera = extras!!.getString("camera")
        headerTitle.text="$camera"
        webView.webViewClient = WebViewClient()
        webView.settings.setSupportZoom(true)
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(mStreamUri)
        setView(10)
        //navigateCamera("presetHome")
        arrowCenter.setOnClickListener(this)
        arrow1.setOnClickListener(this)
        arrow2.setOnClickListener(this)
        arrow3.setOnClickListener(this)
        arrow4.setOnClickListener(this)
        arrow5.setOnClickListener(this)
        arrow6.setOnClickListener(this)
        arrow8.setOnClickListener(this)
        arrow9.setOnClickListener(this)
        back.setOnClickListener(this)
    }

    private fun setView(isActive: Int) {
        if(isActive==10)
        {
            arrowCenter.setImageResource(R.drawable.camera_center_selected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)

        }
        if(isActive==0)
        {
            navigateCamera("presetHome")
            arrowCenter.setImageResource(R.drawable.camera_center_selected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)

        }
        if(isActive==1)
        {
            navigateCamera("moveConUp")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_selected_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)
        }
        if(isActive==2)
        {
            navigateCamera("moveConUpRight")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_selected_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)
        }
        if(isActive==3)
        {
            navigateCamera("moveConRight")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_selected_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)
        }
        if(isActive==4)
        {
            navigateCamera("moveConDownRight")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_selected_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)
        }
        if(isActive==5)
        {
            navigateCamera("moveConDown")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_selected_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)
        }
        if(isActive==6)
        {
            navigateCamera("moveConDownLeft")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_selected_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_9)
        }
        if(isActive==8)
        {
            navigateCamera("moveConDownLeft")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_selected_8)
            arrow9.setImageResource(R.drawable.arrow_9)
        }
        if(isActive==9)
        {
            navigateCamera("moveConDownLeft")
            arrowCenter.setImageResource(R.drawable.camera_center_unselected)
            arrow1.setImageResource(R.drawable.arrow_1)
            arrow2.setImageResource(R.drawable.arrow_2)
            arrow3.setImageResource(R.drawable.arrow_3)
            arrow4.setImageResource(R.drawable.arrow_4)
            arrow5.setImageResource(R.drawable.arrow_5)
            arrow6.setImageResource(R.drawable.arrow_6)
            arrow8.setImageResource(R.drawable.arrow_8)
            arrow9.setImageResource(R.drawable.arrow_selected_9)
        }
    }

    private fun navigateCamera(cmd: String) {
        showLoading("Moving camera...")
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.cameraNavigation(
                "Bearer $token",
                "mobile",
                cmd,
                id!!,
                1
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                    closeLoading()
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    Log.d("jsonObj",jsonObj.toString())
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                    } else {
                        closeLoading()
                        Toast.makeText(
                            this@LiveCameraViewActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }


            })
        }

    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.back -> {
                finish()
            }
            R.id.arrow1 -> {
                setView(1)
            }
            R.id.arrow2 -> {
                setView(2)
            }
            R.id.arrow3 -> {
                setView(3)
            }
            R.id.arrow4 -> {
                setView(4)
            }
            R.id.arrow5 -> {
                setView(5)
            }
            R.id.arrow6 -> {
                setView(6)
            }
            R.id.arrow8 -> {
                setView(8)
            }
            R.id.arrow9 -> {
                setView(9)
            }
            R.id.arrowCenter -> {
                setView(0)

            }
        }
    }
}

