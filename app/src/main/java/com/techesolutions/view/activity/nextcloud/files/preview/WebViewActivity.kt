package com.techesolutions.view.activity.nextcloud.files.preview;
import android.os.Bundle
import android.util.Base64
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.techesolutions.R
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils
import kotlinx.android.synthetic.main.activity_web_view.*
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException


class WebViewActivity : AppCompatActivity() {
    val EXTRA_STREAM_URL = "STREAM_URL"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        val extras = intent.extras
        //file_id = extras.getBoolean(FILE_ID);
        //file_id = extras.getBoolean(FILE_ID);
        val file = extras!!.getString(EXTRA_STREAM_URL)
        val file1=file!!.replace(" ","%20")
        val mStreamUri = "https://office.teche.solutions/remote.php/webdav$file1"
        webView.webViewClient = WebViewClient()
        webView.settings.setSupportZoom(true)
        webView.settings.javaScriptEnabled = true
        val url = FileUtils.getPdfUrl(mStreamUri)
        webView.loadUrl("https://docs.google.com/gview?embedded=true&url=$url")
        // webView.setWebViewClient(wvc);
        webView.setWebViewClient(object:WebViewClient() {
            override fun shouldOverrideUrlLoading(view:WebView, url:String):Boolean {
                //return true load with system-default-browser or other browsers, false with your webView
                val basicAuth =
                    "Basic " + String(Base64.encode("mobtest1:Mobtest@1".toByteArray(), Base64.NO_WRAP))

                //val headerMap = HashMap()
                val headerMap:HashMap<String,String> = HashMap<String,String>()
                //put all headers in this header map
                headerMap.put("Authorization", basicAuth)
                view.loadUrl(url, headerMap)
                return false
            }
        })

    }
}
/*

var wvc: WebViewClient = object : WebViewClient() {
    override fun shouldInterceptRequest(view: WebView?, url: String?): WebResourceResponse? {
        return try {
            //PRDownloader.initialize(this);
            val basicAuth =
                "Basic " + String(Base64.encode("mobtest1:Mobtest@1".toByteArray(), Base64.NO_WRAP))

            val okHttpClient = OkHttpClient()
            val request: Request = HttpUrl.Builder().addHeader("Authorization", basicAuth)
                .build()
            val response: Response = okHttpClient.newCall(request).execute()
            return WebResourceResponse(
                response.header(
                    "text/html", response.body!!
                        .contentType()!!.type
                ),  // You can set something other as default content-type
                response.header(
                    "content-encoding",
                    "utf-8"
                ),  // Again, you can set another encoding as default
                response.body!!.byteStream()
            )
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }
}
*/

