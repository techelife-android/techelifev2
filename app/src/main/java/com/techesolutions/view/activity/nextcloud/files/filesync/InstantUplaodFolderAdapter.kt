package com.techesolutions.view.activity.nextcloud.files.filesync

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.view.activity.nextcloud.notes.NavigationAdapter
import com.techesolutions.view.callbacks.ItemClickListener

class InstantUplaodFolderAdapter(val context: Context, var dataSource: MutableList<RemoteFile>) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.language_items, parent, false)
            vh = ItemHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }
        vh.label.text = dataSource!!.get(position).remotePath
        return view
    }

    override fun getItem(position: Int): Any? {
        return dataSource!![position];
    }

    override fun getCount(): Int {
        return dataSource!!.size;
    }

    override fun getItemId(position: Int): Long {
        return position.toLong();
    }

    private class ItemHolder(row: View?) {
        val label: TextView
        val img: ImageView

        init {
            label = row?.findViewById(R.id.txtName) as TextView
            img = row?.findViewById(R.id.select) as ImageView
        }
    }

}
