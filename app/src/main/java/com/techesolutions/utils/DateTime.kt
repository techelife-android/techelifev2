package com.techesolutions.utils

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
/**
 * Created by Neelam on 19-12-2020.
 */
object DateTime {
    fun getFromMillies(time: Long): String {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return format.format(Date(time))
    }

    fun getFromMillies(time: Long, format: String?): String {
        val sdf = SimpleDateFormat(format)
        return sdf.format(Date(time))
    }

    fun getFromMilliesInUTC(time: Long): String {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        format.timeZone = TimeZone.getTimeZone("GMT")
        return format.format(Date(time))
    }

    fun getFromMilliesInUTC(time: Long, formate: String?): String {
        val format = SimpleDateFormat(formate)
        format.timeZone = TimeZone.getTimeZone("GMT")
        return format.format(Date(time))
    }

    @Throws(ParseException::class)
    fun getDateFromUTC(
        datetime: String?, formatFrom: String?,
        formatTo: String?
    ): String {
        val sdf1 = SimpleDateFormat(formatFrom)
        val sdf2 = SimpleDateFormat(formatTo)
        sdf1.timeZone = TimeZone.getTimeZone("GMT")
        return sdf2.format(sdf1.parse(datetime))
    }

    @Throws(ParseException::class)
    fun getInMilliesFromUTC(time: String?): Long {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        format.timeZone = TimeZone.getTimeZone("GMT")
        return format.parse(time).time
    }

    @Throws(ParseException::class)
    fun getInMillies(time: String?): Long {
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return format.parse(time).time
    }

    @Throws(ParseException::class)
    fun getInMillies(time: String?, formate: String?): Long {
        val format = SimpleDateFormat(formate)
        return format.parse(time).time
    }

    val nowInUTC: String
        get() {
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            format.timeZone = TimeZone.getTimeZone("GMT")
            return format.format(Date(System.currentTimeMillis()))
        }

    val now: String
        get() {
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            return format.format(Date(System.currentTimeMillis()))
        }

    val currentDate: String
        get() {
            val format = SimpleDateFormat("yyyy-MM-dd")
            return format.format(Date(System.currentTimeMillis()))
        }

    val currentTimeZone: String
        get() {
            val timeZone = TimeZone.getDefault()
            return timeZone.id
        }

    @Throws(ParseException::class)
    fun getDate(datetime: String?): String {
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val format2 = SimpleDateFormat("yyyy-MM-dd")
        return format2.format(format1.parse(datetime))
    }

    @Throws(ParseException::class)
    fun getDate(datetime: Date?): String {
        val format2 = SimpleDateFormat("yyyy-MM-dd")
        return format2.format(datetime)
    }

    @Throws(ParseException::class)
    fun getTime(datetime: String?): String {
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val format2 = SimpleDateFormat("HH:mm:ss")
        return format2.format(format1.parse(datetime))
    }

    @Throws(ParseException::class)
    fun getDateTime(
        datetime: String?, formatFrom: String?,
        formatTo: String?
    ): String {
        val sdf1 = SimpleDateFormat(formatFrom)
        val sdf2 = SimpleDateFormat(formatTo)
        return sdf2.format(sdf1.parse(datetime))
    }

    @Throws(ParseException::class)
    fun convertDateTimeBwTimeZone(
        datetime: String?,
        originalTimezone: String?, finalTimeZone: String?
    ): String? {
        val patternString = "yyyy-MM-dd HH:mm:ss"
        return convertDateTimeBwTimeZone(
            datetime, patternString,
            originalTimezone, finalTimeZone
        )
    }

    @Throws(ParseException::class)
    fun convertDateTimeBwTimeZone(
        datetime: String?,
        patternString: String?,
        originalTimezone: String?,
        finalTimeZone: String?
    ): String? {
        try {
            val originalFormate: DateFormat = SimpleDateFormat(patternString)
            originalFormate.timeZone = TimeZone.getTimeZone(originalTimezone)
            val finalFormat: DateFormat = SimpleDateFormat(patternString)
            finalFormat.timeZone = TimeZone.getTimeZone(finalTimeZone)
            val timestamp = originalFormate.parse(datetime)
            return finalFormat.format(timestamp)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return datetime
    }

    val today: String
        get() {
            val calendar = Calendar.getInstance()
            val format = SimpleDateFormat("yyyy-MM-dd")
            return format.format(calendar.time)
        }

    val thisMonthFirstDate: String
        get() {
            val calendar = Calendar.getInstance()
            calendar[Calendar.DAY_OF_MONTH] = 1
            val format = SimpleDateFormat("yyyy-MM-dd")
            return format.format(calendar.time)
        }

    val thisMonthLastDate: String
        get() {
            val calendar = Calendar.getInstance()
            calendar[Calendar.DAY_OF_MONTH] = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
            val format = SimpleDateFormat("yyyy-MM-dd")
            return format.format(calendar.time)
        }

    val dayOfWeek: Int
        get() {
            val calendar = Calendar.getInstance()
            return calendar[Calendar.DAY_OF_WEEK]
        }
    /**
     * ISO 8601 strings
     * (in the following format: "2008-03-01T13:00:00+01:00"). It supports
     * parsing the "Z" timezone, but many other less-used features are
     * missing.
     */
    /**
     * Transform Calendar to ISO 8601 string.
     */
    private fun fromCalendar(calendar: Calendar): String {
        val date = calendar.time
        val formatted = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .format(date)
        return formatted.substring(0, 22) + ":" + formatted.substring(22)
    }

    /**
     * Get current date and time formatted as ISO 8601 string.
     */
    val nowInISO: String
        get() = fromCalendar(Calendar.getInstance())

    /**
     * Get current date and time formatted as ISO 8601 string.
     */
    fun getInISO(calendar: Calendar): String {
        return fromCalendar(calendar)
    }

    /**
     * Transform ISO 8601 string to Calendar.
     */
    @Throws(ParseException::class)
    fun toCalendarFromISO(iso8601string: String): Calendar {
        val calendar = GregorianCalendar.getInstance()
        var s = iso8601string.replace("Z", "+00:00")
        s = try {
            s.substring(0, 22) + s.substring(23) // to get rid of the ":"
        } catch (e: IndexOutOfBoundsException) {
            throw ParseException("Invalid length", 0)
        }
        val date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s)
        calendar.time = date
        return calendar
    }

    fun getDate2(datetime: String?): String {
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val format2 = SimpleDateFormat("MM-dd-yyyy")
        try {
            return format2.format(format1.parse(datetime))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ""
    }

    fun getTime2(datetime: String?): String {
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val format2 = SimpleDateFormat("HH:mm:ss")
        try {
            return format2.format(format1.parse(datetime))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ""
    }

    fun getTime2New(datetime: String?): String {
        val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val format2 = SimpleDateFormat("HH:mm")
        try {
            return format2.format(format1.parse(datetime))
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ""
    }
}
