package com.techesolutions.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.language.DataModel
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.MySharedPreference
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.login.LoginActivity
import com.techesolutions.view.adapter.LanguageAdapter
import com.techesolutions.view.callbacks.ItemClickListener
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LanguageSelectionActivity : AppCompatActivity() , View.OnClickListener,ItemClickListener{
    private var dataModel: ArrayList<DataModel>? = null
    private lateinit var listView: ListView
    private lateinit var adapter: LanguageAdapter
    private lateinit var next: TextView
    private val progressDialog = CustomProgressDialog()
    var language=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_language)
        listView = findViewById<View>(R.id.listView) as ListView
        next=findViewById<View>(R.id.btNext) as TextView
       // val shared =
         //   getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        //language = shared.getString( Constants.KEY_LANGUAGE, "").toString()
        language=intent.getStringExtra("defaultLanguage")
        dataModel = ArrayList<DataModel>()
        if(language.equals("es"))
        {
            dataModel!!.add(DataModel("English", "en",false))
            dataModel!!.add(DataModel("Spanish", "sp",true))

        }
        else{
            dataModel!!.add(DataModel("English", "en",true))
            dataModel!!.add(DataModel("Spanish", "sp",false))

        }
//        dataModel!!.add(DataModel("English", "en",false))
//        dataModel!!.add(DataModel("Spanish", "sp",false))

        adapter = LanguageAdapter(applicationContext,dataModel!!,this)
        listView.adapter = adapter
       /* listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val dModel: DataModel = dataModel!![position] as DataModel
           *//* for (item in dataModel.indices) {
                list[item].checked = !list[item].checked
            }*//*
//            if(dataModel.name!!.equals("English"))
//            {
//                dataModel!!.add(DataModel("English", "en",true))
//                DataModel("Spanish", "sp",false)
//            }
//            if(dataModel.name!!.equals("Spanish"))
//            {
//                DataModel("English", "en",false)
//                DataModel("Spanish", "sp",true)
//            }
//            dataModel.add(DataModel)
            //dModel.checked = !dModel.checked
            language= dModel.short_name!!
            Toast.makeText(this,language, Toast.LENGTH_SHORT).show()
//            if(language.equals("en"))
//            {
//                dataModel.checked=true
//            }

            adapter.notifyDataSetChanged()
        }*/
        next.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
      when(v!!.id)
      {
          R.id.btNext -> {
              if(language.length<1)
              {
                  onErrorListener(getString(R.string.select_language))

              }
              else {
                  MySharedPreference.setPreference(this, Constants.KEY_LANGUAGE, language)
                  setLanguageAPI(language)
              }
          }
      }
    }
    private fun setLanguageAPI(language: String) {
        showLoading("Please wait")
        val call = NetworkService.apiInterface.setLanguage(language,"mobile")

        call.enqueue(object: Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                val stringResponse = response.body()?.string()
                val jsonObj = JSONObject(stringResponse)
                val success = jsonObj!!.getString("success")
                if (success != null && success.toString().equals("true")) {

                val data = jsonObj!!.getJSONObject("data")
                MySharedPreference.setPreference(this@LanguageSelectionActivity, Constants.LANGUAGE_API_DATA, data.toString())
                    closeLoading()
                val intent = Intent(this@LanguageSelectionActivity, LoginActivity::class.java)
                startActivity(intent)
                    finish()
                 }
                 else
                 {
                     Toast.makeText(this@LanguageSelectionActivity, "Some error. Try again.", Toast.LENGTH_SHORT).show()
                 }
            }


        })

}

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {
       // Toast.makeText(this,dataModel!![adapterPosition].name, Toast.LENGTH_SHORT).show()
        language= dataModel!![adapterPosition].short_name.toString()
    }


}