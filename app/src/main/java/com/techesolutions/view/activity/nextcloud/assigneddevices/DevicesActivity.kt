package com.techesolutions.view.activity.nextcloud.assigneddevices

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.devices.DeviceData
import com.techesolutions.data.remote.model.nextclouddashboard.CameraItems
import com.techesolutions.data.remote.model.nextclouddashboard.DeviceItems
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.adapter.DeviceAdapter
import com.techesolutions.view.callbacks.ItemClickListener
import com.techesolutions.viewmodel.cameras.MasterViewModel
import kotlinx.android.synthetic.main.activity_cemaras.*
import kotlinx.android.synthetic.main.activity_cemaras.noDataText
import kotlinx.android.synthetic.main.activity_iot_room_reading.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Neelam on 05-09-2021.
 */
class DevicesActivity : AppCompatActivity(), View.OnClickListener, ItemClickListener {
    private var dataModel: ArrayList<DeviceItems>? = null
    private var cametaModel: ArrayList<CameraItems>? = null
    private lateinit var spinner: Spinner
    //private lateinit var adapter: RoomAdapter
    lateinit var roomViewModel: MasterViewModel
    private val progressDialog = CustomProgressDialog()
    var recyclerView: RecyclerView? = null
    var devicedata : ArrayList<DeviceData>? = null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_cemaras)
        headerTitle?.text="Assigned Devices"
        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }
        roomViewModel = ViewModelProvider(this).get(MasterViewModel::class.java)
        spinner = findViewById<View>(R.id.spinner) as Spinner
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        fetchRoomData("Bearer $token")


        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val  devicevalue = dataModel!![position].value
            fetchDeviceData("Bearer $token",devicevalue!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

    }

    private fun fetchDeviceData(token: String, device_type: String) {
        devicedata = ArrayList<DeviceData>()
        //devicedata!!.clear()
        if (Utils.isOnline(this@DevicesActivity)) {
            showLoading("Please wait")
            val call = NetworkService.apiInterface.getAssignedDevices(
                token,
                "mobile",
                device_type,
                "1",
                "30"
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    if (response != null) {
                        val stringResponse = response.body()?.string()
                        if(stringResponse!=null) {
                            val jsonObj = JSONObject(stringResponse)
                            val success = jsonObj!!.getString("success")
                            if (success != null && success.toString().equals("true")) {
                                val jObj = jsonObj.getJSONObject("data")
                                Log.d("data", jObj.toString())
                                 val dlist = jObj.getJSONArray("data")
                                if(dlist.length()>0) {
                                    noDataText!!.visibility = View.GONE
                                    recyclerView!!.visibility = View.VISIBLE
                                    for (i in 0 until dlist.length()) {
                                        val data = dlist.getJSONObject(i)

                                        devicedata!!.add(
                                            DeviceData(
                                                data!!.getInt("iotassign_id"),
                                                data!!.getInt("isactive"),
                                                data!!.getString("skuno"),
                                                data!!.getString("sku_category"),
                                                data!!.getString("skuname"),
                                                data!!.getString("description"),
                                                data!!.getString("skutypeid"),
                                                data!!.getString("deviceid"),
                                                data!!.getString("room_range"),
                                                data!!.getString("room_label"),
                                                data!!.getString("door_range"),
                                                data!!.getString("door_label"),
                                                data!!.getString("name"),
                                                data!!.getString("firstname"),
                                                data!!.getString("surname"),
                                                data!!.getString("id"),
                                                data!!.getString("created"),
                                                data!!.getString("buyingskuid"),
                                                data!!.getString("addeddateW")
                                            )
                                        )
                                    }
                                    val deviceAdapter = DeviceAdapter(this@DevicesActivity, devicedata!!)
                                    recyclerView?.adapter = deviceAdapter
                                }
                                else{
                                    noDataText!!.visibility = View.VISIBLE
                                    recyclerView!!.visibility = View.GONE
                                }
                                //setData(jsonObj)
                            } else {
                                Toast.makeText(
                                    this@DevicesActivity,
                                    jsonObj!!.getString("message"),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                        else{
                            Toast.makeText(this@DevicesActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        Toast.makeText(this@DevicesActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                    }

                }

            })
            // Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show()
            /*cameraViewModel.getCameraData(token,"mobile",device_type)!!
                .observe(this, Observer { masterResponse ->
                    val success = masterResponse.success;
                    if (success == true) {
                        closeLoading()
                        try {
                            val gson = Gson()
                            val rooms: String = gson.toJson(masterResponse.data!!)
                            Log.d("data",rooms)
                            *//*val rooms: String = gson.toJson(masterResponse.data!!.cameras)
                            var dlist = JSONArray(rooms)
                            cametaModel = ArrayList<CameraItems>()
                            for (i in 0 until dlist.length()) {
                                val item = dlist.getJSONObject(i)
                                val title = item.optString("title")
                                val label = item.optString("room_label")
                                val url = item.optString("cam_live_stream_url")
                                cametaModel!!.add(CameraItems(title, label, url))
                            }*//*
                        } catch (e: Exception) {
                        }
                      *//*  val cameraAdapter = CameraAdapter(this, cametaModel!!)
                        recyclerView?.adapter = cameraAdapter*//*
                    } else {
                        closeLoading()
                        Toast.makeText(this, masterResponse.message, Toast.LENGTH_SHORT).show()
                    }
                    //Toast.makeText(this, baseResponse.message, Toast.LENGTH_SHORT).show()
                })*/
        }

    }

    override fun onClick(v: View) {
        when (v.id) {

        }
    }

    private fun fetchRoomData(token: String) {

                if (Utils.isOnline(this@DevicesActivity)) {
                showLoading("Please wait")

                // Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show()
                    roomViewModel.getRoomDeviceMasterData(token, "mobile", "MASTER_MOTE")!!
                    .observe(this, Observer { masterResponse ->
                        if(masterResponse!=null) {
                        val success = masterResponse.success;
                        if (success == true) {
                            closeLoading()
                            try {
                                val gson = Gson()
                                val rooms: String = gson.toJson(masterResponse.data!!.devices)
                                var dlist = JSONArray(rooms)
                                dataModel = ArrayList<DeviceItems>()
                                for (i in 0 until dlist.length()) {
                                    val item = dlist.getJSONObject(i)
                                    val label = item.optString("label")
                                    val value = item.optString("value")
                                    dataModel!!.add(DeviceItems(label, value))
                                }
                            } catch (e: Exception) {
                            }
                            val roomAdapter = RoomsDevicesAdapter(this, dataModel!!, this)
                            spinner.adapter = roomAdapter
                        } else {
                            closeLoading()
                            Toast.makeText(this, masterResponse.message, Toast.LENGTH_SHORT).show()
                        }
                        }
                        else{
                            closeLoading()
                            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    })
            }

    }


    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {

    }

}