package com.techesolutions

import android.app.Application
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.fitpolo.support.MokoSupport
import com.owncloud.android.lib.resources.status.OwnCloudVersion
//import com.techesolutions.utils.AppPreferences
import org.joda.time.LocalDateTime

class AppController : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this

        MokoSupport.getInstance().init(applicationContext)

    }

    companion object {
        var mContext: Context? = null
        val TAG = AppController::class.java
            .simpleName

        @get:Synchronized
        var instance: AppController? = null
            private set
    }

}