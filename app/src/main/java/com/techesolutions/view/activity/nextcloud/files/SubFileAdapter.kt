package com.techesolutions.view.activity.nextcloud.files

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.utils.DisplayUtils
import com.techesolutions.view.callbacks.FileClickListener
import java.text.SimpleDateFormat

class SubFileAdapter(
    private val mContext: Context,
    private val resourceLayout: Int,
    private val filelistener: FileClickListener
) : ArrayAdapter<RemoteFile?>(
    mContext, resourceLayout
) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var v = convertView
        if (v == null) {
            val vi: LayoutInflater
            vi = LayoutInflater.from(mContext)
            v = vi.inflate(R.layout.note_folder_items, parent, false)
        }
        val p = getItem(position)
        if (p != null) {
            val tt1 = v!!.findViewById<View>(R.id.title) as TextView
            val dateTV = v.findViewById<View>(R.id.date) as TextView
            val content = v.findViewById<View>(R.id.content) as TextView
            val activity_icon = v.findViewById<View>(R.id.activity_icon) as ImageView
            val delete = v.findViewById<View>(R.id.delete) as ImageView
            if (tt1 != null) {
                var file: String? = null
                if (p.remotePath.endsWith("/")) {
                    Log.i("TAG", p.remotePath.replace("/".toRegex(), ""))
                    //file = p.remotePath.replaceAll("/","");
                    file = p.remotePath.substring(0, p.remotePath.lastIndexOf("/"))
                    val files = file.split("/".toRegex()).toTypedArray()
                    file = files[files.size - 1]
                    Log.i("TAG", "files: $file")
                } else {
                    file = p.remotePath.substring(
                        p.remotePath.lastIndexOf("/") + 1,
                        p.remotePath.length
                    )
                    Log.i("TAG", file)
                }
                // String filename=p.remotePath.replaceAll("/","");
                if (file.length > 15 && file.contains(".")) {
                    val filepre = file.substring(0, 10)
                    val filepost = file.substring(file.lastIndexOf(".", file.length - 1))
                    file = filepre + "..." + file.substring(
                        file.length - 7,
                        file.lastIndexOf(".")
                    ) + filepost
                }
                Log.i("filename", file)
                tt1.text = file
            }
            if (content != null) {
                val formatDate = SimpleDateFormat("dd MMM, yyyy")
                val modifiedTimestamp = p.modifiedTimestamp
                val date = formatDate.format(modifiedTimestamp)
                content.text = DisplayUtils.bytesToHumanReadable(p.size)
                content.visibility = View.VISIBLE
            }
            if (dateTV != null) {
                val formatDate = SimpleDateFormat("dd MMM, yyyy")
                val modifiedTimestamp = p.modifiedTimestamp
                val date = formatDate.format(modifiedTimestamp)
                dateTV.text = DisplayUtils.getRelativeTimestamp(
                    context,
                    modifiedTimestamp
                )
                dateTV.visibility = View.VISIBLE
            }
            Log.i("all data", p.remotePath)
            if (p.mimeType.equals("DIR", ignoreCase = true)) {
                activity_icon.setImageResource(R.drawable.file)
            } else if (p.mimeType.equals(
                    "image/png",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "image/jpeg",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "image/jpg",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "image/heic",
                    ignoreCase = true
                ) || p.mimeType.equals("image/heif", ignoreCase = true)
            ) {
                activity_icon.setImageResource(R.drawable.ic_image)
            } else if (p.mimeType.equals(
                    "text/markdown",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "text/plain",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    ignoreCase = true
                ) || p.mimeType.equals("application/vnd.oasis.opendocument.text", ignoreCase = true)
            ) {
                activity_icon.setImageResource(R.drawable.notes)
            } else if (p.mimeType.equals("application/pdf", ignoreCase = true)) {
                activity_icon.setImageResource(R.drawable.notes)
            } else if (p.mimeType.equals(
                    "video/mp4",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "video/mpeg",
                    ignoreCase = true
                ) || p.mimeType.equals("image/gif", ignoreCase = true)|| p.remotePath.contains(".mov", ignoreCase = true)
            ) {
                activity_icon.setImageResource(R.drawable.ic_video)
            } else if (p.mimeType.equals(
                    "application/xhtml+xml",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/xml",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "text/xml",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "text/csv",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/vnd.ms-excel",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    ignoreCase = true
                )
            ) {
                activity_icon.setImageResource(R.drawable.xls)
            } else if (p.mimeType.equals("text/vcard", ignoreCase = true)) {
                activity_icon.setImageResource(R.drawable.contacts)
            } else {
                activity_icon.setImageResource(R.drawable.notes)
            }
            delete.visibility = View.VISIBLE
            delete.setOnClickListener { filelistener.onDeleteClick(getPosition(p)) }
            v.setOnClickListener { filelistener.onPositionClick(getPosition(p)) }
        }
        return v!!
    }
}