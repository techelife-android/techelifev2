package com.techesolutions.view.activity.nextcloud.notes.shared.model;

public interface Item {
    boolean isSection();
}
