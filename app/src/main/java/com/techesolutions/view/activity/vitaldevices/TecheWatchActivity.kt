package com.techesolutions.view.activity.vitaldevices

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.fitpolo.support.MokoConstants
import com.fitpolo.support.MokoSupport
import com.fitpolo.support.callback.MokoScanDeviceCallback
import com.fitpolo.support.entity.BleDevice
import com.fitpolo.support.entity.OrderEnum
import com.fitpolo.support.entity.OrderTaskResponse
import com.fitpolo.support.log.LogModule
import com.fitpolo.support.task.ZReadBatteryTask
import com.fitpolo.support.task.ZReadHeartRateTask
import com.fitpolo.support.task.ZReadStepTask
import com.fitpolo.support.task.ZWriteSystemTimeTask
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.local.techewatch.ReadingsData
import com.techesolutions.services.NetworkService
import com.techesolutions.services.watch.MokoService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.DashboardActivity
import com.techesolutions.view.adapter.VitalReadingsAdapter
import kotlinx.android.synthetic.main.activity_device_controls.*
import kotlinx.android.synthetic.main.layout_header.*
import kotlinx.android.synthetic.main.next_dialog.*
import okhttp3.ResponseBody
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class TecheWatchActivity : AppCompatActivity(), MokoScanDeviceCallback, View.OnClickListener {
    private var adapter: VitalReadingsAdapter? = null
    var vitalNameLabels: JSONObject? = null
    var vitalUnitLabels: JSONObject? = null
    val PATTERN_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm"

    val EXTRA_CONN_COUNT = "extra_conn_count"

    val PERMISSION_REQUEST_CODE = 1


    val REQUEST_CODE_PERMISSION = 120
    val REQUEST_CODE_PERMISSION_2 = 121
    val REQUEST_CODE_LOCATION_SETTINGS = 122

    val REQUEST_CODE_ENABLE_BT = 1001
    var unit1: String? = null
    var unit2: String? = null
    var keyUnit1: String? = null
    var keyUnit2: String? = null
    var keyUnit3 = ""
    var taskid: String? = null
    var title: String? = null
    var vitalName: String? = null
    var subVitalName: String? = null
    var type: String? = null
    private val progressDialog = CustomProgressDialog()
    private var dateValue: String? = null
    private var mService: MokoService? = null
    private var mDevice: BleDevice? = null
    private var deviceMap: HashMap<String, BleDevice>? = null
    private var mDatas: ArrayList<BleDevice>? = null
    var formatterDate =
        SimpleDateFormat("MMM dd, yyyy | HH:mm")
    var formatDate = SimpleDateFormat("MM-dd-yyyy")
    var formatTime = SimpleDateFormat("HH:mm")
    var formatDate2 = SimpleDateFormat("yyyy-MM-dd")
    var currentFormat =
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var formatterDate1 = SimpleDateFormat("MMM dd, yyyy")
    private val readingsData =
        ArrayList<ReadingsData>()
    private val readingsHeartRateData =
        ArrayList<ReadingsData>()
    var updated_on =""

    var datesValue=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_controls)
        overridePendingTransition(R.anim.act_pull_in_right, R.anim.act_push_out_left)
        Utils.setDimensions(this)
        val now = Date()
        updated_on = formatDate2.format(now)
        updated_on=updated_on+" 00:00:00"
        datesValue=currentFormat.format(now)
        val i = intent
        title = i.getStringExtra("title")
        vitalName = i.getStringExtra("vitalName")
        subVitalName = i.getStringExtra("subVitalName")
        unit1 = i.getStringExtra("unitName1")
        unit2 = i.getStringExtra("unitName2")
        type = i.getStringExtra("type")
       // deviceNameHeaderTV!!.text = "Teche Watch"
        headerTitle!!.text = "Teche Watch"
        deviceImage.setImageResource(R.drawable.ic_smartwatch)
        var shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

        mDatas = ArrayList()
        deviceMap = HashMap()
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        } else if (!mBluetoothAdapter.isEnabled) {
            Toast.makeText(this@TecheWatchActivity,"Bluetooth is off. Please enable your mobile bluetooth",Toast.LENGTH_SHORT).show()
        } else {
            bindService(
                    Intent(this, MokoService::class.java),
                    mServiceConnection,
                    Context.BIND_AUTO_CREATE
            )
            searchDevices()
        }

        ok.text=vitalNameLabels!!.getString("btn_Ok_Completed")
        addManuallyTV!!.text=vitalNameLabels!!.getString("addmanually")
        sendResultsBtnID!!.text=vitalNameLabels!!.getString("taptoretryconnection")
        textViewAllergyHeading!!.text=vitalNameLabels!!.getString("devicename")
        textViewTypeHeading!!.text=vitalNameLabels!!.getString("macaddress")
        connectedHeading!!.text=vitalNameLabels!!.getString("notconnected")
        getBP.setOnClickListener(this)
        doneBT.setOnClickListener(this)
        sendBT.setOnClickListener(this)
        back.setOnClickListener(this)
        retryConnection.setOnClickListener(this)
        addManually.setOnClickListener(this)
        skip.setOnClickListener(this)
        val linearLayoutManager = LinearLayoutManager(this)
        dataRecyclerView.layoutManager = linearLayoutManager
        dataRecyclerView.setHasFixedSize(true)

    }

    fun searchDevices() {
        MokoSupport.getInstance().startScanDevice(this)
    }


    override fun onStartScan() {
        deviceMap!!.clear()
        showLoading("Scanning...")
    }

    override fun onScanDevice(device: BleDevice) {
        deviceMap!![device.address] = device
        mDatas!!.clear()
        mDatas!!.addAll(deviceMap!!.values)
    }

    override fun onStopScan() {
        closeLoading()
        mDatas!!.clear()
        mDatas!!.addAll(deviceMap!!.values)
        if(mDatas.isNullOrEmpty())
        {
                displayRetry()
        }
        else {
            for (i in mDatas!!.indices) {
                val device = mDatas!![i]
                if (device.name.equals("H709", ignoreCase = true)) {
                    // closeLoading()
                    showLoading("Connecting...")
                    macAddressHeaderTV!!.text = device.address
                    mService?.connectBluetoothDevice(device.address)
                    deviceNameHeaderTV!!.text="H709"
                    mDevice = device
                }
            }
        }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }


    private fun showLoading(s: String) {
        progressDialog?.show(this, s)
    }

    private val mReceiver: BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(
                    context: Context,
                    intent: Intent
            ) {
                if (intent != null) {
                    if (MokoConstants.ACTION_DISCOVER_SUCCESS == intent.action) {
                        abortBroadcast()
                        closeLoading()
                       // connectedHeading!!.text = "Connected"
                        connectedHeading!!.text=vitalNameLabels!!.getString("connected")
                        connectedHeading!!.setTextColor(resources.getColor(R.color.colorPrimary))
                        retryConnection!!.visibility = View.GONE
                        setSystemTime()
                        getBattery()
                        getHR!!.visibility = View.GONE
                        resetDate!!.visibility = View.GONE
                        getSteps!!.visibility = View.GONE
                        addManually!!.visibility = View.GONE
                        getBP!!.visibility = View.GONE
                    }
                    if (BluetoothAdapter.ACTION_STATE_CHANGED == intent.action) {
                        val blueState =
                            intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0)
                        when (blueState) {
                            BluetoothAdapter.STATE_TURNING_OFF, BluetoothAdapter.STATE_OFF -> finish()
                        }
                    }
                    if (MokoConstants.ACTION_CONN_STATUS_DISCONNECTED == intent.action) {
                        abortBroadcast()
                        if (MokoSupport.getInstance().isBluetoothOpen && MokoSupport.getInstance()
                                .reconnectCount > 0
                        ) {
                            return
                        }
                        closeLoading()
                        displayRetry()
                    }
                    if (MokoConstants.ACTION_ORDER_RESULT == intent.action) {
                        val response =
                            intent.getSerializableExtra(MokoConstants.EXTRA_KEY_RESPONSE_ORDER_TASK) as OrderTaskResponse
                        val orderEnum = response.order
                        when (orderEnum) {
                            OrderEnum.Z_READ_HEART_RATE -> {
                                val lastestHeartRate =
                                        MokoSupport.getInstance().heartRates
                                if (lastestHeartRate == null || lastestHeartRate.isEmpty()) {
                                   // closeLoading()
                                    try {
                                        var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                        val now = Date()
                                        var dateValue = formatDate.format(now)
                                        val dateString =dateValue
                                        val date =
                                                formatDate.format(currentFormat.parse(dateString))
                                        val time =
                                                formatTime.format(currentFormat.parse(dateString))
                                        val deviceDate =
                                                Utils.getDateFormat("$date $time")
                                        val profileTZ =
                                                Utils.getTimeZoneSting(
                                                        DateTime.now(DateTimeZone.getDefault())
                                                                .toString()
                                                )

                                        val reading = ReadingsData()
                                        reading.date =
                                                formatterDate.format(
                                                        currentFormat.parse(
                                                                dateString
                                                        )
                                                )
                                        reading.dateValue = date
                                        reading.timeValue = time
                                        reading.unit1Name = "Heart Rate"
                                        reading.unit1Value = "0"
                                        reading.unit1Units = "Beats/min"
                                        reading.keyUnit1 = "Heart_Rate_Beats"
                                        reading.dateConverted = dateString

                                        reading.timeConverted = dateString
                                        readingsData.add(reading)
                                        readingsHeartRateData.add(reading)
                                    } catch (e: java.lang.Exception) {
                                        e.printStackTrace()
                                    }
                                    getLastestSteps()
                                    return
                                }
                                for (heartRate in lastestHeartRate) {
                                    LogModule.i(heartRate.toString())
                                    try {
                                        val dateString = heartRate.time + ":00"
                                        val date =
                                                formatDate.format(currentFormat.parse(dateString))
                                        val time =
                                                formatTime.format(currentFormat.parse(dateString))
                                        val deviceDate =
                                                Utils.getDateFormat("$date $time")
                                        val profileTZ =
                                                Utils.getTimeZoneSting(
                                                        DateTime.now(DateTimeZone.getDefault())
                                                                .toString()
                                                )

                                        val reading = ReadingsData()
                                        reading.date =
                                                formatterDate.format(
                                                        currentFormat.parse(
                                                                dateString
                                                        )
                                                )
                                        reading.dateValue = date
                                        reading.timeValue = time
                                        reading.unit1Name = "Heart Rate"
                                        reading.unit1Value = heartRate.value
                                        reading.unit1Units = "Beats/min"
                                        reading.keyUnit1 = "Heart_Rate_Beats"
                                        reading.dateConverted = dateString

                                        reading.timeConverted = dateString
                                        readingsData.add(reading)
                                        readingsHeartRateData.add(reading)
                                    } catch (e: java.lang.Exception) {
                                        e.printStackTrace()
                                    }
                                }
                                getLastestSteps()

//                                val now = Date()
//                                val measurementDate = formatterDate.format(now)
//                                dateValue = formatDate.format(now)
//                                timeValue = formatTime.format(now)
//                                displayResults(
//                                    measurementDate,
//                                    "Hear Rate",
//                                    lastestHeartRate[lastestHeartRate.size - 1].value,
//                                    "Beats/min",
//                                    "",
//                                    "",
//                                    "",
//                                    "",
//                                    "",
//                                    ""
//                                )
                                //lastestSteps
                            }
                            OrderEnum.Z_READ_BATTERY -> {
                                LogModule.i(
                                        "Battery：" + MokoSupport.getInstance().batteryQuantity
                                )
                                batteryHeading!!.visibility=View.VISIBLE
                                batteryHeading!!.text =
                                        "Battery status: " + MokoSupport.getInstance()
                                                .batteryQuantity + "%"
                                getReading()
                            }
                            OrderEnum.Z_READ_STEPS -> {
                                val lastestSteps =
                                        MokoSupport.getInstance().dailySteps
                                if (lastestSteps == null || lastestSteps.isEmpty()) {
                                    closeLoading()
                                    try {
                                        var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                        val now = Date()
                                        var dateValue = formatDate.format(now)
                                        val readDate =dateValue
                                        val date =
                                                formatDate.format(formatDate2.parse(readDate))
                                        val time =
                                                "00:00:00" // formatTime.format(currentFormat.parse(step.date));
                                        val deviceDate =
                                                Utils.getDateFormat("$date $time")
                                        val profileTZ = Utils.getTimeZoneSting(
                                                DateTime.now(DateTimeZone.getDefault())
                                                        .toString()
                                        )
                                        val reading = ReadingsData()
                                        reading.date = formatterDate1.format(
                                                formatDate2.parse(
                                                        readDate
                                                )
                                        )
                                        reading.dateValue = date
                                        reading.timeValue = time
                                        reading.unit1Name = "Steps"
                                        reading.unit1Value = "0"
                                        reading.unit1Units = ""
                                        reading.keyUnit1 = "Steps_add"
                                        reading.unit2Name = "Distance"
                                        reading.unit2Value = "0"
                                        reading.unit2Units = "km"
                                        reading.keyUnit2 = "stepsdistance"
                                        reading.unit3Name = "Calories"
                                        reading.unit3Value = "0"
                                        reading.unit3Units = ""
                                        reading.keyUnit3 = "stepscalories"
                                        reading.unit4Name = ""
                                        reading.unit4Value = "0"
                                        reading.unit4Units = ""
                                        reading.keyUnit4 = "stepsduration"
                                        reading.dateConverted = readDate
                                        reading.timeConverted = readDate
                                        readingsData.add(0, reading)
                                    } catch (e: java.lang.Exception) {
                                        e.printStackTrace()
                                    }
                                    try {
                                        var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                                        val now = Date()
                                        var dateValue = formatDate.format(now)
                                        val dateString =dateValue
                                        val date =
                                                formatDate.format(currentFormat.parse(dateString))
                                        val time =
                                                formatTime.format(currentFormat.parse(dateString))
                                        val deviceDate =
                                                Utils.getDateFormat("$date $time")
                                        val profileTZ =
                                                Utils.getTimeZoneSting(
                                                        DateTime.now(DateTimeZone.getDefault())
                                                                .toString()
                                                )

                                        val reading = ReadingsData()
                                        reading.date =
                                                formatterDate.format(
                                                        currentFormat.parse(
                                                                dateString
                                                        )
                                                )
                                        reading.dateValue = date
                                        reading.timeValue = time
                                        reading.unit1Name = "Heart Rate"
                                        reading.unit1Value = "0"
                                        reading.unit1Units = "Beats/min"
                                        reading.keyUnit1 = "Heart_Rate_Beats"
                                        reading.dateConverted = dateString

                                        reading.timeConverted = dateString
                                        readingsData.add(reading)
                                        readingsHeartRateData.add(reading)
                                    } catch (e: java.lang.Exception) {
                                        e.printStackTrace()
                                    }
                                    displayResults2()
                                    return
                                }
                                for (step in lastestSteps) {
                                    LogModule.i(step.toString())
                                    try {
                                        val readDate = step.date
                                        val date =
                                                formatDate.format(formatDate2.parse(readDate))
                                        val time =
                                                "00:00:00" // formatTime.format(currentFormat.parse(step.date));
                                        val deviceDate =
                                                Utils.getDateFormat("$date $time")
                                        val profileTZ = Utils.getTimeZoneSting(
                                                DateTime.now(DateTimeZone.getDefault())
                                                        .toString()
                                        )
                                        val reading = ReadingsData()
                                        reading.date = formatterDate1.format(
                                                formatDate2.parse(
                                                        readDate
                                                )
                                        )
                                        reading.dateValue = date
                                        reading.timeValue = time
                                        reading.unit1Name = "Steps"
                                        reading.unit1Value = step.count
                                        reading.unit1Units = ""
                                        reading.keyUnit1 = "Steps_add"
                                        reading.unit2Name = "Distance"
                                        reading.unit2Value = step.distance
                                        reading.unit2Units = "km"
                                        reading.keyUnit2 = "stepsdistance"
                                        reading.unit3Name = "Calories"
                                        reading.unit3Value = step.calories
                                        reading.unit3Units = ""
                                        reading.keyUnit3 = "stepscalories"
                                        reading.unit4Name = ""
                                        reading.unit4Value = step.duration
                                        reading.unit4Units = ""
                                        reading.keyUnit4 = "stepsduration"
                                        reading.dateConverted = readDate
                                        reading.timeConverted = readDate
                                        readingsData.add(0, reading)
                                    } catch (e: java.lang.Exception) {
                                        e.printStackTrace()
                                    }
                                }
                                closeLoading()
                                displayResults2()

//
//                                val now1 = Date()
//                                val measurementDate2 = formatterDate.format(now1)
//                                dateValue = formatDate.format(now1)
//                                timeValue = formatTime.format(now1)
//                                displayResults(
//                                    measurementDate2, "", "", "",
//                                    "Steps", lastestSteps[lastestSteps.size - 1].count, "",
//                                    "", "", ""
//                                )
                            }
                        }

                    }
                    if (MokoConstants.ACTION_ORDER_TIMEOUT == intent.action) {
                    Toast.makeText(this@TecheWatchActivity, "Timeout", Toast.LENGTH_SHORT).show();
                    }

                    if (MokoConstants.ACTION_ORDER_FINISH == intent.action) {
                  // Toast.makeText(this@TecheWatchActivity, "Success", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

    private fun getReading() {
        showLoading("Fetching data...")
        getLastestHeartRates()
    }

    private fun getLastestHeartRates() {
        val now = Date()
        val measurementDate = formatDate2.format(now)
        val calendar =
            Utils.strDate2Calendar(updated_on, PATTERN_YYYY_MM_DD_HH_MM)
        MokoSupport.getInstance().sendOrder(ZReadHeartRateTask(mService, calendar))    }

    private fun getLastestSteps() {
        val now = Date()
        val measurementDate = formatDate2.format(now)
        val calendar: Calendar =
            Utils.strDate2Calendar(updated_on, PATTERN_YYYY_MM_DD_HH_MM)
        MokoSupport.getInstance().sendOrder(ZReadStepTask(mService, calendar))    }

    private fun getBattery() {
        MokoSupport.getInstance().sendOrder(ZReadBatteryTask(mService))
    }

    private fun setSystemTime() {
        MokoSupport.getInstance().sendOrder(ZWriteSystemTimeTask(mService))
    }

    private fun displayResults(
            date: String,
            unit1Name: String,
            unit1Value: String,
            unit1Units: String,
            unit2Name: String,
            unit2Value: String,
            unit2Units: String,
            unit3Name: String,
            unit3Value: String,
            unit3Units: String
    ) {
        runOnUiThread { //                AppUtilities.hideProgressDialog(mHud);
            getBP!!.visibility = View.GONE
            readingsLayout!!.visibility = View.VISIBLE
            doneBT!!.visibility = View.VISIBLE
            readingDateTV!!.text = date
            if (unit1Name.length > 0) {
                unitOneName!!.text = unit1Name
                valueUnitOne!!.text = unit1Value
                valueUnitOneName!!.text = unit1Units
            }
            if (unit2Name.length == 0) {
                unitTwoLayout!!.visibility = View.GONE
            } else {
                unitTwoLayout!!.visibility = View.VISIBLE
                unitTwoName!!.text = unit2Name
                valueUnitTwo!!.text = unit2Value
                valueUnitTwoName!!.text = unit2Units
            }
            if (unit3Name.length == 0) {
                unitThreeLayout!!.visibility = View.GONE
            } else {
                unitThreeName!!.text = unit3Name
                valueUnitThree!!.text = unit3Value
                valueUnitThreeName!!.text = unit3Units
            }
        }
    }

    private fun displayResults2() {
        try {
            runOnUiThread {
                closeLoading()
                dataRecyclerView.visibility = View.VISIBLE
                sendBT.visibility = View.VISIBLE
                val adapter =
                    title?.let { VitalReadingsAdapter(this@TecheWatchActivity, readingsData, it) }
                dataRecyclerView.adapter = adapter

            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Utils.showCustomToast(e.message!!, this)
        }
    }

    private val mServiceConnection: ServiceConnection =
        object : ServiceConnection {
            override fun onServiceConnected(
                    name: ComponentName,
                    service: IBinder
            ) {
                mService = (service as MokoService.LocalBinder).getService()
                // 注册广播接收器
                val filter = IntentFilter()
                filter.addAction(MokoConstants.ACTION_CONN_STATUS_DISCONNECTED)
                filter.addAction(MokoConstants.ACTION_DISCOVER_SUCCESS)
                //            filter.addAction(MokoConstants.ACTION_CONN_STATUS_DISCONNECTED);
                filter.addAction(MokoConstants.ACTION_DISCOVER_TIMEOUT)
                filter.addAction(MokoConstants.ACTION_ORDER_RESULT)
                filter.addAction(MokoConstants.ACTION_ORDER_TIMEOUT)
                filter.addAction(MokoConstants.ACTION_ORDER_FINISH)
                filter.addAction(MokoConstants.ACTION_CURRENT_DATA)
                filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
                filter.priority = 100
                registerReceiver(mReceiver, filter)

//            MokoSupport.getInstance().sendOrder(new ZReadVersionTask(mService));
            }

            override fun onServiceDisconnected(name: ComponentName) {}
        }

    override fun onDestroy() {
        super.onDestroy()
        try{
            unregisterReceiver(mReceiver)
            unbindService(mServiceConnection)
            stopService(Intent(this, MokoService::class.java))
        }catch (e: Exception)
        {

        }

    }

    val battery: Unit
        get() {
            MokoSupport.getInstance().sendOrder(ZReadBatteryTask(mService))
        }

    private fun displayRetry() {
        try {
            closeLoading()
            retryConnection!!.visibility = View.VISIBLE
            if(type!=null && type.equals("onego"))
            {
                skip!!.visibility = View.VISIBLE
            }
            else {
                addManually!!.visibility = View.VISIBLE
            }
            readingsLayout!!.visibility = View.GONE
            doneBT!!.visibility = View.GONE
            connectedHeading!!.text=vitalNameLabels!!.getString("notconnected")
            connectedHeading!!.setTextColor(resources.getColor(R.color.peachy_pink))

            getBP!!.visibility = View.GONE
            resetDate!!.visibility = View.GONE
            getHR!!.visibility = View.GONE
            getSteps!!.visibility = View.GONE

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.getBP -> {
                showLoading("Fetching data...")
                lastestHeartRate()
            }
            R.id.doneBT -> {
                if(type!=null && type.equals("onego"))
                {
                    showNextDialog(title!!,this@TecheWatchActivity)

                }
                else {
                    val intent = Intent(this@TecheWatchActivity, ThankyouActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
            R.id.addManually -> {
                //sendDeviceData()
                showDialog(title!!, vitalName!!, unit1!!, unit2!!, this@TecheWatchActivity, progressDialog)
            }
            R.id.sendBT -> {
                sendDeviceData()
            }
            R.id.back -> {
                finish()
            }
            R.id.retryConnection -> {
                searchDevices()
            }
            R.id.skip -> {
                val intent = Intent(this@TecheWatchActivity, TecheThermometerActivity::class.java)
                intent.putExtra("title", vitalNameLabels!!.getString("Temperature"))
                intent.putExtra("vitalName", vitalNameLabels!!.getString("Temperature"))
                intent.putExtra("unitName1", vitalUnitLabels!!.getString("Temperature_F"))
                intent.putExtra("unitName2", vitalUnitLabels!!.getString("Temperature_C"))
                intent.putExtra("type","onego")
                startActivity(intent)
                finish()
            }

        }
    }
    private fun showDialog(
            title: String,
            vitalName: String,
            unit1: String,
            unit2: String,
            context: Context,
            progressDialog: CustomProgressDialog
    ) {
        var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.LANGUAGE_API_DATA, "")
        var vitalUnitLabels: JSONObject? = null
        var vitalNameLabels: JSONObject? = null
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.add_vital_dialog)
        val body = dialog.findViewById(R.id.topHeader) as TextView
        val dateText = dialog.findViewById(R.id.dateText) as TextView
        val timeText = dialog.findViewById(R.id.timeText) as TextView
        val cancel = dialog.findViewById(R.id.cancel) as TextView
        val save = dialog.findViewById(R.id.save) as TextView
        val etDate = dialog.findViewById(R.id.etDate) as EditText
        val etTime = dialog.findViewById(R.id.etTime) as EditText
        val entryUnit1 = dialog.findViewById(R.id.entryUnit1) as TextView
        val entryUnit2 = dialog.findViewById(R.id.entryUnit2) as TextView
        val etEntryUnit2 = dialog.findViewById(R.id.etEntryUnit2) as EditText
        val etEntryUnit1 = dialog.findViewById(R.id.etEntryUnit1) as EditText
        val blood_sugar_RadioBT = dialog.findViewById(R.id.blood_sugar_RadioBT) as LinearLayout
        if(subVitalName!=null)
        {
            body.text = vitalNameLabels!!.getString(subVitalName)
        }
        else{
            body.text = title
        }
        save.text=vitalNameLabels!!.getString("save")!!
        cancel.text=vitalNameLabels!!.getString("cancel")!!
        dateText.text=vitalNameLabels!!.getString("date")!!
        timeText.text=vitalNameLabels!!.getString("time")!!
        entryUnit1.text = unit1
        if(unit2.length>0) {
            entryUnit2.text = unit2
        }
        else{
            etEntryUnit2.visibility=View.GONE
            entryUnit2.visibility=View.GONE
        }
        if(vitalName.equals("Blood_Sugar"))
        {
            blood_sugar_RadioBT.visibility=View.VISIBLE
        }
        val cal = Calendar.getInstance()
        val y = cal.get(Calendar.YEAR)
        val m = cal.get(Calendar.MONTH)
        val d = cal.get(Calendar.DAY_OF_MONTH)
        val date: String =
                Utils.getDisplayedDateFormat(d, m, y,context)

        etDate.setText(date)
        val hh = cal.get(Calendar.HOUR_OF_DAY)
        var mm = cal.get(Calendar.MINUTE)
        if(mm.toString().length==2)
        {
            etTime.setText("" + hh + ":" + mm)
            // mm=Integer.parseInt("0$mm")
        }
        else {
            etTime.setText("" + hh + ":" + "0$mm")
        }
        etDate.setOnClickListener(View.OnClickListener {
            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)
            val datepickerdialog: DatePickerDialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                val date: String = Utils.getDisplayedDateFormat(dayOfMonth, monthOfYear, year, context)
                etDate.setText(date)
            }, y, m, d)

            datepickerdialog.show()
        })
        etTime.setOnClickListener {
            val c: Calendar = Calendar.getInstance()
            val hh = c.get(Calendar.HOUR_OF_DAY)
            val mm = c.get(Calendar.MINUTE)
            val timePickerDialog: TimePickerDialog = TimePickerDialog(
                    context,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        etTime.setText("" + hourOfDay + ":" + minute);
                    },
                    hh,
                    mm,
                    true
            )
            timePickerDialog.show()
        }

        val yesBtn = dialog.findViewById(R.id.save) as TextView
        val noBtn = dialog.findViewById(R.id.cancel) as TextView
        yesBtn.setOnClickListener {
            var data1=etEntryUnit1.text.toString()
            var data2=etEntryUnit2.text.toString()
            if(etDate.text.toString().length<1)
            {
                Toast.makeText(context, "Please select date", Toast.LENGTH_SHORT).show()
            }
            if(subVitalName!=null && subVitalName.equals("Heart_Rate")) {
                if(data1.length<1)
                {
                    Toast.makeText(context, "Please enter value", Toast.LENGTH_SHORT).show()
                }
                else if(data1.length>0 ) {
                    syncManually(
                            vitalName,
                            context,
                            progressDialog,
                            data1,
                            data2,
                            dialog
                    )
                }
            }
            else {
                if (data1.length < 1) {
                    Toast.makeText(context, "Please enter value", Toast.LENGTH_SHORT).show()
                } else if (data2.length < 1) {
                    Toast.makeText(context, "Please enter value", Toast.LENGTH_SHORT).show()
                }
                else if(data1.length>0 && data2.length>0) {
                    syncManually(
                            vitalName,
                            context,
                            progressDialog,
                            data1,
                            data2,
                            dialog
                    )
                }
            }
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    private fun syncManually(
            vitalName: String,
            context: Context,
            progressDialog: CustomProgressDialog,
            data1: String,
            data2: String,
            dialog: Dialog
    ) {
        var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val now = Date()
        var dateValue = formatDate.format(now)
        var token=context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                Constants.KEY_TOKEN, null)!!
        if (Utils.isOnline(context)) {
            showLoading(context, "Syncing data...", progressDialog)
            val params: MutableMap<String, String> =
                HashMap()
            params["Device_ID"] = vitalName!!
            params["Login_User_ID"] = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                    Constants.Login_User_ID, null)!!

            params["Comment"] ="Helllo"
            params["Source"] ="Manual"
            params["apporigin"] ="mobile"
            params["Device_Type"] =vitalName!!
            if(subVitalName!=null && subVitalName.equals("Heart_Rate"))
            {
                params["Vital"] =subVitalName!!
                params["Data[" + 0 + "]" + "[Heart_Rate_Beats]"] =data1
            }
            else {
                params["Vital"] =vitalName!!
                params["Data[" + 0 + "]" + "[Stepcount]"] =data1
                params["Data[" + 0 + "]" + "[Stepscalories]"] = data2
            }

            params["Data[" + 0 + "]" + "[DateandTime]"] = dateValue

            val call = NetworkService.apiInterface.postVitalData("Bearer $token", params)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading(context, progressDialog)
                        dialog.dismiss()
                        val intent = Intent(this@TecheWatchActivity, DashboardActivity::class.java)
                        intent.putExtra("from", "bp")
                        startActivity(intent)
                        finish()
                    } else {
                        closeLoading(context, progressDialog)
                        Toast.makeText(
                                context,
                                "Some error. Try again.",
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        }
    }
    private fun showNextDialog(
            title: String,
            context: Context
    ) {
        var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.LANGUAGE_API_DATA, "")
        var vitalNameLabels: JSONObject? = null
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.next_dialog)
        val titleTv = dialog.findViewById(R.id.title) as TextView
        titleTv.text =  "$title reading sent. Next vital is Thermometer."

        val yesBtn = dialog.findViewById(R.id.next) as TextView
        val noBtn = dialog.findViewById(R.id.goToHome) as TextView
        yesBtn.setOnClickListener {
            val intent = Intent(this@TecheWatchActivity, TecheThermometerActivity::class.java)
            intent.putExtra("title", vitalNameLabels!!.getString("Temperature"))
            intent.putExtra("vitalName", vitalNameLabels!!.getString("Temperature"))
            intent.putExtra("unitName1", vitalUnitLabels!!.getString("Temperature_F"))
            intent.putExtra("unitName2", vitalUnitLabels!!.getString("Temperature_C"))
            intent.putExtra("type","onego")
            startActivity(intent)
            finish()
        }
        noBtn.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(this@TecheWatchActivity, DashboardActivity::class.java)
            intent.putExtra("from","thankyou")
            startActivity(intent)
            finish()
    }
        dialog.show()

    }

    private fun closeLoading(
            context: Context,
            progressDialog: CustomProgressDialog
    ) {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(
            context: Context,
            msg: String,
            progressDialog: CustomProgressDialog
    ) {
        progressDialog?.show(context, msg)
    }

    private fun sendDeviceData() {
        val now = Date()
        dateValue = currentFormat.format(now)
        var token=getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                Constants.KEY_TOKEN, null)!!
        if (Utils.isOnline(this)) {
            showLoading("Syncing data...")
            val params: MutableMap<String, String> =
                HashMap()
            params["Device_ID"] = vitalName!!
            params["Login_User_ID"] = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                    Constants.Login_User_ID, null)!!
            params["Vital"] =vitalName!!
            params["Comment"] ="Helllo"
            params["Source"] ="Device"
            params["apporigin"] ="mobile"
            params["Device_Type"] =vitalName!!
            params["Device_Type"] = "";
           /*params["Data[0][DateandTime]"] = datesValue;
           params["Data[0][Stepcount]"] = "10"
           params["Data[0][Stepscalories]"] = "11";
           params["Data[0][Stepsdistance]"] = "0.1";
           params["Data[0][Stepsduration]"] = "13"
           params["VitalHeartRate[0][DateandTime]"] =datesValue
           params["VitalHeartRate[0][Heart_Rate_Beats]"] = "46";
*/
              for (i in readingsData.indices) {
                  val data: ReadingsData = readingsData.get(i)
                  //val heartRatedata: ReadingsData = readingsHeartRateData.get(i)
                  if(data.unit1Name.equals("Steps"))
                  {
                      //params["Data[$i]" + "[DateandTime]"] = data.dateValue+" "+ data.timeValue+":00"
                      params["Data[$i]" + "[DateandTime]"] = dateValue.toString()

                      if (data.unit1Name.length > 0) {
                          params["Data[$i]" + "[Stepcount]"] = data.unit1Value
//                          params["VitalHeartRate[$i]" + "[DateandTime]"] = data.dateValue
//                          params["VitalHeartRate[$i]" + "[Heart_Rate_Beats]"] = "69"
                      }
                      if (data.unit2Name.length  > 0) {
                          params["Data[$i]" + "[Stepsdistance]"] = data.unit2Value
                      }

                      if (data.unit3Name.length > 0) {
                          params["Data[$i]" + "[Stepscalories]"] = data.unit3Value
                      }

                      if (data.unit4Name.length > 0) {
                          params["Data[$i]" + "[Stepsduration]"] = data.unit4Value
                      }
                  }
                  else if(data.unit1Name.equals("Heart Rate")) {
                      params["VitalHeartRate[$i]" + "[DateandTime]"] = data.dateConverted
                     // params["VitalHeartRate[$i]" + "[DateandTime]"] = data.dateValue+" "+ data.timeValue+":00"
                      params["VitalHeartRate[$i]" + "[Heart_Rate_Beats]"] = data.unit1Value
                  }

                  /*if(data.unit1Name.equals("Steps"))
                  {
                      params["Data[$i]" + "[DateandTime]"] = data.dateValue

                      if (data.unit1Name.length > 0) {
                          params["Data[$i]" + "[Stepcount]"] = data.unit1Value
                          params["VitalHeartRate[$i]" + "[DateandTime]"] = data.dateValue
                          params["VitalHeartRate[$i]" + "[Heart_Rate_Beats]"] = "69"
                      }
                      if (data.unit2Name.length  > 0) {
                          params["Data[$i]"+"[Stepsdistance]"] = data.unit2Value
                      }

                      if (data.unit3Name.length > 0) {
                          params["Data[$i]"+"[Stepscalories]"] = data.unit3Value
                      }

                      if (data.unit4Name.length > 0) {
                          params["Data[$i]"+"[Stepsduration]"] = data.unit4Value
                      }
                  }
                  else if(data.unit1Name.equals("Heart Rate")) {
                      params["VitalHeartRate[$i]" + "[DateandTime]"] = data.dateValue
                      params["VitalHeartRate[$i]" + "[Heart_Rate_Beats]"] = data.unit1Value
                  }

                      if (data.unit1Name.length > 0) {
                          params["Data[$i]" + "[Stepcount]"] = data.unit1Value
                      }*/


          }
            Log.i("params", params.toString())
            val call = NetworkService.apiInterface.postVitalData("Bearer $token", params)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        sendBT!!.visibility = View.GONE
                        doneBT!!.visibility = View.VISIBLE
                    } else {
                        closeLoading()
                        sendBT!!.visibility = View.VISIBLE
                        doneBT!!.visibility = View.GONE
                        Toast.makeText(
                                this@TecheWatchActivity,
                                "Some error. Try again.",
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        }
    }
    private fun lastestHeartRate() {
        val now = Date()
        val measurementDate = formatDate2.format(now)
        val calendar: Calendar =
            Utils.strDate2Calendar("2020-01-19 12:30:08", PATTERN_YYYY_MM_DD_HH_MM)
        MokoSupport.getInstance().sendOrder(ZReadHeartRateTask(mService, calendar))

    }

//    @get:OnClick(R.id.getBP)
//    val reading: Unit
//        get() {
//            showLoading("Fetching data...")
//            lastestHeartRate
//        }
//


}