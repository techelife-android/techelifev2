package com.techesolutions.view.activity.nextcloud.files

interface ExcelReaderListener {
    fun onReadExcelCompleted(stringList: List<String?>?)
}