package com.peng.ppscalelibrary.BleManager.Interface;


import com.peng.ppscalelibrary.BleManager.Model.BleConnectFliterModel;

import java.util.UUID;

public interface BleConnectFliterInterface {

    void targetResponse(UUID service, UUID character, BleConnectFliterModel fliterModel);

    void target2Write(UUID service, UUID character, BleConnectFliterModel fliterModel);
}