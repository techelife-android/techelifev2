package com.techesolutions.data.local.techewatch

class ReadingsData {
    var date = ""
    var unit1Name = ""
    var unit1Value = ""
    var unit1Units = ""
    var unit2Name = ""
    var unit2Value = ""
    var unit2Units = ""
    var unit3Name = ""
    var unit3Value = ""
    var unit3Units = ""
    var unit4Name = ""
    var unit4Value = ""
    var unit4Units = ""
    var keyUnit1 = ""
    var keyUnit2 = ""
    var keyUnit3 = ""
    var keyUnit4 = ""
    var dateValue = ""
    var timeValue = ""
    var dateConverted = ""
    var timeConverted = ""

}
