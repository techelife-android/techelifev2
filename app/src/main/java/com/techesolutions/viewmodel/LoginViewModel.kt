package com.techesolutions.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techesolutions.data.remote.model.login.LoginResponse
import com.techesolutions.data.remote.repository.login.LoginRepository

/**
 * Author: Neelam Upadhyay
 * Created: 30/12/2020
 */
class LoginViewModel: ViewModel() {

    var loginLiveData: MutableLiveData<LoginResponse>? = null

    fun getUser(email: String, password: String, apporogin: String, timezone: String, deviceid: String, deviceinfo: String, lang: String, devicetype: String, ipaddress: String): LiveData<LoginResponse>? {
        loginLiveData = LoginRepository.doLogin(email,password,apporogin,timezone,deviceid,deviceinfo,lang,devicetype,ipaddress)
        return loginLiveData
    }
}