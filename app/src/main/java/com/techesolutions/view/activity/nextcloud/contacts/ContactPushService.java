package com.techesolutions.view.activity.nextcloud.contacts;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.DownloadContactsRemoteOperation;
import com.owncloud.android.lib.resources.files.ReadFolderRemoteOperation;
import com.owncloud.android.lib.resources.files.UploadContactRemoteOperation;
import com.techesolutions.R;
import com.techesolutions.utils.Constants;
import com.techesolutions.view.activity.nextcloud.files.filesync.MediaPushService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static com.techesolutions.view.activity.nextcloud.files.FilesActivity.getRootDirPath;


public class ContactPushService extends IntentService implements OnRemoteOperationListener {
    private static OwnCloudClient mClient;
    private static Handler mHandler;
    private Boolean autoSaveEnable;
    private Boolean uploadEnable;
    SharedPreferences shared;
    File vcfFile;
    String remoteUrl;
    String user;
    String CONTACT_DIR;
    public ContactPushService() {
        super(ContactPushService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if(uploadEnable)
        {
            readContacts(getApplicationContext());
        }
        //pushContacts();
        // readContacts(getApplicationContext());
    }
    @Override
    public void onCreate() {
        super.onCreate();
        shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE);
        autoSaveEnable= shared.getBoolean(Constants.KEY_CONTACT_AUTO_SAVE, false);
        uploadEnable=shared.getBoolean(Constants.KEY_CONTACT_UPLOAD, false);
        mHandler = new Handler();
        //Uri serverUri = Uri.parse(getString(R.string.server_base_url));
        String url=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "");
        user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "");
        String pass=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "");
        CONTACT_DIR = "/addressbooks/users/"+user+"/contacts/";
        remoteUrl=url+"/remote.php/dav/addressbooks/users/"+user+"/contacts";
        Log.i("data", url+", "+user+", "+pass);
        Uri serverUri=null;
        if(url.endsWith("/")) {
            serverUri = Uri.parse(url.substring(0, url.length() - 1));
        }
        else{
            serverUri = Uri.parse(url);
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true);
        mClient.setCredentials(
                OwnCloudCredentialsFactory.newBasicCredentials(
                        user,
                        pass
                )
        );
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void readContacts(Context context) {

        ContentValues cvs = new ContentValues();
        Cursor phones = context.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
                null, ContactsContract.Contacts.DISPLAY_NAME + " ASC");

        if (phones.getCount() > 0) {
            while (phones.moveToNext()) {

                String phoneNumber = phones
                        .getString(phones
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                if (!TextUtils.isEmpty(phoneNumber)) {
                    String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                    cvs.put("name", name );
                    cvs.put("phone_no", phoneNumber );
                    TimerTask timertask = new TimerTask() {
                        @Override
                        public void run() {
                            mHandler.post(new Runnable() {
                                public void run() {
                                    createContact(name,phoneNumber);
                                }
                            });
                        }
                    };
                    Timer timer = new Timer();
                    timer.schedule(timertask, 0, 20000);

                   // phones.moveToNext();
                }

            }

        }

    }

    private void createContact(String name, String contact) {
        Log.i("phones","name,"+contact);
        String VCF_DIRECTORY  = "/"+user+"/vcf_teche";
           /* File vcfdirectory = new File(
                    Environment.getExternalStorageDirectory(), VCF_DIRECTORY
            );*/
        File vcfdirectory=new File(getRootDirPath(getApplicationContext()),VCF_DIRECTORY);
            // have the object build the directory structure, if needed.
            if (!vcfdirectory.exists());
            {
                vcfdirectory.mkdirs();
            }
            vcfFile = new File(
                    vcfdirectory, "contact" + contact.trim() + ".vcf");
        if(!vcfFile.exists()) {
            FileWriter fw = null;
            try {
                fw = new FileWriter(vcfFile);
                fw.write("BEGIN:VCARD\r\n");
                fw.write("VERSION:3.0\r\n");
                // fw.write("N:" + p.getSurname() + ";" + p.getFirstName() + "\r\n");
                fw.write("FN:" + name + "\r\n");
                // fw.write("ORG:" + p.getCompanyName() + "\r\n");
                fw.write("TITLE:" + name + "\r\n");
                fw.write("TEL;TYPE=WORK,VOICE:" + contact + "\r\n");
                fw.write("TEL;TYPE=HOME,VOICE:" + contact + "\r\n");
                // fw.write("ADR;TYPE=WORK:;;" + p.getStreet() + ";" + p.getCity() + ";" + p.getState() + ";" + p.getPostcode() + ";" + p.getCountry() + "\r\n");
                fw.write("EMAIL;TYPE=PREF,INTERNET:" + "abc@gmail.com" + "\r\n");
                fw.write("END:VCARD\r\n");
                fw.close();
                Log.i("phones", "Created");
                uploadContact(vcfFile);
                /*TimerTask timertask = new TimerTask() {
                    @Override
                    public void run() {
                        mHandler.post(new Runnable() {
                            public void run() {
                                uploadContact(vcfFile);
                            }
                        });
                    }
                };
                Timer timer = new Timer();
                timer.schedule(timertask, 0, 20000);*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadContact(File vcfFile) {
        //String remotePath = remoteUrl+"/"+ vcfFile.getName();
        String remotePath = "/"+ vcfFile.getName();
        Log.i("remote path contact", remotePath);
        // Get the last modification date of the file from the file system
        long timeStampLong = System.currentTimeMillis() / 1000;
        String timeStamp = String.valueOf(timeStampLong);
        UploadContactRemoteOperation refreshOperation = new  UploadContactRemoteOperation(
                user,
                vcfFile.getAbsolutePath(),
                remotePath,
                "",
                "",
                timeStamp
        );
        refreshOperation.execute(mClient, this, mHandler);
    }

    @Override
    public void onRemoteOperationFinish(RemoteOperation operation, RemoteOperationResult result) {

        if (!result.isSuccess()) {
           /* Toast.makeText(this, R.string.todo_operation_finished_in_fail, Toast.LENGTH_SHORT)
                    .show();*/
        }
           else if(operation instanceof UploadContactRemoteOperation)
        {

            refreshUploaded();
        }
        else if(operation instanceof DownloadContactsRemoteOperation)
        {

        }
    }

    private void refreshUploaded() {
        String filetoDownload="/"+vcfFile.getName();
        Log.i("file",filetoDownload);
        File downFolder = new File(getRootDirPath(getApplicationContext()) + CONTACT_DIR);
        DownloadContactsRemoteOperation downloadOperation = new DownloadContactsRemoteOperation(user,filetoDownload, downFolder.getAbsolutePath());
        downloadOperation.execute(mClient, this, mHandler);
    }
    public static String getRootDirPath(Context context) {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File file = ContextCompat.getExternalFilesDirs(context.getApplicationContext(), null)[0];
            return file.getAbsolutePath();
        } else {
            return context.getApplicationContext().getFilesDir().getAbsolutePath();
        }
    }
}

