package com.techesolutions.data.remote.model.iot

data class IOTDoors(
    val label: String,
    val value: String
)