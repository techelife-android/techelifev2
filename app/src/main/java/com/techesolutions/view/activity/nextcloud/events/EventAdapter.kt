package com.techesolutions.view.activity.nextcloud.events

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import java.util.*

class EventAdapter(
    context: Context,
    val notesList: ArrayList<Events>,
    private var noteclick: EventsActivity
) : RecyclerView.Adapter<EventAdapter.ViewHolder>() {
    var activity:Context = context
    var eventClickListener=noteclick
  /*  val now = Date()
    var dateValue = formatDate.format(now)
*/
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.event_items, parent, false)
        return ViewHolder(v, activity, eventClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: EventAdapter.ViewHolder, position: Int) {
        notesList.get(position)?.let { holder.bindItems(it, activity, eventClickListener, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return notesList?.size!!
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context,
        eventClickListener: NoteClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            notes: Events,
            context: Context,
            eventClickListener: NoteClickListener,
            position: Int
        ) {
            val textViewName = itemView.findViewById(R.id.title) as TextView
            val content = itemView.findViewById(R.id.content) as TextView
           // val image = itemView.findViewById(R.id.activity_icon) as TextView
            val menu = itemView.findViewById(R.id.rlmenu) as RelativeLayout
            var date = itemView.findViewById(R.id.time) as TextView
            var dateTime = itemView.findViewById(R.id.dateTime) as TextView
            var alert = itemView.findViewById(R.id.alert) as ImageView
            if(notes.timer.length>1)
            {
                alert.visibility=View.VISIBLE
            }
            else{
                alert.visibility=View.GONE
            }
            if(notes.title!=null)
                textViewName.text = notes.title

            content.text=notes.description
            var sdate=notes.stratTime.split("T")
            var edate=notes.endTime.split("T")

           // date.text = sdate!![0]
            var stime: String? ="00:01"
            if(notes.stratTime.length>8) {
                stime= Utils.convertEventTimeFormat (notes.stratTime, context)
            }
            var etime: String? ="23:59"
            if(notes.endTime.length>8) {
                etime =
                Utils.convertEventTimeFormat(notes.endTime, context)
            }
            var eventDate: String?=null
            if(notes.stratTime.length>8) {
                eventDate =
                    Utils.convertEventDateFormat(notes.stratTime, context)
            }
            else{
                if(notes.stratTime.length>1) {
                    eventDate =
                        Utils.convertEventOnlyDateFormat(notes.stratTime, context)
                }
                else{
                    eventDate=""
                }
            }
            if(eventDate.length>1) {
                date.text = stime + "-" + etime + " on " + eventDate
            }
            else{
                date.text = stime + "-" + etime
            }
           // dateTime.text = eventDate
            //date.text = ""+miliSec
           // image.setImageResource(R.drawable.notes)
//            itemView?.setOnClickListener {
//                noteClickListener.onNoteLongClick(position, itemView)
//                noteClickListener.onNoteClick(position)
//            }
            itemView.setOnLongClickListener {
                val position = layoutPosition
               // println("LongClick: $p")
               // noteClickListener.onNoteLongClick(position, itemView)

                true // returning true instead of false, works for me
            }

            itemView.setOnClickListener {
                val position = layoutPosition
                val notes: Events = notes
               // noteClickListener.onNoteClick(position)

                // Toast.makeText(context, "Recycle Click$notes  ", Toast.LENGTH_SHORT).show()
            }
            menu.setOnCreateContextMenuListener({ menu, v, menuInfo ->
                val item = menu.add("Edit")
                item.setOnMenuItemClickListener({ i ->
                    eventClickListener.onNoteClick(position)
                    true // Signifies you have consumed this event, so propogation can stop.
                })
                val anotherItem = menu.add("Mark as important")
                anotherItem.setOnMenuItemClickListener({ i ->
                    // doOtherWorkOnItemClick()
                    true
                })
                val itemRemove = menu.add("Remove")
                itemRemove.setOnMenuItemClickListener({ i ->
                    eventClickListener.onNoteDeleteClick(position)
                    true
                })
            })
            menu.setOnClickListener({ view -> view.showContextMenu() })
        }

    }

    fun getItem(notePosition: Int): Events? {
        return notesList.get(notePosition)
    }

}