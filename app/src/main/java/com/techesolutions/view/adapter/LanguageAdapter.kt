package com.techesolutions.view.adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.techesolutions.R
import com.techesolutions.data.remote.model.language.DataModel
import com.techesolutions.view.callbacks.ItemClickListener

class LanguageAdapter(mContext: Context, private val dataSet: ArrayList<*>, private var mCallback: ItemClickListener) :
        ArrayAdapter<Any?>(mContext, R.layout.language_items, dataSet) {
    private class ViewHolder {
        lateinit var txtName: TextView
        lateinit var checkBox: ImageView
    }

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getItem(position: Int): DataModel {
        return dataSet[position] as DataModel
    }

    override fun getView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
    ): View {
        var convertView = convertView
        val viewHolder: ViewHolder
        val result: View
        if (convertView == null) {
            viewHolder = ViewHolder()
            convertView =
                    LayoutInflater.from(parent.context).inflate(R.layout.language_items, parent, false)
            viewHolder.txtName =
                    convertView.findViewById(R.id.txtName)
            viewHolder.checkBox =
                    convertView.findViewById(R.id.select)
            result = convertView
            convertView.tag = viewHolder


        } else {
            viewHolder = convertView.tag as ViewHolder
            result = convertView
        }
        val item: DataModel = getItem(position)
        viewHolder.txtName.text = item.name
        if (item.checked) {
            viewHolder.checkBox.setVisibility(View.VISIBLE)
        } else
            viewHolder.checkBox.setVisibility(View.GONE)
        //viewHolder.checkBox.isChecked = item.checked
        convertView?.setOnClickListener {
            val list = dataSet as List<DataModel>
            for (item in list.indices) {
                list[item].checked = !list[item].checked
                if(list[item].checked) {
                   // if (list[item].short_name.equals("es")) {
                        mCallback.onPositionClick(item)
                    //}
                }
            }


            notifyDataSetChanged()
        }

        return result
    }
}
