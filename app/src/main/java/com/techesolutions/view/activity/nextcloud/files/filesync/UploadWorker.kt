/*
package com.techesolutions.view.activity.nextcloud.files.filesync

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Handler
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.resources.files.UploadFileRemoteOperation
import com.techesolutions.utils.Constants
import java.io.File

class UploadWorker(context: Context, workerParams: WorkerParameters):
    Worker(context, workerParams) {
    override fun doWork(): Result {
        var mClient: OwnCloudClient? = null
        var  mHandler = Handler()
        val shared = getSharedPreferences("TechePrefs", Activity.MODE_PRIVATE)
        val url = shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        val user = shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass = shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", "$url, $user, $pass")
        var serverUri: Uri? = null
        if (url!!.endsWith("/")) {
            serverUri = Uri.parse(url.substring(0, url.length - 1))
        } else {
            serverUri = Uri.parse(url)
        }
      mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user,
                pass
            )
        )
        // Do the work here--in this case, upload the images.
        startUpload();

        // Indicate whether the work finished successfully with the Result
        return Result.success()
    }

    private fun startUpload(fileUri: String?, type: String?) {
        val fileToUpload = File(fileUri)
        val remotePath = folderpath + fileToUpload.name
        val mimeType = type

        // Get the last modification date of the file from the file system
        val timeStampLong = System.currentTimeMillis() / 1000
        val timeStamp = timeStampLong.toString()
        val uploadOperation =
            UploadFileRemoteOperation(fileToUpload.absolutePath, remotePath, mimeType, timeStamp)
        uploadOperation.addDataTransferProgressListener(this) //addDatatransferProgressListener(this);
        uploadOperation.execute(mClient, this, mHandler)
    }

}*/
