package com.peng.ppscalelibrary.BleManager.Model;


public class BleUserModel {
    public int userHeight;
    public int age;
    public BleEnum.BleSex sex;
    public BleEnum.BleUnit unit;
    public int groupNum;

    public BleUserModel(int userHeight, int age, BleEnum.BleSex sex, BleEnum.BleUnit unit, int groupNum) {
        this.userHeight = userHeight;
        this.age = age;
        this.sex = sex;
        this.unit = unit;
        this.groupNum = groupNum;
    }
}
