package com.techesolutions.view.adapter

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.dashboard.Vital
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import android.text.SpannableStringBuilder
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.DashboardActivity
import com.techesolutions.view.activity.vitaldevices.DeviceInstructionManualActivity
import com.techesolutions.view.activity.vitaldevices.HeartRateDeviceSelectionActivity
import com.techesolutions.view.activity.vitaldevices.VitalDetailsActivity
import com.techesolutions.view.activity.vitaldevices.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
class VitalAdapter(
    context:Context,
   val  userList: ArrayList<Vital>
) : RecyclerView.Adapter<VitalAdapter.ViewHolder>() {
    var activity:Context = context
    var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val now = Date()
    var dateValue = formatDate.format(now)

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VitalAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.vital_items, parent, false)
        return ViewHolder(v, activity,dateValue)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: VitalAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position],activity)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context,
        val dateValue: String
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(vital: Vital,context: Context) {
            val add = itemView.findViewById(R.id.add) as ImageView
            val play = itemView.findViewById(R.id.play) as ImageView
            val textViewName = itemView.findViewById(R.id.activityName) as TextView
            val image = itemView.findViewById(R.id.activity_icon) as ImageView
            val data1  = itemView.findViewById(R.id.subtitle1) as TextView
            val data2  = itemView.findViewById(R.id.subtitle2) as TextView
            val dataValue1  = itemView.findViewById(R.id.subtitle1Value) as TextView
            val dataValue2  = itemView.findViewById(R.id.subtitle2Value) as TextView
            val subtitle1Unit  = itemView.findViewById(R.id.subtitle1Unit) as TextView
            val subtitle2Unit  = itemView.findViewById(R.id.subtitle2Unit) as TextView
            val date  = itemView.findViewById(R.id.date) as TextView
            val takenFrom  = itemView.findViewById(R.id.takenFrom) as TextView
            val itemLayput  = itemView.findViewById(R.id.outerLayout) as ConstraintLayout
            val progressDialog = CustomProgressDialog()
            textViewName.text = vital.Title
            data1.text = vital.data1
            data2.text = vital.data2
            val longDescription = SpannableStringBuilder()
            var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.LANGUAGE_API_DATA, "")
            var vitalUnitLabels: JSONObject? = null
            try {
                val jsonObject = JSONObject(languageData)
                val jsonArray = jsonObject.optJSONArray("config")
                for (i in 0 until jsonArray.length()) {
                    val jsonObject = jsonArray.getJSONObject(i)
                    val name = jsonObject.optString("name")
                    if (name.equals("dashboard")) {
                        vitalUnitLabels = jsonObject.getJSONObject("labels")
                    }
                }
            } catch (e: Exception) {
            }
            if(vital.vitalName.equals("Weight")) {
                if (vital.data2val != null && vital.data2val.length > 0)
                {
                    if (vital.data2val.equals("Kg") || vital.data2val.equals("kg")) {
                        dataValue1.text = vital.data1val
                        dataValue2.text = Utils.weightKgToLbs(vital.data1val)
                        subtitle1Unit.text=vitalUnitLabels!!.getString("Kg")
                        subtitle2Unit.text=vitalUnitLabels!!.getString("Lbs")
                    } else {
                        dataValue2.text = vital.data1val
                        dataValue1.text = Utils.LbstoKg(vital.data1val)
                        subtitle1Unit.text=vitalUnitLabels!!.getString("Kg")
                        subtitle2Unit.text=vitalUnitLabels!!.getString("Lbs")
                    }
                }
            }
            else if(vital.Title.contains("Temperature"))
            {

                var data2:String=""
                if(vital.data1val.toString().equals(""))
                {
                    dataValue1.text = "0.0"
                    dataValue2.text = "0.0"
                }
                else {
                    data2=vital.data1val
                    if(vital.data2val.equals("C"))
                    {
                        dataValue2.text = ""+vital.data1val
                        dataValue1.text = ""+ Utils.convertTemperature(data2,true)
                    }
                    else{
                        dataValue2.text = ""+ Utils.convertTemperature(data2,false)
                        dataValue1.text = ""+vital.data1val
                    }

                }

            }
           /*else if(vital.vitalName.equals("Temperature")) {
                if (vital.data2val != null && vital.data2val.length > 0) {
                    if (vital.data2val.equals("F") || vital.data2val.equals("f")) {
                      if(vital.data1val.toString().equals(""))
                        {
                            dataValue1.text = "0.0"
                            dataValue2.text="0.0"
                        }
                        else {
                            dataValue1.text = vital.data1val
                            dataValue2.text = Utils.convertTemperature(vital.data1val, false)
                        }

                    } else {
                        if(vital.data1val.toString().equals(""))
                        {
                            dataValue1.text = "0.0"
                            dataValue2.text="0.0"
                        }
                        else {
                            dataValue2.text = vital.data1val
                            dataValue1.text = Utils.convertTemperature(vital.data1val, true)
                        }

                    }
                }
            }*/
            else if(vital.Title.contains("Pulse"))
            {
                dataValue1.text = vital.data1val
                dataValue2.text = vital.data2val
                subtitle1Unit.text="%"
                subtitle2Unit.text=""
            }
            else if(vital.Title.contains("Heart"))
            {
                dataValue1.text = vital.data1val
                dataValue2.text = vital.data2val
                subtitle1Unit.text=vitalUnitLabels!!.getString("BPM")
                subtitle2Unit.text=""
            }
            else if(vital.Title.contains("Pressure"))
            {
                dataValue1.text = vital.data1val
                dataValue2.text = vital.data2val
                subtitle1Unit.text=vitalUnitLabels!!.getString("bp_Unit")
                subtitle2Unit.text=vitalUnitLabels!!.getString("bp_Unit")
            }
            else if(vital.Title.contains("Sugar"))
            {
                dataValue1.text = vital.data1val
                dataValue2.text = vital.data2val
                subtitle1Unit.text=vitalUnitLabels!!.getString("Mg_dl")
                subtitle2Unit.text=""
            }
            else{
                dataValue1.text = ""+vital.data1val
                dataValue2.text = ""+ vital.data2val
            }
if(vital.dateandtime.toString().equals(""))
{
    date.text=""
}
            else{
    date.text = Utils.convertVitalFormat(
            ""+vital.dateandtime,
            context
    )+context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.KEY_USER_TIME_ZONE, "")

}
             takenFrom.text = vital.takenfrom

            if(vital.vitalName.equals("Activities"))
            image.setImageResource(R.drawable.activity_selector)

            if(vital.vitalName.equals("Blood_Pressure"))
            image.setImageResource(R.drawable.bp_image_selector)

            if(vital.vitalName.equals("Temperature"))
            image.setImageResource(R.drawable.tempreture_selector)

            if(vital.vitalName.equals("Pulse_Oximetry"))
            image.setImageResource(R.drawable.po_selector)

            if(vital.vitalName.equals("Heart_Rate"))
                image.setImageResource(R.drawable.heart_selector)

            if(vital.vitalName.equals("Weight"))
            image.setImageResource(R.drawable.ws_selector)

            if(vital.vitalName.equals("Blood_Sugar"))
            image.setImageResource(R.drawable.blood_sugar_selector)
            play.setOnClickListener(View.OnClickListener {
                val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                if (mBluetoothAdapter == null) {
                    // Device does not support Bluetooth
                } else if (!mBluetoothAdapter.isEnabled) {
                    Toast.makeText(context,"Bluetooth is off. Please enable your mobile bluetooth",Toast.LENGTH_SHORT).show()
                } else {

                if(vital.vitalName.equals("Heart_Rate")) {
                    val intent =
                        Intent(context, HeartRateDeviceSelectionActivity::class.java)
                    intent.putExtra("title", vital.Title)
                    intent.putExtra("vitalName", vital.vitalName)
                    context.startActivity(intent)
                }
                else{
                    if(context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getBoolean(Constants.KEY_DONT_SHOW, false)) {
                        if (vital.vitalName!!.equals("Temperature")) {
                            var  intent = Intent(context, TecheThermometerActivity::class.java)
                            intent.putExtra("title", vital.Title)
                            intent.putExtra("vitalName", vital.vitalName)
                            intent.putExtra("unitName1", vital.data1)
                            intent.putExtra("unitName2", vital.data2)
                            context.startActivity(intent)
                        }
                        else if(vital.vitalName!!.equals("Activities"))
                        {
                            var intent = Intent(context, TecheWatchActivity::class.java)
                            intent.putExtra("title", vital.Title)
                            intent.putExtra("vitalName", vital.vitalName)
                            intent.putExtra("unitName1", vital.data1)
                            intent.putExtra("unitName2", vital.data2)
                            context.startActivity(intent)
                        }
                        else if(vital.vitalName!!.equals("Pulse_Oximetry"))
                        {
                            var   intent = Intent(context, TechePOActivity::class.java)
                            intent.putExtra("title", vital.Title)
                            intent.putExtra("vitalName", vital.vitalName)
                            intent.putExtra("unitName1", vital.data1)
                            intent.putExtra("unitName2", vital.data2)
                            context.startActivity(intent)
                        }
                        else if(vital.vitalName!!.equals("Blood_Pressure"))
                        {
                            var   intent = Intent(context, TecheBPActivity::class.java)
                            intent.putExtra("title", vital.Title)
                            intent.putExtra("vitalName", vital.vitalName)
                            intent.putExtra("unitName1", vital.data1)
                            intent.putExtra("unitName2", vital.data2)
                            context.startActivity(intent)
                        }
                        else if(vital.vitalName!!.equals("Blood_Sugar"))
                        {
                            var   intent = Intent(context, TecheBSActivity::class.java)
                            intent.putExtra("title", vital.Title)
                            intent.putExtra("vitalName", vital.vitalName)
                            intent.putExtra("unitName1", vital.data1)
                            intent.putExtra("unitName2", vital.data2)
                            context.startActivity(intent)
                        }
                        else if(vital.vitalName!!.equals("Weight")){
                            var  i = Intent(context, TecheWSActivity::class.java)
                            i.putExtra("title", vital.Title)
                            i.putExtra("vitalName", vital.vitalName)
                            i.putExtra("unitName1", vital.data1)
                            i.putExtra("unitName2", vital.data2)
                            context.startActivity(i)
                        }


                    }
                    else{
                        val intent =
                                Intent(context, DeviceInstructionManualActivity::class.java)
                        intent.putExtra("title", vital.Title)
                        intent.putExtra("vitalName", vital.vitalName)
                        intent.putExtra("unitName1", vital.data1)
                        intent.putExtra("unitName2", vital.data2)
                        context.startActivity(intent)
                    }
                }
            }
            })
            add.setOnClickListener(View.OnClickListener {
                showDialog(vital.Title,vital.vitalName,vital.data1,vital.data2,context,progressDialog)
            })
            itemLayput.setOnClickListener(View.OnClickListener {
                val intent =
                    Intent(context, VitalDetailsActivity::class.java)
                intent.putExtra("title", vital.Title)
                intent.putExtra("vitalName", vital.vitalName)
                intent.putExtra("unitName1", vital.data1)
                intent.putExtra("unitName2", vital.data2)
                context.startActivity(intent)
            })
        }

        private fun showDialog(
            title: String,
            vitalName: String,
            unit1: String,
            unit2: String,
            context: Context,
            progressDialog: CustomProgressDialog
        ) {
            var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.LANGUAGE_API_DATA, "")
            var vitalUnitLabels: JSONObject? = null
            var vitalNameLabels: JSONObject? = null
            try {
                val jsonObject = JSONObject(languageData)
                val jsonArray = jsonObject.optJSONArray("config")
                for (i in 0 until jsonArray.length()) {
                    val jsonObject = jsonArray.getJSONObject(i)
                    val name = jsonObject.optString("name")
                    if (name.equals("dashboard")) {
                        vitalUnitLabels = jsonObject.getJSONObject("labels")
                    }
                    if (name.equals("vitals")) {
                        vitalNameLabels = jsonObject.getJSONObject("labels")
                    }
                }
            } catch (e: Exception) {
            }
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.add_vital_dialog)
            val body = dialog.findViewById(R.id.topHeader) as TextView
            val dateText = dialog.findViewById(R.id.dateText) as TextView
            val timeText = dialog.findViewById(R.id.timeText) as TextView
            val cancel = dialog.findViewById(R.id.cancel) as TextView
            val save = dialog.findViewById(R.id.save) as TextView
            val etDate = dialog.findViewById(R.id.etDate) as EditText
            val etTime = dialog.findViewById(R.id.etTime) as EditText
            val entryUnit1 = dialog.findViewById(R.id.entryUnit1) as TextView
            val entryUnit2 = dialog.findViewById(R.id.entryUnit2) as TextView
            val etEntryUnit2 = dialog.findViewById(R.id.etEntryUnit2) as EditText
            val etEntryUnit1 = dialog.findViewById(R.id.etEntryUnit1) as EditText
            val etNote = dialog.findViewById(R.id.etNote) as EditText
            val blood_sugar_RadioBT = dialog.findViewById(R.id.blood_sugar_RadioBT) as LinearLayout
            val contextGroup = dialog.findViewById(R.id.contextGroup) as RadioGroup
            var contextChecked: String="Random"
            contextGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->
                val checkedRadioButton = contextGroup?.findViewById(contextGroup.checkedRadioButtonId) as? RadioButton
                checkedRadioButton?.let {

                    if (checkedRadioButton.isChecked)
                        //Toast.makeText(context, "RadioButton: ${checkedRadioButton?.text}", Toast.LENGTH_LONG).show()
                    contextChecked=checkedRadioButton?.text.toString()
                }

//                contextChecked = checkedRadioButton?.text
//                Toast.makeText(context,contextChecked,Toast.LENGTH_SHORT).show()
            })
            val cal = Calendar.getInstance()
            val y = cal.get(Calendar.YEAR)
            val m = cal.get(Calendar.MONTH)
            val d = cal.get(Calendar.DAY_OF_MONTH)
            val date: String =
                    Utils.getDisplayedDateFormat(d, m, y,context)

            etDate.setText(date)
            val hh = cal.get(Calendar.HOUR_OF_DAY)
            var mm = cal.get(Calendar.MINUTE)
            if(mm.toString().length==2)
            {
                etTime.setText("" + hh + ":" + mm)
               // mm=Integer.parseInt("0$mm")
            }
            else {
                etTime.setText("" + hh + ":" + "0$mm")
            }
            body.text = title
            save.text=vitalNameLabels!!.getString("save")!!
            cancel.text=vitalNameLabels!!.getString("cancel")!!
            dateText.text=vitalNameLabels!!.getString("date")!!
            timeText.text=vitalNameLabels!!.getString("time")!!
            entryUnit1.text = unit1
            if(unit2.length>0) {
                entryUnit2.text = unit2
            }
            else{
                etEntryUnit2.visibility=View.GONE
                entryUnit2.visibility=View.GONE
            }
            if(vitalName.equals("Blood_Sugar"))
            {
                blood_sugar_RadioBT.visibility=View.VISIBLE
            }
            etDate.setOnClickListener(View.OnClickListener {
                val cal = Calendar.getInstance()
                val y = cal.get(Calendar.YEAR)
                val m = cal.get(Calendar.MONTH)
                val d = cal.get(Calendar.DAY_OF_MONTH)


                val datepickerdialog:DatePickerDialog = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                    // Display Selected date in textbox
                   // etDate.setText("" + dayOfMonth + "/" +MONTH[monthOfYear] + "/" + year)
                    val date: String =
                        Utils.getDisplayedDateFormat(dayOfMonth, monthOfYear, year,context)

                      etDate.setText(date)
                }, y, m, d)

                datepickerdialog.show()
            })
            etTime.setOnClickListener {
                val c: Calendar = Calendar.getInstance()
                val hh = c.get(Calendar.HOUR_OF_DAY)
                val mm = c.get(Calendar.MINUTE)
                val timePickerDialog: TimePickerDialog = TimePickerDialog(
                    context,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        etTime.setText("" + hourOfDay + ":" + minute);
                    },
                    hh,
                    mm,
                    true
                )
                timePickerDialog.show()
            }

            val yesBtn = dialog.findViewById(R.id.save) as TextView
            val noBtn = dialog.findViewById(R.id.cancel) as TextView
            yesBtn.setOnClickListener {
                var data1=etEntryUnit1.text.toString()
                var data2=etEntryUnit2.text.toString()
                var notes=etNote.text.toString()
                if(etDate.text.toString().length<1)
                {
                    Toast.makeText(context,"Please select date",Toast.LENGTH_SHORT).show()
                }
               else if(etTime.text.toString().length<1)
                {
                    Toast.makeText(context,"Please select time",Toast.LENGTH_SHORT).show()
                }

                if(vitalName.equals("Activities")||vitalName.equals("Blood_Pressure")) {
                    if ( data1.length<1) {
                        Toast.makeText(context, "Please enter value", Toast.LENGTH_SHORT).show()
                    }
                    else if(data2.length < 1)
                    {
                        Toast.makeText(context, "Please enter value", Toast.LENGTH_SHORT).show()
                    }
                    else if (data1.length > 0 && data2.length > 0) {
                        sendDeviceData(
                            vitalName,
                            context,
                            progressDialog,
                            data1,
                            data2,
                            notes,
                            contextChecked,
                            dialog
                        )
                    }
                }

                else if(vitalName.equals("Temperature")||vitalName.equals("Weight")) {
                    if (data1.length < 1 && data2.length < 1) {
                        Toast.makeText(context, "Please enter value", Toast.LENGTH_SHORT).show()
                    } else if (data1.length > 1 || data2.length > 1) {
                        sendDeviceData(
                            vitalName,
                            context,
                            progressDialog,
                            data1,
                            data2,
                            notes,
                            contextChecked,
                            dialog
                        )
                    }
                }
                else {
                    sendDeviceData(
                        vitalName,
                        context,
                        progressDialog,
                        data1,
                        "",
                        notes,
                        contextChecked,
                        dialog
                    )
                }
            }
            noBtn.setOnClickListener { dialog.dismiss() }
            dialog.show()

        }

        private fun sendDeviceData(
            vitalName: String,
            context: Context,
            progressDialog: CustomProgressDialog,
            data1: String,
            data2: String,
            notes:String,
            contextChecked:String,
            dialog: Dialog
        ) {

            var token=context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                Constants.KEY_TOKEN, null)!!
            if (Utils.isOnline(context)) {
                showLoading(context,"Syncing data...",progressDialog)
                val params: MutableMap<String, String> =
                    HashMap()
                params["Device_ID"] = vitalName!!
                params["Login_User_ID"] = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                    Constants.Login_User_ID, null)!!
                params["Vital"] =vitalName!!
                params["Comment"] ="Helllo"
                params["Source"] ="Manual"
                params["apporigin"] ="mobile"
                params["Device_Type"] =vitalName!!
                if(vitalName.equals("Activities"))
                {
                    params["Data[" + 0 + "]"+"[Stepcount]"] =data1
                    params["Data[" + 0 + "]"+"[Stepscalories]"] = data2
                }
                else  if(vitalName.equals("Blood_Pressure"))
                {
                    params["Data[" + 0 + "]"+"[Systolic_bp]"] =data1
                    params["Data[" + 0 + "]"+"[Diastolic_bp]"] = data2
                }
                else  if(vitalName.equals("Temperature"))
                {
                    if(data1.length>0)
                    {
                        params["Data[" + 0 + "]"+"[Temperature]"] =data1
                        params["Data[" + 0 + "]"+"[Temperature_Unit]"] = "F"
                    }
                    else{
                        params["Data[" + 0 + "]"+"[Temperature]"] =data2
                        params["Data[" + 0 + "]"+"[Temperature_Unit]"] = "C"
                    }


                }
                else  if(vitalName.equals("Pulse_Oximetry"))
                {
                    params["Data[" + 0 + "]"+"[O2_Sat]"] =data1
                }
                else  if(vitalName.equals("Heart_Rate"))
                {
                    params["Data[" + 0 + "]"+"[Heart_Rate_Beats]"] =data1
                }
                else  if(vitalName.equals("Weight"))
                {
                    if(data1.length>0)
                    {
                        params["Data[" + 0 + "]"+"[Weight]"] =data1
                        params["Data[" + 0 + "]"+"[Weight_Unit]"] = "Kg"
                    }
                    else{
                        params["Data[" + 0 + "]"+"[Weight]"] =data2
                        params["Data[" + 0 + "]"+"[Weight_Unit]"] = "Lbs"
                    }

                }
                else  if(vitalName.equals("Blood_Sugar"))
                {
                    params["Data[0][Context]"] = contextChecked;
                    params["Data[0][Notes]"] = notes!!
                    params["Data[0][Sugar]"] = data1;
                }
                params["Data[" + 0 + "]"+"[DateandTime]"] = dateValue

                val call = NetworkService.apiInterface.postVitalData("Bearer $token", params )

                call.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        Log.v("DEBUG : ", t.message.toString())
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        val stringResponse = response.body()?.string()
                        val jsonObj = JSONObject(stringResponse)
                        val success = jsonObj!!.getString("success")
                        if (success != null && success.toString().equals("true")) {
                            closeLoading(context,progressDialog)
                            dialog.dismiss()
                            val intent = Intent( context, DashboardActivity::class.java)
                            intent.putExtra("from","adapter")
                            context.startActivity(intent)
                           } else {
                            closeLoading(context,progressDialog)
                            Toast.makeText(
                                context,
                                "Some error. Try again.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                })

            }
        }

        private fun closeLoading(
            context: Context,
            progressDialog: CustomProgressDialog
        ) {
            if (progressDialog != null && progressDialog?.dialog.isShowing())
                progressDialog?.dialog.dismiss()    }

        private fun showLoading(
            context: Context,
            msg: String,
            progressDialog: CustomProgressDialog
        ) {
            progressDialog?.show(context, msg)
        }
    }

}