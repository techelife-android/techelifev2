package com.techesolutions.data.remote.model.support

data class TicketSupportData(
    val id: Int,
    val ticket_no: String,
    val subject: String?,
    val description: String,
    val status: String?,
    val from_id: String?,
    val to_id: String?,
    val created_date: String,
    val updated_date: String,
    val from_name: String,
    val from_usertype: String,
    val to_name: String
)