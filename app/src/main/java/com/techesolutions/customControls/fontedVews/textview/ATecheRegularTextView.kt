package com.techesolutions.customControls.fontedVews.textview

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.techesolutions.utils.Utils
/**
 * Created by Neelam on 20-12-2020.
 */
class ATecheRegularTextView : AppCompatTextView {
    constructor(context: Context) : super(context) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        if (Utils.aTecheRegularFont != null) {
            typeface = Utils.aTecheRegularFont
        } else {
            Utils.setDimensions(context)
            typeface = Utils.aTecheRegularFont
            println("Oxigen ligh tnot loaded")
        }
    }
}
