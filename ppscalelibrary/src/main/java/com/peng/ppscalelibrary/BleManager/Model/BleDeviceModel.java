package com.peng.ppscalelibrary.BleManager.Model;

public class BleDeviceModel {
    String deviceMac;
    String deviceName;

    public String getDeviceMac() {
        return deviceMac;
    }

    public void setDeviceMac(String deviceMac) {
        this.deviceMac = deviceMac;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public BleDeviceModel(String deviceMac, String deviceName) {
        this.deviceMac = deviceMac;
        this.deviceName = deviceName;
    }
}
