package com.techesolutions.services

import com.google.gson.JsonObject
import com.techesolutions.data.remote.model.forgetpassword.CodeVerificationResponse
import com.techesolutions.data.remote.model.forgetpassword.Forgot
import com.techesolutions.data.remote.model.forgetpassword.ResetPasswordResponse
import com.techesolutions.data.remote.model.login.LoginResponse
import com.techesolutions.utils.Constants
import com.techesolutions.data.remote.model.nextclouddashboard.CameraDataResponse
import com.techesolutions.data.remote.model.nextclouddashboard.MasterModel
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * The APIService interface which will contain the semantics of all the REST calls.
 * Author: Neelam Upadhyay
 * Created: 28/12/2020
 */
interface ApiService {

    @GET(Constants.LANGUAGE_URL)
    fun setLanguage(
        @Query(Constants.LANGUAGE) lang: String?,
        @Query(Constants.APPORIGIN) apporigin: String?
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST(Constants.TIMEZONE_URL)
    fun setTimezone(
        @Header("Authorization") authorization: String?,
        @Field("apporigin") apporigin: String,
        @Field(
            "id"
        ) id: String,
        @Field("timezone") timezone: String
    ): Call<ResponseBody>

//    @FormUrlEncoded
//    @POST(Constants.LOGIN_URL)
//    fun doLogin(@Field("email") email: String, @Field("password") password: String): Call<LoginResponse>

    @FormUrlEncoded
    @POST(Constants.LOGIN_URL)
    fun doLogin(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("apporigin") apporigin: String,
        @Field(
            "timezone"
        ) timezone: String,
        @Field("deviceid") deviceid: String,
        @Field("deviceinfo") deviceinfo: String,
        @Field(
            "devicelang"
        ) devicelang: String,
        @Field("devicetype") devicetype: String,
        @Field("ipaddress") ipaddress: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST(Constants.FORGOT_PASSWORD_URL)
    fun forgotPassword(@Field("email") email: String, @Field("apporigin") app: String): Call<Forgot>

    @FormUrlEncoded
    @POST(Constants.RESET_PASSWORD_URL)
    fun resetPassword(
        @Field("hashcode") hashcode: String,
        @Field("apporigin") app: String,
        @Field("new") new: String,
        @Field(
            "new_confirm"
        ) new_confirm: String
    ): Call<ResetPasswordResponse>

    @FormUrlEncoded
    @POST(Constants.OTP_SUBMIT_URL)
    fun verifyCode(
        @Field("verifycode") verifycode: Int,
        @Field("hashcode") hashcode: String,
        @Field("apporigin") app: String
    ): Call<CodeVerificationResponse>

    @GET(Constants.GET_VITALS_URL)
    fun getVilatDeviceList(
        @Header("Authorization") authorization: String?,
        @Query("Login_User_ID") id: String?,
        @Query(
            Constants.APPORIGIN
        ) apporigin: String
    ): Call<ResponseBody>

    @GET(Constants.GET_VITALS_DETAILS)
    fun getVilatDetailsToday(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query(
            "Login_User_ID"
        ) id: String?,
        @Query("vital") vital: String?,
        @Query("filter_type") filter_type: String?,
        @Query(
            "year"
        ) year: String?,
        @Query("current_date") current_date: String?
    ): Call<ResponseBody>

    @GET(Constants.GET_VITALS_DETAILS)
    fun getVilatDetails(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query(
            "Login_User_ID"
        ) id: String?,
        @Query("vital") vital: String?,
        @Query("filter_type") filter_type: String?,
        @Query(
            "year"
        ) year: String?,
        @Query("current_date") current_date: String?,
        @Query("from_date") from_date: String?,
        @Query(
            "to_date"
        ) to_date: String?
    ): Call<ResponseBody>

    @Multipart
    @POST(Constants.SYNC_VITALS_READINGS)
    fun syncVitalData(
        @Header("Authorization") authorization: String?,
        @Part("Device_ID") Device_ID: RequestBody?,
        @Part("Login_User_ID") Login_User_ID: RequestBody?,
        @Part("Vital") Vital: RequestBody?,
        @Part("Comment") Comment: RequestBody?,
        @Part("Source") Source: RequestBody?,
        @Part("apporigin") apporigin: RequestBody?,
        @Part("Device_Type") Device_Type: RequestBody?,
        @Part("Data") Data: RequestBody?
    ): Call<ResponseBody>

/*
    @FormUrlEncoded
    @POST(Constants.SYNC_VITALS_READINGS)
    fun postVitalData(
        @Header("Authorization") authorization: String?,
        @Field("Device_ID") Device_ID: MutableMap<String, String>,
        @Field("Login_User_ID") Login_User_ID: String?,
        @Field("Vital") Vital: String?,
        @Field("Comment") Comment: String?,
        @Field("Source") Source: String?,
        @Field("apporigin") apporigin: String?,
        @Field("Device_Type") Device_Type: String?,
        @Field ("Data") Data: String?
    ): Call<ResponseBody>
*/
    @FormUrlEncoded
    @POST(Constants.SYNC_VITALS_READINGS)
    fun postVitalData(
        @Header("Authorization") authorization: String?,
        @FieldMap params: MutableMap<String, String>
    ): Call<ResponseBody>

    /////Get Notes API****************************************************************
    @GET(Constants.GET_NOTES_URL)
    fun getNotes(): Call<ResponseBody>

    /////Create Notes API****************************************************************
    @POST(Constants.CREATE_NOTES_URL)
    fun createNotes(   @Body bean: JsonObject?): Call<ResponseBody>

    /////Edit Notes API****************************************************************
    @PUT(Constants.EDIT_NOTES_URL+"{Id}")
    fun editNotes(@Path("Id") noteId: Long?,  @Body bean: JsonObject?): Call<ResponseBody>


    /////Delete Notes API****************************************************************
    @DELETE(Constants.DELETE_NOTES_URL+"{Id}")
    fun deleteNote(@Path("Id") noteId: Long? ): Call<ResponseBody>

    @GET(Constants.ROOM_LIST_URL)
    fun getRoomAndDeviceList_(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("type") type: String
    ): Call<MasterModel>

   @GET(Constants.ROOM_LIST_URL)
   fun getRoomAndDeviceList(
       @Header("Authorization") authorization: String?,
       @Query(Constants.APPORIGIN) apporigin: String
   ): Call<ResponseBody>

    @GET(Constants.CAMERA_DATA_URL)
    fun getCameraData(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("room") room: String
    ): Call<CameraDataResponse>

    @GET(Constants.IOT_DATA_URL)
    fun getIotReading(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("room") room: String,
        @Query("filter_type") filter_type: String
    ): Call<ResponseBody>


    @GET(Constants.IOT_DATA_URL)
    fun getIotReadingToday(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("room") room: String,
        @Query("filter_type") filter_type: String,
        @Query("current_date") date: String,
        @Query("page") page: Int,
        @Query("size") size: Int
    ): Call<ResponseBody>

    @GET(Constants.IOT_DATA_URL)
    fun getIotReadingWeekMonth(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("room") room: String,
        @Query("filter_type") filter_type: String,
        @Query("from_date") from_date: String,
        @Query("to_date") to_date: String,
        @Query("page") page: Int,
        @Query("size") size: Int
    ): Call<ResponseBody>

    @GET(Constants.IOT_DATA_URL)
    fun getIotReadingYear(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("room") room: String,
        @Query("filter_type") filter_type: String,
        @Query("year") year: String,
        @Query("page") page: Int,
        @Query("size") size: Int
    ): Call<ResponseBody>


    @GET(Constants.DOOR_BY_ROOM_URL)
    fun getRoomsList(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("room") room: String

    ): Call<ResponseBody>

    @GET(Constants.DOOR_WISE_DATA_URL)
    fun getDoorwiseData(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("room") room: String,
        @Query("door") door: String

    ): Call<ResponseBody>

    //USer Saed Info
    @GET(Constants.USER_WEB_INFO_URL)
    fun getUserWebInfo(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("type") type: String,
        @Query("field") field: String,
        @Query("unique_key") unique_key: String,
        @Query("page") page: String,
        @Query("size") size: String,
        @Query("filterText") filterText: String

    ): Call<ResponseBody>

    @GET("api/saved/credentials/delete/{Id}")
    fun deleteUserInfo(
        @Path("Id")  Id: String,
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("unique_key") type: String

    ): Call<ResponseBody>

/*    /////Delete Notes API****************************************************************
    @DELETE(Constants.DELETE_NOTES_URL+"{Id}")
    fun deleteNote(@Path("Id") noteId: Long? ): Call<ResponseBody>*/


    //Assigned device list
    @GET(Constants.ASSIGEND_DEVICE_LIST_URL)
    fun getAssignedDevices(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("device_type") type: String,
        @Query("page") page: String,
        @Query("size") size: String

    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST(Constants.ADD_USER_INFO_URL)
    fun addUserWebInfo(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Field("type") type: String,
        @Field("unique_key") unique_key: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("name") name: String,
        @Field("url") url : String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/saved/credentials/update/{Id}")
    fun editUserWebInfo(
        @Path("Id")  Id: String,
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Field("type") type: String,
        @Field("unique_key") unique_key: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("name") name: String,
        @Field("url") url : String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST(Constants.ADD_USER_INFO_URL)
    fun addUserCardInfo(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Field("type") type: String,
        @Field("unique_key") unique_key: String,
        @Field("name_on_card") name_on_card: String,
        @Field("card_no") card_no: String,
        @Field("expiry") expiry : String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("api/saved/credentials/update/{Id}")
    fun editUserCardInfo(
        @Path("Id")  Id: String,
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Field("type") type: String,
        @Field("unique_key") unique_key: String,
        @Field("name_on_card") name_on_card: String,
        @Field("card_no") card_no: String,
        @Field("expiry") expiry : String
    ): Call<ResponseBody>

    //Support
    //Get ticket list
    @GET(Constants.TICKET_LIST_URL)
    fun getTickets(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String
    ): Call<ResponseBody>

    //Get comment list
    @GET(Constants.COMMENT_LIST_URL)
    fun getComments(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Query("ticket_id") ticket_id: Int
    ): Call<ResponseBody>

    //Add ticket
    @FormUrlEncoded
    @POST(Constants.ADD_TICKET_URL)
    fun addTickets(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Field("subject") subject: String,
        @Field("description") description: String,
        @Field("status") status : String
    ): Call<ResponseBody>

    //Add comment
    @FormUrlEncoded
    @POST(Constants.ADD_COMMENT_URL)
    fun addComment(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Field("message") subject: String,
        @Field("ticket_id") ticket_id: Int
    ): Call<ResponseBody>

    //Camera navigation
    @FormUrlEncoded
    @POST(Constants.CAMERA_NAVIGATION_URL)
    fun cameraNavigation(
        @Header("Authorization") authorization: String?,
        @Query(Constants.APPORIGIN) apporigin: String,
        @Field("cmd") cmd: String,
        @Field("id") id: String,
        @Field("newcmd") newcmd: Int
    ): Call<ResponseBody>

}