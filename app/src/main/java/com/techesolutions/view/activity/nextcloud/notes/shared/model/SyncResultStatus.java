package com.techesolutions.view.activity.nextcloud.notes.shared.model;

public class SyncResultStatus {
    public boolean pullSuccessful = true;
    public boolean pushSuccessful = true;
}
