package com.techesolutions.ws.util.model

import android.os.Parcel
import android.os.Parcelable
import com.peng.ppscalelibrary.BleManager.Model.LFPeopleGeneral


class BodyDataModel protected constructor(`in`: Parcel?) : LFPeopleGeneral(),
    Parcelable {
    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(getLfFatLevel())
        dest.writeDouble(getLfWeightKg())
        dest.writeDouble(getLfBodyfatPercentage())
    }

    companion object {
        val CREATOR: Parcelable.Creator<BodyDataModel?> =
            object : Parcelable.Creator<BodyDataModel?> {
                override fun createFromParcel(`in`: Parcel): BodyDataModel? {
                    return BodyDataModel(`in`)
                }

                override fun newArray(size: Int): Array<BodyDataModel?> {
                    return arrayOfNulls(size)
                }
            }
    }
}
