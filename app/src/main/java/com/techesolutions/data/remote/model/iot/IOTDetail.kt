package com.techesolutions.data.remote.model.iot

data class IOTDetail(
    val date: String,
    val Title: String,
    val data1val: String,
    val data2val: String,
    val data1: String,
    val data2: String
)