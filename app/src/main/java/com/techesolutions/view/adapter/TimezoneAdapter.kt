package com.techesolutions.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.techesolutions.R
import com.techesolutions.data.remote.model.timezones.TimeZoneModel
import com.techesolutions.view.callbacks.ItemClickListener

class TimezoneAdapter(val context: Context, var dataSource: List<TimeZoneModel>, private var mCallback: ItemClickListener) : BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(R.layout.language_items, parent, false)
            vh = ItemHolder(view)
            view?.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }
        vh.label.text = dataSource.get(position).name
       /* convertView?.setOnClickListener {
            val list = dataSource as List<TimeZoneModel>
            for (item in list.indices) {
                list[item].checked = !list[item].checked
                if (list[item].checked) {
                    // if (list[item].short_name.equals("es")) {
                    mCallback.onPositionClick(item)
                    //}
                }
            }
        }*/

//        val id = context.resources.getIdentifier(dataSource.get(position).name, "drawable", context.packageName)
//        vh.img.setBackgroundResource(id)

        return view
    }

    override fun getItem(position: Int): Any? {
        return dataSource[position];
    }

    override fun getCount(): Int {
        return dataSource.size;
    }

    override fun getItemId(position: Int): Long {
        return position.toLong();
    }

    private class ItemHolder(row: View?) {
        val label: TextView
        val img: ImageView

        init {
            label = row?.findViewById(R.id.txtName) as TextView
            img = row?.findViewById(R.id.select) as ImageView
        }
    }

}
