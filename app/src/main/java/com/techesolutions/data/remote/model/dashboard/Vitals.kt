//package com.techesolutions.data.remote.model.dashboard
//
//import android.content.res.Resources
//import com.techesolutions.R
//
//fun vitalList(resources: Resources): List<Vital> {
//    return listOf(
//        Vital(
//            id = 1,
//            name = resources.getString(R.string.blood_pressure),
//            image = R.drawable.ic_blood_pressure_unselected,
//            readingname1=resources.getString(R.string.bp_reading1),
//            readingname2=resources.getString(R.string.bp_reading2),
//            reading1="100mg",
//            reading2="130mg",
//            description = resources.getString(R.string.taken_from)
//        ),
//        Vital(
//            id = 2,
//            name = resources.getString(R.string.blood_pressure),
//            image = R.drawable.ic_blood_pressure_unselected,
//            readingname1=resources.getString(R.string.bp_reading1),
//            readingname2=resources.getString(R.string.bp_reading2),
//            reading1="100mg",
//            reading2="130mg",
//            description = resources.getString(R.string.taken_from)
//        ),
//        Vital(
//            id = 3,
//            name = resources.getString(R.string.blood_pressure),
//            image = R.drawable.ic_blood_pressure_unselected,
//            readingname1=resources.getString(R.string.bp_reading1),
//            readingname2=resources.getString(R.string.bp_reading2),
//            reading1="100mg",
//            reading2="130mg",
//            description = resources.getString(R.string.taken_from)
//        ),
//        Vital(
//            id = 4,
//            name = resources.getString(R.string.blood_pressure),
//            image = R.drawable.ic_blood_pressure_unselected,
//            readingname1=resources.getString(R.string.bp_reading1),
//            readingname2=resources.getString(R.string.bp_reading2),
//            reading1="100mg",
//            reading2="130mg",
//            description = resources.getString(R.string.taken_from)
//        ),
//        Vital(
//            id = 5,
//            name = resources.getString(R.string.blood_pressure),
//            image = R.drawable.ic_blood_pressure_unselected,
//            readingname1=resources.getString(R.string.bp_reading1),
//            readingname2=resources.getString(R.string.bp_reading2),
//            reading1="100mg",
//            reading2="130mg",
//            description = resources.getString(R.string.taken_from)
//        ),
//        Vital(
//            id = 6,
//            name = resources.getString(R.string.blood_pressure),
//            image = R.drawable.ic_blood_pressure_unselected,
//            readingname1=resources.getString(R.string.bp_reading1),
//            readingname2=resources.getString(R.string.bp_reading2),
//            reading1="100mg",
//            reading2="130mg",
//            description = resources.getString(R.string.taken_from)
//    )
//    )
//}