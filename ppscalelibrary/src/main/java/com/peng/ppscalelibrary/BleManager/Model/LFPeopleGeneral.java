package com.peng.ppscalelibrary.BleManager.Model;

import android.text.TextUtils;

import com.holtek.libHTBodyfat.HTPeopleGeneral;
import chipsea.bias.v236.CSBiasAPI;

import java.util.Hashtable;

import static com.peng.ppscalelibrary.BleManager.Model.BleConnectFliterModel.ELECTRONIC_SCALE;


public class LFPeopleGeneral {

    public double lfWeightKg;                                               //体重
    public double lfHeightCm;                                               //身高
    public int lfAge;                                                       //年龄
    public int lfSex;                                                       //性别
    public double lfZTwoLegs;
    public int lfBodyAge;                                                   //身体年龄
    public double lfIdealWeightKg;                                          //理想体重
    public double lfBMI;                                                    //BMI
    public int lfBMR;                                                       //BMR
    public int lfVFAL;                                                      //内脏脂肪等级
    public double lfBoneKg;                                                 //骨量含量 // 骨盐量
    public double lfBodyfatPercentage;                                      //脂肪率
    public double lfWaterPercentage;                                        //水分率
    public double lfMuscleKg;                                               //肌肉含量
    public double lfProteinPercentage;                                      //蛋白质率
    public int lfBodyType;                                                  //身体类型
    public int lfBodyScore;                                                 //身体得分
    public double lfMusclePercentage;                                       //肌肉率
    public double lfBodyfatKg;                                              //体脂量
    public Hashtable<String, String> lfBMIRatingList = new Hashtable();     //BMI健康标准字典
    public Hashtable<String, String> lfBMRRatingList = new Hashtable();     //基础代谢健康标准字典
    public Hashtable<String, String> lfVFALRatingList = new Hashtable();    //内脏脂肪等级标准字典
    public Hashtable<String, String> lfBoneRatingList = new Hashtable();    //骨量等级标准字典
    public Hashtable<String, String> lfBodyfatRatingList = new Hashtable(); //脂肪率健康标准字典
    public Hashtable<String, String> lfWaterRatingList = new Hashtable();   //水分率健康标准
    public Hashtable<String, String> lfMuscleRatingList = new Hashtable();  //肌肉量健康标准
    public Hashtable<String, String> lfProteinRatingList = new Hashtable(); //蛋白健康标准字典
    public double lfStWeightKg;
    public double lfLoseFatWeightKg;                                        //去脂体重
    public double lfControlWeightKg;                                        //体重控制
    public double lfFatControlKg;                                           //脂肪控制量
    public double lfBonePercentage;                                         //骨量率
    public double lfBodyMuscleControlKg;                                    //骨骼肌率
    public double lfVFPercentage;                                           //皮下脂肪
    public String lfHealthLevel;                                            //健康水平
    public String lfFatLevel;                                               //肥胖等级
    public double lfWHR;                                                    //腰臀比
    public String lfHealthReport;                                           //健康评估
    public String scaleType;                                                //称类型
    public String scaleName;                                                //称名称
    private int impedance;                                                  //阻抗值（加密）
    private double lfWaist;                                                 //腰围
    private double lfHipLine;                                               //臀围
    private int lfHeartRate;                                                //心率

    public static final String WIFI_SCALE_HF = "HFWifi Scale";     //汉风Wifi称
    public static final String WIFI_SCALE_XH = "XHWifi Scale";     //新海Wifi称
    private static final  double fatFix = 5.0;

    public LFPeopleGeneral() {
    }

    /**
     *
     * @param lfWeightKg  体重
     * @param lfHeightCm  身高
     * @param lfSex       性别  //0女 1男
     * @param lfAge       年龄
     * @param impedance   阻抗 （称协议解析后会返回）非测脂数据 默认传0
     * @param lfWaist     腰围  默认传0
     * @param lfHipLine   腰臀比 默认传0
     * @param scaleType   称类型
     * @param scaleName   称名称
     */
    public LFPeopleGeneral(double lfWeightKg, double lfHeightCm, int lfSex, int lfAge, int impedance, double lfWaist, double lfHipLine, String scaleType, String scaleName) {
        this.lfWeightKg = lfWeightKg;
        this.lfHeightCm = lfHeightCm;
        this.lfAge = lfAge;
        this.lfSex = lfSex;
        this.impedance = impedance;
        this.lfWaist = lfWaist;
        this.lfHipLine = lfHipLine;
        this.scaleName = scaleName;
        this.scaleType = scaleType;
        if (!TextUtils.isEmpty(scaleType) && scaleType.equals(WIFI_SCALE_XH)){
            this.getXHBodyfatParameters();
        } else {
            this.getBodyfatParameters();
        }
    }

    private void getXHBodyfatParameters() {

        CSBiasAPI.CSBiasV236Resp cSBiasV236Resp = CSBiasAPI.cs_bias_v236(lfSex, lfAge, (int) (lfHeightCm * 10), (int) (lfWeightKg * 10), impedance);

        StringBuilder sb = new StringBuilder();

        if (cSBiasV236Resp.result == CSBiasAPI.CSBIAS_OK && impedance > 0) {
            CSBiasAPI.CSBiasDataV236 data = cSBiasV236Resp.data;
            try {
                this.setLfZTwoLegs(impedance);
                this.setLfBodyAge(cSBiasV236Resp.data.MA);
//            this.setLfIdealWeightKg(bodyfat.htIdealWeightKg);
                this.setLfBMR(data.BMR);
                this.setLfBMI(countLfBmi(getLfWeightKg(), getLfHeightCm()));
                this.setLfVFAL((int) data.VFR);
                this.setLfBoneKg(data.BMC);
                this.setLfBodyfatPercentage(data.BFP);
                this.setLfWaterPercentage(data.BWP);
                this.setLfMuscleKg(data.SLM);
                this.setLfProteinPercentage(data.PP);
//            this.setLfBodyType(bodyfat.htBodyType);
                this.setLfBodyScore(data.SBC);
                this.setLfMusclePercentage(data.SLM / getLfWeightKg());
                this.setLfBodyfatKg(lfWeightKg * data.BFP);
//            this.setLfBMIRatingList(bodyfat.htBMIRatingList);
//            this.setLfBMRRatingList(bodyfat.htBMRRatingList);
//            this.setLfVFALRatingList(bodyfat.htVFALRatingList);
//            this.setLfBoneRatingList(bodyfat.htBoneRatingList);
//            this.setLfBodyfatRatingList(bodyfat.htBodyfatRatingList);
//            this.setLfWaterRatingList(bodyfat.htWaterRatingList);
//            this.setLfMuscleRatingList(bodyfat.htMuscleRatingList);
//            this.setLfProteinRatingList(bodyfat.htProteinRatingList);
                this.setLfStWeightKg(this.countStandWeightKg());
                this.setLfLoseFatWeightKg(this.countLoseFatWeightKg());
                this.setLfControlWeightKg(data.WC);
                this.setLfFatControlKg(data.FC);
//            this.setLfBonePercentage(this.countBonePercentage());
                this.setLfBonePercentage(data.SMM/getLfWeightKg());
                this.setLfBodyMuscleControlKg(this.countBodyMuscleControlKg());
                this.setLfVFPercentage(this.countVFPercentage());
                this.setLfHealthLevel(this.countHealthLevel());
                this.setLfFatLevel(this.countFatLevel());
                this.setLfWHR(this.countHWR());
                this.setLfHealthReport(this.countHealthReport());

            } catch (Exception ex) {
                sb.append("输入错误，错误码：" + ex.getLocalizedMessage());
            }
        } else {
            sb.append("输入错误，错误码：" + cSBiasV236Resp.result);
            this.setLfBMI(countLfBmi(lfWeightKg, lfHeightCm));
        }
    }

    public int getBodyfatParameters() {
        HTPeopleGeneral bodyfat = new HTPeopleGeneral(this.lfWeightKg, this.lfHeightCm, this.lfSex, this.lfAge, this.impedance);
        int errorType = bodyfat.getBodyfatParameters();
        if (errorType == 0) {
            this.setLfZTwoLegs(bodyfat.htZTwoLegs);
            this.setLfIdealWeightKg(bodyfat.htIdealWeightKg);
            this.setLfBMR(bodyfat.htBMR);
            this.setLfBMI(countLfBmi(lfWeightKg, lfHeightCm));
            this.setLfVFAL(bodyfat.htVFAL);
            this.setLfBoneKg(bodyfat.htBoneKg);
            this.setLfBodyfatPercentage(bodyfat.htBodyfatPercentage);
            this.setLfWaterPercentage(bodyfat.htWaterPercentage);
            this.setLfMuscleKg(bodyfat.htMuscleKg);
            this.setLfProteinPercentage(bodyfat.htProteinPercentage);
            this.setLfBodyType(bodyfat.htBodyType);
            this.setLfBodyScore(bodyfat.htBodyScore);
            this.setLfMusclePercentage(bodyfat.htMusclePercentage);
            this.setLfBodyfatKg(bodyfat.htBodyfatKg);
            this.setLfBMIRatingList(bodyfat.htBMIRatingList);
            this.setLfBMRRatingList(bodyfat.htBMRRatingList);
            this.setLfVFALRatingList(bodyfat.htVFALRatingList);
            this.setLfBoneRatingList(bodyfat.htBoneRatingList);
            this.setLfBodyfatRatingList(bodyfat.htBodyfatRatingList);
            this.setLfWaterRatingList(bodyfat.htWaterRatingList);
            this.setLfMuscleRatingList(bodyfat.htMuscleRatingList);
            this.setLfProteinRatingList(bodyfat.htProteinRatingList);
            this.setLfStWeightKg(this.countStandWeightKg());
            this.setLfLoseFatWeightKg(this.countLoseFatWeightKg());
            this.setLfControlWeightKg(this.countControlWeightKg());
            this.setLfFatControlKg(this.countFatControlKg());
            this.setLfBonePercentage(this.countBonePercentage());
            this.setLfBodyMuscleControlKg(this.countBodyMuscleControlKg());
            this.setLfVFPercentage(this.countVFPercentage());
            this.setLfHealthLevel(this.countHealthLevel());
            this.setLfFatLevel(this.countFatLevel());
            this.setLfWHR(this.countHWR());
            this.setLfHealthReport(this.countHealthReport());
            this.setLfBodyAge(this.getPhysicAge(countLfBmi(lfWeightKg, lfHeightCm),bodyfat.htBodyAge));
        } else {
            this.setLfBMI(bodyfat.htBMI);
        }
        return errorType;
    }

    /**
     * 计算BMI
     * @param lfWeightKg
     * @param lfHeightCm
     * @return
     */
    private double countLfBmi(double lfWeightKg, double lfHeightCm) {
        return lfWeightKg / Math.pow(lfHeightCm / 100, 2);
    }

    private double countStandWeightKg() {
        return 21.75D * this.getLfHeightCm() * 0.01D * this.getLfHeightCm() * 0.01D;
    }

    private double countLoseFatWeightKg() {
        return this.getLfWeightKg() * (1.0D - this.getLfBodyfatPercentage() / 100);
    }

    private double countControlWeightKg() {
        return this.getLfWeightKg() - this.getLfStWeightKg();
    }

    private double countFatControlKg() {
        return this.getLfSex() == 1 ? this.calMaleBodyFatControl() : this.calFemaleBodyFatControl();
    }

    private double countBonePercentage() {
        return this.getLfSex() == 1 ? this.getLfLoseFatWeightKg() * 0.56D / this.getLfWeightKg() : this.getLfLoseFatWeightKg() * 0.54D / this.getLfWeightKg();
    }

    private int getPhysicAge(double bmi, int age) {
        double physicAge = 0;
        if (bmi < 22) {
            double temp = (age - 5) + ((22 - bmi) * (5 / (22 - 18.5f)));
            if (Math.abs(temp - age) >= 5) {
                physicAge = age + 5;
            } else {
                physicAge = temp;
            }
        } else if (bmi == 22) {
            physicAge = age - 5;
        } else if (bmi > 22) {
            double temp = (age - 5) + ((bmi - 22) * (5 / (24.9f - 22)));
            if (Math.abs(temp - age) >= 8) {
                physicAge = age + 8;
            } else {
                physicAge = temp;
            }
        }
        return (int)physicAge;
    }

    private double countBodyMuscleControlKg() {
        return this.getLfSex() == 1 ? this.calMaleBodyMuscleControl() : this.calFemaleBodyMuscleControl();
    }

    private double countVFPercentage() {
        return this.getLfBodyfatPercentage() * (2.0D / 3.0D);
    }

    private String countHealthLevel() {
        double BMI = this.getLfBMI();
        return 0.0D <= BMI && BMI < 18.5D ? "偏瘦" : (18.6D <= BMI && BMI < 24.9D ? "标准" : (25.0D <= BMI && BMI < 29.9D ? "超重" : "肥胖"));
    }

    private String countFatLevel() {
        double BMI = this.getLfBMI();
        return 30.0D <= BMI && BMI < 35.0D ? "肥胖1级" : (35.0D <= BMI && BMI < 40.0D ? "肥胖2级" : (40.0D <= BMI && BMI < 90.0D ? "肥胖3级" : "不确定"));
    }

    private double countHWR() {
        return this.getLfWaist() != 0.0D && this.getLfHipLine() != 0.0D ? this.getLfWaist() / this.getLfHipLine() : -1.0D;
    }

    private String countHealthReport() {
        int score = this.getLfBodyScore();
        return 0 <= score && score <= 60 ? "健康存在隐患" : (60 < score && score <= 70 ? "亚健康" : (70 < score && score <= 80 ? "一般" : (80 < score && score <= 90 ? "良好" : (90 < score && score <= 100 ? "非常好" : "不确定"))));
    }

    public double getLfHeightCm() {
        return this.lfHeightCm;
    }

    public double getLfWeightKg() {
        return this.lfWeightKg;
    }

    public void setLfWeightKg(double lfWeightKg) {
        this.lfWeightKg = lfWeightKg;
    }

    public double getLfBodyfatPercentage() {
        if (this.lfBodyfatPercentage <= 0){
            return 0;
        }else{
            if (this.scaleName.equals(ELECTRONIC_SCALE)) {

                return this.lfBodyfatPercentage;
            } else {

                return this.lfBodyfatPercentage - fatFix < 5.0 ? 5.0 : this.lfBodyfatPercentage - fatFix;
            }
        }
    }

    public void setLfBodyfatPercentage(double lfBodyfatPercentage) {
        this.lfBodyfatPercentage = lfBodyfatPercentage;
    }

    public double getLfStWeightKg() {
        return this.lfStWeightKg;
    }

    public int getLfSex() {
        return this.lfSex;
    }

    private double calMaleBodyFatControl() {
        double BMI = this.getLfBMI();
        double FatWeight = this.getLfBodyfatKg();
        int Age = this.getLfAge();
        double B0 = 24.1589D;
        double B1 = -0.6282D;
        double B2 = -0.5865D;
        double B3 = 9.8772D;
        double B4 = -0.368D;
        if (BMI <= 22.5D) {
            return FatWeight < 8.5D ? 9.0D - FatWeight : 0.0D;
        } else {
            double BodyFatControl = B0 + B1 * (double) Age + B2 * BMI + B3 * (double) Age / BMI + B4 * BMI * BMI / (double) Age;
            return BodyFatControl;
        }
    }

    private double calFemaleBodyFatControl() {
        double BMI = this.getLfBMI();
        double FatWeight = this.getLfBodyfatKg();
        int Age = this.getLfAge();
        double B0 = 67.2037D;
        double B1 = 0.6351D;
        double B2 = -2.6706D;
        double B3 = -18.1146D;
        double B4 = -0.2374D;
        if (BMI <= 21.0D) {
            return FatWeight < 10.5D ? 10.2D - FatWeight : 0.0D;
        } else {
            double BodyFatControl = B0 + B1 * (double) Age + B2 * BMI + B3 * (double) Age / BMI + B4 * BMI * BMI / (double) Age;
            return BodyFatControl;
        }
    }

    public double getLfLoseFatWeightKg() {
        return this.lfLoseFatWeightKg;
    }

    private double calMaleBodyMuscleControl() {
        double StandMuscle = this.getLfStWeightKg() * 0.797D;
        double BodyMuscleControl = StandMuscle - this.getLfBodyfatKg();
        return BodyMuscleControl < 0.0D ? 0.0D : BodyMuscleControl;
    }

    private double calFemaleBodyMuscleControl() {
        double StandMuscle = this.getLfStWeightKg() * 0.724D;
        double BodyMuscleControl = StandMuscle - this.getLfBodyfatKg();
        return BodyMuscleControl < 0.0D ? 0.0D : BodyMuscleControl;
    }

    public double getLfBMI() {
        return this.lfBMI;
    }

    public double getLfWaist() {
        return this.lfWaist;
    }

    public void setLfWaist(double lfWaist) {
        this.lfWaist = lfWaist;
    }

    public double getLfHipLine() {
        return this.lfHipLine;
    }

    public void setLfHipLine(double lfHipLine) {
        this.lfHipLine = lfHipLine;
    }

    public int getLfBodyScore() {
        return this.lfBodyScore;
    }

    public void setLfBodyScore(int lfBodyScore) {
        this.lfBodyScore = lfBodyScore;
    }

    public double getLfBodyfatKg() {
        return this.getLfBodyfatPercentage() * this.lfWeightKg;
    }

    public int getLfAge() {
        return this.lfAge;
    }

    public void setLfAge(int lfAge) {
        this.lfAge = lfAge;
    }

    public void setLfBodyfatKg(double lfBodyfatKg) {
        this.lfBodyfatKg = lfBodyfatKg;
    }

    public void setLfBMI(double lfBMI) {
        this.lfBMI = lfBMI;
    }

    public void setLfLoseFatWeightKg(double lfLoseFatWeightKg) {
        this.lfLoseFatWeightKg = lfLoseFatWeightKg;
    }

    public void setLfSex(int lfSex) {
        this.lfSex = lfSex;
    }

    public void setLfStWeightKg(double lfStWeightKg) {
        this.lfStWeightKg = lfStWeightKg;
    }

    public void setLfHeightCm(double lfHeightCm) {
        this.lfHeightCm = lfHeightCm;
    }

    public int getImpedance() {
        return this.impedance;
    }

    public void setImpedance(int impedance) {
        this.impedance = impedance;
    }

    public double getLfZTwoLegs() {
        return this.lfZTwoLegs;
    }

    public void setLfZTwoLegs(double lfZTwoLegs) {
        this.lfZTwoLegs = lfZTwoLegs;
    }

    public int getLfBodyAge() { return this.lfBodyAge; }

    public void setLfBodyAge(int lfBodyAge) {
        this.lfBodyAge = lfBodyAge;
    }

    public double getLfIdealWeightKg() {
        return this.lfIdealWeightKg;
    }

    public void setLfIdealWeightKg(double lfIdealWeightKg) {
        this.lfIdealWeightKg = lfIdealWeightKg;
    }

    public int getLfBMR() {
        return this.lfBMR;
    }

    public void setLfBMR(int lfBMR) {
        this.lfBMR = lfBMR;
    }

    public int getLfVFAL() {
        return this.lfVFAL;
    }

    public void setLfVFAL(int lfVFAL) {
        this.lfVFAL = lfVFAL;
    }

    public double getLfBoneKg() {
        return this.lfBoneKg;
    }

    public void setLfBoneKg(double lfBoneKg) {
        this.lfBoneKg = lfBoneKg;
    }

    public double getLfWaterPercentage() {
        return this.lfWaterPercentage;
    }

    public void setLfWaterPercentage(double lfWaterPercentage) {
        this.lfWaterPercentage = lfWaterPercentage;
    }

    public double getLfMuscleKg() {
        return this.lfMuscleKg;
    }

    public void setLfMuscleKg(double lfMuscleKg) {
        this.lfMuscleKg = lfMuscleKg;
    }

    public double getLfProteinPercentage() {
        return this.lfProteinPercentage;
    }

    public void setLfProteinPercentage(double lfProteinPercentage) {
        this.lfProteinPercentage = lfProteinPercentage;
    }

    public int getLfBodyType() {
        return this.lfBodyType;
    }

    public void setLfBodyType(int lfBodyType) {
        this.lfBodyType = lfBodyType;
    }

    public double getLfMusclePercentage() {
        return this.lfMusclePercentage;
    }

    public void setLfMusclePercentage(double lfMusclePercentage) {
        this.lfMusclePercentage = lfMusclePercentage;
    }

    public Hashtable<String, String> getLfBMIRatingList() {
        return this.lfBMIRatingList;
    }

    public void setLfBMIRatingList(Hashtable<String, String> lfBMIRatingList) {
        this.lfBMIRatingList = lfBMIRatingList;
    }

    public Hashtable<String, String> getLfBMRRatingList() {
        return this.lfBMRRatingList;
    }

    public void setLfBMRRatingList(Hashtable<String, String> lfBMRRatingList) {
        this.lfBMRRatingList = lfBMRRatingList;
    }

    public Hashtable<String, String> getLfVFALRatingList() {
        return this.lfVFALRatingList;
    }

    public void setLfVFALRatingList(Hashtable<String, String> lfVFALRatingList) {
        this.lfVFALRatingList = lfVFALRatingList;
    }

    public Hashtable<String, String> getLfBoneRatingList() {
        return this.lfBoneRatingList;
    }

    public void setLfBoneRatingList(Hashtable<String, String> lfBoneRatingList) {
        this.lfBoneRatingList = lfBoneRatingList;
    }

    public Hashtable<String, String> getLfBodyfatRatingList() {
        return this.lfBodyfatRatingList;
    }

    public void setLfBodyfatRatingList(Hashtable<String, String> lfBodyfatRatingList) {
        this.lfBodyfatRatingList = lfBodyfatRatingList;
    }

    public Hashtable<String, String> getLfWaterRatingList() {
        return this.lfWaterRatingList;
    }

    public void setLfWaterRatingList(Hashtable<String, String> lfWaterRatingList) {
        this.lfWaterRatingList = lfWaterRatingList;
    }

    public Hashtable<String, String> getLfMuscleRatingList() {
        return this.lfMuscleRatingList;
    }

    public void setLfMuscleRatingList(Hashtable<String, String> lfMuscleRatingList) {
        this.lfMuscleRatingList = lfMuscleRatingList;
    }

    public Hashtable<String, String> getLfProteinRatingList() {
        return this.lfProteinRatingList;
    }

    public void setLfProteinRatingList(Hashtable<String, String> lfProteinRatingList) {
        this.lfProteinRatingList = lfProteinRatingList;
    }

    public double getLfControlWeightKg() {
        return this.lfControlWeightKg;
    }

    public void setLfControlWeightKg(double lfControlWeightKg) {
        this.lfControlWeightKg = lfControlWeightKg;
    }

    public double getLfFatControlKg() {
        return this.lfFatControlKg;
    }

    public void setLfFatControlKg(double lfFatControlKg) {
        this.lfFatControlKg = lfFatControlKg;
    }

    public double getLfBonePercentage() {
        return this.lfBonePercentage;
    }

    public void setLfBonePercentage(double lfBonePercentage) {
        this.lfBonePercentage = lfBonePercentage;
    }

    public double getLfBodyMuscleControlKg() {
        return this.lfBodyMuscleControlKg;
    }

    public void setLfBodyMuscleControlKg(double lfBodyMuscleControlKg) {
        this.lfBodyMuscleControlKg = lfBodyMuscleControlKg;
    }

    public double getLfVFPercentage() {
        return this.lfVFPercentage;
    }

    public void setLfVFPercentage(double lfVFPercentage) {
        this.lfVFPercentage = lfVFPercentage;
    }

    public String getLfHealthLevel() {
        return this.lfHealthLevel;
    }

    public void setLfHealthLevel(String lfHealthLevel) {
        this.lfHealthLevel = lfHealthLevel;
    }

    public String getLfFatLevel() {
        return this.lfFatLevel;
    }

    public void setLfFatLevel(String lfFatLevel) {
        this.lfFatLevel = lfFatLevel;
    }

    public double getLfWHR() {
        return this.lfWHR;
    }

    public void setLfWHR(double lfWHR) {
        this.lfWHR = lfWHR;
    }

    public String getLfHealthReport() {
        return this.lfHealthReport;
    }

    public void setLfHealthReport(String lfHealthReport) {
        this.lfHealthReport = lfHealthReport;
    }


    public String getScaleType() {
        return scaleType;
    }

    public void setScaleType(String scaleType) {
        this.scaleType = scaleType;
    }

    public String getScaleName() {
        return scaleName;
    }

    public void setScaleName(String scaleName) {
        this.scaleName = scaleName;
    }

    public int getLfHeartRate() {
        return lfHeartRate;
    }

    public void setLfHeartRate(int lfHeartRate) {
        this.lfHeartRate = lfHeartRate;
    }

    /*@Override
    public String toString() {
        return "LFPeopleGeneral{" +
                "lfWeightKg=" + lfWeightKg + "\n" +
                ", lfHeightCm=" + lfHeightCm + "\n" +
                ", lfAge=" + lfAge + "\n" +
                ", lfSex=" + lfSex + "\n" +
                ", lfZTwoLegs=" + lfZTwoLegs + "\n" +
                ", lfBodyAge=" + lfBodyAge + "\n" +
                ", lfIdealWeightKg=" + lfIdealWeightKg + "\n" +
                ", lfBMI=" + lfBMI + "\n" +
                ", lfBMR=" + lfBMR + "\n" +
                ", lfVFAL=" + lfVFAL + "\n" +
                ", lfBoneKg=" + lfBoneKg + "\n" +
                ", lfBodyfatPercentage=" + lfBodyfatPercentage + "\n" +
                ", lfWaterPercentage=" + lfWaterPercentage + "\n" +
                ", lfMuscleKg=" + lfMuscleKg + "\n" +
                ", lfProteinPercentage=" + lfProteinPercentage + "\n" +
                ", lfBodyType=" + lfBodyType + "\n" +
                ", lfBodyScore=" + lfBodyScore + "\n" +
                ", lfMusclePercentage=" + lfMusclePercentage + "\n" +
                ", lfBodyfatKg=" + lfBodyfatKg + "\n" +
                ", lfBMIRatingList=" + lfBMIRatingList + "\n" +
                ", lfBMRRatingList=" + lfBMRRatingList + "\n" +
                ", lfVFALRatingList=" + lfVFALRatingList + "\n" +
                ", lfBoneRatingList=" + lfBoneRatingList + "\n" +
                ", lfBodyfatRatingList=" + lfBodyfatRatingList + "\n" +
                ", lfWaterRatingList=" + lfWaterRatingList + "\n" +
                ", lfMuscleRatingList=" + lfMuscleRatingList + "\n" +
                ", lfProteinRatingList=" + lfProteinRatingList + "\n" +
                ", lfStWeightKg=" + lfStWeightKg + "\n" +
                ", lfLoseFatWeightKg=" + lfLoseFatWeightKg + "\n" +
                ", lfControlWeightKg=" + lfControlWeightKg + "\n" +
                ", lfFatControlKg=" + lfFatControlKg + "\n" +
                ", lfBonePercentage=" + lfBonePercentage + "\n" +
                ", lfBodyMuscleControlKg=" + lfBodyMuscleControlKg + "\n" +
                ", lfVFPercentage=" + lfVFPercentage + "\n" +
                ", lfHealthLevel='" + lfHealthLevel + '\'' + "\n" +
                ", lfFatLevel='" + lfFatLevel + '\'' + "\n" +
                ", lfWHR=" + lfWHR +
                ", lfHealthReport='" + lfHealthReport + '\'' + "\n" +
                ", scaleType='" + scaleType + '\'' + "\n" +
                ", scaleName='" + scaleName + '\'' + "\n" +
                ", impedance=" + impedance + "\n" +
                ", lfWaist=" + lfWaist + "\n" +
                ", lfHipLine=" + lfHipLine + "\n" +
                ", lfHeartRate=" + lfHeartRate + "\n" +
                '}';
    }*/

    @Override
    public String toString() {
        return "LFPeopleGeneral{" +
                ", 年龄=" + lfAge + "\n" +
                ", 性别=" + lfSex + "\n" +
                ", 身高=" + lfHeightCm + "\n" +
                "  体重=" + lfWeightKg + "\n" +
                ", lfBMI=" + getLfBMI() + "\n" +
                ", 体脂率=" + getLfBodyfatPercentage() + "\n" +
                ", 肌肉量=" + getLfMuscleKg() + "\n" +
                ", 体水分率=" + getLfWaterPercentage() + "\n" +
                ", 内脏脂肪等级=" + getLfVFAL() + "\n" +
                ", 骨量=" + getLfBoneKg() + "\n" +
                ", lfBMR=" + getLfBMR() + "\n" +
                ", 蛋白质率=" + getLfProteinPercentage() + "\n" +
                ", 肥胖等级='" + lfFatLevel + '\'' + "\n" +
                ", 皮下脂肪=" + getLfVFPercentage() + "\n" +
                ", 去脂体重=" + getLfLoseFatWeightKg() + "\n" +
                ", 身体类型=" + getLfBodyType() + "\n" +
                ", 标准体重=" + getLfIdealWeightKg() + "\n" +
                ", 身体年龄=" + getLfBodyAge() + "\n" +
                ", 身体得分=" + getLfBodyScore() + "\n" +
                ", 体脂量=" + getLfBodyfatKg() + "\n" +
                ", lfZTwoLegs=" + lfZTwoLegs + "\n" +
                ", lfBMIRatingList=" + getLfBMIRatingList() + "\n" +
                ", lfMusclePercentage=" + getLfMusclePercentage() + "\n" +
                ", lfBMRRatingList=" + lfBMRRatingList + "\n" +
                ", lfVFALRatingList=" + lfVFALRatingList + "\n" +
                ", lfBoneRatingList=" + lfBoneRatingList + "\n" +
                ", lfBodyfatRatingList=" + lfBodyfatRatingList + "\n" +
                ", lfWaterRatingList=" + lfWaterRatingList + "\n" +
                ", lfMuscleRatingList=" + lfMuscleRatingList + "\n" +
                ", lfProteinRatingList=" + lfProteinRatingList + "\n" +
                ", lfStWeightKg=" + lfStWeightKg + "\n" +
                ", lfControlWeightKg=" + lfControlWeightKg + "\n" +
                ", lfFatControlKg=" + lfFatControlKg + "\n" +
                ", lfBonePercentage=" + lfBonePercentage + "\n" +
                ", lfBodyMuscleControlKg=" + lfBodyMuscleControlKg + "\n" +
                ", lfHealthLevel='" + lfHealthLevel + '\'' + "\n" +
//                ", lfWHR=" + lfWHR +
                ", lfHealthReport='" + lfHealthReport + '\'' + "\n" +
                ", scaleType='" + scaleType + '\'' + "\n" +
                ", scaleName='" + scaleName + '\'' + "\n" +
                ", impedance=" + impedance + "\n" +
                ", lfWaist=" + lfWaist + "\n" +
                ", lfHipLine=" + lfHipLine + "\n" +
                ", lfHeartRate=" + lfHeartRate + "\n" +
                '}';
    }
}
