package com.techesolutions.data.remote.model.dashboard

data class HeartRateSelectionData(
    val Title : String,
    val vitalName : String,
    val subVitalName : String,
    val data1 : String,
    val data2 : String
)