package com.techesolutions.utils

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView


class BoldFontTextView : androidx.appcompat.widget.AppCompatTextView {
    constructor(context: Context) : super(context) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        if (Utils.boldFont != null) {
            typeface = Utils.boldFont
        } else {
            Utils.setDimensions(context)
            typeface = Utils.boldFont
            println("Oxigen ligh tnot loaded")
        }
    }
}
