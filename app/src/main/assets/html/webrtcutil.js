
function fixSDP(input,videoSendCodec,videoSendBitrate)
{
    var sdpParser=require("sdp-transform");
     var supportedCodecs = [{
            codec : "H264",
            payload : 107,
            rate : 90000,
            fmtp : {
                config: "level-asymmetry-allowed=1;packetization-mode=1;profile-level-id=42e01f"
            }
        },{
            codec : "VP9",
            payload : 101,
            rate : 90000
        },{
            codec : "VP8",
            payload : 100,
            rate : 90000
        }];
    if(videoSendCodec)
    {
        var codecIndex = -1;
        for(var i = 0;i<=supportedCodecs.length ;i++ )
        {
            if(videoSendCodec == supportedCodecs[i].codec)
            {
                codecIndex = i;
                break;
            }
        }
        if(codecIndex >= 0)
        {
            var codec = supportedCodecs.splice(codecIndex,1);
            supportedCodecs.unshift(codec[0]);
        }
    }
    var sdp = sdpParser.parse(input);

    var media = undefined;
    var mediaIndex = -1;
    for(var i=0;i<sdp.media.length;i++)
    {
        if(sdp.media[i].type == "video")
        {
            media = sdp.media[i];
            mediaIndex = i;
            break;
        }
    }
    if(!media)
    {
        return input;
    }
 //new fix
  for(var i =0; i < media.rtp.length; i++)
     {
         var codec = media.rtp[i].codec;
         for(var j = 0; j< supportedCodecs.length ; j++)
         {
                 if(supportedCodecs[j].codec === codec)
                 {
                         supportedCodecs[j]._available = true;
                 }
         }
     }

     for(var i =0; i < supportedCodecs.length ; i++)
     {
         if(!supportedCodecs[i]._available)
         {
                 supportedCodecs.splice(i,1);
                 i--;
         }
     }

     for(var i =0; i < supportedCodecs.length ; i++)
     {
         delete supportedCodecs[i]._available;
     }
     //---


    media.ext = [];
    media.rtp = [];
    media.fmtp = [];
    media.rtcpFb = [];
    media.bandwidth = [{ type : "AS" , limit : videoSendBitrate}];
    media.payloads= "";

    supportedCodecs.forEach(function(elem){
        media.payloads += elem.payload + " ";
        media.rtp.push({
            codec : elem.codec,
            payload : elem.payload,
            rate : elem.rate
        });
        if(elem.fmtp)
        {
            media.fmtp.push({
                config : elem.fmtp.config,
                payload : elem.payload
            });
        }

        media.rtcpFb.push({payload : elem.payload , subtype : "fir", type : "ccm"});
        media.rtcpFb.push({payload : elem.payload , type : "goog-remb"});
        media.rtcpFb.push({payload : elem.payload , type : "nack"});
        media.rtcpFb.push({payload : elem.payload , subtype : "pli", type : "nack"});
        //media.rtcpFb.push({payload : elem.payload , type : "transport-cc"});
    });

    media.payloads=media.payloads.trim();
    return sdpParser.write(sdp);
}