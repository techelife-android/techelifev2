package com.techesolutions.utils

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import java.text.ParseException
import java.text.SimpleDateFormat

/**
 * Created by Neelam on 19-12-2020.
 */
class RegularFontTextView : AppCompatTextView {
    constructor(context: Context) : super(context) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        applyCustomFont(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        applyCustomFont(context)
    }

    private fun applyCustomFont(context: Context) {
        if (Utils.aTecheRegularFont != null) {
            typeface = Utils.aTecheRegularFont
        } else {
            Utils.setDimensions(context)
            typeface = Utils.aTecheRegularFont
            println("Oxigen ligh tnot loaded")
        }
    }

    @Throws(ParseException::class)
    fun convertLongDateFormat(
        time: String?,
        context: Context?
    ): String? {
        val currentFOrmat =
            SimpleDateFormat("MM-dd-yyyy HH:mm")
        //        SimpleDateFormat convertFormat = new SimpleDateFormat("MMM dd, yyyy | HH:mm");
//        if(MySharedPreference.getPreferences(context, Constants.KEY_USER_DATE_FORMAT).equalsIgnoreCase("dd-mm-yyyy")){
//            convertFormat = new SimpleDateFormat("dd MMM yyyy | hh:mm a");
//        }
//        Date date = currentFOrmat.parse(time);
//        return convertFormat.format(date);
        var convertFormatDate =
            SimpleDateFormat("MMM dd, yyyy")
        if (MySharedPreference.getPreferences(context, Constants.KEY_USER_DATE_FORMAT)
                .equals("dd-mm-yyyy")
        ) {
            convertFormatDate = SimpleDateFormat("dd MMM yyyy")
        }
        var convertFormatTime = SimpleDateFormat("HH:mm")
        if (MySharedPreference.getPreferences(context, Constants.KEY_USER_TIME_FORMAT)
                .equals("12hour")
        ) {
            convertFormatTime = SimpleDateFormat("hh:mm a")
        }
        val date = currentFOrmat.parse(time)
        val dateF = convertFormatDate.format(date)
        val timeF = convertFormatTime.format(date)
        return "$dateF | $timeF"
    }
}