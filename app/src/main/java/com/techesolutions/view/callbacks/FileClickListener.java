package com.techesolutions.view.callbacks;

public interface FileClickListener {
   void onPositionClick(int position);
   void onDeleteClick(int position);
}
