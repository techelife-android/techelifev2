package com.techesolutions.view.activity.nextcloud.task

data class Tasks(
    var title : String,
    var description: String,
    var stratTime : String,
    var endTime : String,
    var timestamp: String,
    var uid: String,
    var filename: String
    )