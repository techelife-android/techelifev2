package com.techesolutions.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techesolutions.data.remote.model.forgetpassword.CodeVerificationResponse
import com.techesolutions.data.remote.repository.login.VerifyCodeRepository

/**
 * Author: Neelam Upadhyay
 * Created: 30/12/2020
 */
class VerifyCodeViewModel: ViewModel() {

    var verifyCodeLiveData: MutableLiveData<CodeVerificationResponse>? = null

    fun verifyCode(code: Int, hashcode: String, apporigin: String): LiveData<CodeVerificationResponse>? {
        verifyCodeLiveData = VerifyCodeRepository.verifyCode(code,hashcode,apporigin)
        return verifyCodeLiveData
    }
}