package com.techesolutions.view.activity.nextcloud.files

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "file_folder_table")
data class FileFolderData(val fileId: Long,
                          val parentId: Long,
                          val remoteId: Long,
                          val remotePath: String,
                          val localPath: String,
                          val mimeType: String,
                          val fileLength: Long,
                          val creationTimestamp: Long,
                          val modificationTimestamp: Long,
                          val previewAvailable: Boolean,
                          val status: String,
                          @PrimaryKey(autoGenerate = false) val id: Int? = null)