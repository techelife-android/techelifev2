package com.techesolutions.data.remote.model.dashboard

data class BPVital(
    val data1val : String,
    val data2val : String,
    val data3val : String,
    val dateandtime : String
)