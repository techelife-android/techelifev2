package com.techesolutions.view.callbacks

interface ItemClickListener {
    fun onPositionClick(adapterPosition: Int)
}