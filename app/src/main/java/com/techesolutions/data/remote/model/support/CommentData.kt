package com.techesolutions.data.remote.model.support

data class CommentData(
    val ticket_msg_id: Int,
    val ticket_id: Int,
    val message: String?,
    val from_id: String?,
    val to_id: String?,
    val created_date: String,
    val from_name: String,
    val from_usertype: String,
    val to_name: String,
    val from_userimage: String
    )
