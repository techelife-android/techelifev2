package com.techesolutions.view.adapter

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.nextclouddashboard.CameraItems
import com.techesolutions.view.activity.nextcloud.cameras.LiveCameraViewActivity
import java.util.*

class CameraAdapter(
    context: Context,
    val userList: ArrayList<CameraItems>
) : RecyclerView.Adapter<CameraAdapter.ViewHolder>() {
    var activity:Context = context

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CameraAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(
            R.layout.camera_live_view,
            parent,
            false
        )
        return ViewHolder(v, activity)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CameraAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position], activity)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(data: CameraItems, context: Context) {

            val room = itemView.findViewById(R.id.room) as TextView
            val camera  = itemView.findViewById(R.id.camera) as TextView
            var webView  = itemView.findViewById(R.id.webView) as WebView
            var cameraView  = itemView.findViewById(R.id.cameraView) as ConstraintLayout
            var viewNavigation  = itemView.findViewById(R.id.viewNavigation) as TextView
            //var loader  = itemView.findViewById(R.id.loader) as TextView
            room.text = data.room
            camera.text = data.title
           /* var progressBar: ProgressDialog = ProgressDialog(context);
            progressBar.setMessage("Please wait...");*/
            webView.webViewClient = WebViewClient()
            webView.settings.setSupportZoom(true)
            webView.settings.javaScriptEnabled = true
            webView.loadUrl(data.url)
           /* webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    view.loadUrl(url)
                    return true
                }

                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    super.onPageStarted(view, url, favicon)
                    loader.visibility=View.VISIBLE
                    webView.visibility=View.GONE
                }

                override fun onPageFinished(view: WebView, url: String) {
                    loader.visibility=View.GONE
                    webView.visibility=View.VISIBLE
                }

                override fun onReceivedError(
                    view: WebView,
                    errorCode: Int,
                    description: String,
                    failingUrl: String
                ) {
                    loader.visibility=View.GONE
                    webView.visibility=View.VISIBLE
                }
            }
            webView.loadUrl(data.url);*/
            cameraView.setClickable(true);
            webView.setClickable(true);

            itemView.setOnClickListener(View.OnClickListener {
                val intent =
                    Intent(context, LiveCameraViewActivity::class.java)
                intent.putExtra("camera_url", data.url)
                intent.putExtra("id", data.id)
                context.startActivity(intent)
            })
            cameraView.setOnClickListener(View.OnClickListener {
                val intent =
                    Intent(context, LiveCameraViewActivity::class.java)
                intent.putExtra("camera_url", data.url)
                intent.putExtra("id", data.id)
                intent.putExtra("camera", data.title)
                context.startActivity(intent)
            })
            viewNavigation.setOnClickListener(View.OnClickListener {
                val intent =
                    Intent(context, LiveCameraViewActivity::class.java)
                intent.putExtra("camera_url", data.url)
                intent.putExtra("id", data.id)
                intent.putExtra("camera", data.title)
                context.startActivity(intent)
            })
        }
    }

}