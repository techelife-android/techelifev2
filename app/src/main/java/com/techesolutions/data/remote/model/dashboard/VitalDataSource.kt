//package com.techesolutions.data.remote.model.dashboard
//
//import android.content.res.Resources
//import androidx.lifecycle.LiveData
//import androidx.lifecycle.MutableLiveData
//
//class VitalDataSource (resources: Resources) {
//    private val initialFlowerList = vitalList(resources)
//    private val flowersLiveData = MutableLiveData(initialFlowerList)
//
//    /* Adds flower to liveData and posts value. */
//    fun addFlower(flower: Vital) {
//        val currentList = flowersLiveData.value
//        if (currentList == null) {
//            flowersLiveData.postValue(listOf(flower))
//        } else {
//            val updatedList = currentList.toMutableList()
//            updatedList.add(0, flower)
//            flowersLiveData.postValue(updatedList)
//        }
//    }
//
//    /* Removes flower from liveData and posts value. */
//    fun removeFlower(flower: Vital) {
//        val currentList = flowersLiveData.value
//        if (currentList != null) {
//            val updatedList = currentList.toMutableList()
//            updatedList.remove(flower)
//            flowersLiveData.postValue(updatedList)
//        }
//    }
//
//    /* Returns flower given an ID. */
//    fun getFlowerForId(id: Long): Vital? {
//        flowersLiveData.value?.let { flowers ->
//            return flowers.firstOrNull{ it.id == id}
//        }
//        return null
//    }
//
//    fun getFlowerList(): LiveData<List<Vital>> {
//        return flowersLiveData
//    }
//
//    /* Returns a random flower asset for flowers that are added. */
//    fun getRandomFlowerImageAsset(): Int? {
//        val randomNumber = (initialFlowerList.indices).random()
//        return initialFlowerList[randomNumber].image
//    }
//
//    companion object {
//        private var INSTANCE: VitalDataSource? = null
//
//        fun getDataSource(resources: Resources): VitalDataSource {
//            return synchronized(VitalDataSource::class) {
//                val newInstance = INSTANCE ?: VitalDataSource(resources)
//                INSTANCE = newInstance
//                newInstance
//            }
//        }
//    }
//}