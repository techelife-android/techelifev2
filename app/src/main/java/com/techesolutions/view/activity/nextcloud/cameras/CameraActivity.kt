package com.techesolutions.view.activity.nextcloud.cameras

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.nextclouddashboard.CameraItems
import com.techesolutions.data.remote.model.nextclouddashboard.RoomItems
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.adapter.CameraAdapter
import com.techesolutions.view.callbacks.ItemClickListener
import com.techesolutions.viewmodel.cameras.CameraViewModel
import com.techesolutions.viewmodel.cameras.MasterViewModel
import kotlinx.android.synthetic.main.activity_cemaras.*
import kotlinx.android.synthetic.main.activity_cemaras.noDataText
import kotlinx.android.synthetic.main.activity_cemaras.recycler_view
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONArray


/**
 * Created by Neelam on 30-08-2021.
 */
class CameraActivity : AppCompatActivity(), View.OnClickListener, ItemClickListener {
    private var dataModel: ArrayList<RoomItems>? = null
    private var cametaModel: ArrayList<CameraItems>? = null
    private lateinit var spinner: Spinner
    //private lateinit var adapter: RoomAdapter
    lateinit var roomViewModel: MasterViewModel
    lateinit var cameraViewModel: CameraViewModel
    private val progressDialog = CustomProgressDialog()
    var recyclerView: RecyclerView? = null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_cemaras)
        headerTitle?.text="Cameras"
        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }
        roomViewModel = ViewModelProvider(this).get(MasterViewModel::class.java)
        cameraViewModel = ViewModelProvider(this).get(CameraViewModel::class.java)
        spinner = findViewById<View>(R.id.spinner) as Spinner
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        fetchRoomData("Bearer $token")


        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val  roomvalue = dataModel!![position].value
            fetchCameraData("Bearer $token",roomvalue!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

    }

    private fun fetchCameraData(token: String, room: String) {
        if (Utils.isOnline(this@CameraActivity)) {
            showLoading("Please wait")

            // Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show()
            cameraViewModel.getCameraData(token,"mobile",room)!!
                .observe(this, Observer { masterResponse ->
                    val success = masterResponse.success;
                    if (success == true) {
                        closeLoading()
                        try {
                            val gson = Gson()
                            val rooms: String = gson.toJson(masterResponse.data!!.cameras)
                            var dlist = JSONArray(rooms)
                            Log.d("dlist",""+dlist)

                            cametaModel = ArrayList<CameraItems>()
                            for (i in 0 until dlist.length()) {
                                val item = dlist.getJSONObject(i)
                                val id = item.optString("id")
                                val title = item.optString("title")
                                val label = item.optString("room_label")
                                val url = item.optString("cam_live_stream_url")
                                cametaModel!!.add(CameraItems(id,title, label, url))
                            }
                        } catch (e: Exception) {
                        }
                        val cameraAdapter = CameraAdapter(this, cametaModel!!)
                        recyclerView?.adapter = cameraAdapter
                    } else {
                        closeLoading()
                        Toast.makeText(this, masterResponse.message, Toast.LENGTH_SHORT).show()
                    }
                    //Toast.makeText(this, baseResponse.message, Toast.LENGTH_SHORT).show()
                })
        }

    }

    override fun onClick(v: View) {
        when (v.id) {

        }
    }

    private fun fetchRoomData(token: String) {
                if (Utils.isOnline(this@CameraActivity)) {
                showLoading("Please wait")

                // Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show()
                roomViewModel.getRoomDeviceMasterData(token, "mobile", "CAMERA")!!
                    .observe(this, Observer { masterResponse ->
                        if(masterResponse!=null) {
                        val success = masterResponse.success;
                        if (success == true) {
                            closeLoading()
                            try {
                                val gson = Gson()
                                val rooms: String = gson.toJson(masterResponse.data!!.rooms)
                                var dlist = JSONArray(rooms)
                                if(dlist.length()>0) {
                                    noDataText!!.visibility = View.GONE
                                    recycler_view!!.visibility = View.VISIBLE
                                dataModel = ArrayList<RoomItems>()
                                for (i in 0 until dlist.length()) {
                                    val item = dlist.getJSONObject(i)
                                    val id = item.optString("id")
                                    val label = item.optString("label")
                                    val value = item.optString("value")
                                    dataModel!!.add(RoomItems(id, label, value))
                                }
                                    val roomAdapter = RoomAdapter(this, dataModel!!, this)
                                    spinner.adapter = roomAdapter
                                }
                                else{
                                    noDataText.visibility = View.VISIBLE
                                    recycler_view!!.visibility = View.GONE
                                }
                            } catch (e: Exception) {
                            }

                        } else {
                            closeLoading()
                            Toast.makeText(this, masterResponse.message, Toast.LENGTH_SHORT).show()
                        }
                        }
                        else{
                            closeLoading()
                            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    })
            }

    }


    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {

    }

}