package com.techesolutions.view.activity.vitaldevices

import android.content.Context
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Utils
import com.techesolutions.R
import com.techesolutions.data.remote.model.details.VitalDetail
import java.text.ParseException
import java.util.*

public class CustomGraphMarker(
    context: Context?,
    layoutResource: Int,
    list: ArrayList<VitalDetail>?
) :
    MarkerView(context, layoutResource) {

    var tvContent: TextView? =  findViewById<TextView>(R.id.tvContent)
    var mList: List<VitalDetail>? = list
    var _mContext: Context? = context

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    override fun refreshContent(
        e: Entry,
        highlight: Highlight?
    ) {
        try {
            val date: String = mList!![e.x.toInt()].date
           // val time: String = mList!![e.x.toInt()].date
            val dateFormatted: String =
                com.techesolutions.utils.Utils.convertLongDateFormat(
                    date,
                    _mContext
                )

//            if(mList.get((int) e.getX()).getVitalname().equalsIgnoreCase("weight")){
//                tvContent.setText(dateFormatted + "\n"
//                        + Utils.formatNumber(
//                                Float.parseFloat(com.blisshealthcare.blissapp.utils.Utils.getLocalWeight(
//                                        String.valueOf(e.getY()), _mContext)), 0, true));
//            } else {
            //!!.text=date
            tvContent!!.text = """
                $dateFormatted
                ${Utils.formatNumber(e.y, 0, true)}
                """.trimIndent()
            //            }
        } catch (e1: ParseException) {
            e1.printStackTrace()
        }
        //        }
        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF? {
        return MPPointF((-(getWidth() / 2)).toFloat(), (-getHeight()).toFloat())
    }
}
