package com.techesolutions.data.remote.model.dashboard

data class POVital(
    val data1val : String,
    val data2val : String,
    val dateandtime : String
)