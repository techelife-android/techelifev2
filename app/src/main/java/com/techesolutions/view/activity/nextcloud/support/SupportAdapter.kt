package com.techesolutions.view.activity.nextcloud.support

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.support.CommentData
import com.techesolutions.data.remote.model.support.TicketSupportData
import com.techesolutions.data.remote.model.userinfo.WebInfoData
import com.techesolutions.view.activity.nextcloud.userinfo.UserInfoActivity
import com.techesolutions.view.callbacks.ItemClickListener
import com.techesolutions.view.callbacks.UserInfoClickListener

class SupportAdapter(
    context:Context,
    val ticketList: ArrayList<TicketSupportData>,
    private var itemclick: SupportActivity
) : RecyclerView.Adapter<SupportAdapter.ViewHolder>(), Filterable {
    var activity:Context = context
    var infoClickListener=itemclick
    var filterTicketData=ticketList
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SupportAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.support_items, parent, false)
        return ViewHolder(v,activity,infoClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: SupportAdapter.ViewHolder, position: Int) {
        filterTicketData.get(position)?.let { holder.bindItems(it, activity, infoClickListener, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return filterTicketData.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,context: Context, infoClickListener: ItemClickListener) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(vital: TicketSupportData,context: Context, infoClickListener: ItemClickListener, position: Int) {
            val tvTicket  = itemView.findViewById(R.id.tvTicket) as TextView
            val tvDescription  = itemView.findViewById(R.id.tvDescription) as TextView
            val tvSubject  = itemView.findViewById(R.id.tvSubject) as TextView
            val tvStatus  = itemView.findViewById(R.id.tvStatus) as TextView
            val date  = itemView.findViewById(R.id.date) as TextView
            val comment  = itemView.findViewById(R.id.comments) as ImageView
            tvTicket.text = vital.ticket_no
            tvDescription.text = vital.description
            tvSubject.text = vital.subject
            tvStatus.text = vital.status
            date.text = vital.created_date
            comment.setOnClickListener {
                val position = layoutPosition
                //val notes: CloudNote = notes
               // infoClickListener.onPositionClick(position)
                val intent: Intent = Intent(context, CommentsActivity::class.java)
                intent.putExtra("id",vital.id)
                intent.putExtra("date",vital.created_date)
                intent.putExtra("subject",vital.subject)
                intent.putExtra("status",vital.status)
                intent.putExtra("ticket_no",vital.ticket_no)
                intent.putExtra("description",vital.description)
                context.startActivity(intent)
                // Toast.makeText(context, "Recycle Click$notes  ", Toast.LENGTH_SHORT).show()
            }
        }

        private fun showDialog(title: String, context: Context) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.add_web_info_dialog)
            val body = dialog.findViewById(R.id.topHeader) as TextView
            body.text = title
            val yesBtn = dialog.findViewById(R.id.save) as TextView
            val noBtn = dialog.findViewById(R.id.cancel) as TextView
            yesBtn.setOnClickListener {
                dialog.dismiss()
            }
            noBtn.setOnClickListener { dialog.dismiss() }
            dialog.show()

        }
    }

    override fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filterTicketData = ticketList
                } else {
                    val filteredList: MutableList<TicketSupportData> =
                        java.util.ArrayList<TicketSupportData>()
                    for (row in ticketList) {
                        if (row.subject!!.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    filterTicketData = filteredList as java.util.ArrayList<TicketSupportData>
                }
                val filterResults = FilterResults()
                filterResults.values = filterTicketData
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filterTicketData = filterResults.values as java.util.ArrayList<TicketSupportData>
                notifyDataSetChanged()
            }
        }
    }
}