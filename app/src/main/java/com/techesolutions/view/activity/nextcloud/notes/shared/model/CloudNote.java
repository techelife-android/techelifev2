package com.techesolutions.view.activity.nextcloud.notes.shared.model;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import it.niedermann.android.markdown.MarkdownUtil;

/**
 * CloudNote represents a remote note from an OwnCloud server.
 * It can be directly generated from the JSON answer from the server.
 */
public class CloudNote implements Serializable {
    private long remoteId;
    private String title = "";
    private long modified;
    private String content = "";
    private boolean favorite = false;
    private String category = "";
    private String etag = "";
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public CloudNote(long remoteId, long modified, String title, String content, boolean favorite, String category, String etag) {
        this.remoteId = remoteId;
        setTitle(title);
        setContent(content);
        setFavorite(favorite);
        setCategory(category);
        setEtag(etag);
        this.modified = modified;
    }

    public long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(long remoteId) {
        this.remoteId = remoteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = MarkdownUtil.removeMarkdown(title);
    }

    public long getModified() {
        return modified;
    }

//    public String getModified(String format) {
//        if (modified == null)
//            return null;
//        return new SimpleDateFormat(format, Locale.getDefault()).format(this.getModified().getTimeInMillis());
//    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category == null ? "" : category;
    }

    @NonNull
    @Override
    public String toString() {
        return "R" + getRemoteId() + " " + (isFavorite() ? " (*) " : "     ") + getCategory() + " / " + getTitle() + " / " + getContent() + " (" + getModified() + " / " + getEtag() + ")";
    }
}