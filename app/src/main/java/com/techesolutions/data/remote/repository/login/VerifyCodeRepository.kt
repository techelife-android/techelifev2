package com.techesolutions.data.remote.repository.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.techesolutions.data.remote.model.forgetpassword.CodeVerificationResponse
import com.techesolutions.services.NetworkService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Author: Neelam Upadhyay
 * Created: 1/1/2021
 */
object VerifyCodeRepository {
    val varifyCodeResponse = MutableLiveData<CodeVerificationResponse>()

    fun verifyCode(code: Int, hashcode:String,apporigin:String): MutableLiveData<CodeVerificationResponse> {

        val call = NetworkService.apiInterface.verifyCode(code,hashcode,apporigin)

        call.enqueue(object: Callback<CodeVerificationResponse> {
            override fun onFailure(call: Call<CodeVerificationResponse>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<CodeVerificationResponse>,
                response: Response<CodeVerificationResponse>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()

                val msg = data!!.message
                varifyCodeResponse.postValue(data)
                //loginResponse.value = LoginResponse(data)
                    // loginResponse.value(response.body())
            }


        })

        return varifyCodeResponse
    }

}