package com.techesolutions.data.remote.model.userinfo

data class WebInfoData(
    val id: String,
    val username: String,
    val password: String?,
    val user_id: String,
    val created: String?,
    val updated: String?,
    val type: String?,
    val name: String,
    val url: String
)