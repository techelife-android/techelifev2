package com.techesolutions.services.ble

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattService
import android.content.*
import android.os.Handler
import android.os.IBinder
import android.util.Log
import com.techesolutions.services.ble.BluetoothLeService.Companion.ACTION_BP_START_COMMAND_RECEIVED
import com.techesolutions.services.ble.BluetoothLeService.Companion.ACTION_DATA_AVAILABLE
import com.techesolutions.services.ble.BluetoothLeService.Companion.ACTION_GATT_CONNECTED
import com.techesolutions.services.ble.BluetoothLeService.Companion.ACTION_GATT_DISCONNECTED
import com.techesolutions.services.ble.BluetoothLeService.Companion.ACTION_GATT_SERVICES_DISCOVERED
import com.techesolutions.services.ble.BluetoothLeService.Companion.ACTION_SPO2_DATA_AVAILABLE
import com.techesolutions.services.ble.BluetoothLeService.Companion.EXTRA_DATA
import java.util.*

/**
 * Created by ZXX on 2017/4/28.
 */
class BleController(stateListener: BleController.StateListener) {
    //TAG
    private val TAG = this.javaClass.name
    private var mBtAdapter: BluetoothAdapter? = null
    var mStateListener: BleController.StateListener
    private var mBluetoothLeService: BluetoothLeService? = null
    private var chReceiveData: BluetoothGattCharacteristic? = null
    private var chModifyName: BluetoothGattCharacteristic? = null
    private var chBPSendData: BluetoothGattCharacteristic? = null
    var isConnected = false
        private set
    private var mServiceUUID: UUID? = null
    private var mReadCharacteristicUUID: UUID? = null
    private var mWriteCharacteristicUUID: UUID? = null
    private val mSendCommand = false
    private var scanRunnable: Runnable? = null
    /**
     * Get a Controller
     * @return
     */
    //    public static BleController getDefaultBleController(StateListener stateListener) {
    //        if (mBleController == null) {
    //            mBleController = new BleController(stateListener);
    //        }
    //        return mBleController;
    //    }
    /**
     * init UUIDs service and characteristic
     */
    fun init(
        serviceUUID: UUID?,
        receiveCharacteristicUUID: UUID?,
        writeCharacteristicUUID: UUID?
    ) {
        mServiceUUID = serviceUUID
        mReadCharacteristicUUID = receiveCharacteristicUUID
        mWriteCharacteristicUUID = writeCharacteristicUUID
    }

    /**
     * enable bluetooth adapter
     */
    fun enableBtAdapter() {
        if (!mBtAdapter!!.isEnabled) {
            mBtAdapter!!.enable()
        }
    }

    /**
     * connect the bluetooth device
     * @param device
     */
    fun connect(device: BluetoothDevice) {
        mBluetoothLeService!!.connect(device.address)
    }

    /**
     * Disconnect the bluetooth
     */
    fun disconnect() {
        mBluetoothLeService!!.disconnect()
    }

    // Device scan callback.
    private val mLeScanCallback =
        BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
            stateListener.onFoundDevice(
                device
            )
        }

    /**
     * Scan bluetooth devices
     * @param enable
     */
    private val mHandler: Handler
    fun scanLeDevice(enable: Boolean) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            scanRunnable = Runnable { // do something
                mBtAdapter!!.stopLeScan(mLeScanCallback)
                mStateListener.onScanStop(true)
            }
            mHandler.postDelayed(scanRunnable, 10000)
            mBtAdapter!!.startLeScan(mLeScanCallback)
        } else {
            mBtAdapter!!.stopLeScan(mLeScanCallback)
            mStateListener.onScanStop(false)
        }
    }

    /**
     * stop scan
     */
    fun stopScan() {
        mHandler.removeCallbacks(scanRunnable)
        mBtAdapter!!.stopLeScan(mLeScanCallback)
        mStateListener.onScanStop(false)
    }

    // Code to manage Service lifecycle.
    private val mServiceConnection: ServiceConnection =
        object : ServiceConnection {
            override fun onServiceConnected(
                componentName: ComponentName,
                service: IBinder
            ) {
                mBluetoothLeService = (service as BluetoothLeService.LocalBinder).service
                if (!mBluetoothLeService!!.initialize(mReadCharacteristicUUID)) {
                    Log.e(TAG, "Unable to initialize Bluetooth")
                }
            }

            override fun onServiceDisconnected(componentName: ComponentName) {
                mBluetoothLeService = null
            }
        }

    fun bindService(context: Context) {
        val gattServiceIntent = Intent(
            context,
            BluetoothLeService::class.java
        )
        context.bindService(
            gattServiceIntent,
            mServiceConnection,
            Context.BIND_AUTO_CREATE
        )
    }

    fun unbindService(context: Context) {
        context.unbindService(mServiceConnection)
    }

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private val mGattUpdateReceiver: BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(
                context: Context,
                intent: Intent
            ) {
                val action = intent.action
                if (ACTION_GATT_CONNECTED.equals(action)) {
                    stateListener.onConnected()
                    isConnected = true
                } else if (ACTION_GATT_DISCONNECTED.equals(action)) {
                    stateListener.onDisconnected()
                    chModifyName = null
                    chReceiveData = null
                    chBPSendData = null
                    isConnected = false
                } else if (ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                    // Show all the supported services and characteristics on the user interface.
                    initCharacteristic()
                    stateListener.onServicesDiscovered()
                } else if (ACTION_DATA_AVAILABLE.equals(action)) {
                    Log.e(
                        TAG,
                        "onReceive: " + intent.getStringExtra(EXTRA_DATA)
                    )
                } else if (ACTION_SPO2_DATA_AVAILABLE.equals(action)) {
                    stateListener.onReceiveData(intent.getByteArrayExtra(EXTRA_DATA))
                } else if (ACTION_BP_START_COMMAND_RECEIVED.equals(action)) {
                    initNotification()
                }
            }
        }

    fun initNotification() {
        mBluetoothLeService!!.setCharacteristicNotification(chReceiveData!!, true)
    }

    fun registerBtReceiver(context: Context) {
        context.registerReceiver(
            mGattUpdateReceiver,
            makeGattUpdateIntentFilter()
        )
    }

    fun unregisterBtReceiver(context: Context) {
        context.unregisterReceiver(mGattUpdateReceiver)
    }

    fun initCharacteristic() {
        val services =
            mBluetoothLeService!!.supportedGattServices
        val mInfoService: BluetoothGattService? = null
        var mDataService: BluetoothGattService? = null
        if (services == null) return
        for (service in services) {
            if (service!!.uuid == mServiceUUID) {
                mDataService = service
            }
        }
        if (mDataService != null) {
            val characteristics =
                mDataService.characteristics
            for (ch in characteristics) {
                if (ch.uuid == mReadCharacteristicUUID) {
                    chReceiveData = ch
                }
                if (mWriteCharacteristicUUID != null
                    && ch.uuid == mWriteCharacteristicUUID
                ) {
                    chBPSendData = ch
                }

//                else if(ch.getUuid().equals(Const.UUID_MODIFY_BT_NAME)) {
//                    chModifyName = ch;
//                }
            }
        }
    }

    val isChangeNameAvailable: Boolean
        get() = chModifyName != null

    fun changeBtName(name: String?) {
        if (mBluetoothLeService == null || chModifyName == null) return
        if (name == null || name == "") return
        val b = name.toByteArray()
        val bytes = ByteArray(b.size + 2)
        bytes[0] = 0x00
        bytes[1] = b.size.toByte()
        System.arraycopy(b, 0, bytes, 2, b.size)
        mBluetoothLeService!!.write(chModifyName!!, bytes)
    }

    fun writeCharacteristics(command: ByteArray?) {
        mBluetoothLeService!!.startBPMeasurement(chBPSendData!!, command)
    }

    /**
     * BTController interfaces
     */
    interface StateListener {
        fun onFoundDevice(device: BluetoothDevice?)
        fun onConnected()
        fun onDisconnected()
        fun onReceiveData(dat: ByteArray?)
        fun onServicesDiscovered()
        fun onScanStop(timeout: Boolean)
    }

    companion object {
        private val mBleController: BleController? = null
        private fun makeGattUpdateIntentFilter(): IntentFilter {
            val intentFilter = IntentFilter()
            intentFilter.addAction(ACTION_GATT_CONNECTED)
            intentFilter.addAction(ACTION_GATT_DISCONNECTED)
            intentFilter.addAction(ACTION_GATT_SERVICES_DISCOVERED)
            intentFilter.addAction(ACTION_DATA_AVAILABLE)
            intentFilter.addAction(ACTION_SPO2_DATA_AVAILABLE)
            intentFilter.addAction(ACTION_BP_START_COMMAND_RECEIVED)
            return intentFilter
        }
    }

    init {
        mBtAdapter = BluetoothAdapter.getDefaultAdapter()
        mStateListener = stateListener
        mHandler = Handler()
    }
}
//
//import android.bluetooth.BluetoothAdapter
//import android.bluetooth.BluetoothDevice
//import android.bluetooth.BluetoothGattCharacteristic
//import android.bluetooth.BluetoothGattService
//import android.content.*
//import android.os.Handler
//import android.os.IBinder
//import android.util.Log
//import com.techesolutions.services.ble.BluetoothLeService.LocalBinder
//import java.util.*
//
//
///**
// * Created by Neelam on 16-01-2021.
// */
//class BleController(stateListener: BleController.StateListener) {
//    //TAG
//    private val TAG = this.javaClass.name
//    private var mBtAdapter: BluetoothAdapter? = null
//
//    var mStateListener: StateListener? =
//        null
//    private var mBluetoothLeService: BluetoothLeService? = null
//    private var chReceiveData: BluetoothGattCharacteristic? = null
//    private var chModifyName: BluetoothGattCharacteristic? = null
//    private var chBPSendData: BluetoothGattCharacteristic? = null
//    var isConnected = false
//        private set
//    private var mServiceUUID: UUID? = null
//    private var mReadCharacteristicUUID: UUID? = null
//    private var mWriteCharacteristicUUID: UUID? = null
//    private val mSendCommand = false
//    private var scanRunnable: Runnable? = null
//    /**
//     * Get a Controller
//     * @return
//     */
//    //    public static BleController getDefaultBleController(StateListener stateListener) {
//    //        if (mBleController == null) {
//    //            mBleController = new BleController(stateListener);
//    //        }
//    //        return mBleController;
//    //    }
//    /**
//     * init UUIDs service and characteristic
//     */
//    fun init(
//        serviceUUID: UUID?,
//        receiveCharacteristicUUID: UUID?,
//        writeCharacteristicUUID: UUID?
//    ) {
//        mServiceUUID = serviceUUID
//        mReadCharacteristicUUID = receiveCharacteristicUUID
//        mWriteCharacteristicUUID = writeCharacteristicUUID
//    }
//
//    /**
//     * enable bluetooth adapter
//     */
//    fun enableBtAdapter() {
//        if (!mBtAdapter!!.isEnabled) {
//            mBtAdapter!!.enable()
//        }
//    }
//
//    /**
//     * connect the bluetooth device
//     * @param device
//     */
//    fun connect(device: BluetoothDevice) {
//        mBluetoothLeService!!.connect(device.address)
//    }
//
//    /**
//     * Disconnect the bluetooth
//     */
//    fun disconnect() {
//        mBluetoothLeService!!.disconnect()
//    }
//
//    // Device scan callback.
//    private val mLeScanCallback =
//        BluetoothAdapter.LeScanCallback { device, rssi, scanRecord ->
//            mStateListener?.onFoundDevice(
//                device
//            )
//        }
//
//    /**
//     * Scan bluetooth devices
//     * @param enable
//     */
//    private val mHandler: Handler
//    fun scanLeDevice(enable: Boolean) {
//        if (enable) {
//            // Stops scanning after a pre-defined scan period.
//            scanRunnable = Runnable { // do something
//                mBtAdapter!!.stopLeScan(mLeScanCallback)
//                mStateListener?.onScanStop(true)
//            }
//            mHandler.postDelayed(scanRunnable, 10000)
//            mBtAdapter!!.startLeScan(mLeScanCallback)
//        } else {
//            mBtAdapter!!.stopLeScan(mLeScanCallback)
//            mStateListener?.onScanStop(false)
//        }
//    }
//
//    /**
//     * stop scan
//     */
//    fun stopScan() {
//        mHandler.removeCallbacks(scanRunnable)
//        mBtAdapter!!.stopLeScan(mLeScanCallback)
//        mStateListener?.onScanStop(false)
//    }
//
//    // Code to manage Service lifecycle.
//    private val mServiceConnection: ServiceConnection =
//        object : ServiceConnection {
//            override fun onServiceConnected(
//                componentName: ComponentName,
//                service: IBinder
//            ) {
//                mBluetoothLeService = (service as LocalBinder).getService()
//                if (!mBluetoothLeService!!.initialize(mReadCharacteristicUUID)) {
//                    Log.e(TAG, "Unable to initialize Bluetooth")
//                }
//            }
//
//            override fun onServiceDisconnected(componentName: ComponentName) {
//                mBluetoothLeService = null
//            }
//        }
//
//    fun bindService(context: Context) {
//        val gattServiceIntent = Intent(
//            context,
//            BluetoothLeService::class.java
//        )
//        context.bindService(
//            gattServiceIntent,
//            mServiceConnection,
//            Context.BIND_AUTO_CREATE
//        )
//    }
//
//    fun unbindService(context: Context) {
//        context.unbindService(mServiceConnection)
//    }
//
//    // Handles various events fired by the Service.
//    // ACTION_GATT_CONNECTED: connected to a GATT server.
//    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
//    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
//    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
//    //                        or notification operations.
//    private val mGattUpdateReceiver: BroadcastReceiver =
//        object : BroadcastReceiver() {
//            override fun onReceive(
//                context: Context,
//                intent: Intent
//            ) {
//                val action = intent.action
//                if (ACTION_GATT_CONNECTED.equals(action)) {
//                    mStateListener?.onConnected()
//                    isConnected = true
//                } else if (ACTION_GATT_DISCONNECTED.equals(action)) {
//                    mStateListener?.onDisconnected()
//                    chModifyName = null
//                    chReceiveData = null
//                    chBPSendData = null
//                    isConnected = false
//                } else if (ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
//                    // Show all the supported services and characteristics on the user interface.
//                    initCharacteristic()
//                    mStateListener?.onServicesDiscovered()
//                } else if (ACTION_DATA_AVAILABLE.equals(action)) {
//                    Log.e(
//                        TAG,
//                        "onReceive: " + intent.getStringExtra(EXTRA_DATA)
//                    )
//                } else if (ACTION_SPO2_DATA_AVAILABLE.equals(action)) {
//                    mStateListener?.onReceiveData(intent.getByteArrayExtra(EXTRA_DATA))
//                } else if (ACTION_BP_START_COMMAND_RECEIVED.equals(action)) {
//                    initNotification()
//                }
//            }
//        }
//
//    fun initNotification() {
//        mBluetoothLeService!!.setCharacteristicNotification(chReceiveData!!, true)
//    }
//
//    fun registerBtReceiver(context: Context) {
//        context.registerReceiver(
//            mGattUpdateReceiver,
//            makeGattUpdateIntentFilter()
//        )
//    }
//
//    fun unregisterBtReceiver(context: Context) {
//        context.unregisterReceiver(mGattUpdateReceiver)
//    }
//
//    fun initCharacteristic() {
//        val services =
//            mBluetoothLeService!!.getSupportedGattServices()
//        val mInfoService: BluetoothGattService? = null
//        var mDataService: BluetoothGattService? = null
//        if (services == null) return
//        for (service in services) {
//            if (service!!.uuid == mServiceUUID) {
//                mDataService = service
//            }
//        }
//        if (mDataService != null) {
//            val characteristics =
//                mDataService.characteristics
//            for (ch in characteristics) {
//                if (ch.uuid == mReadCharacteristicUUID) {
//                    chReceiveData = ch
//                }
//                if (mWriteCharacteristicUUID != null
//                    && ch.uuid == mWriteCharacteristicUUID
//                ) {
//                    chBPSendData = ch
//                }
//
////                else if(ch.getUuid().equals(Const.UUID_MODIFY_BT_NAME)) {
////                    chModifyName = ch;
////                }
//            }
//        }
//    }
//
//    val isChangeNameAvailable: Boolean
//        get() = chModifyName != null
//
//    fun changeBtName(name: String?) {
//        if (mBluetoothLeService == null || chModifyName == null) return
//        if (name == null || name == "") return
//        val b = name.toByteArray()
//        val bytes = ByteArray(b.size + 2)
//        bytes[0] = 0x00
//        bytes[1] = b.size.toByte()
//        System.arraycopy(b, 0, bytes, 2, b.size)
//        mBluetoothLeService!!.write(chModifyName!!, bytes)
//    }
//
//    fun writeCharacteristics(command: ByteArray?) {
//        mBluetoothLeService!!.startBPMeasurement(chBPSendData!!, command)
//    }
//
//    /**
//     * BTController interfaces
//     */
//    interface StateListener {
//        fun onFoundDevice(device: BluetoothDevice?)
//        fun onConnected()
//        fun onDisconnected()
//        fun onReceiveData(dat: ByteArray?)
//        fun onServicesDiscovered()
//        fun onScanStop(timeout: Boolean)
//    }
//
//    companion object {
//        private val mBleController: BleController? = null
//        private fun makeGattUpdateIntentFilter(): IntentFilter {
//            val intentFilter = IntentFilter()
//            intentFilter.addAction(ACTION_GATT_CONNECTED)
//            intentFilter.addAction(ACTION_GATT_DISCONNECTED)
//            intentFilter.addAction(ACTION_GATT_SERVICES_DISCOVERED)
//            intentFilter.addAction(ACTION_DATA_AVAILABLE)
//            intentFilter.addAction(ACTION_SPO2_DATA_AVAILABLE)
//            intentFilter.addAction(ACTION_BP_START_COMMAND_RECEIVED)
//            return intentFilter
//        }
//    }
//
//    init {
//        mBtAdapter = BluetoothAdapter.getDefaultAdapter()
//        mStateListener = stateListener
//        mHandler = Handler()
//    }
//}
//
////
////import android.bluetooth.BluetoothAdapter
////import android.bluetooth.BluetoothAdapter.LeScanCallback
////import android.bluetooth.BluetoothDevice
////import android.bluetooth.BluetoothGattCharacteristic
////import android.bluetooth.BluetoothGattService
////import android.content.*
////import android.os.Handler
////import android.os.IBinder
////import android.util.Log
////import java.util.*
////
/////**
//// * Created by ZXX on 2017/4/28.
//// */
////private var mServiceUUID: UUID? = null
////private var mReadCharacteristicUUID: UUID? = null
////private var mWriteCharacteristicUUID: UUID? = null
////
////public class BleController {
////    //TAG
////    private val TAG = this.javaClass.name
////    private var mBtAdapter: BluetoothAdapter? = null
////    var mStateListener: StateListener
////    private var mBluetoothLeService: BluetoothLeService? = null
////    private var chReceiveData: BluetoothGattCharacteristic? = null
////    private var chModifyName: BluetoothGattCharacteristic? = null
////    private var chBPSendData: BluetoothGattCharacteristic? = null
////    var isConnected = false
////        private set
////        private val mSendCommand = false
////    private var scanRunnable: Runnable? = null
////
////    fun BleController(stateListener: StateListener) {
////        mBtAdapter = BluetoothAdapter.getDefaultAdapter()
////        mStateListener = stateListener
////        mHandler = Handler()
////    }
////    /**
////     * Get a Controller
////     * @return
////     */
////    //    public static BleController getDefaultBleController(StateListener stateListener) {
////    //        if (mBleController == null) {
////    //            mBleController = new BleController(stateListener);
////    //        }
////    //        return mBleController;
////    //    }
////    /**
////     * init UUIDs service and characteristic
////     */
////    fun init(
////        serviceUUID: UUID?,
////        receiveCharacteristicUUID: UUID?,
////        writeCharacteristicUUID: UUID?
////    ) {
////        mServiceUUID = serviceUUID
////        mReadCharacteristicUUID = receiveCharacteristicUUID
////        mWriteCharacteristicUUID = writeCharacteristicUUID
////    }
////
////    /**
////     * enable bluetooth adapter
////     */
////    fun enableBtAdapter() {
////        if (!mBtAdapter!!.isEnabled) {
////            mBtAdapter!!.enable()
////        }
////    }
////
////    /**
////     * connect the bluetooth device
////     * @param device
////     */
////    fun connect(device: BluetoothDevice) {
////        mBluetoothLeService!!.connect(device.address)
////    }
////
////    /**
////     * Disconnect the bluetooth
////     */
////    fun disconnect() {
////        mBluetoothLeService!!.disconnect()
////    }
////
////    // Device scan callback.
////    private val mLeScanCallback =
////        LeScanCallback { device, rssi, scanRecord -> mStateListener.onFoundDevice(device) }
////
////    /**
////     * Scan bluetooth devices
////     * @param enable
////     */
////    private var mHandler: Handler
////    fun scanLeDevice(enable: Boolean) {
////        if (enable) {
////            // Stops scanning after a pre-defined scan period.
////            scanRunnable = Runnable { // do something
////                mBtAdapter!!.stopLeScan(mLeScanCallback)
////                mStateListener.onScanStop(true)
////            }
////            mHandler.postDelayed(scanRunnable, 10000)
////            mBtAdapter!!.startLeScan(mLeScanCallback)
////        } else {
////            mBtAdapter!!.stopLeScan(mLeScanCallback)
////            mStateListener.onScanStop(false)
////        }
////    }
////
////    /**
////     * stop scan
////     */
////    fun stopScan() {
////        mHandler.removeCallbacks(scanRunnable)
////        mBtAdapter!!.stopLeScan(mLeScanCallback)
////        mStateListener.onScanStop(false)
////    }
////
////    // Code to manage Service lifecycle.
////    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
////        override fun onServiceConnected(
////            componentName: ComponentName,
////            service: IBinder
////        ) {
////            mBluetoothLeService = (service as BluetoothLeService.LocalBinder).getService()
////            if (!mBluetoothLeService!!.initialize(mReadCharacteristicUUID)) {
////                Log.e(TAG, "Unable to initialize Bluetooth")
////            }
////        }
////
////        override fun onServiceDisconnected(componentName: ComponentName) {
////            mBluetoothLeService = null
////        }
////    }
////
////    fun bindService(context: Context) {
////        val gattServiceIntent = Intent(context, BluetoothLeService::class.java)
////        context.bindService(
////            gattServiceIntent,
////            mServiceConnection,
////            Context.BIND_AUTO_CREATE
////        )
////    }
////
////    fun unbindService(context: Context) {
////        context.unbindService(mServiceConnection)
////    }
////
////    // Handles various events fired by the Service.
////    // ACTION_GATT_CONNECTED: connected to a GATT server.
////    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
////    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
////    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
////    //                        or notification operations.
////    private val mGattUpdateReceiver: BroadcastReceiver = object : BroadcastReceiver() {
////        override fun onReceive(
////            context: Context,
////            intent: Intent
////        ) {
////            val action = intent.action
////            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
////                mStateListener.onConnected()
////                isConnected = true
////            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
////                mStateListener.onDisconnected()
////                chModifyName = null
////                chReceiveData = null
////                chBPSendData = null
////                isConnected = false
////            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
////                // Show all the supported services and characteristics on the user interface.
////                initCharacteristic()
////                mStateListener.onServicesDiscovered()
////            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
////                Log.e(
////                    TAG,
////                    "onReceive: " + intent.getStringExtra(BluetoothLeService.EXTRA_DATA)
////                )
////            } else if (BluetoothLeService.ACTION_SPO2_DATA_AVAILABLE.equals(action)) {
////                mStateListener.onReceiveData(intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA))
////            } else if (BluetoothLeService.ACTION_BP_START_COMMAND_RECEIVED.equals(action)) {
////                initNotification()
////            }
////        }
////    }
////
////    fun initNotification() {
////        chReceiveData?.let { mBluetoothLeService!!.setCharacteristicNotification(it, true) }
////    }
////
////    fun registerBtReceiver(context: Context) {
////        context.registerReceiver(
////            mGattUpdateReceiver,
////            makeGattUpdateIntentFilter()
////        )
////    }
////
////    fun unregisterBtReceiver(context: Context) {
////        context.unregisterReceiver(mGattUpdateReceiver)
////    }
////
////    fun initCharacteristic() {
////        val services: List<BluetoothGattService> =
////            mBluetoothLeService!!.getSupportedGattServices() as List<BluetoothGattService>
////        val mInfoService: BluetoothGattService? = null
////        var mDataService: BluetoothGattService? = null
////        if (services == null) return
////        for (service in services) {
////            if (service.uuid == mServiceUUID) {
////                mDataService = service
////            }
////        }
////        if (mDataService != null) {
////            val characteristics =
////                mDataService.characteristics
////            for (ch in characteristics) {
////                if (ch.uuid == mReadCharacteristicUUID) {
////                    chReceiveData = ch
////                }
////                if (mWriteCharacteristicUUID != null
////                    && ch.uuid == mWriteCharacteristicUUID
////                ) {
////                    chBPSendData = ch
////                }
////
//////                else if(ch.getUuid().equals(Const.UUID_MODIFY_BT_NAME)) {
//////                    chModifyName = ch;
//////                }
////            }
////        }
////    }
////
////    val isChangeNameAvailable: Boolean
////        get() = chModifyName != null
////
////    fun changeBtName(name: String?) {
////        if (mBluetoothLeService == null || chModifyName == null) return
////        if (name == null || name == "") return
////        val b = name.toByteArray()
////        val bytes = ByteArray(b.size + 2)
////        bytes[0] = 0x00
////        bytes[1] = b.size.toByte()
////        System.arraycopy(b, 0, bytes, 2, b.size)
////        mBluetoothLeService!!.write(chModifyName!!, bytes)
////    }
////
////    fun writeCharacteristics(command: ByteArray?) {
////        chBPSendData?.let { mBluetoothLeService!!.startBPMeasurement(it, command) }
////    }
////
////    /**
////     * BTController interfaces
////     */
////    interface StateListener {
////        fun onFoundDevice(device: BluetoothDevice?)
////        fun onConnected()
////        fun onDisconnected()
////        fun onReceiveData(dat: ByteArray?)
////        fun onServicesDiscovered()
////        fun onScanStop(timeout: Boolean)
////    }
////
////    companion object {
////        private val mBleController: BleController? =
////            null
////
////        private fun makeGattUpdateIntentFilter(): IntentFilter {
////            val intentFilter = IntentFilter()
////            intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED)
////            intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED)
////            intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED)
////            intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE)
////            intentFilter.addAction(BluetoothLeService.ACTION_SPO2_DATA_AVAILABLE)
////            intentFilter.addAction(BluetoothLeService.ACTION_BP_START_COMMAND_RECEIVED)
////            return intentFilter
////        }
////    }
////
////    init {
////        mBtAdapter = BluetoothAdapter.getDefaultAdapter()
////        mStateListener = stateListener
////        mHandler = Handler()
////    }
////}