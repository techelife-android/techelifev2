package com.techesolutions.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.devices.DeviceData
import java.util.*

class DeviceAdapter(
    context:Context,
    val  userList: ArrayList<DeviceData>
) : RecyclerView.Adapter<DeviceAdapter.ViewHolder>() {
    var activity:Context = context

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.asigned_devices_items, parent, false)
        return ViewHolder(v, activity)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: DeviceAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position],activity)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(data: DeviceData, context: Context) {

            val deviceName = itemView.findViewById(R.id.deviceName) as TextView
            val tvSkuNo  = itemView.findViewById(R.id.tvSkuNo) as TextView
            val tvID  = itemView.findViewById(R.id.tvID) as TextView
            val tvRoom  = itemView.findViewById(R.id.tvRoom) as TextView
            val device_icon  = itemView.findViewById(R.id.device_icon) as ImageView
            deviceName.text = data.skuname
            tvSkuNo.text = data.skuno
            tvID.text = data.deviceid
            tvRoom.text = data.room_label

            if(data.sku_category.equals("MASTER_MOTE"))
            device_icon.setImageResource(R.drawable.ic_sensor)

            if(data.sku_category.equals("DOOR"))
             device_icon.setImageResource(R.drawable.ic_smart_door)

            if(data.sku_category.equals("HUB"))
                device_icon.setImageResource(R.drawable.ic_network_hub)

            if(data.sku_category.equals("VITAL"))
                device_icon.setImageResource(R.drawable.ic_vitals)

            if(data.sku_category.equals("CAMERA"))
                device_icon.setImageResource(R.drawable.ic_cameras)

            if(data.sku_category.equals("STORAGE"))
                device_icon.setImageResource(R.drawable.ic_data_storage)

            if(data.sku_category.equals("ROUTER"))
                device_icon.setImageResource(R.drawable.ic_router)

            if(data.sku_category.equals("LAPTOP"))
                device_icon.setImageResource(R.drawable.ic_laptop)

            if(data.sku_category.equals("TABLET"))
                device_icon.setImageResource(R.drawable.ic_tablet)

            if(data.sku_category.equals("PHONE"))
                device_icon.setImageResource(R.drawable.ic_tablet)

            if(data.sku_category.equals("RANGE_EXTENDER"))
                device_icon.setImageResource(R.drawable.ic_range)

            if(data.sku_category.equals("OTHER"))
                device_icon.setImageResource(R.drawable.ic_thermameter)
        }
    }

}