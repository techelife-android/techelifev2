package com.techesolutions.data.remote.model.iot

data class IOTDoorsReadings(
    val labelRoom: String,
    val labelDoor: String,
    val door_status: Int,
    val entry_timeW: String
)