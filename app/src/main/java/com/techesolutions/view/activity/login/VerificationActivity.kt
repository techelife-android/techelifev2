package com.techesolutions.view.activity.login

import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.viewmodel.VerifyCodeViewModel
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject


/**
 * Created by Neelam on 23-12-2020.
 */
class VerificationActivity : AppCompatActivity(), View.OnClickListener {
    var btConfirm: TextView? = null
    var verification_note: TextView? = null
    var enterCodeLabel: TextView? = null
    var heading: TextView? = null
    var back:ImageView?=null
    var etCode1: EditText? = null
    var etCode2: EditText? = null
    var etCode3: EditText? = null
    var etCode4: EditText? = null
    lateinit var verifyCodeViewModel: VerifyCodeViewModel
    private var languageData: String? = null
    var jsonValidations: JSONObject?=null
    var hashcode: String = ""
    private val progressDialog = CustomProgressDialog()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_verification)
        headerTitle.visibility = View.GONE
        verifyCodeViewModel = ViewModelProvider(this).get(VerifyCodeViewModel::class.java)
        hashcode = intent.getStringExtra("hashcode")
        back=findViewById<View>(R.id.back) as ImageView
        back?.setVisibility(View.VISIBLE)
        back?.setOnClickListener(this)
        verification_note = findViewById<View>(R.id.verification_note) as TextView
        enterCodeLabel = findViewById<View>(R.id.enterCodeText) as TextView
        heading = findViewById<View>(R.id.heading) as TextView
        btConfirm = findViewById<View>(R.id.btConfirm) as TextView
        etCode1 = findViewById<View>(R.id.etCode1) as EditText
        etCode2 = findViewById<View>(R.id.etCode2) as EditText
        etCode3 = findViewById<View>(R.id.etCode3) as EditText
        etCode4 = findViewById<View>(R.id.etCode4) as EditText
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if(name.equals("forgotPasswordVerify")) {
                    val jsonObjectLabels = jsonObject.getJSONObject("labels")
                    verification_note?.text=jsonObjectLabels.getString("enter_security_code").toString()
                    enterCodeLabel?.text=jsonObjectLabels.getString("enter_code").toString()
                    heading?.text=jsonObjectLabels.getString("verification").toString()
                    btConfirm?.text=jsonObjectLabels.getString("confirm").toString()
                    jsonValidations=jsonObjectLabels.getJSONObject("validations")
                    Log.v("DEBUG : ", jsonObjectLabels.toString())

                }
            }

        } catch (e: Exception) {
        }
        etCode1!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
                //
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun afterTextChanged(editable: Editable) {
                //
                if (editable.length > 0) {
                    etCode1!!.clearFocus()
                    etCode2!!.requestFocus()
                    etCode2!!.setCursorVisible(true)
                }
            }
        })
        etCode2!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
                //
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun afterTextChanged(editable: Editable) {
                //
                if (editable.length > 0) {
                    etCode2!!.clearFocus()
                    etCode3!!.requestFocus()
                    etCode3!!.setCursorVisible(true)
                }
            }
        })
        etCode3!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
                //
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
            }

            override fun afterTextChanged(editable: Editable) {
                //
                if (editable.length > 0) {
                    etCode3!!.clearFocus()
                    etCode4!!.requestFocus()
                    etCode4!!.setCursorVisible(true)
                }
            }
        })
        btConfirm?.setOnClickListener(this)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btConfirm -> {
                setValidation()
            }
            R.id.back -> {
                finish()
            }
        }
    }

    private fun setValidation() {
        if (TextUtils.isEmpty(etCode1!!.text.toString()) || TextUtils.isEmpty(etCode2!!.text.toString()) || TextUtils.isEmpty(
                etCode3!!.text.toString()
            ) || TextUtils.isEmpty(etCode4!!.text.toString())
        ) {
            if(jsonValidations!=null && jsonValidations!!.length()>0)
                onErrorListener(jsonValidations!!.getString("email_not_empty").toString())
            else
            onErrorListener(getString(R.string.wrong_verification_code))
        } else {
            if (Utils.isOnline(this@VerificationActivity)) {
                showLoading("Please wait")
                val sb = StringBuilder()
                sb.append(etCode1!!.text.toString());
                sb.append(etCode2!!.text.toString());
                sb.append(etCode3!!.text.toString());
                sb.append(etCode4!!.text.toString());
               // Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show()
                verifyCodeViewModel.verifyCode(Integer.parseInt(sb.toString()), hashcode,"mobile")!!
                    .observe(this, Observer { baseResponse ->

                        val success = baseResponse.success;
                        if (success == true) {closeLoading()
                            val intent = Intent(this@VerificationActivity,
                                ResetPasswordActivity::class.java)
                            intent.putExtra("hashcode",hashcode)
                            startActivity(intent)

                            finish()
                        } else {
                            closeLoading()
                            Toast.makeText(this, baseResponse.message, Toast.LENGTH_SHORT).show()
                        }
                            //Toast.makeText(this, baseResponse.message, Toast.LENGTH_SHORT).show()
                    })
            }

        }
    }


    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

}