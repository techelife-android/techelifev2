package com.techesolutions.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.timezones.TimeZoneModel
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.MySharedPreference
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.NextCloudDashboardActivity
import com.techesolutions.view.adapter.TimezoneAdapter
import com.techesolutions.view.callbacks.ItemClickListener
import kotlinx.android.synthetic.main.activity_select_timezone.*
import okhttp3.ResponseBody
import org.joda.time.DateTimeUtils
import org.joda.time.DateTimeZone
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class TimezoneSelectionActivity : AppCompatActivity() , View.OnClickListener,ItemClickListener{
    private var dataModel: ArrayList<TimeZoneModel>? = null
    private lateinit var spinner: Spinner
    private val progressDialog = CustomProgressDialog()
    var timezones=""
    var timezoneLocal=""
    var differentTime=""
    var nextCloudEnable=""
    private var languageData: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_timezone)
        spinner = findViewById<View>(R.id.spinner) as Spinner
        val shared =
          getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        nextCloudEnable = shared.getString(Constants.KEY_NEXT_CLOUD, "").toString()
        languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("login")) {
                    val jsonObjectLabels = jsonObject.getJSONObject("labels")
                    headerTitle?.text = jsonObjectLabels.getString("timezoneTitle").toString()
                    timezonetext?.text = jsonObjectLabels.getString("mytimezone").toString()
                    differentTime= jsonObjectLabels.getString("timezoneMSG").toString()

                }
            }

        } catch (e: Exception) {
        }
        //timeTV!!.text=shared.getString(Constants.KEY_USER_TIME_ZONE,"")
        val dz = DateTimeZone.forID(TimeZone.getDefault().id)
        val loacltimezone = dz.getNameKey(DateTimeUtils.currentTimeMillis())
        // differentTime=differentTime.replace("(0)",shared.getString(Constants.KEY_USER_TIME_ZONE,""))
        differentTimes!!.text="Your timezone is "+loacltimezone+", But profile timezone is "+shared.getString(Constants.KEY_USER_TIME_ZONE, "")+". Do you want to change it?"
        timezones = shared.getString(Constants.KEY_TIME_ZONES, "").toString()
        var dlist = JSONArray(timezones)
        dataModel = ArrayList<TimeZoneModel>()
        // val vital = jsonObjectLabels.getString("Vital").toString()
        for (i in 0 until dlist.length()) {
            val item = dlist.getJSONObject(i)
            val shortname = item.optString("timezone")
            val fulltimezone = item.optString("timezone_text")
            val country = item.optString("country")
            dataModel!!.add(TimeZoneModel(fulltimezone, shortname, country, false))
        }
        dataModel!!.add(0, TimeZoneModel("Select", "Select", "", false))
        val customDropDownAdapter = TimezoneAdapter(this, dataModel!!, this)
        spinner.adapter = customDropDownAdapter
        ////spinner.setSelection(customDropDownAdapter.getItemIndexById(dataModel!!.GetCountryId()));
        var position=0
        var profileTimezone=shared.getString(Constants.KEY_USER_TIME_ZONE, "")
        Log.i("profileTimezone",profileTimezone)
        for (i in dataModel!!.indices) {
            if (dataModel!![i].short_name.equals(profileTimezone)) {
                Log.i("profileTimezone",""+i)
                position = i
            }
        }
        Log.i("profileTimezone",""+position)

        spinner!!.setSelection(position)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
            ) {
                 // get selected item text
                if(position>0) {
                   // Toast.makeText(this@TimezoneSelectionActivity, "" + dataModel!![position].name, Toast.LENGTH_SHORT).show()
                    timezoneLocal = dataModel!![position].short_name.toString()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }
        var timezone=shared.getString(Constants.KEY_USER_TIME_ZONE, "")
        if (timezone!= null) {
            //val spinnerPosition = adapter.getPosition(timezone)
            //spinner.setSelection(spinnerPosition)

        }

        save.setOnClickListener(this)
        cancel.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
      when(v!!.id)
      {
          R.id.save -> {
              if (timezoneLocal.length < 1) {
                  onErrorListener(getString(R.string.select_timezone))

              } else {
//                  MySharedPreference.setPreference(this, Constants.KEY_LANGUAGE, language)
//                  setLanguageAPI(language)
                  setTimezoneAPI(timezoneLocal)
              }
          }
          R.id.cancel -> {
              if (nextCloudEnable.equals("1")) {
                  val intent = Intent(this@TimezoneSelectionActivity, NextCloudDashboardActivity::class.java)
                  intent.putExtra("from", "splash1")
                  startActivity(intent)
                  finish();
              }
              else {
                  val intent = Intent(this@TimezoneSelectionActivity, DashboardActivity::class.java)
                  intent.putExtra("from", "timezone")
                  startActivity(intent)
                  finish()
              }
          }
      }


    }
    private fun setTimezoneAPI(timezone: String) {
        showLoading("Please wait")
        val shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.setTimezone("Bearer $token", "mobile", userId, timezone)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {

                        // val data = jsonObj!!.getJSONObject("data")
                        MySharedPreference.setPreference(this@TimezoneSelectionActivity, Constants.KEY_MOBILE_TIME_ZONE, timezone)
                        closeLoading()
                        if (nextCloudEnable.equals("1")) {
                            val intent = Intent(this@TimezoneSelectionActivity, NextCloudDashboardActivity::class.java)
                            intent.putExtra("from", "splash1")
                            startActivity(intent)
                            finish();
                        }
                        else {
                            val intent = Intent(this@TimezoneSelectionActivity, DashboardActivity::class.java)
                            intent.putExtra("from", "timezone")
                            startActivity(intent)
                            finish()
                        }
                    } else {
                        Toast.makeText(this@TimezoneSelectionActivity, "Some error. Try again.", Toast.LENGTH_SHORT).show()
                    }
                }


            })
        }


}

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {
       Toast.makeText(this, dataModel!![adapterPosition].short_name, Toast.LENGTH_SHORT).show()
        timezoneLocal= dataModel!![adapterPosition].short_name.toString()
    }


}