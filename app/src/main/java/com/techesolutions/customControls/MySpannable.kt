package com.techesolutions.customControls

import android.content.Context
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import com.techesolutions.R

/**
 * Created by Neelam on 20-12-2020.
 */
class MySpannable(context: Context, isUnderline: Boolean) :
    ClickableSpan() {
    private var isUnderline = false
    private val context: Context
    override fun updateDrawState(ds: TextPaint) {
        ds.isUnderlineText = isUnderline
        ds.color = context.resources.getColor(R.color.colorPrimaryDark)
        //ds.setColor(Color.parseColor("#0abaae"));
    }

    override fun onClick(widget: View) {}

    /**
     * Constructor
     */
    init {
        this.isUnderline = isUnderline
        this.context = context
    }
}
