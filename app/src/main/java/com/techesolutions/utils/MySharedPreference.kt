package com.techesolutions.utils

import android.content.Context

/**
 * Created by Neelam on 19-12-2020.
 */
object MySharedPreference {
    var mContext: Context? = null
    fun setPreference(
        context: Context?,
        key: String?,
        value: String
    ) {
        mContext = context
        val editor = mContext!!.getSharedPreferences(
            "TechePrefs",
            Context.MODE_PRIVATE
        ).edit()
        editor.putString(key, value)
        editor.commit()
    }

    fun getPreferences(context: Context?, key: String?): String? {
        mContext = context
        val prefs = mContext!!.getSharedPreferences(
            "MyPrefs",
            Context.MODE_PRIVATE
        )
        return prefs.getString(key, "")
    }

    fun setBooleanPreference(
        context: Context?,
        key: String?,
        value: Boolean
    ) {
        mContext = context
        val editor = mContext!!.getSharedPreferences(
            "TechePrefs",
            Context.MODE_PRIVATE
        ).edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getBooleanPreferences(context: Context?, key: String?): Boolean? {
        mContext = context
        val prefs = mContext!!.getSharedPreferences(
            "MyPrefs",
            Context.MODE_PRIVATE
        )
        return prefs.getBoolean(key, false)
    }
}
