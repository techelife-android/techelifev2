package com.techesolutions.view.activity.nextcloud.events

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.UploadCalenderRemoteOperation
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.CalendarMode
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.timezones.TimeZoneModel
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.events.decorators.MySelectorDecorator
import com.techesolutions.view.activity.nextcloud.events.decorators.OneDayDecorator
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils
import com.techesolutions.view.adapter.EventAlertAdapter
import com.techesolutions.view.adapter.TimezoneAdapter
import com.techesolutions.view.callbacks.ItemClickListener
import kotlinx.android.synthetic.main.activity_create_event.*
import kotlinx.android.synthetic.main.activity_create_event.cancel
import kotlinx.android.synthetic.main.activity_create_event.save
import kotlinx.android.synthetic.main.layout_header.*
import org.joda.time.DateTimeZone
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class CreateEventActivity : AppCompatActivity(), OnDateSelectedListener, View.OnClickListener,
    OnRemoteOperationListener , ItemClickListener {
    private var widget: MaterialCalendarView? = null
    private val progressDialog = CustomProgressDialog()
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    val DEBUG = true //BuildConfig.DEBUG;
    var selectedDate: String = ""
    var selectedStartDate: String = ""
    var timestamp: String = ""
    var startDate: Date? = null
    var action: String? = null
    var from: String? = null
    var category: String? = null
    var desc: String? = null
    var title: String? = null
    var name: String? = null
    var user: String? = null
    var eventStartTime: String? = null
    var eventEndTime: String? = null
    var fileName: String? = null
    var min: Int=-1
    private val FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    private val oneDayDecorator: OneDayDecorator = OneDayDecorator()
    private var alertModel: ArrayList<EventsAlertTimes>? = null
    var remotePath:String? = null
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_event)
        headerTitle.text = "Create Event"

        back.setOnClickListener(this)
        monthView?.setOnClickListener(this)
        weekView?.setOnClickListener(this)
        cancel?.setOnClickListener(this)
        etTimeFrom?.setOnClickListener(this)
        etTimeTo?.setOnClickListener(this)
        save?.setOnClickListener(this)
        //Toast.makeText(this@CreateEventActivity,UUID.randomUUID().toString(), Toast.LENGTH_SHORT).show()

        mHandler = Handler()
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String? = shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user = shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String? = shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", url + ", " + user + ", " + pass)
        var serverUri: Uri? = null
        if (url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url.length - 1))
        } else {
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
                pass!!
            )
        )


        
        // Gets the calendar from the view
        widget = findViewById(R.id.calendarView)
        widget!!.setOnDateChangedListener(this)
        //widget!!.setShowOtherDates(MaterialCalendarView.SHOW_ALL)

        val instance: LocalDate = LocalDate.now()
        widget!!.setSelectedDate(instance)

        val min: LocalDate =
            LocalDate.of(instance.getYear(), org.threeten.bp.Month.JANUARY, 1)
        val max: LocalDate =
            LocalDate.of(instance.getYear(), org.threeten.bp.Month.DECEMBER, 31)

        widget!!.state().edit().setMinimumDate(min).setMaximumDate(max).commit()

        widget!!.addDecorators(
            MySelectorDecorator(this),
            oneDayDecorator
        )

        // Extract the day from the text
        val calendar = Calendar.getInstance()
        val currentDate: Date = calendar.getTime()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        // val sdf = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'")
        val i: Intent = getIntent()
        category = i.getStringExtra("category")
        from = i.getStringExtra("from")
        action = i.getStringExtra("action")
        name = i.getStringExtra("name")
        //set alert times
        if (name.equals("VTODO")) {
            alertLL!!.visibility = View.GONE
        }
        else{
            alertLL!!.visibility = View.VISIBLE
        }
        setAlert()
        if (action.equals("edit")) {
            headerTitle.text = "Edit Event"
            if (i.getStringExtra("stime").length > 8) {
                selectedStartDate = Utils.convertEventDate(
                    i.getStringExtra("stime"),
                    this@CreateEventActivity
                )
            } else {
                if (i.getStringExtra("stime").length > 1) {
                    selectedStartDate = Utils.convertEventEditOnlyDateFormat(
                        i.getStringExtra("stime"),
                        this@CreateEventActivity
                    )
                } else {
                    selectedStartDate = sdf.format(currentDate)
                }
            }
            launchExistMode()
        } else {
            selectedStartDate = sdf.format(currentDate)
        }
        // Toast.makeText(this, "Current Day: $selectedStartDate", Toast.LENGTH_SHORT).show()

        val sdfTimestamp = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'")
        timestamp = sdfTimestamp.format(currentDate)
        // Toast.makeText(this, "Timestamp Day: $timestamp", Toast.LENGTH_SHORT).show()

        etTimeFrom.setOnClickListener {
            val c: Calendar = Calendar.getInstance()
            val hh = c.get(Calendar.HOUR_OF_DAY)
            var mm = c.get(Calendar.MINUTE)
            etTimeFrom.setText(Utils.getDisplayedTimeFormat(hh, mm, this@CreateEventActivity))

            val timePickerDialog: TimePickerDialog = TimePickerDialog(
                this@CreateEventActivity,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    etTimeFrom.setText(
                        Utils.getDisplayedTimeFormat(
                            hourOfDay,
                            minute,
                            this@CreateEventActivity
                        )
                    )
                },
                hh,
                mm,
                true
            )
            timePickerDialog.show()
        }
        etTimeTo.setOnClickListener {
            var min: Int = 0
            val c: Calendar = Calendar.getInstance()
            val hh = c.get(Calendar.HOUR_OF_DAY)
            var mm = c.get(Calendar.MINUTE)

            etTimeTo.setText(Utils.getDisplayedTimeFormat(hh, mm, this@CreateEventActivity))
            val timePickerDialog: TimePickerDialog = TimePickerDialog(
                this@CreateEventActivity,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    etTimeTo.setText(
                        Utils.getDisplayedTimeFormat(
                            hourOfDay,
                            minute,
                            this@CreateEventActivity
                        )
                    )
                },
                hh,
                mm,
                true
            )
            timePickerDialog.show()
        }
    }

    private fun setAlert() {
        alertModel = ArrayList<EventsAlertTimes>()
        alertModel!!.add(0, EventsAlertTimes("None", -1))
        alertModel!!.add(1, EventsAlertTimes("1 minute before", 1))
        alertModel!!.add(2, EventsAlertTimes("At a time of event", 0))
        alertModel!!.add(3, EventsAlertTimes("5 minute before", 5))
        alertModel!!.add(4, EventsAlertTimes("10 minute before", 10))
        alertModel!!.add(5, EventsAlertTimes("15 minute before", 15))
        alertModel!!.add(6, EventsAlertTimes("1 hour before", 60))
        alertModel!!.add(7, EventsAlertTimes("2 hour before", 120))
        val customDropDownAdapter = EventAlertAdapter(this, alertModel!!, this)
        spinner!!.adapter = customDropDownAdapter
        ////spinner.setSelection(customDropDownAdapter.getItemIndexById(dataModel!!.GetCountryId()));

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                // get selected item text
               min= alertModel!![position].mins
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }
    }

    private fun launchExistMode() {
        val calendar = Calendar.getInstance()
        val currentDate: Date = calendar.getTime()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        var stime: String? = "00:01"
        if (getIntent().getStringExtra("stime").length > 8) {
            stime = Utils.convertEventTimeFormat(
                getIntent().getStringExtra("stime"),
                this@CreateEventActivity
            )
        }
        var etime: String? = "23:59"
        if (getIntent().getStringExtra("etime").length > 8) {
            etime =
                Utils.convertEventTimeFormat(
                    getIntent().getStringExtra("etime"),
                    this@CreateEventActivity
                )
        }

        var eventDate: String? = sdf.format(currentDate)
        if (getIntent().getStringExtra("stime").length > 8) {
            eventDate =
                Utils.convertEventDateFormat(
                    getIntent().getStringExtra("stime"),
                    this@CreateEventActivity
                )
        } else {
            if (getIntent().getStringExtra("etime").length > 1) {
                eventDate =
                    Utils.convertEventOnlyDateFormat(
                        getIntent().getStringExtra("stime"),
                        this@CreateEventActivity
                    )
            }
        }
        etTimeFrom!!.setText(stime)
        etTimeTo!!.setText(etime)
        etDesc!!.setText(getIntent().getStringExtra("desc"))
        etSummery!!.setText(getIntent().getStringExtra("title"))
    }

    /* override fun onDayClick(date: Date?) {
         Log.i("date", "" + date)
         //startDate=date
         val sdf = SimpleDateFormat("yyyy-MM-dd")
         selectedStartDate = sdf.format(date)
         Log.i("date", "" + selectedStartDate)
        // Toast.makeText(this, "onDayClick: $selectedStartDate", Toast.LENGTH_SHORT).show()
     }*/

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.back -> {
                finish()
            }

            R.id.monthView -> {
                monthView.setBackgroundResource(R.drawable.calender_view_selected)
                weekView.setBackgroundResource(R.drawable.vital_card_bd)
                monthView.setTextColor(
                    ContextCompat.getColor(
                        this@CreateEventActivity,
                        R.color.white
                    )
                );

                weekView.setTextColor(
                    ContextCompat.getColor(
                        this@CreateEventActivity,
                        R.color.blueyGrey
                    )
                );
                monthView.setTypeface(Typeface.DEFAULT_BOLD);
                weekView.setTypeface(Typeface.DEFAULT);
                widget!!.state().edit()
                    .setCalendarDisplayMode(CalendarMode.MONTHS)
                    .commit()
            }
            R.id.weekView -> {
                weekView.setBackgroundResource(R.drawable.calender_view_selected)
                monthView.setBackgroundResource(R.drawable.vital_card_bd)
                weekView.setTextColor(
                    ContextCompat.getColor(
                        this@CreateEventActivity,
                        R.color.white
                    )
                );
                monthView.setTextColor(
                    ContextCompat.getColor(
                        this@CreateEventActivity,
                        R.color.blueyGrey
                    )
                );
                weekView.setTypeface(Typeface.DEFAULT_BOLD);
                monthView.setTypeface(Typeface.DEFAULT);
                widget!!.state().edit()
                    .setCalendarDisplayMode(CalendarMode.WEEKS)
                    .commit()
            }
            R.id.save -> {
                createCal()
            }

        }
    }

    private fun createCal() {
        if (TextUtils.isEmpty(etTimeFrom!!.text.toString())) {
            onErrorListener("Please select start time")
            return
        }
        if (TextUtils.isEmpty(etTimeTo!!.text.toString())) {
            onErrorListener("Please select end time")
            return
        }
        var ICS_DIRECTORY: String = "/$user/ics_teche";
        var icsFile: File? = null
        showLoading("Please wait..")
        var Description = etDesc.text.toString()
        var startTime = etTimeFrom.text.toString()
        var endTime = etTimeTo.text.toString()
        var sDateTime = selectedStartDate + " " + startTime
        var eDateTime = selectedStartDate + " " + endTime
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        val dateoutFormat = SimpleDateFormat("yyyyMMdd'T'HHmmss")
        val sdate: Date = inputFormat.parse(sDateTime)
        var sDate = dateoutFormat.format(sdate)

        val edate: Date = inputFormat.parse(eDateTime)
        var eDate = dateoutFormat.format(edate)

        var Summery = etSummery.text.toString()

        var alartTime: String?=null


        //  Toast.makeText(this, "selectedStartDate Day: $sDate", Toast.LENGTH_SHORT).show()
        val timezone = DateTimeZone.forID(TimeZone.getDefault().id)
        try {
            val udidValue=UUID.randomUUID().toString()
            val vdfdirectory = File(FileUtils.getRootDirPath(applicationContext), ICS_DIRECTORY)
            // have the object build the directory structure, if needed.
            if (!vdfdirectory.exists()) {
                vdfdirectory.mkdirs()
            }
            if (action.equals("edit")) {
                fileName = getIntent().getStringExtra("filename")
            } else {
                fileName = "cal" + Calendar.getInstance().getTimeInMillis() + ".ics"
            }
            icsFile = File(
                vdfdirectory,
                fileName
            )
//            if (!icsFile.exists()) {
//                icsFile.createNewFile();
//            }

            if (from.equals("event")) {
                if(min!=-1) {
                    if(min==0)
                    {
                        alartTime="-PT0M"
                    }
                    if(min==1)
                    {
                        alartTime="-PT1M"
                    }
                    if(min==5)
                    {
                        alartTime="-PT5M"
                    }
                    if(min==10)
                    {
                        alartTime="-PT10M"
                    }
                    if(min==15)
                    {
                        alartTime="-PT15M"
                    }
                    if(min==60)
                    {
                        alartTime="-PT1H"
                    }
                    if(min==120)
                    {
                        alartTime="-PT2H"
                    }
                    var fw: FileWriter? = null
                    fw = FileWriter(icsFile)
                    fw.write("BEGIN:VCALENDAR\r\n");
                    fw.write("VERSION:2.0\r\n");
                    fw.write("PRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\n");
                    fw.write("BEGIN:VEVENT\r\n");
                   // fw.write("UID:teche_" + Calendar.getInstance().timeInMillis + "@office.teche.solutions\r\n")
                    fw.write("UID:teche_" + udidValue + "\r\n")
                    fw.write("DTSTART:" + sDate + "\r\n")
                    fw.write("DTEND:" + eDate + "\r\n")
                    fw.write("DTSTAMP:" + timestamp + "\r\n")
                    fw.write("SEQUENCE:0\r\n")
                    fw.write("STATUS:TENTATIVE\r\n")
                    fw.write("DESCRIPTION:" + Description + "\r\n");
                    fw.write("BEGIN:VALARM\r\n");
                    fw.write("ACTION:DISPLAY\r\n");
                    fw.write("DESCRIPTION:event reminder\r\n");
                    fw.write("TRIGGER:" +alartTime + "\r\n");
                    fw.write("X-WR-ALARMUID:" +udidValue+ "\r\n");
                    fw.write("END:VALARM\r\n");
                    fw.write("SUMMARY:" + Summery + "\r\n");
                    fw.write("END:VEVENT\r\n");
                    fw.write("END:VCALENDAR\r\n");
                    fw.close()
                }
                else{
                    var fw: FileWriter? = null
                    fw = FileWriter(icsFile)
                    fw.write("BEGIN:VCALENDAR\r\n");
                    fw.write("VERSION:2.0\r\n");
                    fw.write("PRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\n");
                    fw.write("BEGIN:VEVENT\r\n");
                    fw.write("UID:teche_" + udidValue + "\r\n")
                    fw.write("DTSTART:" + sDate + "\r\n")
                    fw.write("DTEND:" + eDate + "\r\n")
                    fw.write("DTSTAMP:" + timestamp + "\r\n")
                    fw.write("SEQUENCE:0\r\n")
                    fw.write("STATUS:TENTATIVE\r\n")
                    fw.write("DESCRIPTION:" + Description + "\r\n");
                    fw.write("SUMMARY:" + Summery + "\r\n");
                    fw.write("END:VEVENT\r\n");
                    fw.write("END:VCALENDAR\r\n");
                    fw.close()
                }
            } else {
               /* fw.write("BEGIN:VCALENDAR\r\n");
                fw.write("VERSION:2.0\r\n");
                fw.write("PRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\n");
                fw.write("BEGIN:VTODO\r\n");
                fw.write("UID:teche_" + udidValue + "\r\n")
                fw.write("DTSTART:" + sDate + "\r\n")
                fw.write("DUE:" + eDate + "\r\n")
                fw.write("DTSTAMP:" + timestamp + "\r\n")
                fw.write("SEQUENCE:0\r\n")
                fw.write("STATUS:TENTATIVE\r\n")
                fw.write("DESCRIPTION:" + Description + "\r\n");
                fw.write("SUMMARY:" + Summery + "\r\n");
                fw.write("END:VTODO\r\n");
                fw.write("END:VCALENDAR\r\n");
                fw.close()*/
                var fw: FileWriter? = null
                fw = FileWriter(icsFile)

                fw.write("BEGIN:VCALENDAR\r\n");
                fw.write("VERSION:2.0\r\n");
                fw.write("PRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\n");
                fw.write("BEGIN:VTODO\r\n");
                fw.write("UID:mobtest2_" + udidValue + "\r\n")
                fw.write("DTSTART:" + sDate + "\r\n")
                fw.write("DUE:" + eDate + "\r\n")
                fw.write("DTSTAMP:" + timestamp + "\r\n")
                fw.write("SEQUENCE:0\r\n")
                fw.write("STATUS:TENTATIVE\r\n")
                fw.write("DESCRIPTION:" + Description + "\r\n");
                fw.write("SUMMARY:" + Summery + "\r\n");
                fw.write("END:VTODO\r\n");
                fw.write("END:VCALENDAR\r\n");
                fw.close()
            }
            //Log.d("file",fw.toString())


            /* Intent i = new Intent(); //this will import vcf in contact list
           i.setAction(android.content.Intent.ACTION_VIEW);
           i.setDataAndType(Uri.fromFile(vcfFile), "text/x-vcard");
           startActivity(i);*/
            //Toast.makeText(this@CreateEventActivity, "Created!", Toast.LENGTH_SHORT).show()
            uploadCalender(icsFile!!)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun uploadCalender(icsFile: File) {
         remotePath = "/" + icsFile.name
        Log.i("remote path Calender", remotePath)
        // Get the last modification date of the file from the file system
        val timeStampLong = System.currentTimeMillis() / 1000
        val timeStamp = timeStampLong.toString()
        val uploadOperation = UploadCalenderRemoteOperation(
            user + " " + category,
            icsFile.getAbsolutePath(),
            remotePath,
            "",
            "",
            timeStamp
        )
        uploadOperation.execute(mClient, this, mHandler)


    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {
        if (!result!!.isSuccess()) {

            closeLoading()
        } else if (operation is UploadCalenderRemoteOperation) {

            onSuccessfulUpload(operation as UploadCalenderRemoteOperation?, result)

        }
    }

    private fun onSuccessfulUpload(
        uploadContactRemoteOperation: UploadCalenderRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        closeLoading()
        val intent = Intent()
        intent.putExtra("filename",remotePath)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onDateSelected(
        widget: MaterialCalendarView,
        date: CalendarDay,
        selected: Boolean
    ) {


        //If you change a decorate, you need to invalidate decorators
        oneDayDecorator.setDate(date.date)
        selectedStartDate = FORMATTER.format(date.date)
        widget.invalidateDecorators()
    }

    override fun onPositionClick(adapterPosition: Int) {
        TODO("Not yet implemented")
    }

}