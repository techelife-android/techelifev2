package com.techesolutions.viewmodel.cameras

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techesolutions.data.remote.repository.login.cameras.MasterDataRepository
import com.techesolutions.data.remote.model.nextclouddashboard.MasterModel

/**
 * Author: Neelam Upadhyay
 * Created: 18/08/2021
 */
class MasterViewModel: ViewModel() {

    var masterLiveData: MutableLiveData<MasterModel>? = null

    fun getRoomDeviceMasterData(token: String, apporigin: String, type: String): LiveData<MasterModel>? {
        masterLiveData = MasterDataRepository.getRoomDeviceData(token,apporigin,type)
        return masterLiveData
    }
}