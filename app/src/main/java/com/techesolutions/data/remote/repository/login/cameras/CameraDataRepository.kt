package com.techesolutions.data.remote.repository.login.cameras

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.techesolutions.services.NetworkService
import com.techesolutions.data.remote.model.nextclouddashboard.CameraDataResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Author: Neelam Upadhyay
 * Created: 18/08/2021
 */
object CameraDataRepository {
    val cameraDataResponse = MutableLiveData<CameraDataResponse>()

    fun getCameraData( token: String, apporigin: String, room: String): MutableLiveData<CameraDataResponse> {

        val call = NetworkService.apiInterface.getCameraData(token,apporigin,room)

        call.enqueue(object: Callback<CameraDataResponse> {
            override fun onFailure(call: Call<CameraDataResponse>, t: Throwable) {
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<CameraDataResponse>,
                response: Response<CameraDataResponse>
            ) {
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()
                cameraDataResponse.postValue(data)
            }

        })

        return cameraDataResponse
    }

}