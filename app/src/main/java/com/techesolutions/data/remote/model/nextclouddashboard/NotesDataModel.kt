package com.techesolutions.data.remote.model.nextclouddashboard

class NotesDataModel internal constructor(var id: Long?, var title: String?, var modified: Long?, var category: String?, var favorite: Boolean?, var error: Boolean?, var content: String?)
