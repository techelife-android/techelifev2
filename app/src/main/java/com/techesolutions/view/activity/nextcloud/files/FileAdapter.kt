package com.techesolutions.view.activity.nextcloud.files

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.techesolutions.view.callbacks.FileClickListener
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.utils.DisplayUtils
import java.text.SimpleDateFormat

class FileAdapter(
    private val mContext: Context,
    private val resourceLayout: Int,
    private val filelistener: FileClickListener
) : ArrayAdapter<RemoteFile?>(
    mContext, resourceLayout
) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var v = convertView
        if (v == null) {
            val vi: LayoutInflater
            vi = LayoutInflater.from(mContext)
            v = vi.inflate(R.layout.note_folder_items, parent, false)
            //v = vi.inflate(resourceLayout, null);
        }
        val p = getItem(position)
        if (p != null) {
            val tt1 = v!!.findViewById<View>(R.id.title) as TextView
            val dateTV = v.findViewById<View>(R.id.date) as TextView
            val content = v.findViewById<View>(R.id.content) as TextView
            val activity_icon = v.findViewById<View>(R.id.activity_icon) as ImageView
            val delete = v.findViewById<View>(R.id.delete) as ImageView
            if (tt1 != null) {
                var filename = p.remotePath.replace("/".toRegex(), "")
                if (filename.startsWith(".")) {
                    tt1.text = filename
                } else if (filename.length > 15 && filename.contains(".")) {
                    val filepre = filename.substring(0, 10)
                    val filepost =
                        filename.substring(filename.lastIndexOf(".", filename.length - 1))
                    if (filepost != null) filename = filepre + "..." + filename.substring(
                        filename.length - 7,
                        filename.lastIndexOf(".")
                    ) + filepost
                }
                Log.i("filename", filename)
                tt1.text = filename
            }
            if (content != null) {
                val formatDate = SimpleDateFormat("dd MMM, yyyy")
                val modifiedTimestamp = p.modifiedTimestamp
                val date = formatDate.format(modifiedTimestamp)
                content.text = DisplayUtils.bytesToHumanReadable(p.size)
                content.visibility = View.VISIBLE
            }
            if (dateTV != null) {
                val formatDate = SimpleDateFormat("dd MMM, yyyy")
                val modifiedTimestamp = p.modifiedTimestamp
                val date = formatDate.format(modifiedTimestamp)
                dateTV.text = DisplayUtils.getRelativeTimestamp(
                    context,
                    modifiedTimestamp
                )
                dateTV.visibility = View.VISIBLE
            }
            Log.i("all data", p.mimeType)
            if (p.mimeType.equals("DIR", ignoreCase = true)) {
                activity_icon.setImageResource(R.drawable.file)
            } else if (p.mimeType.equals(
                    "image/png",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "image/jpeg",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "image/jpg",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "image/heic",
                    ignoreCase = true
                ) || p.mimeType.equals("image/heif", ignoreCase = true)
            ) {
                activity_icon.setImageResource(R.drawable.ic_image)
            } else if (p.mimeType.equals(
                    "text/markdown",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "text/plain",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    ignoreCase = true
                ) || p.mimeType.equals("application/vnd.oasis.opendocument.text", ignoreCase = true)
            ) {
                activity_icon.setImageResource(R.drawable.notes)
            } else if (p.mimeType.equals("application/pdf", ignoreCase = true)) {
                activity_icon.setImageResource(R.drawable.pdf)
            } else if (p.mimeType.equals(
                    "video/mp4",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "video/mpeg",
                    ignoreCase = true
                ) || p.mimeType.equals("image/gif", ignoreCase = true)|| p.remotePath.contains(".mov", ignoreCase = true)
            ) {
                activity_icon.setImageResource(R.drawable.ic_video)
            } else if (p.mimeType.equals(
                    "application/xhtml+xml",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/xml",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "text/xml",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "text/csv",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/vnd.ms-excel",
                    ignoreCase = true
                ) || p.mimeType.equals(
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    ignoreCase = true
                )
            ) {
                activity_icon.setImageResource(R.drawable.xls)
            } else {
                activity_icon.setImageResource(R.drawable.notes)
            }
            delete.visibility = View.VISIBLE
            delete.setOnClickListener { filelistener.onDeleteClick(getPosition(p)) }
            v.setOnClickListener {
                filelistener.onPositionClick(getPosition(p))
            }
        }
        return v!!
    }
}