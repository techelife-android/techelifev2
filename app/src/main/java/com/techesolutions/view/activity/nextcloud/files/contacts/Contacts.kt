package com.techesolutions.view.activity.nextcloud.files.contacts

data class Contacts(
    var name : String,
    var telType: String,
    var tel : String,
    var memberType : String
    )