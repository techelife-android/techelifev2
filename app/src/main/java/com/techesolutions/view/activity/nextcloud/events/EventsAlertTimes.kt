package com.techesolutions.view.activity.nextcloud.events

data class EventsAlertTimes(
    var title : String,
    var mins: Int
    )