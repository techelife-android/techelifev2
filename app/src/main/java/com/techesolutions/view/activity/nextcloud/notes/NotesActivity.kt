package com.techesolutions.view.activity.nextcloud.notes

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.IntroView
import com.techesolutions.services.ApiService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.notes.persistence.NoteServerSyncHelper
import com.techesolutions.view.activity.nextcloud.notes.persistence.NotesDatabase
import com.techesolutions.view.activity.nextcloud.notes.shared.model.CloudNote
import com.techesolutions.view.activity.nextcloud.notes.shared.model.DBNote
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import kotlinx.android.synthetic.main.layout_header.*
import kotlinx.android.synthetic.main.layout_header_with_title.*
import kotlinx.android.synthetic.main.layout_header_with_title.headerTitle
import kotlinx.android.synthetic.main.notes_delete_dialog.*
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.UnsupportedEncodingException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


/**
 * Created by Neelam on 24-02-2020.
 */

class NotesActivity : AppCompatActivity(), View.OnClickListener, NoteClickListener {
    private val create_note_cmd: Int  = 0
    private val show_single_note_cmd: Int = 1
    private var delete = 0
    var recyclerView: RecyclerView? = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    var from: String? = null
    private val introView = IntroView()
    var url: String? = null
    var user: String? = null
    var pass: String? = null
    private var progressDialog = CustomProgressDialog()
    protected var db: NotesDatabase? = null
    var categories: List<NavigationAdapter.CategoryNavigationItem>? = null
    var adapter: NotesAdapter? = null
    var dbNotes: List<DBNote>? = null
    var data = arrayOf(
            "Category", "Share", "Delete"
    )
    lateinit var adapterDialog: ArrayAdapter<String>
    lateinit var listView: ListView
    lateinit var alertDialog: AlertDialog.Builder
    lateinit var dialog: AlertDialog
    private val adapterCategories: NavigationAdapter? = null

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant", "WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_notes)
        Utils.statusBarSetup(this)

        db = NotesDatabase.getInstance(this)
        dbNotes = db!!.searchNotes(1, "", getIntent().getStringExtra("category"), false)
        Log.d("notes", dbNotes.toString())
        categories= db!!.getCategories(1)
        Log.d("category", categories.toString())

        //getting recyclerview from xml
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView

        val shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        url=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
         user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
         pass=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        headerTitle!!.text = "Notes"
        if(db!=null && dbNotes.isNullOrEmpty())
        {
            var token = getAuthToken()
            Log.i("token", token)
            getNotes(token)
        }
        else {
            recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
            adapter = NotesAdapter(this@NotesActivity, dbNotes, this)

            //now adding the adapter to recyclerview
            recyclerView?.adapter = adapter
        }
        create!!.visibility = View.VISIBLE
        back.setOnClickListener(this)
        create.setOnClickListener(this)
       /* refresh!!.visibility = View.VISIBLE
        refresh.setOnClickListener(this)*/
    }

    private fun getNotes(token: String) {
        showLoading("Please wait...")
        val client = OkHttpClient.Builder()
            .addInterceptor(BasicAuthInterceptor(user!!, pass!!))
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES) // write timeout
            .readTimeout(1, TimeUnit.MINUTES) // read timeout
            .build()
      /*  if (url!!.endsWith("/")) {
            url = url!!.substring(0, url!!.length - 1)
        }*/
        if (!url!!.endsWith("/")) {
            url = url+"/"
        }
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        fun <T> buildService(service: Class<T>): T{
            return retrofit.create(service)
        }
        val request = buildService(ApiService::class.java)
        // val call = request.getMovies(getString(R.string.api_key))

        //  val call = NextcloudNetworkService(url).apiInterface.getNotes(token)
        val call = request.getNotes()
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                closeLoading()
                Log.v("DEBUG : ", t.message.toString())

            }

            override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
            ) {
                closeLoading()
                Log.i("data", "" + response)
                val stringResponse = response.body()?.string()
                if (stringResponse != null) {
                    val dlist = JSONArray(stringResponse)
                    Log.i("dlist", "" + dlist)
                    if (dlist != null && dlist.length() > 0) {

                        Log.i("dlist", "" + dlist)
                        val remoteNotes = ArrayList<CloudNote>()
                        // pull remote changes: update or create each remote note
                        var remoteNote: CloudNote? = null
                        for (i in 0 until dlist.length()) {
                            val item = dlist.getJSONObject(i)
                            remoteNote = CloudNote(
                                    item.getLong("id"),
                                    item.getLong("modified"),
                                    item.getString("title"),
                                    item.getString("content"),
                                    item.getBoolean("favorite"),
                                    item.getString("category"),
                                    null
                            )
                            remoteNotes.add(remoteNote)
                            Log.v(NoteServerSyncHelper.TAG, "   ... create")
//                            db!!. recreateDatabase()
                            db!!.addNote(1, remoteNote)
                            refreshNotes()
                        }
                    } else {
                        Toast.makeText(
                                this@NotesActivity,
                                "No Data",
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            }


        })
        adapter = NotesAdapter(this@NotesActivity, dbNotes, this)

        //now adding the adapter to recyclerview
        recyclerView?.adapter = adapter
    }
    override fun onNoteClick(position: Int) {
        val note: DBNote = adapter!!.getItem(position) as DBNote
        val intent = Intent(applicationContext, CreateNotesActivity::class.java)
        intent.putExtra(Constants.PARAM_NOTE_ID, note.remoteId)
        intent.putExtra(Constants.PARAM_TITLE, note.title)
        intent.putExtra(Constants.PARAM_CATEGORY, note.category)
        intent.putExtra(Constants.PARAM_CONTENT, note.content)
        val gson = Gson()


      /*  val jsonNotes: String = gson.toJson(dbNotes)
        intent.putExtra(Constants.PARAM_NOTE, jsonNotes)*/
        //intent.putExtra(Constants.PARAM_NOTE, note.toString())
        startActivityForResult(intent, show_single_note_cmd)
    }

    override fun onNoteDeleteClick(position: Int) {
        val note: CloudNote = adapter!!.getItem(position) as CloudNote
        deleteNote(note.remoteId)
    }

    override fun onNoteLongClick(position: Int, v: View?): Boolean {
        val selected: CloudNote? = adapter!!.getItem(position)
        openDialog(this@NotesActivity, selected!!.remoteId)
        return true
    }


    fun getAuthToken(): String {
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        val user: String?=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", url+", "+user+", "+pass)
        var data = ByteArray(0)
        try {
            data = (user + ":" + pass).toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP)
    }
    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                finish()
            }
            R.id.refresh -> {
               getNotes("")
            }

            R.id.create -> {
                val intent = Intent(applicationContext, CreateNotesActivity::class.java)
                intent.putExtra(Constants.PARAM_NOTE_ID, 0)
                intent.putExtra(Constants.PARAM_NOTE, "")
                intent.putExtra(Constants.PARAM_CATEGORY, "")
                intent.putExtra(Constants.PARAM_CONTENT, "")
                startActivityForResult(intent, create_note_cmd)
            }
        }

    }

    private fun closeIntro() {
        if (introView != null && introView?.dialog.isShowing())
            introView?.dialog.dismiss()
    }

    private fun showIntro() {
        introView?.show(this)
        introView.dialog.findViewById<View>(R.id.cp_bg_view).setOnClickListener(View.OnClickListener { view ->
            closeIntro()
        })
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == show_single_note_cmd) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
             // refreshNotes()
                getNotes("")
                //adding some dummy data to the list
              return
            }
        }
        if (requestCode == create_note_cmd) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                var token = getAuthToken()
                Log.i("token", token)
                getNotes(token)

            }
        }
    }

    fun openDialog(notesActivity: NotesActivity, id: Long?) {
        val dialog = Dialog(notesActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.notes_delete_dialog)
        //Cancel
        /*   val rlCancel = dialog.findViewById(R.id.rlCancel) as RelativeLayout
        rlCancel.setOnClickListener { dialog.dismiss() }
*/
        //Delete
        val rlDelete= dialog.findViewById(R.id.rlDelete) as RelativeLayout
        rlDelete.setOnClickListener {
             deleteNote(id)
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun deleteNote(id: Long?) {
        showLoading("Deleting")
        val client = OkHttpClient.Builder()
            .addInterceptor(BasicAuthInterceptor(user!!, pass!!))
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES) // write timeout
            .readTimeout(1, TimeUnit.MINUTES) // read timeout
            .build()
       /* if (url!!.endsWith("/")) {
            url = url!!.substring(0, url!!.length - 1)
        }*/
        if (!url!!.endsWith("/")) {
            url = url+"/"
        }
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        fun <T> buildService(service: Class<T>): T{
            return retrofit.create(service)
        }
        val request = buildService(ApiService::class.java)

        val call = request.deleteNote(id)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())
                val data = response.body().toString()
                Log.v("data : ", data)
                closeLoading()
                id?.let { db!!.deleteNote(it) }
                refreshNotes()
            }
        })

    }

    private fun refreshNotes() {
        dbNotes = db!!.searchNotes(1, "", getIntent().getStringExtra("category"), false)
        adapter = NotesAdapter(this@NotesActivity, dbNotes, this)
        recyclerView?.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        refreshNotes()
    }
}
