package com.techesolutions.data.remote.model.nextclouddashboard

import com.google.gson.annotations.SerializedName

data class MasterModel (

    @field:SerializedName("data")
    val data: Data? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null
)
data class Data(
    @field:SerializedName("rooms")
    val rooms: List<RoomItems?>? = null,

    @field:SerializedName("device_types")
    val devices: List<DeviceItems?>? = null

)


data class RoomItems(

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("value")
    val value: String? = null
)
data class DeviceItems(

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("value")
    val value: String? = null
)


