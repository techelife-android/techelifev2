package com.techesolutions.view.activity.nextcloud.events

data class Events(
    var type : String,
    var title : String,
    var description: String,
    var stratTime : String,
    var endTime : String,
    var timestamp: String,
    var uid: String,
    var filename: String,
    var timer: String
    )