package com.techesolutions.view.activity.nextcloud.contacts

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import java.util.*

class ContactAdapter(
    context: Context,
    val notesList: ArrayList<Contacts>,
    private var contactClick: ContactActivity
) : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {
    var activity:Context = context
    var contactClickListener=contactClick
  /*  val now = Date()
    var dateValue = formatDate.format(now)
*/
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.contact_items, parent, false)
        return ViewHolder(v, activity, contactClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ContactAdapter.ViewHolder, position: Int) {
        notesList.get(position)?.let { holder.bindItems(
            it,
            activity,
            contactClickListener,
            position
        ) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return notesList?.size!!
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context,
        contactClickListener: NoteClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            notes: Contacts,
            context: Context,
            contactClickListener: NoteClickListener,
            position: Int
        ) {
            val textViewName = itemView.findViewById(R.id.title) as TextView
            val content = itemView.findViewById(R.id.content) as TextView
            val image = itemView.findViewById(R.id.activity_icon) as TextView
            val delete = itemView.findViewById(R.id.delete) as ImageView
            val message = itemView.findViewById(R.id.message) as ImageView
            val call = itemView.findViewById(R.id.call) as ImageView
            val date = itemView.findViewById(R.id.date) as TextView
            if(notes.name!=null)
            content.text=notes.tel
            textViewName.text = notes.name
            if(notes.name!=null && notes.name.length>0)
            image.text=notes.name.subSequence(0, 1)
            date.text = notes.memberType
            date.visibility=View.VISIBLE
            //date.text = ""+miliSec
           // image.setImageResource(R.drawable.notes)
//            itemView?.setOnClickListener {
//                noteClickListener.onNoteLongClick(position, itemView)
//                noteClickListener.onNoteClick(position)
//            }
            itemView.setOnLongClickListener {
                val position = layoutPosition
               // println("LongClick: $p")
               // noteClickListener.onNoteLongClick(position, itemView)

                true // returning true instead of false, works for me
            }

            itemView.setOnClickListener {
                val position = layoutPosition
                contactClickListener.onNoteClick(position)

                // Toast.makeText(context, "Recycle Click$notes  ", Toast.LENGTH_SHORT).show()
            }
            delete.setOnClickListener {
                val position = layoutPosition
                //val notes: CloudNote = notes
               // noteClickListener.onNoteDeleteClick(position)

                // Toast.makeText(context, "Recycle Click$notes  ", Toast.LENGTH_SHORT).show()
            }

            message.setOnClickListener {
                val position = layoutPosition
                sendSMS(context,notes.tel,"Hello")
            }
            call.setOnClickListener {
                val position = layoutPosition
                phoneCall(context,notes.tel)
            }
        }

        // Phone call
        fun phoneCall(context: Context, phoneNo: String) {

           //call select sim
            val callIntent = Intent(
                Intent.ACTION_VIEW, Uri.parse(
                    "tel:$phoneNo"
                )
            )
            callIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(callIntent)

            //call without select sim
            //Intent intent = new Intent(Intent.ACTION_CALL);
            // intent.setData(Uri.parse("tel:" + phoneNo));
            //startActivity(intent);
        }

        // Send SMS
        fun sendSMS(context: Context, phoneNo: String, msg: String) {
            val smsVIntent = Intent(Intent.ACTION_VIEW)
            smsVIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            smsVIntent.type = "vnd.android-dir/mms-sms"
            smsVIntent.putExtra("address", phoneNo)
            smsVIntent.putExtra("sms_body", msg)
            try {
                context.startActivity(smsVIntent)
            } catch (ex: Exception) {
                Toast.makeText(
                    context, "Your sms has failed...",
                    Toast.LENGTH_LONG
                ).show()
                ex.printStackTrace()
            }
        }


    }

    fun getItem(notePosition: Int): Contacts? {
        return notesList.get(notePosition)
    }


}