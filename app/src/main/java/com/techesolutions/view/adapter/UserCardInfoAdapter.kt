package com.techesolutions.view.adapter

import android.app.Dialog
import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.userinfo.CardInfoData
import com.techesolutions.view.activity.nextcloud.userinfo.UserInfoActivity
import com.techesolutions.view.callbacks.UserInfoClickListener

class UserCardInfoAdapter(
    context:Context,
    val dataList: ArrayList<CardInfoData>,
    private var itemclick: UserInfoActivity
) : RecyclerView.Adapter<UserCardInfoAdapter.ViewHolder>()  {
    var activity:Context = context
    var infoClickListener=itemclick
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserCardInfoAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.user_card_info_items, parent, false)
        return ViewHolder(v,activity,infoClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: UserCardInfoAdapter.ViewHolder, position: Int) {
        dataList.get(position)?.let { holder.bindItems(it, activity, infoClickListener, position,dataList) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,context: Context, infoClickListener: UserInfoClickListener) : RecyclerView.ViewHolder(itemView) /*, View.OnClickListener*/
    {
        fun bindItems(
            vital: CardInfoData,
            context: Context,
            infoClickListener: UserInfoClickListener,
            position: Int,
            dataList: ArrayList<CardInfoData>
        ) {
            val lock = itemView.findViewById(R.id.lock) as ImageView
            val etCardNumber = itemView.findViewById(R.id.etCardNumber) as TextView
            val etcardHolderName = itemView.findViewById(R.id.etcardHolderName) as TextView
            val etexpirydate = itemView.findViewById(R.id.etexpirydate) as TextView
            //val first12:String= vital.card_no.substring(0,11).replaceAll("*")
            var cnum:String=vital.card_no
            val last4:String= vital.card_no.substring(cnum.lastIndex-3,cnum.lastIndex+1)
            etCardNumber.text = "************"+last4
            etCardNumber.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
            etcardHolderName.text = vital.name_on_card
            etexpirydate.text = vital.expiry
            val menu = itemView.findViewById(R.id.rlmenu) as RelativeLayout
            val rllock = itemView.findViewById(R.id.rllock) as RelativeLayout
            menu.setOnCreateContextMenuListener({ menu, v, menuInfo ->
                val item = menu.add("Edit")
                item.setOnMenuItemClickListener({ i ->
                    infoClickListener.onEditClick(position, vital.type, "edit")
                    true // Signifies you have consumed this event, so propogation can stop.
                })

                val itemRemove = menu.add("Delete")
                itemRemove.setOnMenuItemClickListener({ i ->
                    infoClickListener.onDeleteClick(position)
                    true
                })
            })
            menu.setOnClickListener({ view -> view.showContextMenu() })
           // rllock.setOnClickListener(this)
            rllock.setOnClickListener {
                lock.setBackgroundResource(R.drawable.ic_open_lock)
                etCardNumber.text=vital.card_no
                etCardNumber.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f)
            }
            //rllock.setOnClickListener { infoClickListener.onViewUpdate(lock,etCardNumber,adapterPosition) }
        }

        private fun showDialog(title: String, context: Context) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.add_web_info_dialog)
            val body = dialog.findViewById(R.id.topHeader) as TextView
            body.text = title
            val yesBtn = dialog.findViewById(R.id.save) as TextView
            val noBtn = dialog.findViewById(R.id.cancel) as TextView
            yesBtn.setOnClickListener {
                dialog.dismiss()
            }
            noBtn.setOnClickListener { dialog.dismiss() }
            dialog.show()

        }

        /*override fun onClick(v: View?) {
            if(position == adapterPosition) {
               //v!!.setBackgroundResource(R.drawable.ic_open_lock)
               (v!!.findViewById(R.id.lock) as ImageView).setBackgroundResource(R.drawable.ic_open_lock)

            } else {
                (v!!.findViewById(R.id.lock) as ImageView).setBackgroundResource(R.drawable.ic_close_lock)
                (v.findViewById(R.id.etCardNumber) as TextView).text="****************"
            }
           // val position = adapterPosition
            *//*onItemClicked(position)*//*
        }*/
    }


}