package com.techesolutions.data.remote.model.userinfo

data class CardInfoData(
    val id: String,
    val user_id: String,
    val created: String?,
    val updated: String?,
    val type: String?,
    val name_on_card: String,
    val card_no: String,
    val expiry: String,
    val cvv: String
)