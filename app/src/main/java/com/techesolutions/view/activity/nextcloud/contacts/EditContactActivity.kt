package com.techesolutions.view.activity.nextcloud.contacts

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.UploadContactRemoteOperation
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils.getRootDirPath
import kotlinx.android.synthetic.main.create_notes.bottom_bar
import kotlinx.android.synthetic.main.create_notes.cancel
import kotlinx.android.synthetic.main.create_notes.save
import kotlinx.android.synthetic.main.edit_contact.*
import kotlinx.android.synthetic.main.layout_header.*
import kotlinx.android.synthetic.main.layout_header_with_title.headerTitle
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.util.*


/**
 * Created by Neelam on 24-12-2020.
 */

class EditContactActivity : AppCompatActivity(), View.OnClickListener, OnRemoteOperationListener {
    private var note_title: String?=null
    private var content: String?=null
    var recyclerView: RecyclerView? = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    var from: String? = null
    var isEdit: Boolean = false
    private var progressDialog = CustomProgressDialog()
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    var vcfFile: File? =null
    var remoteUrl: String?=null
    var url: String?=null
    var user: String?=null
    var pass: String?=null
    var CONTACT_DIR: String?=null
    var shared: SharedPreferences? = null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant", "WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.edit_contact)
        Utils.statusBarSetup(this)
        mHandler = Handler()
        shared = getSharedPreferences("TechePrefs", MODE_PRIVATE)
        url=shared!!.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user=shared!!.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        pass=shared!!.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        CONTACT_DIR = "/addressbooks/users/"+user+"/contacts/"
        remoteUrl=url+"/remote.php/dav/addressbooks/users/"+user+"/contacts"
        Log.i("data", url + ", " + user + ", " + pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url!!.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
                pass!!
            )
        )
        launchNoteMode()
        save.setOnClickListener(this)
        cancel.setOnClickListener(this)
        back.setOnClickListener(this)
        create.setOnClickListener(this)

    }

    fun launchNoteMode() {
        val i: Intent = getIntent()
       var action = i.getStringExtra("action")
        if(action.equals("edit"))
        {
            bottom_bar!!.visibility = View.GONE
            create!!.text = "Edit"
            create!!.visibility = View.VISIBLE
            launchExistingNote(false)
        }
    }

    private fun launchExistingNote(editable: Boolean) {
        val i: Intent = getIntent()
        etName!!.setText(i.getStringExtra("name"))
        etContact!!.setText(i.getStringExtra("tel"))
        etRelation!!.setText(i.getStringExtra("memberType"))
        headerTitle!!.text = "Contact"
        if (editable) {
            bottom_bar.visibility=View.VISIBLE
            etName!!.isEnabled = true
            etContact!!.isEnabled = true
            etRelation!!.isEnabled = true
        } else {
            bottom_bar.visibility=View.GONE
            etName!!.isEnabled = false
            etContact!!.isEnabled = false
            etRelation!!.isEnabled = false
        }


    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                finish()
            }
            R.id.cancel -> {
                finish()
            }
            R.id.save -> {
                saveContact()
            }
            R.id.create -> {
                if (create!!.text.equals("Edit")) {
                    isEdit = true
                    launchExistingNote(isEdit)
                }

            }
        }

    }
private  fun saveContact()
{
    var VCF_DIRECTORY: String = "/$user/vcf_teche"
    var fileName: String?=null
    showLoading("Please wait..")
    var name=etContact.text.toString()
    var contact=etName.text.toString()
    var relation=etRelation.text.toString()
    Log.i("et data", name + ": " + contact)
    try
    {
        // File vcfFile = new File(this.getExternalFilesDir(null), "generated.vcf");
        val vdfdirectory =
            File(getRootDirPath(applicationContext), VCF_DIRECTORY)
        // have the object build the directory structure, if needed.
        if (!vdfdirectory.exists())
        {
            vdfdirectory.mkdirs()
        }
        if(getIntent().getStringExtra("action").equals("edit"))
        {
            fileName=getIntent().getStringExtra("filename")
        }
        else{
            fileName="contact" + Calendar.getInstance().getTimeInMillis() + ".vcf"
        }
        vcfFile = File(
            vdfdirectory,
            "/" + fileName
        )
        var fw: FileWriter? = null
        fw = FileWriter(vcfFile)
        fw.write("BEGIN:VCARD\r\n")
        fw.write("VERSION:3.0\r\n")
        // fw.write("N:" + p.getSurname() + ";" + p.getFirstName() + "\r\n");
        fw.write("FN:" + contact + "\r\n")
        // fw.write("ORG:" + p.getCompanyName() + "\r\n");
        fw.write("TITLE:" + contact + "\r\n");
        fw.write("TEL;TYPE=WORK,VOICE:" + name + "\r\n")
        fw.write("TEL;TYPE=HOME,VOICE:" + name + "\r\n");
        // fw.write("ADR;TYPE=WORK:;;" + p.getStreet() + ";" + p.getCity() + ";" + p.getState() + ";" + p.getPostcode() + ";" + p.getCountry() + "\r\n");
        fw.write("EMAIL;TYPE=PREF,INTERNET:" + "abc@gmail.com" + "\r\n")
        fw.write("END:VCARD\r\n")
        fw.close()

        //Toast.makeText(this@EditContactActivity, "Saving Contact...", Toast.LENGTH_SHORT).show()
        uploadContact(vcfFile!!)
    }
    catch (e: IOException) {
        e.printStackTrace()
    }
}

    private fun uploadContact(vcfFile: File) {
        //val fileToUpload: File = vcfFile
        //val remotePath: String = remoteUrl+"/"+ vcfFile.name
        val remotePath: String = "/"+ vcfFile.name
        Log.i("remote path contact", remotePath)
        // Get the last modification date of the file from the file system
        val timeStampLong = System.currentTimeMillis() / 1000
        val timeStamp = timeStampLong.toString()
        /*  UploadFileRemoteOperation uploadOperation =
                    new UploadFileRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, timeStamp);
            uploadOperation.execute(mClient, this, mHandler);*/
        val uploadOperation = UploadContactRemoteOperation(
            user,
            vcfFile.getAbsolutePath(),
            remotePath,
            "",
            "",
            timeStamp
        )
        Log.i("contact data", vcfFile.getAbsolutePath() + "," + remotePath)
        uploadOperation.execute(mClient, this, mHandler)


    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {
        if (!result!!.isSuccess()) {
            closeLoading()

        }
        else if(operation is UploadContactRemoteOperation)
        {

            onSuccessfulUpload(operation as UploadContactRemoteOperation?, result)

        }
    }

    private fun onSuccessfulUpload(
        uploadCalenderRemoteOperation: UploadContactRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        closeLoading()
        val intent = Intent()
        intent.putExtra("updatedFile", vcfFile!!.name)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}

