package com.techesolutions.view.activity.vitaldevices

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.details.VitalDetail
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.adapter.VitalDetailsAdapter
import kotlinx.android.synthetic.main.activity_vital_details.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class VitalDetailsActivity : AppCompatActivity(), View.OnClickListener, OnChartValueSelectedListener {
    private var progressDialog = CustomProgressDialog()
    var title: String? = null
    var vitalName: String? = null
    var unit1: String? = null
    var unit2: String? = null
    var token: String? = null
    var userId: String? = null
    var chart1: LineChart? = null
    var chart2: LineChart? = null
    var viewFlipper: ViewFlipper? = null
    private var newArray = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    var recyclerView:RecyclerView?=null
    var newArray1: ArrayList<VitalDetail>? = null


    var format =
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    var formatDate =
        SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
    var formatTime =
        SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
    var currentActive = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_vital_details)
        Utils.setDimensions(this)
        val i: Intent = getIntent()
        title = i.getStringExtra("title")
        vitalName = i.getStringExtra("vitalName")
        unit1 = i.getStringExtra("unitName1")
        unit2 = i.getStringExtra("unitName2")
//        val deviceModel = i.getStringExtra("deviceModel")
//        val brand = i.getStringExtra("brand")
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
         token = shared.getString(Constants.KEY_TOKEN, null)
         userId = shared.getString(Constants.Login_User_ID, null)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")

        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

        chart1 = findViewById(R.id.chart1) as LineChart
        chart2 = findViewById(R.id.chart2) as LineChart
        viewFlipper= findViewById(R.id.viewFlipper) as ViewFlipper
        setUpHeader(title!!)

    }

    @SuppressLint("WrongConstant")
    open fun setUpHeader(title: String) {
        val headerTitleTV =
            findViewById<View>(R.id.headerTitle) as TextView
        if (title.contains("Temperature")) {
            headerTitleTV.text = "Thermometer"
        } else if (title.contains("Weight")) {
            headerTitleTV.text = "Weight Scale"
        } else if (title.contains("Blood Sugar")) {
            headerTitleTV.text = "Glucometer"
        } else if (title.contains("Activities")) {
            headerTitleTV.text = "Activity Watch"
        } else {
            headerTitleTV.text = title
        }

        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }

        listView?.setOnClickListener(this)
        graphview?.setOnClickListener(this)
        listView.text=vitalNameLabels!!.getString("listview")
        graphview.text=vitalNameLabels!!.getString("graphview")
        todayTv.text=vitalNameLabels!!.getString("today")
        weekTv.text=vitalNameLabels!!.getString("week")
        yearTv.text=vitalNameLabels!!.getString("year")
        monthTv.text=vitalNameLabels!!.getString("month")
        todayTv?.setOnClickListener(this)
        weekTv?.setOnClickListener(this)
        yearTv?.setOnClickListener(this)
        monthTv?.setOnClickListener(this)
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView

        //adding a layoutmanager
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        getTodayData("today", format.format(Date(System.currentTimeMillis())))

    }
    fun setYAxisProperties(chart: LineChart) {
        val rightAxis = chart.axisRight
        rightAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        rightAxis.setDrawLabels(false) // no axis labels
        rightAxis.setDrawAxisLine(false) // no axis line
        rightAxis.setDrawGridLines(false) // no grid lines
        //        rightAxis.setTypeface(Utils.getTypeFaceRegular(this.getContext()));
        rightAxis.setDrawZeroLine(false) // draw a zero line
        //        rightAxis.setGridLineWidth(0.5f);
        val leftAxis = chart.axisLeft
        leftAxis.setDrawLabels(true) // no axis labels
        leftAxis.setDrawAxisLine(true) // no axis line
        leftAxis.setDrawGridLines(false) // no grid lines
        //        leftAxis.setTextSize(6f);
//        leftAxis.setTextColor(getResources().getColor(R.color.txtMainColor));
//        leftAxis.setTypeface(Utils.getTypeFaceRegular(this.getContext()));
        leftAxis.setDrawZeroLine(true) // draw a zero line
        //        leftAxis.setGridLineWidth(1f);
    }

    fun setXAxisProperties(
            chart: LineChart,
            list: List<VitalDetail>
    ) {
        val xAxis2 = chart.xAxis
        xAxis2.position = XAxis.XAxisPosition.BOTTOM
        xAxis2.valueFormatter = MyXAxisValueFormatter(list as List<VitalDetail>)
        xAxis2.labelRotationAngle = -60f
        xAxis2.setDrawGridLines(false)
    }

//    class MyXAxisValueFormatter(list: List<VitalDetail>) : IAxisValueFormatter {
//        override fun getFormattedValue(v: Float, axisBase: AxisBase): String {
//            return try {
//                var newArray=ArrayList<VitalDetail>()
//                return Utils.convertDateFormat2(
//                        newArray.get(v.toInt()).date,
//                        this
//                )
//                null.toString()
//            } catch (e: Exception) {
//                "" + v
//            }
//        }
//    }
class MyXAxisValueFormatter(var newArray1: List<VitalDetail>) : IAxisValueFormatter {
    override fun getFormattedValue(v: Float, axisBase: AxisBase?): String {
        return try {
            //newArray1= ArrayList<VitalDetail>()
            //Utils.convertDateFormat2("2021-02-10 10:10:10", this)

           Utils.convertDateFormat2(newArray1.get(v.toInt()).date, this)
        } catch (e: java.lang.Exception) {
            "" + v
        }
    }
}
    private fun graphViewDetails(list: ArrayList<VitalDetail>, unit1: String?, unit2: String?) {
        chart1?.let { setXAxisProperties(it, list) }
        chart1?.let { setYAxisProperties(it) }
        chart1?.description?.isEnabled = false

//        if (unit1.equals("&#8457", ignoreCase = true)) {
//           unit1 = "°F"
//        }

        var ld: LineData? = null
        if (unit2!!.length > 0) {
            ld = getLineData(
                    getLineDataSet(getEntries(list, 0), unit1, R.color.colorPrimaryDark),
                    getLineDataSet(getEntries(list, 1), unit2, R.color.heading_color)
            )
      }
        else {
            ld =
                getLineData(getLineDataSet(getEntries(list, 0), unit1, R.color.colorPrimaryDark),
                        getLineDataSet(getEntries(list, 1), unit2, R.color.heading_color))
        }
        chart1?.data = ld

        chart1?.invalidate() // refresh

        makeCurveLines(chart1)
        makeFilledGraph(chart1)
        showHighlightedValues(chart1)
        setUpMarkerVIew(chart1)

    }

    fun getEntries(newArray: ArrayList<VitalDetail>, type: Int):List<Entry> {
        newArray1= ArrayList<VitalDetail>(newArray)
        Collections.reverse(newArray)
        val entries = ArrayList<Entry>()
        for (i in 0 until newArray.size)
        {
            var data1:String="0.0"
            var data2:String="0.0"
            if (type == 0)
            {
                if ((newArray!!.get(i).data1val).equals("", ignoreCase = true))
                {
                    data1 = "0.0"
                }
                else
                {
                    data1 = newArray.get(i).data1val
                }

                if (newArray.get(i).Title.equals("Weight", ignoreCase = true))
                {
                    if(data1.contains("Lbs") || data1.contains("lbs"))
                        //data1=data1.substring(0, data1.length - 2)
                    entries.add(createEntry(
                            i.toFloat(), java.lang.Float.valueOf(Utils.getLocalWeight(
                            data1, this)), newArray.get(i).date))
                }
                else
                {
                    entries.add(createEntry(i.toFloat(), java.lang.Float.valueOf(data1), newArray.get(i).date))
                }
            }
            else
                if ((newArray!!.get(i).data2val).equals("", ignoreCase = true))
                {
                    data2 = "0.0"
                }
                else
                {
                    data2 = newArray.get(i).data2val
                    if (newArray.get(i).Title.contains("Sugar", ignoreCase = true)) {
                        data2="0.0"
                        entries.add(createEntry(i.toFloat(), java.lang.Float.valueOf(data2), newArray.get(i).date))
                    }
                    else if (newArray.get(i).Title.equals("Temperature", ignoreCase = true)) {
                        if ((newArray!!.get(i).data1val).equals("", ignoreCase = true)) {
                            data1 = "0.0"
                        } else {
                            data1 = newArray.get(i).data1val
                        }
                        if (data2.equals("C")) {
                            data2 = "" + Utils.convertTemperature(data1, true)
                        } else {
                            data2 = "" + Utils.convertTemperature(data1, false)
                        }
                        entries.add(createEntry(i.toFloat(), java.lang.Float.valueOf(data2), newArray.get(i).date))

                    }
                    if (newArray.get(i).Title.equals("Weight", ignoreCase = true)) {
                        if( newArray.get(i).unit.equals("Kg", ignoreCase = true))
                        {
                            if ((newArray!!.get(i).data1val).equals("", ignoreCase = true))
                            {
                                data1 = "0.0"
                            }
                            else
                            {
                                data1 = newArray.get(i).data1val
                            }
                            data2 = ""+ Utils.getServerWeight(data1,this@VitalDetailsActivity)

                        }
                        entries.add(createEntry(i.toFloat(), java.lang.Float.valueOf(data2), newArray.get(i).date))

                    }
                    else{
                        entries.add(createEntry(i.toFloat(), java.lang.Float.valueOf(data2), newArray.get(i).date))
                    }
                    }


            //entries.add(createEntry(i.toFloat(), java.lang.Float.valueOf(data2),dateF))
        }
        return entries
    }

    private fun createEntry(dataX: Float, dataY: Float, data: String): Entry {
        return Entry(dataX, dataY)
    }

    private fun getLineDataSet(entries: List<Entry>, label: String?, color: Int): LineDataSet {
        val lineDataSet = LineDataSet(entries, label)
        lineDataSet.color = resources.getColor(color)
        return lineDataSet
    }

    private fun getLineData(lineDataSet: LineDataSet, lineDataSet1: LineDataSet): LineData {
        return LineData(lineDataSet, lineDataSet1)

    }

    private fun setUpMarkerVIew(chart: LineChart?) {
        val mv =
                CustomGraphMarker(this, R.layout.custom_graph_marker_view, newArray1)
        // Set the marker to the chart
        mv.setChartView(chart)
        chart!!.setMarker(mv)

    }

    private fun showHighlightedValues(chart: LineChart?) {
        chart!!.data.isHighlightEnabled = true
        chart.invalidate()
    }

    private fun makeCurveLines(chart: LineChart?) {
        val sets = chart!!.data.dataSets
        for (iSet in sets) {
            val set = iSet as LineDataSet
            set.mode = LineDataSet.Mode.CUBIC_BEZIER
        }
        chart.invalidate()
    }

    private fun makeFilledGraph(chart: LineChart?) {
        val sets = chart!!.data.dataSets
        for (iSet in sets) {
            val set = iSet as LineDataSet
            set.setDrawFilled(true)
        }
        chart.invalidate()
    }


    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.listView -> {
                listView.setBackgroundResource(R.drawable.view_selected)
                graphview.setBackgroundResource(R.drawable.vital_card_bd)
                listView.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.white));
                graphview.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                listView.setTypeface(Typeface.DEFAULT_BOLD);
                graphview.setTypeface(Typeface.DEFAULT);
                chartLayout!!.visibility = View.GONE
                recyclerView!!.visibility = View.VISIBLE
            }
            R.id.graphview -> {
                listView.setBackgroundResource(R.drawable.vital_card_bd)
                graphview.setBackgroundResource(R.drawable.view_selected)
                listView.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                graphview.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.white));
                graphview.setTypeface(Typeface.DEFAULT_BOLD);
                listView.setTypeface(Typeface.DEFAULT);
                chartLayout!!.visibility = View.VISIBLE
                recyclerView!!.visibility = View.GONE
            }
            R.id.todayTv -> {
                todayTv.setBackgroundResource(R.drawable.vital_card_selected)
                weekTv.setBackgroundResource(R.drawable.vital_card_bd)
                monthTv.setBackgroundResource(R.drawable.vital_card_bd)
                yearTv.setBackgroundResource(R.drawable.vital_card_bd)
                todayTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.white));
                weekTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                monthTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                yearTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                todayTv.setTypeface(Typeface.DEFAULT_BOLD);
                weekTv.setTypeface(Typeface.DEFAULT);
                monthTv.setTypeface(Typeface.DEFAULT);
                yearTv.setTypeface(Typeface.DEFAULT);
                listData(
                        "Loading", vitalName, "today",
                        format.format(Date(System.currentTimeMillis())),
                        format.format(Date(System.currentTimeMillis())))
            }
            R.id.weekTv -> {
                todayTv.setBackgroundResource(R.drawable.vital_card_bd)
                weekTv.setBackgroundResource(R.drawable.vital_card_selected)
                monthTv.setBackgroundResource(R.drawable.vital_card_bd)
                yearTv.setBackgroundResource(R.drawable.vital_card_bd)
                todayTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                weekTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.white));
                monthTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                yearTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                weekTv.setTypeface(Typeface.DEFAULT_BOLD);
                todayTv.setTypeface(Typeface.DEFAULT);
                monthTv.setTypeface(Typeface.DEFAULT);
                yearTv.setTypeface(Typeface.DEFAULT);
                listData(
                        getString(R.string.loading), vitalName, "week",
                        format.format(Date(System.currentTimeMillis())),
                        format.format(Date(System.currentTimeMillis() - 604800000L)))
            }
            R.id.monthTv -> {
                todayTv.setBackgroundResource(R.drawable.vital_card_bd)
                weekTv.setBackgroundResource(R.drawable.vital_card_bd)
                monthTv.setBackgroundResource(R.drawable.vital_card_selected)
                yearTv.setBackgroundResource(R.drawable.vital_card_bd)
                todayTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                weekTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                monthTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.white));
                yearTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                todayTv.setTypeface(Typeface.DEFAULT);
                weekTv.setTypeface(Typeface.DEFAULT);
                monthTv.setTypeface(Typeface.DEFAULT_BOLD);
                yearTv.setTypeface(Typeface.DEFAULT);
                val c = Calendar.getInstance()
                c.time = Date()
                c.add(Calendar.MONTH, -1)
                listData(
                        getString(R.string.loading), vitalName, "month",
                        format.format(Date(System.currentTimeMillis())),
                        format.format(c.time)
                )
            }
            R.id.yearTv -> {
                todayTv.setBackgroundResource(R.drawable.vital_card_bd)
                weekTv.setBackgroundResource(R.drawable.vital_card_bd)
                monthTv.setBackgroundResource(R.drawable.vital_card_bd)
                yearTv.setBackgroundResource(R.drawable.vital_card_selected)
                todayTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                weekTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                monthTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.blueyGrey));
                yearTv.setTextColor(ContextCompat.getColor(this@VitalDetailsActivity, R.color.white));
                todayTv.setTypeface(Typeface.DEFAULT);
                weekTv.setTypeface(Typeface.DEFAULT);
                monthTv.setTypeface(Typeface.DEFAULT);
                yearTv.setTypeface(Typeface.DEFAULT_BOLD);
                val c = Calendar.getInstance()
                c.time = Date()
                c.add(Calendar.YEAR, -1)
                listData(
                        getString(R.string.loading), vitalName, "year",
                        format.format(Date(System.currentTimeMillis())),
                        format.format(c.time)
                )
            }
        }
    }

    private fun listData(load: String, vName: String?, mode: String?, endDate: String, startDate: String) {
       if(mode.equals("today"))
       {
           getTodayData(mode, endDate)
       }
       else
        {
            getData(mode, endDate, startDate)
        }

    }

private fun getTodayData(mode: String?, current_date: String?)
{
    if (Utils.isOnline(this)) {
        showLoading("Fetching data...")

        val call = NetworkService.apiInterface.getVilatDetailsToday("Bearer $token", "mobile",userId, vitalName, mode, "2021", current_date)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                closeLoading()
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
            ) {
                closeLoading()
                val stringResponse = response.body()?.string()
                val jsonObj = JSONObject(stringResponse)
                val success = jsonObj!!.getString("success")
                if (success != null && success.toString().equals("true")) {
                    setData(jsonObj)

                } else {
                    Toast.makeText(
                            this@VitalDetailsActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                    ).show()
                }
            }

        })

    }

}
    private fun getData(mode: String?, endDate: String, startDate: String)
    {
        if (Utils.isOnline(this)) {
            showLoading("Fetching data...")
            val call = NetworkService.apiInterface.getVilatDetails("Bearer $token", "mobile",userId, vitalName, mode, "2021", endDate, startDate, endDate)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                ) {
                    closeLoading()
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        setData(jsonObj)

                    } else {
                        Toast.makeText(
                                this@VitalDetailsActivity,
                                "Some error. Try again.",
                                Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        }

    }
    private fun setData(jsonObj: JSONObject) {
        val jObj = jsonObj.getJSONObject("data")
        val dlist = jObj.getJSONArray("data")
        val vitals = ArrayList<VitalDetail>()
        for (i in 0 until dlist.length()) {
            val data = dlist.getJSONObject(i)
            if (vitalName.equals("Activities")) {
                vitals.add(
                        VitalDetail(
                                data!!.getString("DateandTime"),
                                title!!,
                                unit1,
                                data!!.getString("Stepcount"),
                                unit2,
                                data!!.getString("Stepscalories"),
                            ""
                        )
                )
            }
            else  if (vitalName.equals("Blood_Pressure")) {
                vitals.add(
                        VitalDetail(
                                data!!.getString("DateandTime"),
                                title!!,
                                unit1,
                                data!!.getString("Systolic_bp"),
                                unit2,
                                data!!.getString("Diastolic_bp"),
                            ""
                        )
                )
            } else  if (vitalName.equals("Temperature")) {
                vitals.add(
                        VitalDetail(
                                data!!.getString("DateandTime"),
                                title!!,
                                unit1,
                                data!!.getString("Temperature"),
                                unit2,
                                data!!.getString("Temperature_Unit"),
                            ""
                        )
                )
            }else  if (vitalName.equals("Pulse_Oximetry")) {
                vitals.add(
                        VitalDetail(
                                data!!.getString("DateandTime"),
                                title!!,
                                unit1,
                                data!!.getString("O2_Sat"),
                                unit2,
                                "",
                            ""
                        )
                )
            }
            else  if (vitalName.equals("Heart_Rate")) {
                vitals.add(
                        VitalDetail(
                                data!!.getString("DateandTime"),
                                title!!,
                                unit1,
                                data!!.getString("Heart_Rate_Beats"),

                                unit2,
                                "",
                       ""
                        )
                )
            }
            else  if (vitalName.equals("Weight")) {
                vitals.add(
                        VitalDetail(
                                data!!.getString("DateandTime"),
                                title!!,
                                unit1,
                                data!!.getString("Weight"),
                                unit2,
                                "",
                            data!!.getString("Weight_Unit")
                        )
                )
            }else  if (vitalName.equals("Blood_Sugar")) {
                vitals.add(
                        VitalDetail(
                                data!!.getString("DateandTime"),
                                title!!,
                                unit1,
                                data!!.getString("Sugar"),
                                "Context",
                                data!!.getString("Context"),
                            ""
                        )
                )
            }
        }

        //adding some dummy data to the list

        val adapter = VitalDetailsAdapter(this@VitalDetailsActivity, vitals)

        //now adding the adapter to recyclerview
        recyclerView?.adapter = adapter

        graphViewDetails(vitals, unit1, unit2)
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        Toast.makeText(this@VitalDetailsActivity, e.toString(), Toast.LENGTH_SHORT).show()
    }

    override fun onNothingSelected() {
        TODO("Not yet implemented")
    }
}