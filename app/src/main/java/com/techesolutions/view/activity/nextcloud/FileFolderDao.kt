package com.techesolutions.view.activity.nextcloud

import androidx.lifecycle.LiveData
import androidx.room.*
import com.techesolutions.view.activity.nextcloud.files.FileFolderData

@Dao
interface FileFolderDao {

    @Insert
    fun insert(note: FileFolderData)

    @Update
    fun update(note: FileFolderData)

    @Delete
    fun delete(note: FileFolderData)

    @Query("delete from file_folder_table")
    fun deleteAllFiles()

    @Query("select * from file_folder_table")
    fun getAllFiles(): LiveData<List<FileFolderData>>
}