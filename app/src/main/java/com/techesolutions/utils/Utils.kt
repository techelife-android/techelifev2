package com.techesolutions.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources.NotFoundException
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.Build
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.techesolutions.R
import com.techesolutions.view.activity.nextcloud.iot.IotDetailsActivity
import com.techesolutions.view.activity.vitaldevices.VitalDetailsActivity
import java.io.File
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Neelam on 19-12-2020.
 */
class Utils {
    fun toMD5(s: String): String {
        try {
            // Create MD5 Hash
            val digest = MessageDigest.getInstance("MD5")
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuffer()
            for (i in messageDigest.indices) hexString.append(
                Integer.toHexString(
                    0xFF and messageDigest[i].toInt()
                )
            )
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }

    companion object {
        var screenHeight = 0
        var screenWidth = 0
        var boldFont: Typeface? = null
        var aTecheRegularFont: Typeface? = null
        var aTecheSemiBoldFont: Typeface? = null
        var aTecheMediumFont: Typeface? = null

        //Screen Size Setting For All Devices by Neelam
        fun setDimensions(_context: Context) {
            try {
                val displaymetrics = DisplayMetrics()
                (_context as Activity).windowManager.defaultDisplay
                    .getMetrics(displaymetrics)
                screenWidth = displaymetrics.widthPixels
                screenHeight = displaymetrics.heightPixels

                boldFont = Typeface.createFromAsset(
                    _context.getAssets(),
                    Constants.BOLD_FONT
                )
                aTecheRegularFont =
                    Typeface.createFromAsset(
                        _context.getAssets(),
                        Constants.A_TECHE_REGULAR_FONT
                    )
                aTecheSemiBoldFont =
                    Typeface.createFromAsset(
                        _context.getAssets(),
                        Constants.A_TECHE_SEMIBOLD_FONT
                    )
                aTecheMediumFont =
                    Typeface.createFromAsset(
                        _context.getAssets(),
                        Constants.A_TECHE_MEDIUM_FONT
                    )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        //For InterNet Checking
        fun isOnline(_Context: Context): Boolean {
            val cm =
                _Context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }

        fun isValidEmail(target: CharSequence?): Boolean {
            return if (target == null) {
                false
            } else {
                Patterns.EMAIL_ADDRESS.matcher(target).matches()
            }
        }

        fun isValidMobile(phone: String?): Boolean {
            return if (phone == null) {
                false
            } else {
                Patterns.PHONE.matcher(phone).matches()
            }
        }

        /**
         * Obtains the LayoutInflater from the given context.
         */
        private fun from_Context(context: Context?): LayoutInflater? {
            var layoutInflater: LayoutInflater? = null
            try {
                if (context != null) {
                    layoutInflater =
                        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                }
                if (layoutInflater == null) {
                    throw AssertionError("LayoutInflater not found.")
                }
            } catch (e: Exception) {
                Log.w("Neelam-->DEBUG", e)
                layoutInflater = null
            }
            return layoutInflater
        }

        fun statusBarSetup(_activity: Activity) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val decor = _activity.window.decorView
                    decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    _activity.window.statusBarColor =
                        _activity.resources.getColor(R.color.colorPrimaryDark)
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        _activity.window
                            .clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                        _activity.window
                            .addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                        _activity.window.statusBarColor =
                            _activity.resources.getColor(R.color.colorPrimaryDark)
                    }
                }
            } catch (e: NotFoundException) {
                e.printStackTrace()
            }
        }

        fun showCustomToast(toastMsg: String, _activity: Activity) {
            var toastMsg = toastMsg
            toastMsg = toastMsg.replace(".", "")
            try {
                val inflater: LayoutInflater =
                    Utils.from_Context(_activity)!!
                val layout: View
                if (inflater != null) {
                    layout = inflater.inflate(
                        R.layout.custom_toast,
                        _activity.findViewById<View>(R.id.custom_toast_layout_id) as ViewGroup
                    )
                    val tv = layout.findViewById<View>(R.id.text) as TextView
                    // The actual toast generated here.
                    val toast = Toast(_activity)
                    tv.text = toastMsg
                    toast.duration = Toast.LENGTH_SHORT
                    toast.view = layout
                    toast.show()
                } else {
                    Toast.makeText(_activity, "" + toastMsg, Toast.LENGTH_SHORT).show()
                }
            } catch (e: java.lang.AssertionError) {
                Log.w("Neelam-->DEBUG", e)
                Toast.makeText(_activity, "" + toastMsg, Toast.LENGTH_SHORT).show()
            } catch (e: java.lang.Exception) {
                Log.w("Neelam-->DEBUG", e)
                Toast.makeText(_activity, "" + toastMsg, Toast.LENGTH_SHORT).show()
            }
        }

        fun findDay(selectedDate: String?, days: Int): String {
            val format = "yyyyy-MM-d"
            val returnDate: String
            val df = SimpleDateFormat(format)
            var date: Date? = null
            try {
                date = df.parse(selectedDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val cal = Calendar.getInstance()
            cal.time = date
            cal.add(Calendar.DATE, days)
            returnDate = df.format(cal.time)
            return returnDate
        }

        fun closeKeyboard(activity: Activity) {
            val view = activity.currentFocus
            if (view != null) {
                val imm =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        @Throws(ParseException::class)
        fun convertLocalToSeverFormat(time: String?): String {
            val currentFOrmat = SimpleDateFormat("MM-dd-yyyy")
            val convertFormat = SimpleDateFormat("yyyy-MM-dd")
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertServerToLocalFormat(time: String?): String {
            val currentFOrmat = SimpleDateFormat("yyyy-MM-dd")
            val convertFormat = SimpleDateFormat("MM-dd-yyyy")
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        fun randomString(len: Int): String {
            val AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            val rnd = SecureRandom()
            val sb = StringBuilder(len)
            for (i in 0 until len) sb.append(AB[rnd.nextInt(AB.length)])
            return sb.toString()
        }

        @Throws(ParseException::class)
        fun convertDateoOrmat(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            var convertFormatDate =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormatDate = SimpleDateFormat("dd MMM yyyy")
            }
            var convertFormatTime = SimpleDateFormat("HH:mm")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("12hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm a")
            }
            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF | $timeF"
        }

        @Throws(ParseException::class)
        fun convertDateoOrmat2(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat =
                SimpleDateFormat("MM-dd-yyyy HH:mm")
            var convertFormatDate =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormatDate = SimpleDateFormat("dd MMM yyyy")
            }
            var convertFormatTime = SimpleDateFormat("HH:mm")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("12hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm a")
            }
            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF | $timeF"
        }

        @Throws(ParseException::class)
        fun convertShortDateFormat(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat = SimpleDateFormat("yyyy-MM-dd")
            var convertFormat =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormat = SimpleDateFormat("dd MMM yyyy")
            }
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertShortDateFormat2(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat =
                SimpleDateFormat("MMMM-dd-yyyy")
            var convertFormat =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormat = SimpleDateFormat("dd MMM yyyy")
            }
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertLongDateFormat(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat =
                SimpleDateFormat("yyyy-MM-dd HH:mm")

            var convertFormatDate =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormatDate = SimpleDateFormat("dd MMM yyyy")
            }
            var convertFormatTime = SimpleDateFormat("HH:mm")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("12hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm a")
            }
            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF | $timeF"
        }

        fun getMeetVersionName(context: Context): String {
            var version = ""
            try {
                val pInfo =
                    context.packageManager.getPackageInfo(context.packageName, 0)
                version = pInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            return version
        }

        @Throws(ParseException::class)
        fun convertLongDateCareFeedFormat(time: String?): String {
            val currentFOrmat =
                SimpleDateFormat("HH:mm MMM dd,yyyy")
            //        SimpleDateFormat convertFormat = new SimpleDateFormat("MMM dd, yyyy | HH:mm");
            val convertFormat =
                SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss")
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertToDate(time: String?): Date {
            val currentFOrmat =
                SimpleDateFormat("KK:mm MMM dd,yyyy")
            return currentFOrmat.parse(time)
        }

        fun formatSeconds(seconds: Int): String {
            return (getTwoDecimalsValue(seconds / 3600) + ":"
                    + getTwoDecimalsValue(seconds / 60) + ":"
                    + getTwoDecimalsValue(seconds % 60))
        }

        private fun getTwoDecimalsValue(value: Int): String {
            return if (value >= 0 && value <= 9) {
                "0$value"
            } else {
                value.toString() + ""
            }
        }

        @Throws(ParseException::class)
        fun convertDateFormat(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            var convertFormat =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormat = SimpleDateFormat("dd MMM yyyy")
            }
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertDateFormat2(
            time: String,
            context: VitalDetailsActivity.MyXAxisValueFormatter
        ): String {
            val currentFOrmat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            var convertFormat = SimpleDateFormat("dd MMM")

            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }
        @Throws(ParseException::class)

        fun convertDateFormat3(
            time: String,
            context: IotDetailsActivity.MyXAxisValueFormatter
        ): String {
            val currentFOrmat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            var convertFormat = SimpleDateFormat("dd MMM")

            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertShortDateFormatNew(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat = SimpleDateFormat("MM-dd-yyyy")
            var convertFormat =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormat = SimpleDateFormat("dd MMM yyyy")
            }
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertShortDateFormatNew2(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat = SimpleDateFormat("MM-dd-yyyy")
            var convertFormat = SimpleDateFormat("MM-dd-yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormat = SimpleDateFormat("dd-MM-yyyy")
            }
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertShortTimeFormatNew(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat = SimpleDateFormat("HH:mm")
            var convertFormat = SimpleDateFormat("HH:mm")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("12hour", ignoreCase = true)
            ) {
                convertFormat = SimpleDateFormat("hh:mm a")
            }
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertFormatNew(
            time: String?,
            context: Context?
        ): String {
            val currentFOrmat =
                SimpleDateFormat("hh:mm a MMM dd, yyyy")
            //        SimpleDateFormat convertFormat = new SimpleDateFormat("MMM dd, yyyy | HH:mm");
//        if(MySharedPreference.getPreferences(context, Constants.KEY_USER_DATE_FORMAT).equalsIgnoreCase("dd-mm-yyyy")){
//            convertFormat = new SimpleDateFormat("dd MMM yyyy | hh:mm a");
//        }
//        Date date = currentFOrmat.parse(time);
//        return convertFormat.format(date);
            var convertFormatDate =
                SimpleDateFormat("MMM dd, yyyy")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("dd-mm-yyyy", ignoreCase = true)
            ) {
                convertFormatDate = SimpleDateFormat("dd MMM yyyy")
            }
            var convertFormatTime = SimpleDateFormat("HH:mm")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("12hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm a")
            }
            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF | $timeF"
        }

        fun insertAt(target: String, position: Int, insert: String): String {
            val targetLen = target.length
            require(!(position < 0 || position > targetLen)) { "position=$position" }
            if (insert.isEmpty()) {
                return target
            }
            if (position == 0) {
                return insert + target
            } else if (position == targetLen) {
                return target + insert
            }
            val insertLen = insert.length
            val buffer = CharArray(targetLen + insertLen)
            target.toCharArray(buffer, 0, 0, position)
            insert.toCharArray(buffer, position, 0, insertLen)
            target.toCharArray(buffer, position + insertLen, position, targetLen)
            return String(buffer)
        }

        fun convertFromOneTimeZoneToOther(
            dt: Date,
            from: String?,
            to: String?
        ): Date {
            val fromTimezone =
                TimeZone.getTimeZone(from) //get Timezone object
            val toTimezone = TimeZone.getTimeZone(to)
            val fromOffset = fromTimezone.getOffset(dt.time).toLong() //get offset
            val toOffset = toTimezone.getOffset(dt.time).toLong()

            //calculate offset difference and calculate the actual time
            val convertedTime = dt.time - (fromOffset - toOffset)
            val d2 = Date(convertedTime)
            val parseFormat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            return d2
        }

        fun convertTimeZone(
            fromTimeZone: String?,
            toTimeZone: String?,
            value: String?
        ): String? {
            val dfNy: DateFormat = SimpleDateFormat("MM-dd-yyyy HH:mm")
            dfNy.timeZone = TimeZone.getTimeZone(fromTimeZone)
            val dfUtc: DateFormat = SimpleDateFormat("MM-dd-yyyy HH:mm")
            dfUtc.timeZone = TimeZone.getTimeZone(toTimeZone)
            return try {
                dfUtc.format(dfNy.parse(value))
            } catch (e: ParseException) {
                null // invalid input
            }
        }

        fun getDateFormat(date: String?): Date? {
            try {
                val sdf = SimpleDateFormat("MM-dd-yyyy HH:mm")
                return sdf.parse(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        @Throws(ParseException::class)
        fun convertLongDateFormat22(time: String?): String {
            val currentFOrmat =
                SimpleDateFormat("MM-dd-yyyy HH:mm")
            val convertFormat = SimpleDateFormat("MM-dd-yyyy")
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        @Throws(ParseException::class)
        fun convertLongDateFormat33(time: String?): String {
            val currentFOrmat =
                SimpleDateFormat("MM-dd-yyyy HH:mm")
            val convertFormat = SimpleDateFormat("HH:mm")
            val date = currentFOrmat.parse(time)
            return convertFormat.format(date)
        }

        fun getTimeZoneSting(timeZone: String): String {
            var timezonename = ""
            timezonename = if (timeZone.contains("ATLANT")) {
                "America/New_York"
            } else if (timeZone.contains("EST")) {
                "US/Eastern"
            } else if (timeZone.contains("CST")) {
                "America/Chicago"
            } else if (timeZone.contains("MST")) {
                "America/Denver"
            } else if (timeZone.contains("PST")) {
                "America/Los_Angeles"
            } else if (timeZone.contains("ALASK")) {
                "America/Anchorage"
            } else if (timeZone.contains("HAW")) {
                "US/Hawaii"
            } else if (timeZone.contains("SAMOA")) {
                "US/Samoa"
            } else if (timeZone.contains("OUTLYING")) {
                "Pacific/Wake"
            } else if (timeZone.contains("AWST")) {
                "Australia/West"
            } else if (timeZone.contains("ACST")) {
                "Australia/North"
            } else if (timeZone.contains("AEST")) {
                "Australia/Queensland"
            } else if (timeZone.contains("IST")) {
                "Asia/Kolkata"
            } else {
                "US/Eastern"
            }
            return timezonename
        }

        fun getCacheDirectory(context: Context): String {
            return Environment.getExternalStorageDirectory().toString() +
                    File.separator + context.resources
                .getString(R.string.app_name) + File.separator + "Cache"
        }

        fun cleanCache(file: File): Boolean {
            if (file.isDirectory) {
                val files = file.listFiles()
                if (files != null) {
                    for (f in files) {
                        if (f.isDirectory) {
                            cleanCache(f)
                        } else {
                            f.delete()
                        }
                    }
                }
            }
            return file.delete()
        }

        fun changeTimeZoneDate(profileTz: String?, dateTime: Date): String {
            val convertedDate =
                convertFromOneTimeZoneToOther(
                    dateTime,
                    currentTomeZone,
                    profileTz
                )
            val parseFormat = SimpleDateFormat("MM-dd-yyyy")
            return parseFormat.format(convertedDate)
        }

        fun changeTimeZoneTime(profileTz: String?, dateTime: Date): String {
            val convertedDate =
                convertFromOneTimeZoneToOther(
                    dateTime,
                    currentTomeZone,
                    profileTz
                )
            val parseFormat = SimpleDateFormat("HH:mm")
            return parseFormat.format(convertedDate)
        }

        private val currentTomeZone: String
            private get() {
                val tz = TimeZone.getDefault()
                return tz.id
            }

        //    public static String timeAgo(Context context, long millis) {
        //        long diff = new Date().getTime() - millis;
        //
        //        Resources r = context.getResources();
        //
        //        String prefix = r.getString(R.string.time_ago_prefix);
        //        String suffix = r.getString(R.string.time_ago_suffix);
        //
        //        double seconds = Math.abs(diff) / 1000;
        //        double minutes = seconds / 60;
        //        double hours = minutes / 60;
        //        double days = hours / 24;
        //        double years = days / 365;
        //
        //        String words;
        //
        //        if (seconds < 45) {
        //            words = r.getString(R.string.time_ago_seconds, Math.round(seconds));
        //        } else if (seconds < 90) {
        //            words = r.getString(R.string.time_ago_minute, 1);
        //        } else if (minutes < 45) {
        //            words = r.getString(R.string.time_ago_minutes, Math.round(minutes));
        //        } else if (minutes < 90) {
        //            words = r.getString(R.string.time_ago_hour, 1);
        //        } else if (hours < 24) {
        //            words = r.getString(R.string.time_ago_hours, Math.round(hours));
        //        } else if (hours < 42) {
        //            words = r.getString(R.string.time_ago_day, 1);
        //        } else if (days < 30) {
        //            words = r.getString(R.string.time_ago_days, Math.round(days));
        //        } else if (days < 45) {
        //            words = r.getString(R.string.time_ago_month, 1);
        //        } else if (days < 365) {
        //            words = r.getString(R.string.time_ago_months, Math.round(days / 30));
        //        } else if (years < 1.5) {
        //            words = r.getString(R.string.time_ago_year, 1);
        //        } else {
        //            words = r.getString(R.string.time_ago_years, Math.round(years));
        //        }
        //
        //        StringBuilder sb = new StringBuilder();
        //
        //        if (prefix != null && prefix.length() > 0) {
        //            sb.append(prefix).append(" ");
        //        }
        //
        //        sb.append(words);
        //
        //        if (suffix != null && suffix.length() > 0) {
        //            sb.append(" ").append(suffix);
        //        }
        //
        //        return sb.toString().trim();
        //    }
        fun weightKgToLbs(value: String): String {
            var weight = 0.0
            weight = try {
                String.format("%.1f", value).toDouble()
            } catch (e: Exception) {
                value.toDouble()
            }
            val weightLbs = weight * 2.204
            return String.format("%.1f", weightLbs)
        }

        fun LbstoKg(value: String): String {
            var weight = 0.0
            weight = try {
                String.format("%.1f", value).toDouble()
            } catch (e: Exception) {
                value.toDouble()
            }
            val weightKg = weight/2.204
            return String.format("%.1f", weightKg)
        }

        fun addChar(str: String?, ch: Char, position: Int): String {
            val sb = StringBuilder(str!!)
            sb.insert(position, ch)
            return sb.toString()
        }

        fun getDisplayedDateFormat(
            date: Int,
            month: Int,
            year: Int,
            context: Context?
        ): String {
            var month = month
            var monthS = "" + ++month
            var dateS = "" + date
            if (monthS.length == 1) {
                monthS = "0$month"
            }
            if (dateS.length == 1) {
                dateS = "0$date"
            }
            return if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_DATE_FORMAT
                ).equals("yyyy-MM-dd", ignoreCase = true)
            ) {
                "$dateS-$monthS-$year"
            } else {
                "$monthS-$dateS-$year"
            }
        }
        fun getDisplayedDateFormatEvent(
            date: Int,
            month: Int,
            year: Int,
            context: Context?
        ): String {
            var month = month
            var monthS = "" + ++month
            var dateS = "" + date
            if (monthS.length == 1) {
                monthS = "0$month"
            }
            if (dateS.length == 1) {
                dateS = "0$date"
            }
            return   "$year-$monthS-$dateS"

        }

        fun convertToServerFormat(
            displayedFormat: String,
            context: Context?,
            requiredFormat: String?
        ): String {
            return try {
                val currentFormat = SimpleDateFormat(
                    MySharedPreference.getPreferences(
                        context,
                        Constants.KEY_USER_DATE_FORMAT
                    )?.replace("m", "M")
                )
                val convertFormat =
                    SimpleDateFormat(requiredFormat)
                val date = currentFormat.parse(displayedFormat)
                convertFormat.format(date)
            } catch (e: Exception) {
                displayedFormat
            }
        }

        fun convertToLocalFormat(
            serverFormat: String,
            context: Context?,
            currentServerFormat: String?
        ): String {
            return try {
                val currentFormat =
                    SimpleDateFormat(currentServerFormat)
                val convertFormat = SimpleDateFormat(
                    MySharedPreference.getPreferences(
                        context,
                        Constants.KEY_USER_DATE_FORMAT
                    )?.replace("m", "M")
                )
                val date = currentFormat.parse(serverFormat)
                convertFormat.format(date)
            } catch (e: Exception) {
                serverFormat
            }
        }

        fun getDisplayedTimeFormat(
            hourOfDay: Int,
            minute: Int,
            context: Context?
        ): String {
            var hourOfDay = hourOfDay
            var hourOfDayS = "" + hourOfDay
            var minutesOfHour = "" + minute
            if (hourOfDay < 10) hourOfDayS = "0$hourOfDay"
            if (minute < 10) minutesOfHour = "0$minute"
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("12hour", ignoreCase = true)
            ) {
                if (hourOfDay > 12) {
                    hourOfDay -= 12
                }
                if (hourOfDay < 10) hourOfDayS = "0$hourOfDay"
            }
            return "$hourOfDayS:$minutesOfHour"
        }

        fun getServerTimeFormat(hourOfDay: Int, minute: Int): String {
            var hourOfDayS = "" + hourOfDay
            var minutesOfHour = "" + minute
            if (hourOfDay < 10) hourOfDayS = "0$hourOfDay"
            if (minute < 10) minutesOfHour = "0$minute"
            return "$hourOfDayS:$minutesOfHour"
        }

        fun initialDisplayDateFormat(context: Context?): String {
            var sdf = SimpleDateFormat("yyyy-MM-dd")
            return sdf.format(Date())
        }

        fun initialDisplayTimeFormat(context: Context?): String {
            val sdf2: SimpleDateFormat
            sdf2 = if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("12hour", ignoreCase = true)
            ) {
                SimpleDateFormat("hh:mm")
            } else {
                SimpleDateFormat("HH:mm")
            }
            return sdf2.format(Date())
        }

        fun initialServerTimeFormat(): String {
            val sdf2 = SimpleDateFormat("HH:mm")
            return sdf2.format(Date())
        }

        fun getLocalWeight(
            weight: String,
            context: Context?
        ): String {
            var weight = weight
                var weightD = 0.0
                weightD = try {
                    String.format("%.1f", weight).toDouble()
                } catch (e: Exception) {
                    weight.toDouble()
                }
                val weightLbs = weightD / 2.204
                weight = String.format("%.1f", weightLbs)

            return weight
        }

        fun getLocalTemp(temp: String, context: Context?): String {
            var temp = temp
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TEMP_UNIT
                ).equals("C", ignoreCase = true)
            ) {
                var fahrenheit = 0.0
                fahrenheit = try {
                    String.format("%.1f", temp).toDouble()
                } catch (e: Exception) {
                    temp.toDouble()
                }

//            double fahrenheit = celsius;
                val celsius = (fahrenheit - 32) * 0.5556
                temp = String.format("%.1f", celsius)
            }
            return temp
        }

        fun getServerWeight(
            weight: String,
            context: Context?
        ): String {
            var weight = weight
            if(weight!=null && weight=="")
            {
                var weightD = 0.0
            }
               var weightD = 0.0
                weightD = try {
                    String.format("%.1f", weight).toDouble()

                } catch (e: Exception) {
                    weight.toDouble()
                }
                val weightLbs = weightD * 2.204
                weight = String.format("%.1f", weightLbs)
            return weight
        }


        fun convertTemperature(value: String, isCelsius: Boolean): String? {
            val celsius: Float
            val fahrenheit: Float
            var tempVlaue: String? = null
            if (isCelsius) {
                // convert to fahrenheit
                fahrenheit = value.toFloat() * 9 / 5 + 32
                tempVlaue = String.format("%.1f", fahrenheit)
            } else {
                // convert to celsius
                celsius = (value.toFloat() - 32) * 5 / 9
                tempVlaue = String.format("%.1f", celsius)
            }
            return tempVlaue
        }

        fun strDate2Calendar(
            strDate: String?,
            pattern: String?
        ): Calendar {
            val sdf =
                SimpleDateFormat(pattern, Locale.US)
            var calendar: Calendar? = null
            try {
                val date = sdf.parse(strDate) //new Date();
                calendar = Calendar.getInstance()
                calendar.time = date
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return calendar!!
        }
        @Throws(ParseException::class)
        fun convertNewFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("MMM dd, yyyy | HH:mm")
            var convertFormatDate = SimpleDateFormat("MMM dd, yyyy")
           // convertFormatDate = SimpleDateFormat("hh:mm a")
            val convertFormatTime = SimpleDateFormat("hh:mm a")

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF | $timeF"
        }
        @Throws(ParseException::class)
        fun convertVitalFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            var convertFormatDate = SimpleDateFormat("MMM dd, yyyy")
            // convertFormatDate = SimpleDateFormat("hh:mm a")
            var convertFormatTime = SimpleDateFormat("HH:mm a")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("24hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm")
            }

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF at $timeF "
        }
        @Throws(ParseException::class)
        fun convertIOTDoorDateTimeFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("MM-dd-yyyy HH:mm")
            var convertFormatDate = SimpleDateFormat("MMM dd, yyyy")
            // convertFormatDate = SimpleDateFormat("hh:mm a")
            var convertFormatTime = SimpleDateFormat("HH:mm a")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("24hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm")
            }

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF at $timeF "
        }

        @Throws(ParseException::class)
        fun convertIOTDoorDateFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("MM-dd-yyyy HH:mm")
            var convertFormatDate = SimpleDateFormat("MMM dd, yyyy")
            // convertFormatDate = SimpleDateFormat("hh:mm a")
            var convertFormatTime = SimpleDateFormat("HH:mm a")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("24hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm")
            }

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            return "$dateF "
        }

        @Throws(ParseException::class)
        fun convertIOTDoorTimeFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("MM-dd-yyyy HH:mm")
            var convertFormatDate = SimpleDateFormat("MMM dd, yyyy")
            // convertFormatDate = SimpleDateFormat("hh:mm a")
            var convertFormatTime = SimpleDateFormat("HH:mm a")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("24hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm")
            }

            val date = currentFOrmat.parse(time)
            val timeF = convertFormatTime.format(date)
            return "$timeF"
        }

        @Throws(ParseException::class)
        fun convertEventDateFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("yyyyMMdd'T'HHmmss")
            var convertFormatDate = SimpleDateFormat("MMM dd, yyyy")
            // convertFormatDate = SimpleDateFormat("hh:mm a")
            var convertFormatTime = SimpleDateFormat("HH:mm a")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("24hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm")
            }

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            val timeF = convertFormatTime.format(date)
            return "$dateF"
        }
        @Throws(ParseException::class)
        fun convertEventOnlyDateFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("yyyyMMdd")
            var convertFormatDate = SimpleDateFormat("MMM dd, yyyy")

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            return "$dateF"
        }
        @Throws(ParseException::class)
        fun convertEventEditOnlyDateFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("yyyyMMdd")
            var convertFormatDate = SimpleDateFormat("yyyy-MM-dd")

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            return "$dateF"
        }
        @Throws(ParseException::class)
        fun convertEventTimeFormat(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("yyyyMMdd'T'HHmmss")
            var convertFormatTime = SimpleDateFormat("HH:mm")
            if (MySharedPreference.getPreferences(
                    context,
                    Constants.KEY_USER_TIME_FORMAT
                ).equals("24hour", ignoreCase = true)
            ) {
                convertFormatTime = SimpleDateFormat("hh:mm")
            }

            val date = currentFOrmat.parse(time)
            val timeF = convertFormatTime.format(date)
            return "$timeF"
        }

        @Throws(ParseException::class)
        fun convertEventDate(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("yyyyMMdd'T'HHmmss")
            var convertFormatDate = SimpleDateFormat("yyyy-MM-dd")

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            return "$dateF"
        }

        @Throws(ParseException::class)
        fun convertTaskDate(time: String, context: Context): String {
            val currentFOrmat = SimpleDateFormat("yyyyMMdd'T'HHmmss")
            var convertFormatDate = SimpleDateFormat("yyyy-MM-dd")

            val date = currentFOrmat.parse(time)
            val dateF = convertFormatDate.format(date)
            return "$dateF"
        }
        fun getToken(context: Context?): String? {
       return MySharedPreference.getPreferences(context, Constants.KEY_TOKEN)
        }


        fun getCurrentDate(): String? {
            val dateTime: String
            val c = Calendar.getInstance()
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            dateTime = sdf.format(c.time)
            return dateTime
        }

    }

//    @Throws(ParseException::class)
//    fun convertNewFormat(
//        time: String?,
//        context: Context?
//    ): String? {
//        val currentFOrmat =
//            SimpleDateFormat("MMM dd, yyyy | HH:mm")
//        var convertFormatDate =
//            SimpleDateFormat("MMM dd, yyyy")
//        if (MySharedPreference.getPreferences(context, Constants.KEY_USER_DATE_FORMAT)
//                .equals("dd-mm-yyyy")
//        ) {
//            convertFormatDate = SimpleDateFormat("dd MMM yyyy")
//        }
//        var convertFormatTime = SimpleDateFormat("HH:mm")
//        if (MySharedPreference.getPreferences(context, Constants.KEY_USER_TIME_FORMAT)
//                .equals("12hour")
//        ) {
//            convertFormatTime = SimpleDateFormat("hh:mm a")
//        }
//        val date = currentFOrmat.parse(time)
//        val dateF = convertFormatDate.format(date)
//        val timeF = convertFormatTime.format(date)
//        return "$dateF | $timeF"
//    }
//
private val isoDateTimeformatter: DateFormat? = SimpleDateFormat(
    "yyyyMMdd'T'HHmmss'Z'", Locale.US
)

    /** make it easier to create a date from its components  */
    fun createDate(
        year: Int, monthOfYear: Int,
        dayOfMonth: Int, hour: Int, minute: Int,
        second: Int
    ): Date? {
        val c = Calendar.getInstance()
        c[year, monthOfYear - 1, dayOfMonth, hour, minute] = second
        return c.time
    }


    /** compatibel to parseIsoDate with iso date format yyyyMMdd'T'HHmmss'Z'  */
    fun toIsoDate(date: Long): String? {
        return toIsoDate(Date(date))
    }

    /** compatibel to parseIsoDate with iso date format yyyyMMdd'T'HHmmss'Z'  */
    fun toIsoDate(date: Date?): String? {
        return isoDateTimeformatter!!.format(date)
    }

    /** compatibel to toIsoDate with iso date format yyyyMMdd'T'HHmmss'Z'  */
    @Throws(ParseException::class)
    fun parseIsoDate(mDateSelectedForAdd: String?): Date? {
        return isoDateTimeformatter!!.parse(mDateSelectedForAdd)
    }

    fun getDiffInSeconds(date1: Date?, date2: Date?): Long {
        return if (date1 == null || date2 == null) Long.MAX_VALUE else (date1.time - date2.time) / 1000
    }
}