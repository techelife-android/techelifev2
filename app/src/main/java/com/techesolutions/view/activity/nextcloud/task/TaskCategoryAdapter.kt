package com.techesolutions.view.activity.nextcloud.task

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.view.callbacks.ItemClickListener
import java.util.*

class TaskCategoryAdapter(
    context: Context,
    val categoryList: ArrayList<TasksCategory>,
    private var itemclick: TaskFoldersActivity
) : RecyclerView.Adapter<TaskCategoryAdapter.ViewHolder>() {
    var activity:Context = context
    var categoryClickListener=itemclick
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskCategoryAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.calender_category, parent, false)
        return ViewHolder(v, activity,categoryClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: TaskCategoryAdapter.ViewHolder, position: Int) {
        categoryList.get(position)?.let { holder.bindItems(it, activity, categoryClickListener, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return categoryList?.size!!
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context,
        categoryClickListener: ItemClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(
            tasks: TasksCategory,
            context: Context,
            categoryClickListener: ItemClickListener,
            position: Int
        ) {
            val textViewName = itemView.findViewById(R.id.title) as TextView
            if(tasks.title!=null)
                textViewName.text = tasks.title

            itemView.setOnLongClickListener {
                val position = layoutPosition
               // println("LongClick: $p")
               // noteClickListener.onNoteLongClick(position, itemView)

                true // returning true instead of false, works for me
            }

            itemView.setOnClickListener {
                val position = layoutPosition
                val tasks: TasksCategory = tasks
               /* val intent =
                    Intent(context, TaskActivity::class.java)
                intent.putExtra("type", tasks.title)
                context.startActivity(intent)*/
                categoryClickListener.onPositionClick(position)
            }
        }

    }

    fun getItem(notePosition: Int): TasksCategory? {
        return categoryList.get(notePosition)
    }

}