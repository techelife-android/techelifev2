package com.techesolutions.view.activity.nextcloud.files.filesync;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.ChunkedFileUploadRemoteOperation;
import com.owncloud.android.lib.resources.files.FileUtils;
import com.owncloud.android.lib.resources.files.ReadFolderRemoteOperation;
import com.owncloud.android.lib.resources.files.UploadFileRemoteOperation;
import com.techesolutions.R;
import com.techesolutions.utils.Constants;

import java.io.File;
import java.util.ArrayList;

public class MediaPushService extends IntentService implements OnRemoteOperationListener {
    private static OwnCloudClient mClient;
    private static Handler mHandler;
    private String instantUploadFolder;
    private Boolean photoEnable;
    private Boolean videoEnable;
    SharedPreferences shared;
    int photoindex = 0;
    int videoindex = 0;
    public MediaPushService() {
        super(MediaPushService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE);
        photoEnable= shared.getBoolean(Constants.KEY_PHOTO_UPLOAD, false);
        videoEnable=shared.getBoolean(Constants.KEY_VIDEO_UPLOAD, false);
        instantUploadFolder= shared.getString(Constants.KEY_REMOTE_FOLDER, "");
       /* if(instantUploadFolder.equals("") ||instantUploadFolder.equals("/"))
        {
            instantUploadFolder="/InstantUpload/";
        }*/
        mHandler = new Handler();
        String url=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "");
        String user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "");
        String pass=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "");
        Log.i("data", url+", "+user+", "+pass);
        Uri serverUri=null;
        if(url.endsWith("/")) {
            serverUri = Uri.parse(url.substring(0, url.length() - 1));
        }
        else{
            serverUri = Uri.parse(url);
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true);
        mClient.setCredentials(
                OwnCloudCredentialsFactory.newBasicCredentials(
                        user,
                        pass
                )
        );
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onHandleIntent(Intent intent) {
        if(photoEnable) {
            readPhoto(getApplicationContext());
        }
        if(videoEnable) {
            readVideo(getApplicationContext());
        }
    }

   /* private void readVideo(Context applicationContext) {
        final String orderBy = MediaStore.Video.Media._ID + " DESC";
        Cursor videocursor = applicationContext.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, null,
                null, orderBy);

        int video_column_index = videocursor
                .getColumnIndex(MediaStore.Video.Media._ID);
        int count = videocursor.getCount();
        String[] arrPath = new String[count];

        for (int i = 0; i < count; i++) {
            videocursor.moveToPosition(i);
            int id = videocursor.getInt(video_column_index);
            int dataColumnIndex = videocursor
                    .getColumnIndex(MediaStore.Video.Media.DATA);
            arrPath[i] = videocursor.getString(dataColumnIndex);
            Log.i("videos", arrPath[i]);
            File fileToUpload = new File(arrPath[i]);
            String remotePath = instantUploadFolder + fileToUpload.getName();
            String mimeType ="mp4";

            // Get the last modification date of the file from the file system
            Long timeStampLong = System.currentTimeMillis() / 1000;
            String timeStamp = timeStampLong.toString();
            UploadFileRemoteOperation uploadOperation =
                    new UploadFileRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, timeStamp);
            uploadOperation.execute(mClient, this, mHandler);
            *//*ChunkedFileUploadRemoteOperation uploadOperation =
                    new ChunkedFileUploadRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, "",timeStamp,true);
            uploadOperation.execute(mClient, this, mHandler);*//*
        }
        videocursor.close();
    }*/
   private void readVideo(Context applicationContext) {
       final String orderBy = MediaStore.Video.Media._ID + " DESC";
       Cursor videocursor = applicationContext.getContentResolver().query(
               MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, null,
               null, orderBy);

     /*  int video_column_index = videocursor
               .getColumnIndex(MediaStore.Video.Media._ID);*/
       if(videocursor!=null) {
           int count = videocursor.getCount();
           if (count > 0) {
               String[] arrPath = new String[count];
               videocursor.moveToPosition(videoindex);
               //int id = videocursor.getInt(video_column_index);
               int dataColumnIndex = videocursor
                       .getColumnIndex(MediaStore.Video.Media.DATA);
               if (count > videoindex) {
                   arrPath[videoindex] = videocursor.getString(dataColumnIndex);
                   Log.i("videos", arrPath[videoindex]);
                   instantUploadFolder = shared.getString(Constants.KEY_REMOTE_FOLDER, "");
                   if (instantUploadFolder.equals("") || instantUploadFolder.equals("/")) {
                       instantUploadFolder = "/InstantVideoUpload/";
                   }
                   File fileToUpload = new File(arrPath[videoindex]);

                   String remotePath = instantUploadFolder + fileToUpload.getName();
                   String mimeType = "mp4";

                   // Get the last modification date of the file from the file system
                   Long timeStampLong = System.currentTimeMillis() / 1000;
                   String timeStamp = timeStampLong.toString();
                   UploadFileRemoteOperation uploadOperation =
                           new UploadFileRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, timeStamp);
                   uploadOperation.execute(mClient, this, mHandler);
               }
     /*  for (int i = 0; i < count; i++) {
           videocursor.moveToPosition(i);
           int id = videocursor.getInt(video_column_index);
           int dataColumnIndex = videocursor
                   .getColumnIndex(MediaStore.Video.Media.DATA);
           arrPath[i] = videocursor.getString(dataColumnIndex);
           Log.i("videos", arrPath[i]);
           File fileToUpload = new File(arrPath[i]);
           String remotePath = instantUploadFolder + fileToUpload.getName();
           String mimeType ="mp4";

           // Get the last modification date of the file from the file system
           Long timeStampLong = System.currentTimeMillis() / 1000;
           String timeStamp = timeStampLong.toString();
           UploadFileRemoteOperation uploadOperation =
                   new UploadFileRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, timeStamp);
           uploadOperation.execute(mClient, this, mHandler);

       }*/
           }
           videocursor.close();
       }
   }
    public void readPhoto(Context context) {

        final String orderBy = MediaStore.Images.Media._ID + " DESC";
        Cursor imagecursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null,
                null, orderBy);

        /*int image_column_index = imagecursor
                .getColumnIndex(MediaStore.Images.Media._ID);*/
        if (imagecursor != null) {
            int count = imagecursor.getCount();
            if(count > 0){
            String[] arrPath = new String[count];
            imagecursor.moveToPosition(photoindex);
            //  int id = imagecursor.getInt(image_column_index);

            if (count > photoindex) {
                int dataColumnIndex = imagecursor
                        .getColumnIndex(MediaStore.Images.Media.DATA);
                arrPath[photoindex] = imagecursor.getString(dataColumnIndex);
                Log.i("images", arrPath[photoindex]);
                File fileToUpload = new File(arrPath[photoindex]);
                instantUploadFolder = shared.getString(Constants.KEY_REMOTE_FOLDER, "");
                if (instantUploadFolder.equals("") || instantUploadFolder.equalsIgnoreCase("/")) {
                    instantUploadFolder = "/InstantPhotoUpload/";
                }
                String remotePath = instantUploadFolder + fileToUpload.getName();
                String mimeType = "png";

                // Get the last modification date of the file from the file system
                Long timeStampLong = System.currentTimeMillis() / 1000;
                String timeStamp = timeStampLong.toString();
                UploadFileRemoteOperation uploadOperation =
                        new UploadFileRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, timeStamp);
                uploadOperation.execute(mClient, this, mHandler);
            }
   /*     for (int i = 0; i < count; i++) {
            imagecursor.moveToPosition(i);
            int id = imagecursor.getInt(image_column_index);
            int dataColumnIndex = imagecursor
                    .getColumnIndex(MediaStore.Images.Media.DATA);
            arrPath[i] = imagecursor.getString(dataColumnIndex);
            Log.i("images", arrPath[i]);
            File fileToUpload = new File(arrPath[i]);
            String remotePath = instantUploadFolder + fileToUpload.getName();
            String mimeType ="png";

            // Get the last modification date of the file from the file system
            Long timeStampLong = System.currentTimeMillis() / 1000;
            String timeStamp = timeStampLong.toString();
         *//*   WorkRequest uploadWorkRequest =
                    new OneTimeWorkRequest.Builder(UploadWorker.class)
                            .build();
            WorkManager.getInstance(context).enqueue(uploadWorkRequest);*//*

            UploadFileRemoteOperation uploadOperation =
                    new UploadFileRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, timeStamp);
            uploadOperation.execute(mClient, this, mHandler);

            *//*ChunkedFileUploadRemoteOperation uploadOperation =
                    new ChunkedFileUploadRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, "",timeStamp,true);
            uploadOperation.execute(mClient, this, mHandler);*//*

        }*/
        }
        imagecursor.close();
    }
        }

    @Override
    public void onRemoteOperationFinish(RemoteOperation operation, RemoteOperationResult result) {
        if (operation instanceof UploadFileRemoteOperation) {
           // Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT).show();
            if(photoEnable) {
                photoindex=photoindex+1;
                readPhoto(getApplicationContext());
            }
            if(videoEnable) {
                videoindex=videoindex+1;
                readVideo(getApplicationContext());
            }
        }
       }

    private void startRefresh() {
        ReadFolderRemoteOperation refreshOperation = new ReadFolderRemoteOperation(FileUtils.PATH_SEPARATOR);
        refreshOperation.execute(mClient, this, mHandler);
    }
}




