package com.techesolutions.view.activity.nextcloud.task

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.UploadCalenderRemoteOperation
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.utils.Utils.Companion.convertShortDateFormat2
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils
import kotlinx.android.synthetic.main.activity_create_task.*
import kotlinx.android.synthetic.main.layout_header.*
import org.joda.time.DateTimeZone
import org.threeten.bp.format.DateTimeFormatter
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CreateTaskActivity : AppCompatActivity() ,  View.OnClickListener,
    OnRemoteOperationListener {
    private val progressDialog = CustomProgressDialog()
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    val DEBUG = true //BuildConfig.DEBUG;
    var selectedDate: String=""
    var selectedStartDate: String=""
    var timestamp: String=""
    var startDate: Date?=null
    var action: String?=null
    var from: String?=null
    var category: String?=null
    var desc: String?=null
    var title: String?=null
    var user: String?=null
    var eventStartTime: String?=null
    var eventEndTime: String?=null
    var fileName: String?=null
    var remotePath:String? = null
    private val FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_task)
        headerTitle.text = "Create Task"

        back.setOnClickListener(this)
        cancel?.setOnClickListener(this)
        etTimeFrom?.setOnClickListener(this)
        etTimeTo?.setOnClickListener(this)
        save?.setOnClickListener(this)
        mHandler = Handler()
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", url + ", " + user + ", " + pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
                pass!!
            )
        )
              // Extract the day from the text
        val calendar = Calendar.getInstance()
        val currentDate:Date=calendar.getTime()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        // val sdf = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'")
        //val i: Intent = getIntent()
        category = getIntent().getStringExtra("category")
        from = getIntent().getStringExtra("from")
        action = getIntent().getStringExtra("action")
        if(action.equals("edit"))
        {
            headerTitle.text = "Edit Task"
            if(getIntent().getStringExtra("stime").length>8) {
                selectedStartDate = Utils.convertEventDate(
                    getIntent().getStringExtra("stime"),
                    this@CreateTaskActivity
                )
            }
            else{
                if(getIntent().getStringExtra("stime").length>1) {
                    selectedStartDate = Utils.convertEventEditOnlyDateFormat(
                        getIntent().getStringExtra("stime"),
                        this@CreateTaskActivity
                    )
                }
                else{
                    selectedStartDate=sdf.format(currentDate)
                }
            }
            launchExistMode()
        }
        else{
            selectedStartDate = sdf.format(currentDate)
        }
       // Toast.makeText(this, "Current Day: $selectedStartDate", Toast.LENGTH_SHORT).show()

        val sdfTimestamp = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'")
        timestamp = sdfTimestamp.format(currentDate)
       // Toast.makeText(this, "Timestamp Day: $timestamp", Toast.LENGTH_SHORT).show()

        etTimeFrom.setOnClickListener {
            val c: Calendar = Calendar.getInstance()
            val y = c.get(Calendar.YEAR)
            val m = c.get(Calendar.MONTH)
            val d = c.get(Calendar.DAY_OF_MONTH)
            val datepickerdialog: DatePickerDialog = DatePickerDialog(
                this@CreateTaskActivity,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    val date: String = Utils.getDisplayedDateFormatEvent(
                        dayOfMonth,
                        monthOfYear,
                        year,
                        this@CreateTaskActivity
                    )
                    showTime(date)
                },
                y,
                m,
                d
            )
            datepickerdialog.show()


        }
        etTimeTo.setOnClickListener {
            if (etTimeFrom.text.toString().length > 1) {

                val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm")
                formatter.isLenient = false

               /* val curDate = Date()
                val curMillis = curDate.time
                val curTime = formatter.format(curDate)*/

                val oldTime = etTimeFrom.text.toString()
                val oldDate = formatter.parse(oldTime)
                val oldMillis = oldDate.time


                /*val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm")
                try {
                    val mDate = sdf.parse(
                        convertShortDateFormat2(
                            etTimeFrom.text.toString(),
                            this@CreateTaskActivity
                        )
                    )
                    val timeInMilliseconds = mDate.time
                    Toast.makeText(
                        this@CreateTaskActivity,
                        "" + timeInMilliseconds,
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: ParseException) {
                    e.printStackTrace()
                }*/
               // val timeInMilliseconds: Long = mDate.getTime()
                var min: Int = 0
                val c: Calendar = Calendar.getInstance()
                val y = c.get(Calendar.YEAR)
                val m = c.get(Calendar.MONTH)
                val d = c.get(Calendar.DAY_OF_MONTH)
                val datepickerdialog: DatePickerDialog = DatePickerDialog(
                    this@CreateTaskActivity,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        val date: String = Utils.getDisplayedDateFormatEvent(
                            dayOfMonth,
                            monthOfYear,
                            year,
                            this@CreateTaskActivity
                        )
                        showToTime(date)
                    },
                    y,
                    m,
                    d
                )
                datepickerdialog.getDatePicker().setMinDate(oldMillis);
                datepickerdialog.show()
            }
            else{
                onErrorListener("Select start date and time")
            }
        }
    }

    private fun showTime(date: String) {
        val c: Calendar = Calendar.getInstance()
        val hh = c.get(Calendar.HOUR_OF_DAY)
        var mm = c.get(Calendar.MINUTE)
        etTimeFrom.setText(
            date + " " + Utils.getDisplayedTimeFormat(
                hh,
                mm,
                this@CreateTaskActivity
            )
        )

        val timePickerDialog: TimePickerDialog = TimePickerDialog(
            this@CreateTaskActivity,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                etTimeFrom.setText(
                    date + " " +
                            Utils.getDisplayedTimeFormat(
                                hourOfDay,
                                minute,
                                this@CreateTaskActivity
                            )
                )
            },
            hh,
            mm,
            true
        )
        timePickerDialog.show()
    }

    private fun showToTime(date: String) {
        var min: Int=0
        val c: Calendar = Calendar.getInstance()
        val hh = c.get(Calendar.HOUR_OF_DAY)
        var mm = c.get(Calendar.MINUTE)
        //val curMillis: Long = date.getTime()
        etTimeTo.setText(date + " " + Utils.getDisplayedTimeFormat(hh, mm, this@CreateTaskActivity))
        val timePickerDialog: TimePickerDialog = TimePickerDialog(
            this@CreateTaskActivity,
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                etTimeTo.setText(
                    date + " " +
                            Utils.getDisplayedTimeFormat(
                                hourOfDay,
                                minute,
                                this@CreateTaskActivity
                            )
                )
            },
            hh,
            mm,
            true
        )
        timePickerDialog.show()
    }

    private fun launchExistMode() {
        val calendar = Calendar.getInstance()
        val currentDate:Date=calendar.getTime()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        var stime: String? =""
        if(getIntent().getStringExtra("stime").length>8) {
            stime= Utils.convertEventTimeFormat(
                getIntent().getStringExtra("stime"),
                this@CreateTaskActivity
            )
        }
        var etime: String? =""
        if(getIntent().getStringExtra("etime").length>8) {
            etime =
                Utils.convertEventTimeFormat(
                    getIntent().getStringExtra("etime"),
                    this@CreateTaskActivity
                )
        }

        var eventDate: String?=sdf.format(currentDate)
        if(getIntent().getStringExtra("stime").length>8) {
            eventDate =
                Utils.convertEventDateFormat(
                    getIntent().getStringExtra("stime"),
                    this@CreateTaskActivity
                )
        }
     /*   else{
            if(getIntent().getStringExtra("etime").length>1) {
                eventDate =
                    Utils.convertEventOnlyDateFormat(
                        getIntent().getStringExtra("stime"),
                        this@CreateTaskActivity
                    )
            }
        }*/
        etTimeFrom!!.setText(stime)
        etTimeTo!!.setText(etime)
        etDesc!!.setText(getIntent().getStringExtra("desc"))
        etSummery!!.setText(getIntent().getStringExtra("title"))
    }

   /* override fun onDayClick(date: Date?) {
        Log.i("date", "" + date)
        //startDate=date
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        selectedStartDate = sdf.format(date)
        Log.i("date", "" + selectedStartDate)
       // Toast.makeText(this, "onDayClick: $selectedStartDate", Toast.LENGTH_SHORT).show()
    }*/
    private fun showTimeDialog(context: CreateTaskActivity) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.select_time)
        val etcontactNumber = dialog.findViewById(R.id.etContact) as EditText
        val etrelation = dialog.findViewById(R.id.etRelation) as EditText
        val etname = dialog.findViewById(R.id.etName) as EditText
        val cancel = dialog.findViewById(R.id.cancel) as TextView
        val save = dialog.findViewById(R.id.save) as TextView
        save.setOnClickListener {
            showLoading("Please wait..")
            var name=etcontactNumber.text.toString()
            var contact=etname.text.toString()
            var relation=etrelation.text.toString()
            Log.i("et data", name + ": " + contact)

        }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.back -> {
                finish()
            }
            R.id.save -> {
                createCal()
            }

            }
    }

    private fun createCal() {
       /* if (TextUtils.isEmpty(etTimeFrom!!.text.toString())) {
            onErrorListener("Please select start time")
            return
        }*/
        if (TextUtils.isEmpty(etSummery!!.text.toString())) {
            onErrorListener("Please enter title")
            return
        }
        var ICS_DIRECTORY: String = "/$user/ics_teche";
        var icsFile: File? =null
        showLoading("Please wait..")
        var Description=etDesc.text.toString()
        var startTime=etTimeFrom.text.toString()
        var endTime=etTimeTo.text.toString()
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
        val dateoutFormat=SimpleDateFormat("yyyyMMdd'T'HHmmss")
        var sDate: String=""
        var eDate: String=""
        if(startTime.length>1)
        {
            var sDateTime=startTime
            val sdate: Date = inputFormat.parse(sDateTime)
            sDate = dateoutFormat.format(sdate)
        }
        if(endTime.length>1)
        {
            var eDateTime=endTime
            val edate: Date = inputFormat.parse(eDateTime)
            eDate = dateoutFormat.format(edate)

        }

        var Summery=etSummery.text.toString()
      //  Toast.makeText(this, "selectedStartDate Day: $sDate", Toast.LENGTH_SHORT).show()
        val timezone = DateTimeZone.forID(TimeZone.getDefault().id)
        try
        {
            val vdfdirectory =  File(FileUtils.getRootDirPath(applicationContext), ICS_DIRECTORY)
            // have the object build the directory structure, if needed.
            if (!vdfdirectory.exists())
            {
                vdfdirectory.mkdirs()
            }
            if(action.equals("edit"))
            {
                fileName=getIntent().getStringExtra("filename")
            }
            else{
                fileName= "cal" + Calendar.getInstance().getTimeInMillis() + ".ics"
            }
            icsFile = File(
                vdfdirectory,
                fileName
            )
//            if (!icsFile.exists()) {
//                icsFile.createNewFile();
//            }
            var fw: FileWriter? = null
            fw = FileWriter(icsFile)

            fw.write("BEGIN:VCALENDAR\r\n");
            fw.write("VERSION:2.0\r\n");
            fw.write("PRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\n");
            fw.write("BEGIN:VTODO\r\n");
            fw.write("UID:mobtest2_" + Calendar.getInstance().timeInMillis + "@office.teche.solutions\r\n")
            if(startTime.length>1) {
                fw.write("DTSTART:" + sDate + "\r\n")
            }
            if(endTime.length>1) {
                fw.write("DUE:" + eDate + "\r\n")
            }
            fw.write("DTSTAMP:" + timestamp + "\r\n")
            fw.write("SEQUENCE:0\r\n")
            fw.write("STATUS:CONFIRMED\r\n")
            fw.write("DESCRIPTION:" + Description + "\r\n");
            fw.write("SUMMARY:" + Summery + "\r\n");
            fw.write("END:VTODO\r\n");
            fw.write("END:VCALENDAR\r\n");
            fw.close()
            /* Intent i = new Intent(); //this will import vcf in contact list
           i.setAction(android.content.Intent.ACTION_VIEW);
           i.setDataAndType(Uri.fromFile(vcfFile), "text/x-vcard");
           startActivity(i);*/
            //Toast.makeText(this@CreateEventActivity, "Created!", Toast.LENGTH_SHORT).show()
            uploadCalender(icsFile!!)
        }
        catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun uploadCalender(icsFile: File) {
        remotePath = "/"+ icsFile.name
        Log.i("remote path Calender", remotePath)
        // Get the last modification date of the file from the file system
        val timeStampLong = System.currentTimeMillis() / 1000
        val timeStamp = timeStampLong.toString()
        val uploadOperation = UploadCalenderRemoteOperation(
            user + " " + category,
            icsFile.getAbsolutePath(),
            remotePath,
            "",
            "",
            timeStamp
        )
        uploadOperation.execute(mClient, this, mHandler)


    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {
        if (!result!!.isSuccess()) {

            closeLoading()
        }
        else if(operation is UploadCalenderRemoteOperation)
        {

            onSuccessfulUpload(operation as UploadCalenderRemoteOperation?, result)

        }
    }
    private fun onSuccessfulUpload(
        uploadContactRemoteOperation: UploadCalenderRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        closeLoading()
        val intent = Intent()
        intent.putExtra("filename",remotePath)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}