package com.techesolutions.utils

/**
 * Created by Neelam on 19/12/2020.
 */
object Constants {


    const val storage_folder: String="TecheCloud"

    //Fonts*********************************************
    const val BOLD_FONT = "fonts/Roboto-Black.ttf"
    const val A_TECHE_REGULAR_FONT = "fonts/Roboto-Regular.ttf"
    const val A_TECHE_SEMIBOLD_FONT = "fonts/Roboto-Black.ttf"
    const val A_TECHE_MEDIUM_FONT = "fonts/Roboto-Medium.ttf"

    // LANGUALE SETUP**********************************************
    const val KEY_LANGUAGE = "KEY_LANGUAGE"
    const val LANGUAGE = "lang"
    const val LANGUAGE_API_DATA = "LANGUAGE_API_DATA"

    // LOGIN SETUP**********************************************
    const val KEY_LOGGEDIN = "KEY_LOGGEDIN"
    const val KEY_TOKEN = "KEY_TOKEN"
    const val KEY_USERNAME = "KEY_USERNAME"
    const val KEY_PASSWORD = "KEY_PASSWORD"
    const val Login_User_ID = "Login_User_ID"

    //BASE URL**************************************************
    const val BASE_URL = "https://teche.life/api/"

   // const val NEXTCLOUD_BASE_URL = "https://office.teche.solutions/"
   //  const val BASE_URL = "https://teche.life/apitest/"

    //LANGUAGE API
    const val LANGUAGE_URL = "api/language/config"
    const val APPORIGIN = "apporigin"


    //TIMEZONE API
    const val TIMEZONE_URL = "api/patient/update/timezone"

    //LOGIN API
    const val LOGIN_URL = "api/login"
    const val FORGOT_PASSWORD_URL = "api/forgot/password"
    const val GET_VERIFICATION_CODE_URL = "api/getVerificationCode"
    const val RESET_PASSWORD_URL = "api/reset/password"
    const val OTP_SUBMIT_URL = "api/forgot/password/verify"

    //DASHBOARD
    const val GET_VITALS_URL = "api/get/recent/vitals"
    const val GET_VITALS_DETAILS = "api/get/vital/list"
    const val SYNC_VITALS_READINGS = "api/post/vital"


    //Notes
    const val GET_NOTES_URL = "index.php/apps/notes/api/v1/notes"
    const val CREATE_NOTES_URL = "index.php/apps/notes/api/v1/notes"
    const val EDIT_NOTES_URL = "index.php/apps/notes/api/v1/notes/"
    const val DELETE_NOTES_URL = "index.php/apps/notes/api/v1/notes/"

    //Vitals
    const val KEY_USER_MASTER_AFFILIATION = "KEY_USER_MASTER_AFFILIATION"
    const val KEY_USER_PHONE_FORMAT = "KEY_USER_PHONE_FORMAT"
    const val KEY_USER_WEIGHT_UNIT = "KEY_USER_WEIGHT_UNIT"
    const val KEY_USER_HEIGHT_UNIT = "KEY_USER_HEIGHT_UNIT"
    const val KEY_USER_TEMP_UNIT = "KEY_USER_TEMP_UNIT"
    const val KEY_USER_PHONE_PREFIX = "KEY_USER_PHONE_PREFIX"
    const val KEY_USER_DATE_FORMAT = "KEY_USER_DATE_FORMAT"
    const val KEY_USER_TIME_FORMAT = "KEY_USER_TIME_FORMAT"
    const val KEY_USER_AFFILIATION_TIMEZONE = "KEY_USER_AFFILIATION_TIMEZONE"
    const val KEY_USER_TIME_ZONE = "USER_TIME_ZONE"
    const val KEY_TIME_ZONES = "KEY_TIME_ZONES"
    const val KEY_MOBILE_TIME_ZONE = "KEY_MOBILE_TIME_ZONE"
    const val KEY_TIME_ZONE_VERIFIED = "KEY_TIME_ZONE_VERIFIED"
    const val KEY_DONT_SHOW = "KEY_DONT_SHOW"
    const val KEY_NEXT_CLOUD = "KEY_NEXT_CLOUD"
    const val KEY_NEXT_CLOUD_FEATURES = "KEY_NEXT_CLOUD_FEATURES"
    const val KEY_NEXT_CLOUD_URL = "KEY_NEXT_CLOUD_URL"
    const val KEY_NEXT_CLOUD_USER = "KEY_NEXT_CLOUD_USER"
    const val KEY_NEXT_CLOUD_PASSWORD = "KEY_NEXT_CLOUD_PASSWORD"
    const val KEY_UNIQUE = "KEY_UNIQUE"

    //Notes
    const val MIMETYPE_TEXT_PLAIN = "text/plain"
    const val PARAM_NOTE_ID = "noteId"
    const val PARAM_NOTE = "note"
    const val PARAM_TITLE = "title"
    const val PARAM_ACCOUNT_ID = "accountId"
    const val PARAM_CATEGORY = "category"
    const val PARAM_CONTENT = "content"
    const val PARAM_FAVORITE = "favorite"

    // FILE PREFS SETUP**********************************************
    const val KEY_PHOTO_UPLOAD = "KEY_PHOTO_UPLOAD"
    const val KEY_VIDEO_UPLOAD = "KEY_VIDEO_UPLOAD"
    const val KEY_REMOTE_FOLDER = "KEY_REMOTE_FOLDER"

    // Contact PREFS SETUP**********************************************
    const val KEY_CONTACT_UPLOAD = "KEY_CONTACT_UPLOAD"
    const val KEY_CONTACT_AUTO_SAVE = "KEY_CONTACT_AUTO_SAVE"

    //master Data for rooms and devices
    const val ROOM_LIST_URL = "api/get/iot/mastermote/pagedata"
    const val IOT_DATA_URL = "api/get/iot/mastermote/list"
    const val DOOR_WISE_DATA_URL = "api/get/iot/door/list"
    const val DOOR_BY_ROOM_URL = "api/get/iot/room/doors"

    //USer Saved info
    const val USER_WEB_INFO_URL = "api/saved/credentials/list"
    const val ADD_USER_INFO_URL = "api/saved/credentials/add"
    const val EDIT_USER_INFO_URL = "api/saved/credentials/update/"
    const val DELETE_USER_INFO_URL = "api/saved/credentials/delete/"

    //Device list
    const val ASSIGEND_DEVICE_LIST_URL="api/get/user/device/list"
    const val KEY_USER_INFO_PASSWORD = "user_info_password"

    //support

    const val TICKET_LIST_URL= "api/ticket/list"
    const val ADD_TICKET_URL= "api/ticket/add"
    const val ADD_COMMENT_URL= "api/message/send"
    const val COMMENT_LIST_URL= "api/message/list"

    //Camera navigation
    const val CAMERA_DATA_URL = "api/camera/list"
    const val CAMERA_NAVIGATION_URL="api/camera/controls"

}
