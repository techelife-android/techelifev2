package com.techesolutions.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.dashboard.HeartRateSelectionData
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.vitaldevices.DeviceInstructionManualActivity
import com.techesolutions.view.activity.vitaldevices.TecheBPActivity
import com.techesolutions.view.activity.vitaldevices.TechePOActivity
import com.techesolutions.view.activity.vitaldevices.TecheWatchActivity
import java.util.*

class HeartRateSelectionAdapter(
    context:Context,
   val  userList: ArrayList<HeartRateSelectionData>
) : RecyclerView.Adapter<HeartRateSelectionAdapter.ViewHolder>() {
    var activity:Context = context
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeartRateSelectionAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_heartrate_selection, parent, false)
        return ViewHolder(v,activity)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: HeartRateSelectionAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position],activity)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,context: Context) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(vital: HeartRateSelectionData,context: Context) {
            val textViewName = itemView.findViewById(R.id.activityName) as TextView
            val image = itemView.findViewById(R.id.deviceImage) as ImageView
            textViewName.text = vital.Title

            if(vital.vitalName.equals("Activities"))
            image.setImageResource(R.drawable.ic_smartwatch)

            if(vital.vitalName.equals("Pulse_Oximetry"))
            image.setImageResource(R.drawable.ic_po)

            if(vital.vitalName.equals("Blood_Pressure"))
            image.setImageResource(R.drawable.ic_bp)

            itemView.setOnClickListener(View.OnClickListener {
                if(context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getBoolean(Constants.KEY_DONT_SHOW, false)) {
                    if(vital.vitalName!!.equals("Activities"))
                    {
                        var intent = Intent(context, TecheWatchActivity::class.java)
                        intent.putExtra("title", vital.Title)
                        intent.putExtra("vitalName", vital.vitalName)
                        intent.putExtra("subVitalName", vital.subVitalName)
                        intent.putExtra("unitName1", vital.data1)
                        intent.putExtra("unitName2", vital.data2)
                        context.startActivity(intent)
                    }
                    else if(vital.vitalName!!.equals("Pulse_Oximetry"))
                    {
                        var   intent = Intent(context, TechePOActivity::class.java)
                        intent.putExtra("title", vital.Title)
                        intent.putExtra("vitalName", vital.vitalName)
                        intent.putExtra("subVitalName", vital.subVitalName)
                        intent.putExtra("unitName1", vital.data1)
                        intent.putExtra("unitName2", vital.data2)
                        context.startActivity(intent)
                    }
                    else if(vital.vitalName!!.equals("Blood_Pressure"))
                    {
                        var   intent = Intent(context, TecheBPActivity::class.java)
                        intent.putExtra("title", vital.Title)
                        intent.putExtra("vitalName", vital.vitalName)
                        intent.putExtra("subVitalName", vital.subVitalName)
                        intent.putExtra("unitName1", vital.data1)
                        intent.putExtra("unitName2", vital.data2)
                        context.startActivity(intent)
                    }
                }
                else {
                    val intent =
                            Intent(context, DeviceInstructionManualActivity::class.java)
                    intent.putExtra("title", vital.Title)
                    intent.putExtra("vitalName", vital.vitalName)
                    intent.putExtra("subVitalName", vital.subVitalName)
                    intent.putExtra("unitName1", vital.data1)
                    intent.putExtra("unitName2", vital.data2)
                    context.startActivity(intent)
                }
            })
        }

    }

}