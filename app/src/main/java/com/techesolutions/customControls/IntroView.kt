package com.techesolutions.customControls

import android.app.Activity
import android.app.Dialog
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import com.techesolutions.R
import kotlinx.android.synthetic.main.intro_view.view.*
import org.json.JSONObject
import com.techesolutions.utils.MySharedPreference
import android.content.Context
import com.techesolutions.utils.Constants

class IntroView {
    var vitalUnitLabels: JSONObject? = null
    lateinit var dialog: CustomDialog

    fun show(context: Context): Dialog {
        return show(context, null)
    }

    fun show(context: Context, title: CharSequence?): Dialog {
        val inflater = (context as Activity).layoutInflater
        val view = inflater.inflate(R.layout.intro_view, null)
        val shared =
            context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

        // Card Color
        view.cp_bg_view.setBackgroundColor(Color.parseColor("#70000000"))
        dialog = CustomDialog(context)
        dialog.setContentView(view)
        view.textAll.text=vitalUnitLabels!!.getString("playallintro")
        view.textAdd.text=vitalUnitLabels!!.getString("plussingleintro")
        view.textPlay.text=vitalUnitLabels!!.getString("playsingleintro")

        dialog.show()


        return dialog
    }

    private fun setColorFilter(drawable: Drawable, color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            drawable.colorFilter = BlendModeColorFilter(color, BlendMode.SRC_ATOP)
        } else {
            @Suppress("DEPRECATION")
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
        }
    }

    class CustomDialog(context: Context) : Dialog(context, R.style.CustomDialogTheme) {
        init {
            // Set Semi-Transparent Color for Dialog Background
            window?.decorView?.rootView?.setBackgroundResource(android.R.color.transparent)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                window?.decorView?.setOnApplyWindowInsetsListener { _, insets ->
                    insets.consumeSystemWindowInsets()
                }
            }
        }
    }
}