package com.techesolutions.services.watch

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Binder
import android.os.IBinder
import android.os.Message
import android.os.Vibrator
import com.fitpolo.support.MokoConstants
import com.fitpolo.support.MokoSupport
import com.fitpolo.support.callback.MokoConnStateCallback
import com.fitpolo.support.callback.MokoOrderTaskCallback
import com.fitpolo.support.entity.OrderTaskResponse
import com.fitpolo.support.handler.BaseMessageHandler
import com.fitpolo.support.log.LogModule

val EXTRA_CONN_COUNT = "extra_conn_count"
class MokoService : Service(), MokoConnStateCallback, MokoOrderTaskCallback {

    override fun onConnectSuccess() {
        val intent =
            Intent(MokoConstants.ACTION_DISCOVER_SUCCESS)
        sendOrderedBroadcast(intent, null)
    }

    override fun onDisConnected() {
        val intent =
            Intent(MokoConstants.ACTION_CONN_STATUS_DISCONNECTED)
        sendOrderedBroadcast(intent, null)
    }

    override fun onConnTimeout(reConnCount: Int) {
        val intent =
            Intent(MokoConstants.ACTION_DISCOVER_TIMEOUT)
        intent.putExtra(EXTRA_CONN_COUNT, reConnCount)
        sendBroadcast(intent)
    }
    private val mBinder: IBinder = LocalBinder()
    override fun onBind(intent: Intent?): IBinder? {
        LogModule.i("绑定MokoService...onBind")
        return mBinder    }
    inner class LocalBinder : Binder() {
        fun getService() : MokoService? {
            return this@MokoService
        }
    }
    private var mediaPlayer: MediaPlayer? = null
    override fun onFindPhone() {
        val vib =
            getSystemService(VIBRATOR_SERVICE) as Vibrator
        if (vib.hasVibrator()) {
            vib.vibrate(longArrayOf(500, 1000, 500, 1000, 500, 1000, 500, 1000), -1)
        }
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(
                this,
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            )
            if (mediaPlayer == null) {
                LogModule.i("未设置铃声")
                return
            }
            mediaPlayer!!.start()
            mHandler?.postDelayed(Runnable {
                if (mediaPlayer != null && mediaPlayer!!.isPlaying()) {
                    mediaPlayer!!.stop()
                    mediaPlayer!!.release()
                    mediaPlayer = null
                }
            }, 10000)
            return
        }
        if (mediaPlayer!!.isPlaying) {
            mediaPlayer!!.stop()
            mediaPlayer!!.release()
            mediaPlayer = null
            mediaPlayer = MediaPlayer.create(
                this,
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            )
            mediaPlayer!!.start()
            mHandler?.removeMessages(0)
            mHandler?.postDelayed(Runnable {
                if (mediaPlayer != null && mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.stop()
                    mediaPlayer!!.release()
                    mediaPlayer = null
                }
            }, 10000)
        } else {
            mediaPlayer = MediaPlayer.create(
                this,
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            )
            if (mediaPlayer == null) {
                LogModule.i("未设置铃声")
                return
            }
            mediaPlayer!!.start()
            mHandler?.removeMessages(0)
            mHandler?.postDelayed(Runnable {
                if (mediaPlayer != null && mediaPlayer!!.isPlaying) {
                    mediaPlayer!!.stop()
                    mediaPlayer!!.release()
                    mediaPlayer = null
                }
            }, 10000)
        }
    }

    override fun onOrderResult(response: OrderTaskResponse?) {
        val intent =
            Intent(Intent(MokoConstants.ACTION_ORDER_RESULT))
        intent.putExtra(MokoConstants.EXTRA_KEY_RESPONSE_ORDER_TASK, response)
        sendOrderedBroadcast(intent, null)
    }

    override fun onOrderTimeout(response: OrderTaskResponse?) {
        val intent =
            Intent(Intent(MokoConstants.ACTION_ORDER_TIMEOUT))
        intent.putExtra(MokoConstants.EXTRA_KEY_RESPONSE_ORDER_TASK, response)
        sendOrderedBroadcast(intent, null)    }

    override fun onOrderFinish() {
        sendOrderedBroadcast(Intent(MokoConstants.ACTION_ORDER_FINISH), null)
    }
    var mHandler: ServiceHandler? = null

    class ServiceHandler(service: MokoService?) :
        BaseMessageHandler<MokoService?>(service) {

        override fun handleMessage(t: MokoService?, msg: Message?) {
            TODO("Not yet implemented")
        }
    }

    override fun onCreate() {
        LogModule.i("创建MokoService...onCreate")
        super.onCreate()
    }

    fun connectBluetoothDevice(address: String?) {
        MokoSupport.getInstance().connDevice(this, address, this)
    }
    fun disConnectBle() {
        MokoSupport.getInstance().reconnectCount = 0
        MokoSupport.getInstance().disConnectBle()
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    ///////////////////////////////////////////////////////////////////////////
    fun isSyncData(): Boolean {
        return MokoSupport.getInstance().isSyncData
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        LogModule.i("启动MokoService...onStartCommand")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onLowMemory() {
        LogModule.i("内存吃紧，销毁MokoService...onLowMemory")
        disConnectBle()
        super.onLowMemory()
    }
    override fun onUnbind(intent: Intent?): Boolean {
        LogModule.i("解绑MokoService...onUnbind")
        return super.onUnbind(intent)
    }


    override fun onDestroy() {
        LogModule.i("销毁MokoService...onDestroy")
        disConnectBle()
        MokoSupport.getInstance().setOpenReConnect(false)
        super.onDestroy()
    }

}


