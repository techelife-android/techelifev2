package com.techesolutions.view.adapter

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.techesolutions.utils.Constants
import org.json.JSONObject
import android.view.Window
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.remote.model.iot.IOTDetail
import com.techesolutions.utils.Utils
import okhttp3.internal.userAgent
import java.util.ArrayList

class IOTReadingAdapter(
    context:Context
) : RecyclerView.Adapter<IOTReadingAdapter.ViewHolder>() {
    var activity:Context = context
    var iotDetailsList: ArrayList<IOTDetail> = ArrayList<IOTDetail>()
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IOTReadingAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.device_reading_item, parent, false)
        return ViewHolder(v,activity)
    }
    fun addData(dataViews: ArrayList<IOTDetail>) {
       // iotDetailsList.clear()
        for(data in dataViews){
            this.iotDetailsList?.add(data)
        }
        notifyDataSetChanged()
    }
    //this method is binding the data on the list
    override fun onBindViewHolder(holder: IOTReadingAdapter.ViewHolder, position: Int) {
        holder.bindItems(iotDetailsList[position],activity)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return iotDetailsList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,context: Context) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(vital: IOTDetail, context: Context) {
            val data1  = itemView.findViewById(R.id.unitOneName) as TextView
            val data2  = itemView.findViewById(R.id.unitTwoName) as TextView
            val dataValue1  = itemView.findViewById(R.id.valueUnitOne) as TextView
            val dataValue2  = itemView.findViewById(R.id.valueUnitTwo) as TextView
            val date  = itemView.findViewById(R.id.valueUnitThree) as TextView
            data1.text = vital.data1
            data2.text = vital.data2
            var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.LANGUAGE_API_DATA, "")
            var vitalUnitLabels: JSONObject? = null
            if(vital.Title.contains("Weight"))
            {

                var data2:String=""
                if(vital.data1val.toString().equals(""))
                {
                    dataValue1.text = "0.0"
                    data2="0.0"
                }
                else {
                    dataValue1.text = ""+vital.data1val+" "+vitalUnitLabels!!.getString("Kg")
                    data2=vital.data1val
                }
                dataValue2.text = ""+ Utils.getServerWeight(data2,context)+" "+vitalUnitLabels!!.getString("Lbs")

            }
            else if(vital.Title.contains("Temperature"))
            {

                var data2:String=""
                if(vital.data1val.toString().equals(""))
                {
                    dataValue1.text = "0.0"
                    dataValue2.text = "0.0"
                }
                else {
                    data2=vital.data1val
                    if(vital.data2val.equals("C"))
                    {
                        dataValue2.text = ""+vital.data1val
                        dataValue1.text = ""+ Utils.convertTemperature(data2,true)
                    }
                    else{
                        dataValue2.text = ""+ Utils.convertTemperature(data2,false)
                        dataValue1.text = ""+vital.data1val
                    }

                }

            }
            else if(vital.Title.contains("Pulse"))
            {
                dataValue1.text = vital.data1val+"%"
                dataValue2.text = vital.data2val
            }
            else if(vital.Title.contains("Heart"))
            {
                dataValue1.text = vital.data1val+" "+vitalUnitLabels!!.getString("BPM")
                dataValue2.text = vital.data2val
            }
            else if(vital.Title.contains("Pressure"))
            {
                dataValue1.text = vital.data1val+" "+vitalUnitLabels!!.getString("bp_Unit")
                dataValue2.text = vital.data2val+" "+vitalUnitLabels!!.getString("bp_Unit")
            }
            else if(vital.Title.contains("Sugar"))
            {
                dataValue1.text = vital.data1val+" "+vitalUnitLabels!!.getString("Mg_dl")
                dataValue2.text = vital.data2val
            }

            else{
                dataValue1.text = ""+vital.data1val
                dataValue2.text = ""+ vital.data2val
            }

            date.text=Utils.convertIOTDoorDateTimeFormat(
                vital.date,
                context
            )+context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.KEY_USER_TIME_ZONE, "")

            // vital.date

        }
        private fun showDialog(title: String, context: Context) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.add_vital_dialog)
            val body = dialog.findViewById(R.id.topHeader) as TextView
            body.text = title
            val yesBtn = dialog.findViewById(R.id.save) as TextView
            val noBtn = dialog.findViewById(R.id.cancel) as TextView
            yesBtn.setOnClickListener {
                dialog.dismiss()
            }
            noBtn.setOnClickListener { dialog.dismiss() }
            dialog.show()

        }
    }
}