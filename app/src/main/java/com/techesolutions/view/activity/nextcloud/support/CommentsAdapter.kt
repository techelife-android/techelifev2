package com.techesolutions.view.activity.nextcloud.support

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.techesolutions.R
import com.techesolutions.data.remote.model.support.CommentData

class CommentsAdapter(
    context:Context,
    val commentList: ArrayList<CommentData>

) : RecyclerView.Adapter<CommentsAdapter.ViewHolder>() , Filterable {
    var activity:Context = context
    var filterDataList=commentList
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return ViewHolder(v,activity)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CommentsAdapter.ViewHolder, position: Int) {
        filterDataList.get(position)?.let { holder.bindItems(it, activity, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return filterDataList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,context: Context) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(vital: CommentData,context: Context,  position: Int) {
            val tvName  = itemView.findViewById(R.id.tvName) as TextView
            val tvComment  = itemView.findViewById(R.id.tvComment) as TextView
            val date  = itemView.findViewById(R.id.date) as TextView
            val userImage  = itemView.findViewById(R.id.userImage) as ImageView
            tvName.text = vital.from_name
            tvComment.text = vital.message
            date.text = vital.created_date
            Glide.with(context)
                .load(vital.from_userimage)
                .error(R.drawable.image_icon)
                .placeholder(R.drawable.image_icon)
                .into(userImage);
        }

    }

    override fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    filterDataList = commentList
                } else {
                    val filteredList: MutableList<CommentData> =
                        java.util.ArrayList<CommentData>()
                    for (row in commentList) {
                        if (row.message!!.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row)
                        }
                    }
                    filterDataList = filteredList as java.util.ArrayList<CommentData>
                }
                val filterResults = FilterResults()
                filterResults.values = filterDataList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filterDataList = filterResults.values as java.util.ArrayList<CommentData>
                notifyDataSetChanged()
            }
        }
    }
}