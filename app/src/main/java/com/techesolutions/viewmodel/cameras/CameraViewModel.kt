package com.techesolutions.viewmodel.cameras

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techesolutions.data.remote.repository.login.cameras.CameraDataRepository
import com.techesolutions.data.remote.model.nextclouddashboard.CameraDataResponse

/**
 * Author: Neelam Upadhyay
 * Created: 18/08/2021
 */
class CameraViewModel: ViewModel() {

    var cameraLiveData: MutableLiveData<CameraDataResponse>? = null

    fun getCameraData( token: String, apporigin: String, room: String): LiveData<CameraDataResponse>? {
        cameraLiveData = CameraDataRepository.getCameraData(token,apporigin,room)
        return cameraLiveData
    }
}