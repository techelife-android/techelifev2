package com.techesolutions.view.activity.nextcloud.contacts

data class Contacts(
    var name : String,
    var telType: String,
    var tel : String,
    var memberType : String,
    var filename : String
    )