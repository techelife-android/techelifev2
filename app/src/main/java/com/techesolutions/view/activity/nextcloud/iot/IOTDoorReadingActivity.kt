package com.techesolutions.view.activity.nextcloud.iot

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.iot.IOTDoors
import com.techesolutions.data.remote.model.iot.IOTDoorsReadings
import com.techesolutions.data.remote.model.nextclouddashboard.RoomItems
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.adapter.DoorWiseDataAdapter
import com.techesolutions.view.callbacks.ItemClickListener
import com.techesolutions.viewmodel.cameras.CameraViewModel
import com.techesolutions.viewmodel.cameras.MasterViewModel
import kotlinx.android.synthetic.main.activity_iot_door_reading.*
import kotlinx.android.synthetic.main.activity_iot_door_reading.noDataText
import kotlinx.android.synthetic.main.activity_iot_room_reading.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Neelam on 21-08-2021.
 */
class IOTDoorReadingActivity : AppCompatActivity(), ItemClickListener {
    private var dataModel: ArrayList<RoomItems>? = null
    private var doorModel: ArrayList<IOTDoors>? = null
    private var doorWiseDataModel: ArrayList<IOTDoorsReadings>? = null
    private lateinit var spinnerRoom: Spinner
    private lateinit var spinnerDoor: Spinner
    var roomvalue: String = ""

    //private lateinit var adapter: RoomAdapter
    lateinit var roomViewModel: MasterViewModel
    lateinit var cameraViewModel: CameraViewModel
    private val progressDialog = CustomProgressDialog()

    var recyclerView: RecyclerView? = null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_iot_door_reading)
        headerTitle?.text = "Door Readings"
        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }
        roomViewModel = ViewModelProvider(this).get(MasterViewModel::class.java)
        cameraViewModel = ViewModelProvider(this).get(CameraViewModel::class.java)
        spinnerRoom = findViewById<View>(R.id.spinnerRoom) as Spinner
        spinnerDoor = findViewById<View>(R.id.spinnerDoor) as Spinner
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        fetchRoomData("Bearer $token")


        spinnerRoom.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                roomvalue = dataModel!![position].value!!

                fetchDoorData("Bearer $token",roomvalue!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

        spinnerDoor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
              val  doorvalue = doorModel!![position].value!!
               fetchIOTData("Bearer $token", roomvalue!!, doorvalue!!)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

    }

    private fun fetchDoorData(token: String, roomvalue: String) {
        if (Utils.isOnline(this@IOTDoorReadingActivity)) {
            showLoading("Please wait")

            val call = NetworkService.apiInterface.getRoomsList(token, "mobile", roomvalue)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())

                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    val stringResponse = response.body()?.string()
                    if (stringResponse != null) {
                        val jsonObj = JSONObject(stringResponse)
                        val success = jsonObj!!.getString("success")
                        if (success != null && success.toString().equals("true")) {
                            val dlist = jsonObj.getJSONArray("data")
                            Log.i("dlist", "" + dlist)
                            doorModel = ArrayList<IOTDoors>()
                             if (dlist.length() > 0) {
                                 noDataText!!.visibility = View.GONE
                                 recyclerView!!.visibility = View.VISIBLE
                                 for (i in 0 until dlist.length()) {
                                     val data = dlist.getJSONObject(i)
                                     doorModel!!.add(
                                         IOTDoors(
                                             data!!.getString("label"),
                                             data!!.getString("value")
                                         )
                                     )
                                     Log.i("doors", "" + doorModel)

                                 }
                                 val doorAdapter =
                                     DoorAdapter(this@IOTDoorReadingActivity, doorModel!!)
                                 spinnerDoor.adapter = doorAdapter
                             }
                            else{
                                     noDataText!!.visibility = View.VISIBLE
                                     recyclerView!!.visibility = View.GONE
                            }
                        }
                    }
                }
            })
        }
    }

    private fun fetchIOTData(token: String, room: String,door: String) {
        if (Utils.isOnline(this@IOTDoorReadingActivity)) {
            showLoading("Fetching Data")

            val call = NetworkService.apiInterface.getDoorwiseData(token, "mobile", room,door)

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())

                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    val stringResponse = response.body()?.string()
                    if (stringResponse != null) {
                        val jsonObj = JSONObject(stringResponse)
                        val success = jsonObj!!.getString("success")
                        if (success != null && success.toString().equals("true")) {
                            val jobj = jsonObj.getJSONObject("data")
                            val dlist = jobj.getJSONArray("data")
                            Log.i("dlist", "" + dlist)
                            doorWiseDataModel = ArrayList<IOTDoorsReadings>()
                            if (dlist.length() > 0) {
                                noDataText!!.visibility = View.GONE
                                recyclerView!!.visibility = View.VISIBLE
                                for (i in 0 until dlist.length()) {
                                    val data = dlist.getJSONObject(i)
                                    doorWiseDataModel!!.add(
                                        IOTDoorsReadings(
                                            data!!.getString("room_label"),
                                            data!!.getString("door_label"),
                                            data!!.getInt("door_status"),
                                            data!!.getString("entry_timeW")
                                        )
                                    )
                                    Log.i("doors", "" + doorWiseDataModel)

                                }
                                val adapter = DoorWiseDataAdapter(
                                    this@IOTDoorReadingActivity,
                                    doorWiseDataModel!!
                                )
                                recyclerView?.adapter = adapter
                            }
                            else{
                                noDataText!!.visibility = View.VISIBLE
                                recyclerView!!.visibility = View.GONE
                            }
                        }
                    }

                }

            })
        }

    }

    private fun fetchRoomData(token: String) {
        if (Utils.isOnline(this@IOTDoorReadingActivity)) {
            showLoading("Please wait")

            roomViewModel.getRoomDeviceMasterData(token, "mobile", "DOOR")!!
                .observe(this, Observer { masterResponse ->
                    if(masterResponse!=null) {
                        val success = masterResponse.success;
                        if (success == true) {
                            closeLoading()
                            try {
                                val gson = Gson()
                                val rooms: String = gson.toJson(masterResponse.data!!.rooms)
                                var dlist = JSONArray(rooms)
                                if(dlist.length()>0) {
                                    noDataText!!.visibility = View.GONE
                                    recycler_view!!.visibility = View.VISIBLE
                                    dataModel = ArrayList<RoomItems>()
                                    for (i in 0 until dlist.length()) {
                                        val item = dlist.getJSONObject(i)
                                        val id = item.optString("id")
                                        val label = item.optString("label")
                                        val value = item.optString("value")
                                        dataModel!!.add(RoomItems(id, label, value))
                                    }
                                    val roomAdapter = RoomAdapter(this, dataModel!!, this)
                                    spinnerRoom.adapter = roomAdapter
                                }
                                else{
                                    noDataText.visibility = View.VISIBLE
                                    recycler_view!!.visibility = View.GONE
                                }
                            } catch (e: Exception) {
                            }

                        } else {
                            closeLoading()
                            Toast.makeText(this, masterResponse.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        closeLoading()
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
                    }
                    //Toast.makeText(this, baseResponse.message, Toast.LENGTH_SHORT).show()
                })
        }

    }


    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {

    }

}