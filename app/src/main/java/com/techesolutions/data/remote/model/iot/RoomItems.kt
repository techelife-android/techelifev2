package com.techesolutions.data.remote.model.iot

import com.google.gson.annotations.SerializedName

data class RoomItems(

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("value")
    val value: String? = null
)



