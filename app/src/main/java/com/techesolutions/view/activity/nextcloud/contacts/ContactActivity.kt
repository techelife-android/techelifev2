package com.techesolutions.view.activity.nextcloud.contacts;

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.*
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils.getRootDirPath
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import ezvcard.Ezvcard
import kotlinx.android.synthetic.main.activity_documents.*
import kotlinx.android.synthetic.main.activity_pdf_view.*
import kotlinx.android.synthetic.main.layout_header.*
import java.io.*
import java.util.*
import kotlin.collections.ArrayList

class ContactActivity : AppCompatActivity(), OnRemoteOperationListener,
    OnDatatransferProgressListener, NoteClickListener , View.OnClickListener{
    private val create_note_cmd = 0
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var file: String?=null
    var recyclerView: RecyclerView? = null
    var adapter: ContactAdapter? = null
    lateinit var dbContacts: ArrayList<Contacts>
    private val progressDialog = CustomProgressDialog()
    private lateinit var FilePathStrings: Array<String>
    private lateinit var listFile: Array<File>
    var UploadedVcfFile: String? =null
    var url: String?=null
    var user: String?=null
    var pass: String?=null
    var CONTACT_DIR: String?=null
    var remoteUrl: String?=null
    var shared: SharedPreferences? = null
    var downFolder: File?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
        shared = getSharedPreferences("TechePrefs", MODE_PRIVATE)
        headerTitle.text = "Contacts"
        dbContacts= ArrayList()
        dbContacts!!.clear()
        mHandler = Handler()
         url=shared!!.getString(Constants.KEY_NEXT_CLOUD_URL, "")
         user=shared!!.getString(Constants.KEY_NEXT_CLOUD_USER, "")
         pass=shared!!.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
         CONTACT_DIR = "/addressbooks/users/"+user+"/contacts/"
         remoteUrl=url+"/remote.php/dav/addressbooks/users/"+user+"/contacts"
                Log.i("data", url + ", " + user + ", " + pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url!!.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
                pass!!
            )
        )
        //startRefresh()
        downFolder = File(getRootDirPath(baseContext) + CONTACT_DIR)
        dbContacts.clear()
        if(downFolder!!.listFiles()!=null) {
            listFile = downFolder!!.listFiles()
            if (listFile != null && listFile.size > 0) {
                for (i in 0 until listFile.size) {
                    val downloadedFile = File(downFolder, listFile.get(i).name)
                    if (downloadedFile.exists()) {
                        downloadedFile?.let {
                            readVCF(
                                it
                            )
                        }
                    } else {
                        startRefresh()
                    }
                }

            }
        }
        else{
            startRefresh()
       }

        settingsContact.visibility = View.VISIBLE
        settingsContact.setOnClickListener {
            val intent = Intent(
                this@ContactActivity,
                ContactSettingsActivity::class.java
            )
            startActivity(intent)
        }
        back.setOnClickListener(this)
        createContact!!.visibility = View.VISIBLE
       // create!!.text="New Contact"
        createContact.setOnClickListener(this)
        refresh!!.visibility = View.VISIBLE
        refresh.setOnClickListener(this)
    }

    private fun startRefresh() {
        showLoading("Please wait...")
        val refreshOperation = ReadContactsRemoteOperation(FileUtils.PATH_SEPARATOR, user)
        //val refreshOperation = ReadContactsRemoteOperation(remoteUrl)
        refreshOperation.execute(mClient, this, mHandler)
    }

    @SuppressLint("WrongConstant")
    private fun readVCF(downloadedFile: File) {
        //dbContacts = ArrayList<Contacts>()
        try {
            val vcards = Ezvcard.parse(downloadedFile).all()
            Log.i("Contacts", vcards.toString())

            for (vcard in vcards) {
                Log.i("Name: ", vcard.formattedName.value)
                var name=""
                var mobile=""
                var mobType=""
                var memberType="Friend"
                if(!vcard.formattedName.value.isEmpty())
                {
                    name= vcard.formattedName.value
                }
                if(!vcard.telephoneNumbers.isEmpty()){
                    mobType= vcard.telephoneNumbers[0].types.toString()
                }
                if(!vcard.telephoneNumbers.isEmpty()){
                    mobile=vcard.telephoneNumbers[0].text
                }

                if(!vcard.relations.isEmpty()){
                    memberType=vcard.relations[0].text
                }
                dbContacts!!.add(
                    Contacts(
                        name,
                        mobType,
                        mobile,
                        memberType,
                        downloadedFile.name
                    )
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.i("dbcontact", dbContacts.toString())

          recyclerView = findViewById(R.id.recycler_view) as RecyclerView
          recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
          adapter = ContactAdapter(this@ContactActivity, dbContacts, this)

          //now adding the adapter to recyclerview
          recyclerView?.adapter = adapter
          recyclerView?.adapter!!.notifyDataSetChanged()
        // File file = new File( p.getRemotePath());
    }

    private fun startDownload(filename: String) {
        file=filename
        Log.i("Contacts", "downloading")
        Log.i("download path", filename)

        val downFolder = File(getRootDirPath(baseContext) + CONTACT_DIR)
        if (!downFolder.parentFile.exists()) downFolder.parentFile.mkdirs()
        if (!downFolder.exists() || downFolder.isFile)
            downFolder.mkdir()
        if(filename.length>2) {
            val downloadOperation =
                DownloadContactsRemoteOperation(user, filename, downFolder.absolutePath);
            downloadOperation.addDatatransferProgressListener(this)
            downloadOperation.execute(mClient, this, mHandler)
        }
        else{
            closeLoading()
        }
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {

        if (!result!!.isSuccess()) {
           /* Toast.makeText(this, R.string.todo_operation_finished_in_fail, Toast.LENGTH_SHORT)
                .show()*/
            closeLoading()
        }
        else if (operation is ReadContactsRemoteOperation) {
            onSuccessfulRefresh(operation as ReadContactsRemoteOperation?, result)
        }
        else if(operation is UploadContactRemoteOperation)
        {
           /* Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                .show()*/
            onSuccessfulUpload(operation as UploadContactRemoteOperation?, result)

        }
        else {
            closeLoading()
            /* Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                 .show()*/
            val downFolder = File(getRootDirPath(baseContext) + CONTACT_DIR)
            //downFolder.mkdir()
            Log.i("download folder", downFolder.absolutePath)
            dbContacts.clear()
            if(downFolder.exists() && downFolder.listFiles()!=null) {
                listFile = downFolder.listFiles()
                if (listFile != null && listFile.size > 0) {
                    for (i in 0 until listFile.size) {
                        val downloadedFile = File(downFolder, listFile.get(i).name)
                        downloadedFile?.let {
                            readVCF(
                                it
                            )
                        }
                    }

                }
            }
            else{
                downFolder.mkdir()
                if(downFolder.exists() && downFolder.listFiles()!=null) {
                    listFile = downFolder.listFiles()
                    if (listFile != null && listFile.size > 0) {
                        for (i in 0 until listFile.size) {
                            val downloadedFile = File(downFolder, listFile.get(i).name)
                            downloadedFile?.let {
                                readVCF(
                                    it
                                )
                            }
                        }

                    }
                }
            }
        }
    }

    private fun onSuccessfulUpload(
        uploadContactRemoteOperation: UploadContactRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        closeLoading()
        refreshUploaded("")
    }

    private fun refreshUploaded(file: String) {
        showLoading("Please wait")
        val downFolder = File(getRootDirPath(baseContext) + CONTACT_DIR)
        var updatedFile: String=""
      if(file.length>1)
          updatedFile="/"+file
        else{
          updatedFile="/"+UploadedVcfFile!!
        }
      //  startDownload(UploadedVcfFile!!)
        val downloadOperation = DownloadContactsRemoteOperation(
            user,
            updatedFile,
            downFolder.absolutePath
        )
        downloadOperation.addDatatransferProgressListener(this)
        downloadOperation.execute(mClient, this, mHandler)
    }

    private fun onSuccessfulRefresh(
        readContactsRemoteOperation: ReadContactsRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        //mFilesAdapter.clear()
        val files: MutableList<RemoteFile> = ArrayList()
        for (obj in result.data) {
            files!!.add(obj as RemoteFile)
            Log.i("Contacts data", files.toString())
        }
        if (files != null && files.size>0) {
            val it: Iterator<RemoteFile> = files.iterator()
            while (it.hasNext()) {

                startDownload(it.next().remotePath)
                //mFilesAdapter.add(it.next())
            }
           // mFilesAdapter.remove(mFilesAdapter.getItem(0))
        }
        else {
            closeLoading()
            Toast.makeText(this@ContactActivity, "No data available", Toast.LENGTH_SHORT).show()
        }

        //closeLoading()
      //  mFilesAdapter.notifyDataSetChanged()
    }

    private fun closeLoading() {
        if (progressDialog != null&& progressDialog?.dialog!=null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }
    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileAbsoluteName: String?
    ) {
        /*val percentage =
            if (totalToTransfer > 0) totalTransferredSoFar * 100 / totalToTransfer else 0
        mHandler!!.post {
            val progressView: TextView? = null
            if (progressBar != null) {
                progressBar.progress = percentage.toInt()
            }
        }
        progressBar.visibility= View.VISIBLE*/
    }

    override fun onNoteClick(position: Int) {
        val intent: Intent = Intent(
            this@ContactActivity,
            EditContactActivity::class.java
        )
        intent.putExtra("action", "edit")
        intent.putExtra("from", "contact")
        intent.putExtra("name", dbContacts[position].name)
        intent.putExtra("tel", dbContacts[position].tel)
        intent.putExtra("memberType", dbContacts[position].memberType)
        intent.putExtra("filename", dbContacts[position].filename)
        startActivityForResult(intent, 1)
    }

    override fun onNoteDeleteClick(position: Int) {
        TODO("Not yet implemented")
    }

    override fun onNoteLongClick(position: Int, v: View?): Boolean {
        TODO("Not yet implemented")
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                finish()
            }

            R.id.createContact -> {
                showCreateContactDialog(this@ContactActivity)
            }
            R.id.refresh -> {
                if (downFolder != null && downFolder!!.exists() && downFolder!!.listFiles().size > 0) {
                    // downFolder!!.listFiles().de
                    if (downFolder!!.isDirectory()) {
                        val children: Array<String> = downFolder!!.list()
                        for (i in children.indices) {
                            File(downFolder, children[i]).delete()
                        }
                    }
                    //downFolder!!.delete()
                    dbContacts!!.clear()
                    startRefresh()
                } else {
                    startRefresh()
                }
            }
        }
    }

    private fun showCreateContactDialog(context: ContactActivity) {
        var VCF_DIRECTORY: String = "/$user/vcf_teche"
        var vcfFile: File? =null

        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.create_contacts)
        val etcontactNumber = dialog.findViewById(R.id.etContact) as EditText
        val etrelation = dialog.findViewById(R.id.etRelation) as EditText
        val etname = dialog.findViewById(R.id.etName) as EditText
        val cancel = dialog.findViewById(R.id.cancel) as TextView
        val save = dialog.findViewById(R.id.save) as TextView
        save.setOnClickListener {
            showLoading("Please wait..")
            var contact=etcontactNumber.text.toString()
            var name=etname.text.toString()
            var relation=etrelation.text.toString()
            Log.i("et data", name + ": " + contact)
            try
            {
                // File vcfFile = new File(this.getExternalFilesDir(null), "generated.vcf");
                val vdfdirectory =
                    File(getRootDirPath(applicationContext), VCF_DIRECTORY)

                // have the object build the directory structure, if needed.
                if (!vdfdirectory.exists())
                {
                    vdfdirectory.mkdirs()
                }
                UploadedVcfFile="contact" + Calendar.getInstance().getTimeInMillis() + ".vcf"
                vcfFile = File(
                    vdfdirectory,
                    UploadedVcfFile
                )
                var fw:FileWriter? = null
                fw = FileWriter(vcfFile)
                fw.write("BEGIN:VCARD\r\n")
                fw.write("VERSION:3.0\r\n")
                // fw.write("N:" + p.getSurname() + ";" + p.getFirstName() + "\r\n");
                fw.write("FN:" + name + "\r\n")
                // fw.write("ORG:" + p.getCompanyName() + "\r\n");
                fw.write("TITLE:" + name + "\r\n");
                fw.write("TEL;TYPE=WORK,VOICE:" + contact + "\r\n")
                fw.write("TEL;TYPE=HOME,VOICE:" + contact + "\r\n");
                // fw.write("ADR;TYPE=WORK:;;" + p.getStreet() + ";" + p.getCity() + ";" + p.getState() + ";" + p.getPostcode() + ";" + p.getCountry() + "\r\n");
                fw.write("EMAIL;TYPE=PREF,INTERNET:" + name+"@gmail.com" + "\r\n")
                fw.write("END:VCARD\r\n")
                fw.close()
                /* Intent i = new Intent(); //this will import vcf in contact list
               i.setAction(android.content.Intent.ACTION_VIEW);
               i.setDataAndType(Uri.fromFile(vcfFile), "text/x-vcard");
               startActivity(i);*/
                //Toast.makeText(this@ContactActivity, "Created!", Toast.LENGTH_SHORT).show()
                uploadContact(vcfFile!!)
                dialog.dismiss()
            }
            catch (e: IOException) {
                e.printStackTrace()
            }
          /*  val p = getPerson()
            val vcfFile = File(getRootDirPath(baseContext), "generated.vcf")
            val vcard = VCard()
            vcard.setVersion(VCardVersion.V3_0)
            val n = StructuredNameType()
            n.setFamily(p.getSurname())
            n.setGiven(p.getFirstName())
            vcard.setStructuredName(n)
            vcard.setFormattedName(FormattedNameType(p.getFirstName() + " " + p.getSurname()))
            val org = OrganizationType()
            org.addValue(p.getCompanyName())
            vcard.setOrganization(org)
            vcard.addTitle(TitleType(p.getTitle()))
            val tel = TelephoneType(p.getWorkPhone())
            tel.addType(TelephoneTypeParameter.WORK)
            tel.addType(TelephoneTypeParameter.VOICE)
            vcard.addTelephoneNumber(tel)
            tel = TelephoneType(p.getHomePhone())
            tel.addType(TelephoneTypeParameter.HOME)
            tel.addType(TelephoneTypeParameter.VOICE)
            vcard.addTelephoneNumber(tel)
            val adr = AddressType()
            adr.setStreetAddress(p.getStreet())
            adr.setLocality(p.getCity())
            adr.setRegion(p.getState())
            adr.setPostalCode(p.getPostcode())
            adr.setCountry(p.getCountry())
            adr.addType(AddressTypeParameter.WORK)
            vcard.addAddress(adr)
            val email = EmailType(p.getEmailAddress())
            email.addType(EmailTypeParameter.PREF)
            email.addType(EmailTypeParameter.INTERNET)
            vcard.addEmail(email)
            vcard.write(vcfFile)
            val i = Intent()
            i.setAction(android.content.Intent.ACTION_VIEW)
            i.setDataAndType(Uri.fromFile(vcfFile), "text/vcard")
            startActivity(i)
*/

        }
        cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    private fun uploadContact(vcfFile: File) {
        //val fileToUpload: File = vcfFile
        val remotePath: String = "/"+ vcfFile.name
        Log.i("remote path contact", remotePath)
        // Get the last modification date of the file from the file system
        val timeStampLong = System.currentTimeMillis() / 1000
        val timeStamp = timeStampLong.toString()
        /*  UploadFileRemoteOperation uploadOperation =
                    new UploadFileRemoteOperation(fileToUpload.getAbsolutePath(), remotePath, mimeType, timeStamp);
            uploadOperation.execute(mClient, this, mHandler);*/
        val uploadOperation = UploadContactRemoteOperation(
            user,
            vcfFile.getAbsolutePath(),
            remotePath,
            "",
            "",
            timeStamp
        )
        uploadOperation.execute(mClient, this, mHandler)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                val updatedFile = data!!.getStringExtra("updatedFile")
                refreshUploaded(updatedFile)
                //startRefresh()
            }
        }
    }
    override fun onResume() {
        super.onResume()
        val uploadEnable = shared!!.getBoolean(Constants.KEY_CONTACT_UPLOAD, false)
        if(uploadEnable) {
            startService(Intent(applicationContext, ContactPushService::class.java))
        }
    }
}
