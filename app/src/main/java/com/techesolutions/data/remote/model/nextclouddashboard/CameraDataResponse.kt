package com.techesolutions.data.remote.model.nextclouddashboard

import com.google.gson.annotations.SerializedName

data class CameraDataResponse(

    @field:SerializedName("data")
    val data: CameraData? = null,

    @field:SerializedName("success")
    val success: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class CameraData(
    @field:SerializedName("data")
    val cameras: List<CameraItems>? = null

)

data class CameraItems(

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("room_label")
    val room: String? = null,

    @field:SerializedName("cam_live_stream_url")
    val url: String? = null
)


