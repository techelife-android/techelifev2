package com.techesolutions.data.remote.repository.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.techesolutions.data.remote.model.forgetpassword.CodeVerificationResponse
import com.techesolutions.data.remote.model.forgetpassword.ResetPasswordResponse
import com.techesolutions.services.NetworkService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Author: Neelam Upadhyay
 * Created: 1/1/2021
 */
object ResetPasswordRepository {
    val resetPasswordResponse = MutableLiveData<ResetPasswordResponse>()

    fun resetPassword( hashcode:String,apporigin: String,password: String, rePassword:String): MutableLiveData<ResetPasswordResponse> {

        val call = NetworkService.apiInterface.resetPassword(hashcode,apporigin, password,rePassword)

        call.enqueue(object: Callback<ResetPasswordResponse> {
            override fun onFailure(call: Call<ResetPasswordResponse>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<ResetPasswordResponse>,
                response: Response<ResetPasswordResponse>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()
                resetPasswordResponse.postValue(data)
                //loginResponse.value = LoginResponse(data)
                    // loginResponse.value(response.body())
            }


        })

        return resetPasswordResponse
    }

}