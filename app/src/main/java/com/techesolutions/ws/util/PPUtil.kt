package com.techesolutions.ws.util.util

import com.peng.ppscalelibrary.BleManager.Model.BleEnum.BleUnit
import java.math.BigDecimal
import java.text.DecimalFormat
object PPUtil {
    fun kgToLB_ForFatScale(tempKg: Double): String {
        if (0.0 == tempKg) return "0.0"
        val b0 = BigDecimal(tempKg.toString())
        val b1 = BigDecimal("1155845")
        val b2 = BigDecimal("16")
        val b4 = BigDecimal("65536")
        val b5 = BigDecimal("2")
        val b3: BigDecimal =
            BigDecimal(b0.multiply(b1).toDouble())
                .divide(b2, 5, BigDecimal.ROUND_HALF_EVEN)
                .divide(b4, 1, BigDecimal.ROUND_HALF_UP)
        val templb: Float = b3.multiply(b5).toFloat()
        return templb.toString()
    }

    fun kgToJin(lb: Double): String {
        val kg = lb * 2
        val myformat = DecimalFormat("######0.0")
        return myformat.format(kg)
    }

    fun keep1Point3(kg: Double): Float {
        val b = BigDecimal(kg)
        return b.setScale(1, BigDecimal.ROUND_HALF_UP).toFloat()
    }

    /**
     * 传入kg，根据重量单位得出相应值
     *
     * @param htWeightKg
     * @return
     */
    fun getWeight(unit: BleUnit, htWeightKg: Double): String {
        return if (unit == BleUnit.BLE_UNIT_KG) {
            keep1Point3(htWeightKg).toString()
        } else if (unit == BleUnit.BLE_UNIT_LB) {
            kgToLB_ForFatScale(htWeightKg)
        } else {
            kgToJin(htWeightKg) + "斤"
        }
    }
}

