//package com.techesolutions.viewmodel
//
//import android.content.Context
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.ViewModelProvider
//import com.techesolutions.data.remote.model.dashboard.Vital
//import com.techesolutions.data.remote.model.dashboard.VitalDataSource
//import kotlin.random.Random
//
//class VitalViewModel(val dataSource: VitalDataSource) : ViewModel() {
//
//    val vitalsLiveData = dataSource.getFlowerList()
//
//    /* If the name and description are present, create new Flower and add it to the datasource */
////    fun insertFlower(flowerName: String?, flowerDescription: String?) {
////        if (flowerName == null || flowerDescription == null) {
////            return
////        }
////
////        val image = dataSource.getRandomFlowerImageAsset()
////        val newFlower = Vital(
////            Random.nextLong(),
////            flowerName,
////            image,
////            flowerDescription,
////            readingname1,
////            readingname2,
////            reading1,
////            reading2
////        )
////
////        dataSource.addFlower(newFlower)
////    }
//}
//
//class VitalsListViewModelFactory(private val context: Context) : ViewModelProvider.Factory {
//
//    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        if (modelClass.isAssignableFrom(VitalViewModel::class.java)) {
//            @Suppress("UNCHECKED_CAST")
//            return VitalViewModel(
//                dataSource = VitalDataSource.getDataSource(context.resources)
//            ) as T
//        }
//        throw IllegalArgumentException("Unknown ViewModel class")
//    }
//}