package com.techesolutions.services

import androidx.multidex.BuildConfig
import com.techesolutions.utils.Constants.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkService {
    val retrofitClient: Retrofit.Builder by lazy {

    val levelType: HttpLoggingInterceptor.Level
    if (BuildConfig.BUILD_TYPE.contentEquals("debug"))
        levelType = HttpLoggingInterceptor.Level.BODY else levelType = HttpLoggingInterceptor.Level.NONE

    val logging = HttpLoggingInterceptor()
    logging.setLevel(levelType)

    val okhttpClient = OkHttpClient.Builder()
    okhttpClient.addInterceptor(logging)

        Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okhttpClient.build())
        .addConverterFactory(GsonConverterFactory.create())
}

    val apiInterface: ApiService by lazy {
        retrofitClient
            .build()
            .create(ApiService::class.java)
    }
}
