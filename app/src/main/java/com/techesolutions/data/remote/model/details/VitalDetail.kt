package com.techesolutions.data.remote.model.details

data class VitalDetail(
    val date: String,
    val Title: String,
    val data1: String?,
    val data1val: String,
    val data2: String?,
    val data2val: String,
    val unit:String
)