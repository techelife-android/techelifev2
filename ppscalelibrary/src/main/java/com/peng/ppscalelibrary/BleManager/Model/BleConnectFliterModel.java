package com.peng.ppscalelibrary.BleManager.Model;

import java.util.ArrayList;
import java.util.List;

public class BleConnectFliterModel {

    private String scaleName;
    private String serviceUUID;
    private String characteristicWriteUUID;
    private String characteristicNofifyUUID;
    private int advDataLength;
    private int advDataType;
    private int historyDataLength;
    private int historyEndDataLength;
    private int beaconBytesLenght;
    private int bmdjDataLength;
    private String characteristicBMDJUUID;
    public String getScaleName() {
        return scaleName;
    }

    public void setScaleName(String scaleName) {
        this.scaleName = scaleName;
    }

    public String getServiceUUID() {
        return serviceUUID;
    }

    public void setServiceUUID(String serviceUUID) {
        this.serviceUUID = serviceUUID;
    }

    public String getCharacteristicWriteUUID() {
        return characteristicWriteUUID;
    }

    public void setCharacteristicWriteUUID(String characteristicWriteUUID) {
        this.characteristicWriteUUID = characteristicWriteUUID;
    }

    public String getCharacteristicNofifyUUID() {
        return characteristicNofifyUUID;
    }

    public void setCharacteristicNofifyUUID(String characteristicNofifyUUID) {
        this.characteristicNofifyUUID = characteristicNofifyUUID;
    }

    public int getAdvDataLength() {
        return advDataLength;
    }

    public void setAdvDataLength(int advDataLength) {
        this.advDataLength = advDataLength;
    }

    public int getAdvDataType() {
        return advDataType;
    }

    public int getHistoryDataLength() {
        return historyDataLength;
    }

    public int getHistoryEndDataLength() {
        return historyEndDataLength;
    }

    public void setAdvDataType(int advDataType) {
        this.advDataType = advDataType;
    }

    public int getBeaconBytesLenght() {
        return beaconBytesLenght;
    }

    public void setBeaconBytesLenght(int beaconBytesLenght) {
        this.beaconBytesLenght = beaconBytesLenght;
    }

    public int getBmdjDataLength() {
        return bmdjDataLength;
    }

    public void setBmdjDataLength(int bmdjDataLength) {
        this.bmdjDataLength = bmdjDataLength;
    }

    public String getCharacteristicBMDJUUID() {
        return characteristicBMDJUUID;
    }

    public void setCharacteristicBMDJUUID(String characteristicBMDJUUID) {
        this.characteristicBMDJUUID = characteristicBMDJUUID;
    }

    public static final String ENERGY_SCALE = "Energy Scale";
    public static final String BODYFAT_SCALE = "BodyFat Scale";
    public static final String HEALTH_SCALE = "Health Scale";
    public static final String HEARTRATE_SCALE = "HeartRate Scale";
    public static final String BM_SCALE = "BM Scale";
    public static final String ELECTRONIC_SCALE = "Electronic Scale";
    public static final String CF367_SCALE = "LEFU_SCALE CF376";
    public static final String ADORE_SCALE = "ADORE";
    public static final String LF_SCALE = "LFScale";
    public static final String BF_SCALE = "BFScale";
    public static final String HUMAN_SCALE = "Human Scale";
    public static final String WEIGHT_SCALE = "Weight Scale";



    public BleConnectFliterModel(String scaleName, String serviceUUID, String characteristicWriteUUID, String characteristicNofifyUUID, int advDataLength, int advDataType, int historyDataLength, int historyEndDataLength, int beaconBytesLenght) {
        this.scaleName = scaleName;
        this.serviceUUID = serviceUUID;
        this.characteristicWriteUUID = characteristicWriteUUID;
        this.characteristicNofifyUUID = characteristicNofifyUUID;
        this.advDataLength = advDataLength;
        this.advDataType = advDataType;
        this.historyDataLength = historyDataLength;
        this.historyEndDataLength = historyEndDataLength;
        this.beaconBytesLenght = beaconBytesLenght;
        this.characteristicBMDJUUID = "";
        this.bmdjDataLength = 0;
    }

    public static BleConnectFliterModel getLFScaleFliter() {
        // LFScale 配置
        BleConnectFliterModel lfScaleFliter = new BleConnectFliterModel(
                LF_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                17,
                0xff,
                0,
                0,
                62
        );
        return lfScaleFliter;
    }

    public static BleConnectFliterModel getBFScaleFliter() {
        // BFSCALE 配置
        BleConnectFliterModel bfScaleFliter = new BleConnectFliterModel(
                BF_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                17,
                0xff,
                0,
                0,
                62
        );
        return bfScaleFliter;
    }

    public static List<BleConnectFliterModel> getFliterArr() {

        //Energy Scale 配置
        BleConnectFliterModel energyScaleFliter = new BleConnectFliterModel(
                ENERGY_SCALE ,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                0,
                0,
                88
        );

        //BodyFat Scale 配置
        BleConnectFliterModel bodyFatScaleFliter = new BleConnectFliterModel(
                BODYFAT_SCALE ,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                0,
                0,
                90
        );

        // Health Scale 配置
        BleConnectFliterModel healthScaleFliter = new BleConnectFliterModel(
                HEALTH_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                0,
                0,
                88
        );

        // HeartRate Scale 配置
        BleConnectFliterModel heartRateScaleFliter = new BleConnectFliterModel(
                HEARTRATE_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                36,
                4,
                94
        );

        // LEFU_SCALE CF376 配置
        BleConnectFliterModel cf367ScaleFliter = new BleConnectFliterModel(
                CF367_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                17,
                0xff,
                0,
                0,
                96
        );

        // Electronic Scale 配置
        BleConnectFliterModel electronicScaleFliter = new BleConnectFliterModel(
                ELECTRONIC_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                8,
                0xff,
                0,
                0,
                70
        );

        // Adore 配置
        BleConnectFliterModel adoreScaleFliter = new BleConnectFliterModel(
                ADORE_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                36,
                4,
                74
        );

        // LFScale 配置
        BleConnectFliterModel lfScaleFliter = new BleConnectFliterModel(
                LF_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                17,
                0xff,
                0,
                0,
                62
        );

        // LFScale 配置
        BleConnectFliterModel bfScaleFliter = new BleConnectFliterModel(
                BF_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                17,
                0xff,
                0,
                0,
                62
        );


        // Human Scale 配置
        BleConnectFliterModel humanScaleFliter = new BleConnectFliterModel(
                HUMAN_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                0,
                0,
                86
        );

        // Weight Scale 配置
        BleConnectFliterModel weightScaleFliter = new BleConnectFliterModel(
                WEIGHT_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                36,
                4,
                88
        );

        // BM Scale 配置
        BleConnectFliterModel bmScaleFliter = new BleConnectFliterModel(
                BM_SCALE,
                "0000fff0",
                "0000fff1",
                "0000fff4",
                19,
                0xff,
                0,
                0,
                80
        );
        bmScaleFliter.setBmdjDataLength(12);
        bmScaleFliter.setCharacteristicBMDJUUID("0000fff5");


        List<BleConnectFliterModel> fliterList = new ArrayList<>();
        fliterList.add(energyScaleFliter);
        fliterList.add(bodyFatScaleFliter);
        fliterList.add(healthScaleFliter);
        fliterList.add(electronicScaleFliter);
        fliterList.add(cf367ScaleFliter);
        fliterList.add(heartRateScaleFliter);
        fliterList.add(adoreScaleFliter);
        fliterList.add(lfScaleFliter);
        fliterList.add(bfScaleFliter);
        fliterList.add(bmScaleFliter);
        fliterList.add(humanScaleFliter);
        fliterList.add(weightScaleFliter);
        return  fliterList;
    }

    @Override
    public String toString() {
        return "BleConnectFliterModel{" +
                "scaleName='" + scaleName + '\'' +
                ", serviceUUID='" + serviceUUID + '\'' +
                ", characteristicWriteUUID='" + characteristicWriteUUID + '\'' +
                ", characteristicNofifyUUID='" + characteristicNofifyUUID + '\'' +
                ", advDataLength=" + advDataLength +
                ", advDataType=" + advDataType +
                ", historyDataLength=" + historyDataLength +
                ", historyEndDataLength=" + historyEndDataLength +
                ", beaconBytesLenght=" + beaconBytesLenght +
                ", bmdjDataLength=" + bmdjDataLength +
                ", characteristicBMDJUUID='" + characteristicBMDJUUID + '\'' +
                '}';
    }
}
