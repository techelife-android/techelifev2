package com.techesolutions.ws.util.util

import com.peng.ppscalelibrary.BleManager.Model.BleEnum
import com.peng.ppscalelibrary.BleManager.Model.BleUserModel
import com.peng.ppscalelibrary.BleManager.Model.LFPeopleGeneral

class DataUtil {
    var bodyDataModel: LFPeopleGeneral
    var userModel: BleUserModel

    companion object {
        private var dataUtil: DataUtil? = null
        fun util(): DataUtil? {
            if (dataUtil == null) {
                synchronized(DataUtil::class.java) {
                    if (dataUtil == null) {
                        dataUtil =
                            DataUtil()
                    }
                }
            }
            return dataUtil
        }
    }

    init {
        bodyDataModel = LFPeopleGeneral()
        userModel = BleUserModel(180, 18, BleEnum.BleSex.Male, BleEnum.BleUnit.BLE_UNIT_KG, 0)
    }
}
