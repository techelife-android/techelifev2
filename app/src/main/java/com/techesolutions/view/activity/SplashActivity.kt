package com.techesolutions.view.activity

import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.techesolutions.R
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.login.LoginActivity
import com.techesolutions.view.activity.nextcloud.NextCloudDashboardActivity
import org.joda.time.DateTimeUtils
import org.joda.time.DateTimeZone
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Neelam on 20-12-2020.
 */
class SplashActivity : AppCompatActivity() {
    lateinit var myLocale: Locale
    private var currentLanguage = "en"
    private var timezone = ""
    private var verifiedtimezone = ""
    private var loggedIn = false
    private var nextCloud = ""
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        currentLanguage = shared.getString(Constants.KEY_LANGUAGE, "").toString()
        verifiedtimezone = shared.getString(Constants.KEY_TIME_ZONE_VERIFIED, "").toString()
        timezone = shared.getString(Constants.KEY_TIME_ZONE_VERIFIED, "").toString()
        nextCloud = shared.getString(Constants.KEY_NEXT_CLOUD, "").toString()
        //  timezone = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.KEY_MOBILE_TIME_ZONE, "").toString()

        val defaultLanguage = Locale.getDefault().getLanguage();

         uttam@517
        ////testing..............

        TimeZone.setDefault(null)
        val dz = DateTimeZone.forID(TimeZone.getDefault().id)
        val timezone = dz.getNameKey(DateTimeUtils.currentTimeMillis())
        Log.i("timezone", timezone)

        loggedIn = shared.getBoolean(Constants.KEY_LOGGEDIN, false)

        setContentView(R.layout.activity_splash)

        Utils.statusBarSetup(this)

        Handler().postDelayed({
            /*if (loggedIn) {
                if (nextCloud.equals("1")) {
                    val intent = Intent(this@SplashActivity, NextCloudDashboardActivity::class.java)
                    intent.putExtra("from", "splash1")
                    startActivity(intent)
                    finish();
                } else {
                    val intent = Intent(this@SplashActivity, DashboardActivity::class.java)
                    intent.putExtra("from", "splash1")
                    startActivity(intent)
                    finish();
                }

            } else {*/
                if (currentLanguage != null && currentLanguage.length > 1) {
                    val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                    intent.putExtra("from", "splash")
                    startActivity(intent)
                    finish();
                } else {
                    val intent = Intent(this@SplashActivity, LanguageSelectionActivity::class.java)
                    intent.putExtra("defaultLanguage", defaultLanguage)
                    startActivity(intent)
                    finish();
                }
            //}
        }, 3000)
    }

    fun formatDateToString(date: Date?, format: String?,
                           timeZone: String?): String? {
        // null check
        var timeZone = timeZone
        if (date == null) return null
        // create SimpleDateFormat object with input format
        val sdf = SimpleDateFormat(format)
        // default system timezone if passed null or empty
        if (timeZone == null || "".equals(timeZone.trim { it <= ' ' }, ignoreCase = true)) {
            timeZone = Calendar.getInstance().timeZone.id
        }
        // set timezone to SimpleDateFormat
        sdf.setTimeZone(TimeZone.getTimeZone(timeZone))
        // return Date in required format with timezone as String
        return sdf.format(date)
    }

}