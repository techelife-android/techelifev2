package com.techesolutions.view.activity.nextcloud.iot

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.nextclouddashboard.CameraItems
import com.techesolutions.data.remote.model.nextclouddashboard.RoomItems
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.callbacks.ItemClickListener
import com.techesolutions.viewmodel.cameras.CameraViewModel
import com.techesolutions.viewmodel.cameras.MasterViewModel
import kotlinx.android.synthetic.main.activity_iot_room_reading.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Neelam on 03-09-2021.
 */
class IOTReadingActivity : AppCompatActivity(), View.OnClickListener, ItemClickListener {
    private var dataModel: ArrayList<RoomItems>? = null
    private lateinit var spinner: Spinner
    var roomvalue: String = ""
    var token: String?=""
    var dataFetched: Boolean=false

    lateinit var roomViewModel: MasterViewModel
    lateinit var cameraViewModel: CameraViewModel
    private val progressDialog = CustomProgressDialog()

    //var recyclerView: RecyclerView? = null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_iot_room_reading)
        headerTitle?.text = "IOT Readings"
        refresh?.visibility=View.VISIBLE
        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }
        refresh.setOnClickListener(this)
        outerLayoutCo2.setOnClickListener(this)
        outerLayoutTemp.setOnClickListener(this)
        outerLayoutSmoke.setOnClickListener(this)
        outerLayoutGas.setOnClickListener(this)
        outerLayoutHumidity.setOnClickListener(this)
        outerLayoutSensor.setOnClickListener(this)
        roomViewModel = ViewModelProvider(this).get(MasterViewModel::class.java)
        cameraViewModel = ViewModelProvider(this).get(CameraViewModel::class.java)
        spinner = findViewById<View>(R.id.spinner) as Spinner

        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        token = shared.getString(Constants.KEY_TOKEN, null)
        fetchRoomData("Bearer $token")

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if(dataFetched) {
                    //if(position>0) {
                        roomvalue = dataModel!![position].value!!
                        Log.d("Test", "" + dataFetched)
                        fetchIOTData( roomvalue!!)
                    //}
                 /*   else{
                        roomvalue = dataModel!![0].value!!
                    }

                    fetchIOTData( roomvalue!!)*/
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

    }

    private fun fetchIOTData( room: String) {
        if (Utils.isOnline(this@IOTReadingActivity)) {
           val token= getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(Constants.KEY_TOKEN, null)

            Log.d("Test apihit",room)
            Log.i("Test apihit: ", token)
            showLoading("Please wait")

            val call = NetworkService.apiInterface.getIotReading("Bearer $token", "mobile", room, "recent")

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    // closeLoading()
                    Log.v("DEBUG : ", t.message.toString())
                }
                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    Log.d("Test",response.toString())
                    Log.d("Test",response.headers().toString())
                    Log.d("Test",response.errorBody().toString())
                    val stringResponse = response.body()?.string()
                   // Log.d("Test",stringResponse)
                    if (stringResponse != null) {
                        val jsonObj = JSONObject(stringResponse)
                        val success = jsonObj!!.getString("success")
                        if (success != null && success.toString().equals("true")) {
                            val jobj = jsonObj.getJSONObject("data")
                            val dlist = jobj.getJSONArray("data")
                            Log.i("dlist", "" + dlist)
                            if (dlist.length() > 0) {
                                noDataText!!.visibility = View.GONE
                                scrollView!!.visibility = View.VISIBLE


                                val item = dlist.getJSONObject(0)
                                Log.d("item", item.toString())

                                subtitle1Value.text = item.getString("temperature") + " °F"
                                subtitle2Value.text = "" + Utils.convertTemperature(
                                    item.getString("temperature"),
                                    false
                                ) + " °C"
                                co2Value.text = item.getString("co") + " PPM"
                                SmokeValue.text = item.getString("smoke") + " PPM"
                                GasValue.text = item.getString("lpg") + " PPM"
                                humidityValue.text = item.getString("humidity") + " %"
                                if(item.getString("human_presence").equals("0")) {
                                    SensorValue.text = "Human Not Detected"
                                }
                                else{
                                    SensorValue.text = "Human Detected"
                                }

                                val time = Utils.convertIOTDoorDateTimeFormat(
                                    "" + item.getString("entry_timeW"),
                                    this@IOTReadingActivity
                                ) + getSharedPreferences(
                                    "TechePrefs",
                                    Context.MODE_PRIVATE
                                ).getString(
                                    Constants.KEY_USER_TIME_ZONE,
                                    ""
                                )


                                takenFromCo2Date.text = time
                                takenFromSmokeDate.text = time
                                takenFromGasDate.text = time
                                takenFromHumidityDate.text = time
                                takenFromSensorDate.text = time
                                takenFrom.text = time
                            }
                                else{
                                    noDataText!!.visibility = View.VISIBLE
                                    scrollView!!.visibility = View.GONE
                                }
                            }
                        }
                    else{
                        noDataText!!.visibility = View.VISIBLE
                        scrollView!!.visibility = View.GONE
                    }

                }

            })
        }

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.refresh->{
                fetchIOTData(roomvalue!!)
            }
            R.id.outerLayoutTemp -> {
                val intent = Intent(this@IOTReadingActivity, IotDetailsActivity::class.java)
                intent.putExtra("title", "Temperature")
                intent.putExtra("room", roomvalue)
                intent.putExtra("unitName1", "F")
                intent.putExtra("unitName2", "C")
                startActivity(intent)
            }
            R.id.outerLayoutSmoke -> {
                val intent = Intent(this@IOTReadingActivity, IotDetailsActivity::class.java)
                intent.putExtra("title", "Smoke")
                intent.putExtra("room", roomvalue)
                intent.putExtra("unitName1", "PPM")
                intent.putExtra("unitName2", "")
                startActivity(intent)
            }
            R.id.outerLayoutHumidity -> {
                val intent = Intent(this@IOTReadingActivity, IotDetailsActivity::class.java)
                intent.putExtra("title", "Humidity")
                intent.putExtra("room", roomvalue)
                intent.putExtra("unitName1", "%")
                intent.putExtra("unitName2", "")
                startActivity(intent)
            }
            R.id.outerLayoutGas -> {
                val intent = Intent(this@IOTReadingActivity, IotDetailsActivity::class.java)
                intent.putExtra("title", "LPG")
                intent.putExtra("room", roomvalue)
                intent.putExtra("unitName1", "PSI")
                intent.putExtra("unitName2", "")
                startActivity(intent)
            }
            R.id.outerLayoutCo2 -> {
                val intent = Intent(this@IOTReadingActivity, IotDetailsActivity::class.java)
                intent.putExtra("title", "Carbon Dioxide")
                intent.putExtra("room", roomvalue)
                intent.putExtra("unitName1", "PPM")
                intent.putExtra("unitName2", "")
                startActivity(intent)
            }
            R.id.outerLayoutSensor -> {
                val intent = Intent(this@IOTReadingActivity, IotDetailsActivity::class.java)
                intent.putExtra("title", "Detected")
                intent.putExtra("room", roomvalue)
                intent.putExtra("unitName1", "PIR")
                intent.putExtra("unitName2", "")
                startActivity(intent)
            }
        }
    }

    private fun fetchRoomData(token: String) {
        if (Utils.isOnline(this@IOTReadingActivity)) {
            showLoading("Please wait")

            // Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show()
            roomViewModel.getRoomDeviceMasterData(token, "mobile", "MASTER_MOTE")!!
                .observe(this, Observer { masterResponse ->
                    if(masterResponse!=null) {
                        val success = masterResponse.success;
                        if (success == true) {
                            closeLoading()
                            try {
                                val gson = Gson()
                                val rooms: String = gson.toJson(masterResponse.data!!.rooms)
                                var dlist = JSONArray(rooms)
                                if(dlist.length()>0) {
                                    noDataText!!.visibility = View.GONE
                                    scrollView!!.visibility = View.GONE
                                    dataModel = ArrayList<RoomItems>()
                                    for (i in 0 until dlist.length()) {
                                        val item = dlist.getJSONObject(i)
                                        val id = item.optString("id")
                                        val label = item.optString("label")
                                        val value = item.optString("value")
                                        dataModel!!.add(RoomItems(id, label, value))
                                    }
                                    val roomAdapter = RoomAdapter(this, dataModel!!, this)
                                    spinner.adapter = roomAdapter
                                    dataFetched=true
                                }
                                else{
                                    noDataText!!.visibility = View.VISIBLE
                                   scrollView!!.visibility = View.GONE
                                }
                            } catch (e: Exception) {
                            }

                        } else {
                            closeLoading()
                            noDataText!!.visibility = View.VISIBLE
                            scrollView!!.visibility = View.GONE
                            Toast.makeText(this, masterResponse.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        closeLoading()
                        noDataText!!.visibility = View.VISIBLE
                        scrollView!!.visibility = View.GONE
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
                    }
                    //Toast.makeText(this, baseResponse.message, Toast.LENGTH_SHORT).show()
                })
        }

    }


    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {

    }

}