package com.techesolutions.view.activity.nextcloud.notes

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.view.activity.nextcloud.notes.NotesActivity
import com.techesolutions.view.activity.nextcloud.notes.shared.model.CloudNote
import com.techesolutions.view.activity.nextcloud.notes.shared.model.DBNote
import com.techesolutions.view.activity.nextcloud.notes.shared.model.Item
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import java.text.SimpleDateFormat
import java.util.*

private val selected: List<Int> = ArrayList()
private val itemList: List<Item> = ArrayList()

class NotesAdapter(
        context: Context,
        val notesList: List<DBNote>?,
        private var noteclick: NotesActivity
) : RecyclerView.Adapter<NotesAdapter.ViewHolder>() {
    var activity:Context = context
    var noteClickListener=noteclick
  /*  val now = Date()
    var dateValue = formatDate.format(now)
*/
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.notes_items, parent, false)
        return ViewHolder(v, activity, noteClickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NotesAdapter.ViewHolder, position: Int) {
        notesList?.get(position)?.let { holder.bindItems(it, activity, noteClickListener, position) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return notesList?.size!!
    }

    //the class is hodling the list view
    class ViewHolder(
            itemView: View,
            context: Context,
            noteClickListener: NoteClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(notes: DBNote, context: Context, noteClickListener: NoteClickListener, position: Int) {
            val textViewName = itemView.findViewById(R.id.title) as TextView
            val content = itemView.findViewById(R.id.content) as TextView
            val image = itemView.findViewById(R.id.activity_icon) as ImageView
            val delete = itemView.findViewById(R.id.delete) as ImageView
            val date = itemView.findViewById(R.id.date) as TextView
            if(notes.content!=null && notes.content.length>0)
            content.text=notes.content
            else{
                content.text=notes.excerpt
            }
            textViewName.text = notes.title
            var miliSec=notes.modified
            //miliSec.timeInMillis
           Log.i("notes", notes.toString())
            var formatDate = SimpleDateFormat("dd MMM, yyyy")

            val result = miliSec.let { Date(it) }

            date.text = formatDate.format(result)
            //date.text = ""+miliSec
            image.setImageResource(R.drawable.notes)
//            itemView?.setOnClickListener {
//                noteClickListener.onNoteLongClick(position, itemView)
//                noteClickListener.onNoteClick(position)
//            }
            itemView.setOnLongClickListener {
                val position = layoutPosition
               // println("LongClick: $p")
                noteClickListener.onNoteLongClick(position, itemView)

                true // returning true instead of false, works for me
            }

            itemView.setOnClickListener {
                val position = layoutPosition
                val notes: CloudNote = notes
                noteClickListener.onNoteClick(position)

                // Toast.makeText(context, "Recycle Click$notes  ", Toast.LENGTH_SHORT).show()
            }
            delete.setOnClickListener {
                val position = layoutPosition
                //val notes: CloudNote = notes
                noteClickListener.onNoteDeleteClick(position)

                // Toast.makeText(context, "Recycle Click$notes  ", Toast.LENGTH_SHORT).show()
            }
        }

    }

    fun getItem(notePosition: Int): CloudNote? {
        return notesList!!.get(notePosition)
    }

}