package com.techesolutions.view.activity.nextcloud.notes

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.IntroView
import com.techesolutions.data.remote.model.nextclouddashboard.NotesDataModel
import com.techesolutions.services.ApiService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Constants.PARAM_CATEGORY
import com.techesolutions.utils.Constants.PARAM_CONTENT
import com.techesolutions.utils.Constants.PARAM_NOTE
import com.techesolutions.utils.Constants.PARAM_NOTE_ID
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.notes.persistence.NotesDatabase
import com.techesolutions.view.activity.nextcloud.notes.shared.model.CloudNote
import com.techesolutions.view.activity.nextcloud.notes.shared.model.DBNote
import kotlinx.android.synthetic.main.create_notes.*
import kotlinx.android.synthetic.main.layout_header.*
import kotlinx.android.synthetic.main.layout_header_with_title.headerTitle
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.UnsupportedEncodingException
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by Neelam on 24-12-2020.
 */

class CreateNotesActivity : AppCompatActivity(), View.OnClickListener {
    private var category: String?=null
    private var selectedCategory: String=""
    private var note_title: String?=null
    private var content: String?=null
    var recyclerView: RecyclerView? = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    var notesData: JSONObject? = null
    var noteData: ArrayList<NotesDataModel>? = null
    var from: String? = null
    var noteId: Long = 0
    var isEdit: Boolean = false
    private val introView = IntroView()
    private var progressDialog = CustomProgressDialog()
    protected var db: NotesDatabase? = null
    var categories: List<NavigationAdapter.CategoryNavigationItem>? = null
    var note: DBNote? = null
    var noteList: List<DBNote>? = null
    private lateinit var spinner: Spinner
    var url: String? = null
    var user: String? = null
    var pass: String? = null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant", "WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.create_notes)
        Utils.statusBarSetup(this)
        spinner = findViewById<View>(R.id.spinner) as Spinner

        val i: Intent = getIntent()
        category = i.getStringExtra(PARAM_CATEGORY)
        note_title = i.getStringExtra(PARAM_NOTE)
        content = i.getStringExtra(PARAM_CONTENT)
        /*val noteListAsString = intent.getStringExtra(PARAM_NOTE)

        val gson = Gson()
        val type: Type = object : TypeToken<List<CloudNote>?>() {}.getType()
        val noteList: List<CloudNote> = gson.fromJson(noteListAsString, type)
        for (note in noteList) {
            Log.i("Car Data", note.remoteId.toString() + "-" + note.title)
        }*/

//        etFolder!!.setText(category)
        db = NotesDatabase.getInstance(this)
        categories = db!!.getCategories(1)
        val categoryAdapter = CategoryAdapter(this, categories)
        spinner.adapter = categoryAdapter
        var position=0
        for (i in categories!!.indices) {
            if (categories!![i].label.equals(category)) {
                Log.i("profileTimezone", "" + i)
                position = i
            }
        }

        spinner!!.setSelection(position)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
            ) {
                // get selected item text
                if(position>0) {
                    selectedCategory=  categories!![position].label.toString()
                    etFolder!!.setText(selectedCategory)
                  
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // another interface callback
            }
        }

        val shared =
                getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)

        url=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        pass=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        launchNoteMode()
        save.setOnClickListener(this)
        cancel.setOnClickListener(this)
        back.setOnClickListener(this)
        create.setOnClickListener(this)

    }

    fun launchNoteMode() {
        if (categories!!.size > 0) {
            etFolder.visibility = View.GONE
            spinner.visibility = View.VISIBLE
        } else {
            etFolder.visibility = View.VISIBLE

            spinner.visibility = View.GONE
        }
        val noteId: Long = getNoteId()
        Log.i("noteId", "" + noteId)
        if (noteId > 0) {
            bottom_bar!!.visibility = View.GONE
            create!!.text = "Edit"
            create!!.visibility = View.VISIBLE
            note = db!!.getNote(1, noteId)
          /*  val noteList = ArrayList<DBNote>()
            noteList.add(note!!)*/
            Log.i("notes", note.toString())
           /* if(note!!.remoteId==noteId)
            {
                Log.i("notes", note.toString())

            }*/

            //etNote.text=noteList
            launchExistingNote(false)
        } else {
            headerTitle!!.text = " Create Notes"
            bottom_bar!!.visibility = View.VISIBLE
            launchNewNote()
        }
    }

    private fun launchNewNote() {
        etNote!!.isEnabled = true
        etTitle!!.isEnabled = true
        etFolder!!.isEnabled = true
    }

    private fun launchExistingNote(editable: Boolean) {
        Log.i("lauch note", note.toString())
        etFolder!!.setText(note!!.category)
        etTitle!!.setText(note!!.title)
        etNote!!.setText(note!!.excerpt)
//        if(note!=null)
//        {
//            etNote!!.text=note.title
//        }
        if (editable) {
            create!!.text = "Save"
            headerTitle!!.text = "Edit Notes"
            etNote!!.isEnabled = true
            etTitle!!.isEnabled = true
            etFolder!!.isEnabled = true

        } else {
            headerTitle!!.text = "Notes"
            etNote!!.isEnabled = false
            etTitle!!.isEnabled = false
            etFolder!!.isEnabled = false
           // etFolder!!.text=note!!.title
        }


    }

    @JvmName("getNoteId1")
    fun getNoteId(): Long {
        return intent.getLongExtra(PARAM_NOTE_ID, 0)
    }

    fun getAuthToken(): String {
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        val user: String?=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", url+", "+user+", "+pass)
        var data = ByteArray(0)
        try {
            data = (user + ":" + pass).toByteArray(charset("UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP)
    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                finish()
            }
            R.id.cancel -> {
                finish()
            }
            R.id.save -> {
                createNotes()
            }
            R.id.create -> {
                if (create!!.text.equals("Edit")) {
                    isEdit = true
                    launchExistingNote(isEdit)
                } else {
                    editNotes()
                }

            }
        }

    }

    private fun createNotes() {
        if (TextUtils.isEmpty(etTitle!!.text.toString())) {
            onErrorListener("Title is blank")
        } else if (TextUtils.isEmpty(etNote!!.text.toString())) {
            onErrorListener("Note is blank")
        } else {
            showLoading("Creating Note...")
            createNote()

        }
    }
private  fun createNote()
{
    val jsonObject = JsonObject()
    jsonObject.addProperty("content", etNote!!.text.toString())
    jsonObject.addProperty("category", etFolder!!.text.toString())
    jsonObject.addProperty("favorite", false)
    jsonObject.addProperty("title", etTitle!!.text.toString())
    jsonObject.addProperty("modified", Calendar.getInstance().timeInMillis)
    Log.v("json : ", jsonObject.toString() )
    val client = OkHttpClient.Builder()
        .addInterceptor(BasicAuthInterceptor(user!!, pass!!))
        .connectTimeout(1, TimeUnit.MINUTES)
        .writeTimeout(1, TimeUnit.MINUTES) // write timeout
        .readTimeout(1, TimeUnit.MINUTES) // read timeout
        .build()
  /*  if (url!!.endsWith("/")) {
        url = url!!.substring(0, url!!.length - 1)
    }*/
    if (!url!!.endsWith("/")) {
        url = url+"/"
    }
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun <T> buildService(service: Class<T>): T{
        return retrofit.create(service)
    }
    val request = buildService(ApiService::class.java)
    val call = request.createNotes( jsonObject)

    call.enqueue(object : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            Log.v("DEBUG : ", t.message.toString())
        }

        override fun onResponse(
            call: Call<ResponseBody>,
            response: Response<ResponseBody>
        ) {
            // TODO("Not yet implemented")
            Log.v("DEBUG : ", response.body().toString())

            val data = response.body().toString()
            Log.v("data : ", data)
            closeLoading()
            val intent = Intent()
            setResult(Activity.RESULT_OK, intent)
            finish()

        }
    })
}
    private fun editNotes() {
        if (TextUtils.isEmpty(etTitle!!.text.toString())) {
            onErrorListener("Title is blank")
        } else if (TextUtils.isEmpty(etNote!!.text.toString())) {
            onErrorListener("Note is blank")
        } else {
            //var category = etFolder!!.text.toString()
            showLoading("Updating Note...")
            deleteNote (note!!.remoteId)
        }
    }

    private fun deleteNote(id: Long?) {
        val client = OkHttpClient.Builder()
            .addInterceptor(BasicAuthInterceptor(user!!, pass!!))
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES) // write timeout
            .readTimeout(1, TimeUnit.MINUTES) // read timeout
            .build()
       /* if (url!!.endsWith("/")) {
            url = url!!.substring(0, url!!.length - 1)
        }*/
        if (!url!!.endsWith("/")) {
            url = url+"/"
        }
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        fun <T> buildService(service: Class<T>): T{
            return retrofit.create(service)
        }
        val request = buildService(ApiService::class.java)

        val call = request.deleteNote(id)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())
                val data = response.body().toString()
                Log.v("data : ", data)
               // closeLoading()
                id?.let { db!!.deleteNote(it) }
                createNote()
            }
        })

    }

    private fun refreshNotes() {
        val jsonObject = JsonObject()
        jsonObject.addProperty("content", etNote!!.text.toString())
        jsonObject.addProperty("category", selectedCategory)
        jsonObject.addProperty("favorite", false)
        jsonObject.addProperty("title", etTitle!!.text.toString())
        jsonObject.addProperty("modified", Calendar.getInstance().timeInMillis)
        Log.v("json : ", jsonObject.toString() )
        val client = OkHttpClient.Builder()
            .addInterceptor(BasicAuthInterceptor(user!!, pass!!))
            .connectTimeout(1, TimeUnit.MINUTES)
            .writeTimeout(1, TimeUnit.MINUTES) // write timeout
            .readTimeout(1, TimeUnit.MINUTES) // read timeout
            .build()
       /* if (url!!.endsWith("/")) {
            url = url!!.substring(0, url!!.length - 1)
        }*/
        if (!url!!.endsWith("/")) {
            url = url+"/"
        }
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        fun <T> buildService(service: Class<T>): T{
            return retrofit.create(service)
        }
        val request = buildService(ApiService::class.java)
        val call = request.editNotes(getNoteId(),  jsonObject)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body().toString()
                Log.v("data : ", data)
                var remoteNote: CloudNote? = null
                remoteNote=CloudNote(
                    note !!.remoteId,
                    Calendar.getInstance().timeInMillis,
                    etTitle!!.text.toString(),
                    etNote!!.text.toString(),
                    false,
                    selectedCategory,
                    null
                )
                closeLoading()
                val intent = Intent()
                setResult(Activity.RESULT_OK, intent)
                finish()

            }
        })

    }

    private fun closeIntro() {
        if (introView != null && introView?.dialog.isShowing())
            introView?.dialog.dismiss()
    }

    private fun showIntro() {
        introView?.show(this)
        introView.dialog.findViewById<View>(R.id.cp_bg_view).setOnClickListener(View.OnClickListener { view ->
            closeIntro()
        })
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }
}

