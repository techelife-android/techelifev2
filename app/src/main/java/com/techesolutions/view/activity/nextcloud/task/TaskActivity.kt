package com.techesolutions.view.activity.nextcloud.task;

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.*
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.BuildConfig
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils.getRootDirPath
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import kotlinx.android.synthetic.main.activity_documents.*
import kotlinx.android.synthetic.main.activity_pdf_view.*
import kotlinx.android.synthetic.main.layout_header.*
import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Property
import java.io.*
import java.util.*
import kotlin.collections.ArrayList


class TaskActivity : AppCompatActivity(), OnRemoteOperationListener,
    OnDatatransferProgressListener, NoteClickListener , View.OnClickListener{
    private val create_note_cmd = 0
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var file: String?=null
    private var user: String?=null
    var recyclerView: RecyclerView? = null
    var EVENT_DIR: String?=null
    var adapter: TaskAdapter? = null
    lateinit var dbCalender: ArrayList<Tasks>
    private val progressDialog = CustomProgressDialog()
    private lateinit var FilePathStrings: Array<String>
    private lateinit var listFile: Array<File>
    var deletedFile:String?=null
    var downFolder:File?=null
    var type: String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
        headerTitle.text = "Tasks"
        dbCalender= ArrayList()
        dbCalender!!.clear()
        mHandler = Handler()
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        type = intent.getStringExtra("type")
        //EVENT_DIR = "/calendars/"+user+"/personal/"
        EVENT_DIR = "/calendars/"+user+"/"+type+"/"
        Log.i("data", url+", "+user+", "+pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
               pass!!
            )
        )
        downFolder = File(getRootDirPath(baseContext) + EVENT_DIR)
        dbCalender.clear()
        if( downFolder!!.exists()) {
            if(downFolder!!.listFiles()!=null)
            listFile = downFolder!!.listFiles()
            if (listFile != null && listFile.size > 0) {
                for (i in 0 until listFile.size) {
                    val downloadedFile = File(downFolder, listFile.get(i).name)
                    if (downloadedFile.exists()) {
                        downloadedFile?.let {
                            readICS(
                                it
                            )
                        }
                    } else {
                        startRefresh()
                    }
                }

            }
        }
        else{
            //downFolder.mkdir()
            startRefresh()
       }

        back.setOnClickListener(this)
        calender!!.visibility = View.GONE
        addEvent!!.visibility = View.VISIBLE
        addEvent.setOnClickListener(this)
        calender.setOnClickListener(this)
        refreshEvent!!.visibility = View.VISIBLE
        refreshEvent.setOnClickListener(this)
    }

    private fun startRefresh() {
        showLoading("Please wait...")
        val refreshOperation = ReadCalanderRemoteOperation(user,FileUtils.PATH_SEPARATOR,type)
        refreshOperation.execute(mClient, this, mHandler)
    }

    @SuppressLint("WrongConstant")
    private fun readICS(downloadedFile: File) {
        //dbCalender = ArrayList<Contacts>()
        try {
            val fin = FileInputStream(downloadedFile)
            //Uri uri = Uri.fromFile(file);
            val uri =
                FileProvider.getUriForFile(
                    this@TaskActivity,
                    BuildConfig.APPLICATION_ID + ".provider",
                    downloadedFile
                )

            //use ical4j to parse the event
            val cb = CalendarBuilder()
            val vcalendar: net.fortuna.ical4j.model.Calendar = cb.build(
                getStreamFromOtherSource(
                    this,
                    uri
                )
            )

           /* val builder = CalendarBuilder()

            var calendar: net.fortuna.ical4j.model.Calendar? = builder.build(fin)*/
            if(vcalendar!=null) {
                var title=""
                var description=""
                var sTime=""
                var eTime=""
                var timestamp=""
                var uid=""
                val i = vcalendar!!.getComponents().iterator()
                while (i.hasNext()) {
                    val component = i.next() as Component
                    Log.i("calender", "Component [" + component.getName() + "]")
                    val j = component.getProperties().iterator()
                    while (j.hasNext()) {
                        val property = j.next() as Property

                        if(property.getName().equals("SUMMARY")) {
                             title = property.getValue()
                        }
                        if(property.getName().equals("DESCRIPTION")) {
                             description = property.getValue()
                        }
                        if(property.getName().equals("DTSTART")) {
                            sTime = property.getValue()
                        }
                        if(property.getName().equals("DUE")) {
                           eTime = property.getValue()
                        }

                        if(property.getName().equals("DTSTAMP")) {
                            timestamp = property.getValue()
                        }
                        if(property.getName().equals("UID")) {
                           uid = property.getValue()
                        }


                        Log.i(
                            "calender",
                            "Property [" + property.getName() + ", " + property.getValue() + "]"
                        )
                    }
                    if(component.getName().equals("VTODO")) {
                        dbCalender!!.add(
                            Tasks(
                                title,
                                description,
                                sTime,
                                eTime,
                                timestamp,
                                uid,
                                downloadedFile.name
                            )
                        )
                    }
                }

            }

          } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.i("dbEvents", dbCalender.toString())
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        adapter = TaskAdapter(this@TaskActivity, dbCalender, this)

        //now adding the adapter to recyclerview
        recyclerView?.adapter = adapter
        recyclerView?.adapter!!.notifyDataSetChanged()
          /*recyclerView = findViewById(R.id.recycler_view) as RecyclerView
          recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
          adapter = EventAdapter(this@EventsActivity, dbCalender, this)

          //now adding the adapter to recyclerview
          recyclerView?.adapter = adapter
          recyclerView?.adapter!!.notifyDataSetChanged()*/
        // File file = new File( p.getRemotePath());
    }
    private fun getStreamFromOtherSource(context: Context, contentUri: Uri): InputStream? {
        val res = context.applicationContext.contentResolver
        val uri = Uri.parse(contentUri.toString())
        val `is`: InputStream?
        `is` = try {
            res.openInputStream(uri)
        } catch (e: FileNotFoundException) {
            ByteArrayInputStream(ByteArray(0))
        }
        return `is`
    }
    private fun startDownload(filename: String) {
        file=filename
        Log.i("Calander", "downloading")
        Log.i("Calander", "download path: " + filename)

        val downFolder = File(getRootDirPath(baseContext) + EVENT_DIR)
       // downFolder.mkdir()
        //val f = File("somedirname1/somedirname2/somefilename")
        if (!downFolder.parentFile.exists()) downFolder.parentFile.mkdirs()
        if (!downFolder.exists()) downFolder.mkdir()
       if(filename.length>2) {
           val downloadOperation = DownloadEventsRemoteOperation(user,filename, downFolder.absolutePath,type)
           downloadOperation.addDatatransferProgressListener(this)
           downloadOperation.execute(mClient, this, mHandler)
       }
        else{
            closeLoading()
        }
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {

        if (!result!!.isSuccess()) {
            closeLoading()
        }
        else if (operation is ReadCalanderRemoteOperation) {
            onSuccessfulRefresh(operation as ReadCalanderRemoteOperation?, result)
        }
        else if(operation is UploadContactRemoteOperation)
        {

            onSuccessfulUpload(operation as UploadCalenderRemoteOperation?, result)

        }
        else if(operation is RemoveEventRemoteOperation)
        {

            closeLoading()
            /* Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                 .show()*/
            val downFolder = File(getRootDirPath(baseContext) + EVENT_DIR)
            Log.i("download folder", downFolder.absolutePath)
            if(downFolder.exists()) {
                val downloadedFile = File(downFolder, deletedFile)
                if (downloadedFile.exists()) {
                    downloadedFile.delete()
                }
            }
            dbCalender.clear()
            if(downFolder.exists()) {
                listFile = downFolder.listFiles()
                if (listFile != null && listFile.size > 0) {
                    for (i in 0 until listFile.size) {
                        val downloadedFile = File(downFolder, listFile.get(i).name)
                        Log.i("file event", downloadedFile.toString())
                        downloadedFile?.let {
                            readICS(
                                it
                            )
                        }
                    }

                }
            }
        }
        else {
            //closeLoading()
            /* Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                 .show()*/
            val downFolder = File(getRootDirPath(baseContext) + EVENT_DIR)
            Log.i("download folder", downFolder.absolutePath)
            dbCalender.clear()
            if(downFolder.exists()) {
                listFile = downFolder.listFiles()
                if (listFile != null && listFile.size > 0) {
                    for (i in 0 until listFile.size) {
                        val downloadedFile = File(downFolder, listFile.get(i).name)
                        Log.i("file event", downloadedFile.toString())
                        downloadedFile?.let {
                            readICS(
                                it
                            )
                        }
                    }

                }
            }
            else{
                downFolder.mkdir()
                /*if(downFolder.listFiles()!=null)
                listFile = downFolder.listFiles()
                if (listFile != null && listFile.size > 0) {
                    for (i in 0 until listFile.size) {
                        val downloadedFile = File(downFolder, listFile.get(i).name)
                        Log.i("file event",downloadedFile.toString())
                        *//*downloadedFile?.let {
                            readVCF(
                                it
                            )
                        }*//*
                    }

                }*/
            }
        }
    }
    private fun onSuccessfulUpload(
        uploadContactRemoteOperation: UploadCalenderRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        closeLoading()
        startRefresh()
    }

    private fun onSuccessfulRefresh(
        readContactsRemoteOperation: ReadCalanderRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        //mFilesAdapter.clear()
        val files: MutableList<RemoteFile> = ArrayList()
        for (obj in result.data) {
            files!!.add(obj as RemoteFile)
            Log.i("Calander data", files.toString())
        }
        if (files != null && files.size>0 ) {
            val it: Iterator<RemoteFile> = files.iterator()
            while (it.hasNext()) {
                startDownload(it.next().remotePath)
                //mFilesAdapter.add(it.next())
            }
           // mFilesAdapter.remove(mFilesAdapter.getItem(0))
        }
        else{
            closeLoading()
            Toast.makeText(this@TaskActivity,"No data available", Toast.LENGTH_SHORT).show()
        }

        //closeLoading()
      //  mFilesAdapter.notifyDataSetChanged()
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog!=null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }
    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileAbsoluteName: String?
    ) {
    }

    override fun onNoteClick(position: Int) {
        val intent: Intent = Intent(
            this@TaskActivity,
            CreateTaskActivity::class.java
        )
        intent.putExtra("action", "edit")
        intent.putExtra("from", "task")
        intent.putExtra("category", type)
        intent.putExtra("title", dbCalender[position].title)
        intent.putExtra("desc", dbCalender[position].description)
        intent.putExtra("stime",dbCalender[position].stratTime)
        intent.putExtra("etime",dbCalender[position].endTime)
        intent.putExtra("filename",dbCalender[position].filename)
        startActivityForResult(intent, 1)
    }

    override fun onNoteDeleteClick(position: Int) {
        showDeleteDialog(position)
    }

    override fun onNoteLongClick(position: Int, v: View?): Boolean {
        TODO("Not yet implemented")
    }
    private fun showDeleteDialog(position: Int) {
        val factory = LayoutInflater.from(this)
        val deleteDialogView = factory.inflate(R.layout.delete_dialog, null)
        val deleteDialog = AlertDialog.Builder(this).create()
        deleteDialog.setView(deleteDialogView)
        deleteDialogView.findViewById<View>(R.id.yes).setOnClickListener { //your business logic
            deleteDialog.dismiss()
            val p: Tasks = adapter!!.getItem(position)!!
            val path = "/"+p.filename
            startRemoteDeletion(path, p.filename)
        }
        deleteDialogView.findViewById<View>(R.id.no).setOnClickListener { deleteDialog.dismiss() }
        deleteDialog.show()
    }

    private fun startRemoteDeletion(path: String, filename: String) {
        showLoading("Deleting file")
        deletedFile=filename
        val removeOperation = RemoveEventRemoteOperation(user,type,path)
        removeOperation.execute(mClient, this, mHandler)
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                finish()
            }
            R.id.refreshEvent -> {
                if (downFolder != null && downFolder!!.exists() && downFolder!!.listFiles().size > 0) {
                    // downFolder!!.listFiles().de
                    if (downFolder!!.isDirectory()) {
                        val children: Array<String> = downFolder!!.list()
                        for (i in children.indices) {
                            File(downFolder, children[i]).delete()
                        }
                    }
                    //downFolder!!.delete()
                    dbCalender!!.clear()
                    startRefresh()
                } else {
                    startRefresh()
                }
            }

            R.id.addEvent -> {
                val intent: Intent = Intent(
                    this@TaskActivity,
                    CreateTaskActivity::class.java
                )
                intent.putExtra("action", "create")
                intent.putExtra("category", type)
                intent.putExtra("from", "task")
                startActivityForResult(intent, 1)
                //showCreateContactDialog(this@EventsActivity)
            }
        }
    }
     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
         super.onActivityResult(requestCode, resultCode, data)
         // Check which request we're responding to
         if (requestCode == 1) {
             // Make sure the request was successful
             if (resultCode == Activity.RESULT_OK) {
                 val filename=data!!.getStringExtra("filename")
                 Log.i("file",filename)
                 startDownload(filename)
             //startRefresh()
             }
         }
     }

}
