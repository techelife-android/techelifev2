package com.techesolutions.view.activity.nextcloud.files.preview;
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.DownloadFileRemoteOperation
import com.techesolutions.R
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils.getRootDirPath
import kotlinx.android.synthetic.main.activity_image_preview.*
import kotlinx.android.synthetic.main.activity_pdf_view.*
import kotlinx.android.synthetic.main.layout_header.*
import java.io.File
import java.lang.ref.WeakReference
import java.util.*

class  PreviewImageActivity : AppCompatActivity(), OnRemoteOperationListener,
    OnDatatransferProgressListener {
    val EXTRA_STREAM_URL = "STREAM_URL"
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var filename: String?=null
    private var file:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_preview)
        mHandler = Handler()

        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        val user: String?=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", url + ", " + user + ", " + pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
                pass!!
            )
        )
        val extras = intent.extras
        file = extras!!.getString(EXTRA_STREAM_URL)
        if (file!!.endsWith("/")) {
            filename = file!!.substring(0, file!!.lastIndexOf("/"))
            val files: Array<String> = file!!.split("/".toRegex()).toTypedArray()
            val f = files[files.size - 1]
            Log.i("TAG", "files: $f")
            // Log.i("TAG",subFolder.replaceAll("/",""));
            (findViewById<View>(R.id.headerTitle) as TextView).text = f
        } else {
            val file: String =
                file!!.substring(file!!.lastIndexOf("/") + 1, file!!.length)
            filename = file
        }
       // filename=file!!.replace("/", "")

        val downFolder = File(getRootDirPath(baseContext))
        val downloadedFile = File(downFolder, file)
        if(downloadedFile.exists())
        {
            showImageFromFile(downloadedFile)
        }
        else {

            startDownload(file!!)
        }
        headerTitle!!.text = filename
        back.setOnClickListener { finish() }
        findViewById<View>(R.id.refreshFiles).visibility = View.GONE
        findViewById<View>(R.id.refreshFiles).setOnClickListener {
            if (file != null) {
                startDownload(file!!)
            }
        }
    }
    private fun startDownload(remotePath: String) {
        val downFolder = File(getRootDirPath(baseContext))
        downFolder.mkdir()
        val downloadOperation = DownloadFileRemoteOperation(remotePath, downFolder.absolutePath)
        downloadOperation.addDatatransferProgressListener(this)
        downloadOperation.execute(mClient, this, mHandler)
    }
    private fun showImageFromFile(file: File) {
        empty_progress.visibility=View.GONE
        Glide.with(this)
                .load(file.absolutePath)
                .into(fullScreenImageView);

    }

  override fun onRemoteOperationFinish(
      operation: RemoteOperation<*>?,
      result: RemoteOperationResult<*>?
  ) {
        if (!result!!.isSuccess()) {
            Toast.makeText(this, R.string.todo_operation_finished_in_fail, Toast.LENGTH_SHORT)
                .show()
            Log.e("PdfViewActivity", result.getLogMessage(), result.getException())
        } else {
//            Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
//                .show()
            val downFolder = File(getRootDirPath(baseContext))
            val downloadedFile = File(downFolder, file)
            showImageFromFile(downloadedFile)
        }
    }

    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileAbsoluteName: String?
    ) {
        /*val percentage =
            if (totalToTransfer > 0) totalTransferredSoFar * 100 / totalToTransfer else 0
        mHandler!!.post {
            val progressView: TextView? = null
            if (progressBar != null) {
                progressBar.progress = percentage.toInt()
            }
        }
        progressBar.visibility= View.VISIBLE*/
    }
}
