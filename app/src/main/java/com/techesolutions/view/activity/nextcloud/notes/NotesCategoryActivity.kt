package com.techesolutions.view.activity.nextcloud.notes

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.IntroView
import com.techesolutions.data.remote.model.nextclouddashboard.NotesDataModel
import com.techesolutions.services.ApiService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.notes.persistence.NoteServerSyncHelper
import com.techesolutions.view.activity.nextcloud.notes.persistence.NotesDatabase
import com.techesolutions.view.activity.nextcloud.notes.shared.model.CloudNote
import com.techesolutions.view.activity.nextcloud.notes.shared.model.DBNote
import kotlinx.android.synthetic.main.layout_header.*
import kotlinx.android.synthetic.main.layout_header.headerTitle
import kotlinx.android.synthetic.main.layout_header_with_title.*
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.UnsupportedEncodingException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class NotesCategoryActivity : AppCompatActivity(), View.OnClickListener {
    private val create_note_cmd = 0
    private val show_single_note_cmd: Int = 1
    var recyclerView: RecyclerView? = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    var categories: List<NavigationAdapter.CategoryNavigationItem>? = null

    var from: String? = null
    private val introView = IntroView()
    private var progressDialog = CustomProgressDialog()
    protected var db: NotesDatabase? = null
    var adapter: NotesCategoryAdapter? = null
    var dbNotes: List<DBNote>? = null
    lateinit var listView: ListView
    lateinit var dialog: AlertDialog
    var url: String? = null
    var user: String? = null
    var pass: String? = null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    @SuppressLint("WrongConstant", "WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_notes)
        Utils.statusBarSetup(this)
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        url=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        pass=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        db = NotesDatabase.getInstance(this)
        // processExtraData()

        dbNotes = db!!.getNotes(1)
        Log.d("notes", dbNotes.toString())
        categories= db!!.getCategories(1)
        Log.d("categor", categories.toString())

        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        headerTitle!!.text = "Notes"
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        if(categories!!.size<1) {
            getNotes()
        }
        else{
            adapter = NotesCategoryAdapter(this@NotesCategoryActivity, categories)
            recyclerView?.adapter = adapter
        }
        create!!.visibility = View.VISIBLE
        upload!!.visibility = View.GONE
        create!!.text="Create Note"
        back.setOnClickListener(this)
        create.setOnClickListener(this)
        refresh!!.visibility = View.VISIBLE
        refresh.setOnClickListener(this)
    }

    private fun getNotes() {
        showLoading("Please wait...")
         val client = OkHttpClient.Builder()
             .addInterceptor(BasicAuthInterceptor(user!!, pass!!))
             .connectTimeout(1, TimeUnit.MINUTES)
             .writeTimeout(1, TimeUnit.MINUTES) // write timeout
             .readTimeout(1, TimeUnit.MINUTES) // read timeout
             .build()
        /*if (url!!.endsWith("/")) {
            url = url!!.substring(0, url!!.length - 1)
        }*/
        if (!url!!.endsWith("/")) {
            url = url+"/"
        }
         val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        fun <T> buildService(service: Class<T>): T{
            return retrofit.create(service)
        }
        val request = buildService(ApiService::class.java)

        val call = request.getNotes()

        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                closeLoading()
                Log.v("DEBUG : ", t.message.toString())

            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                closeLoading()
                Log.i("notes data", "" + response)
                val stringResponse = response.body()?.string()
                if (stringResponse != null) {
                    val dlist = JSONArray(stringResponse)
                    Log.i("dlist", "" + dlist)
                    if (dlist != null && dlist.length() > 0) {

                        Log.i("dlist", "" + dlist)
                        val notesData = ArrayList<NotesDataModel>()
                        val remoteNotes = ArrayList<CloudNote>()
                        // val vital = jsonObjectLabels.getString("Vital").toString()
                        for (i in 0 until dlist.length()) {
                            val item = dlist.getJSONObject(i)
                            notesData.add(
                                NotesDataModel(
                                    item.getLong("id"),
                                    item.getString("title"),
                                    item.getLong("modified"),
                                    item.getString("category"),
                                    item.getBoolean("favorite"),
                                    item.getBoolean("error"),
                                    item.getString("content")
                                )
                            )
                        }
                        // pull remote changes: update or create each remote note
                        var remoteNote: CloudNote? = null
                        for (i in 0 until dlist.length()) {
                            val item = dlist.getJSONObject(i)
                            remoteNote = CloudNote(
                                item.getLong("id"),
                                item.getLong("modified"),
                                item.getString("title"),
                                item.getString("content"),
                                item.getBoolean("favorite"),
                                item.getString(
                                    "category"
                                ),
                                null
                            )
                            remoteNotes.add(remoteNote)
                            Log.v(NoteServerSyncHelper.TAG, "   ... create")
                            db!!.addNote(1, remoteNote)
                        }
                        categories = db!!.getCategories(1)
                        //adding some dummy data to the list
                        adapter = NotesCategoryAdapter(this@NotesCategoryActivity, categories)
                        recyclerView?.adapter = adapter
                    } else {
                        Toast.makeText(
                            this@NotesCategoryActivity,
                            "No Data",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            }

        })

    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                finish()
            }

            R.id.create -> {
                val intent = Intent(applicationContext, CreateNotesActivity::class.java)
                intent.putExtra(Constants.PARAM_NOTE_ID, 0)
                startActivityForResult(intent, show_single_note_cmd)
            }
            R.id.refresh -> {
                getNotes()
            }

        }

    }

    private fun closeIntro() {
        if (introView != null && introView?.dialog.isShowing())
            introView?.dialog.dismiss()
    }

    private fun showIntro() {
        introView?.show(this)
        introView.dialog.findViewById<View>(R.id.cp_bg_view).setOnClickListener(View.OnClickListener { view ->
            closeIntro()
        })
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == show_single_note_cmd) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                getNotes()
            }
        }
        if (requestCode == create_note_cmd) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                getNotes()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        dbNotes = db!!.getNotes(1)
        categories= db!!.getCategories(1)
        adapter = NotesCategoryAdapter(this@NotesCategoryActivity, categories)
        recyclerView?.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

}

