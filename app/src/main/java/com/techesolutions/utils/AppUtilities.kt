package com.techesolutions.utils

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.preference.PreferenceManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.io.File
import java.net.Inet4Address
import java.net.NetworkInterface
import java.net.SocketException
import java.util.*

/**
 * Created by Neelam on 19-12-2020.
 */
class AppUtilities private constructor() {
    /**
     * Save object as String to Shared Preference
     *
     * @param context The context of the current state
     * @param key     The name of the preference.
     * @param value   The value for the preference.
     */
    fun saveToSharedPreference(
        context: Context?,
        key: String?,
        value: Any?
    ) {
        val localValue: String?
        localValue = if (value != null && context != null) {
            value.toString()
        } else {
            null
        }
        val sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(context)
        val editor = sharedPreferences.edit()
        editor.putString(key, localValue)
        editor.apply()
    }

    /**
     * Get String value from Shared Preference
     *
     * @param context The context of the current state
     * @param key     The name of the preference to retrieve
     * @return Value for the given preference if exist else null.
     */
    fun getFromSharedPreference(
        context: Context?,
        key: String?
    ): String? {
        val sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(context)
        return sharedPreferences.getString(key, null)
    }

    /**
     * @param activity current screen
     * @param view     Current focus view
     */
    fun hideKeyboard(activity: Activity, view: View?) {
        // Check if no view has focus:
        var view = view
        if (view == null) {
            view = activity.currentFocus
        }
        if (view != null) {
            val inputManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    var WIDGETS_LIST: ArrayList<String?> =
        object : ArrayList<String?>() {
            init {
                //add("My Pending Tasks");
                add("My Care Feed")
                add("My Activities")
            }
        }
    var QUICK_TASKS_LIST: ArrayList<String?> =
        object : ArrayList<String?>() {
            init {
                //add("My Pending Tasks");
                add("Vital")
                //        add("Meeting Room");
                add("Validate Phone/Email")
                add("Todo")
                add(" Activities ")
                add("Manage Care Community")
                add("Care Resources")
                add("Request Care")
                add("Care Exchange")
                add("My Documents")
                add("Request Medication")
                add(" Medications ")
                add("AddConsult")
                add("How Are You Feeling")
            }
        }
    var QUICK_TASKS_LIST_PROVIDER: ArrayList<String?> =
        object : ArrayList<String?>() {
            init {
                //add("My Pending Tasks");
                add("Todo")
                //        add("Meeting Room");
                add("Care Network")
            }
        }
    var MEET_WIDGETS_LIST: ArrayList<String?> =
        object : ArrayList<String?>() {
            init {
//        add("Meeting Room");
                add("Message")
                add("Care Community")
                add("Activities Today and Future")
                add("Weather")
                add("MY Care Resources")
                add("My Medications")
                add("My Request Care")
                add("Care Exchange")
                add("Health Card")
                add("Patient Population")
                add("New Action Plan")
                add("Calendar Visits View")
                add("Vital Alert")
                add("Reminder track")
                //        add("My Care Feed");
//        add("My Activities");
            }
        }
    var LHR_TABS_LIST: ArrayList<String?> =
        object : ArrayList<String?>() {
            init {
//        add("Access to LifeHealthRecord Profile");
//        add("Summary");
//        add("Payment Summary");
                add("Care Community")
                add("Medications")
                //        add("Health Conditions");
                add("Case Feeds")
                add("Request Care")
                add("Documents")
                add("Vitals")
                //        add("Symptom Report");
//        add("Care Action Plan");
                add("Visits and Activities")
                add("Reminders")
                add("Educational Modules")
                //        add("Health Screening");
//        add("Vacc. / Immun. Record");
//        add("Encounters");
//        add("Transition History");
//        add("To Do's");
//        add("Video Monitoring");
//        add("Labs");
//        add("Referral");
//        add("Imaging");
//        add("Care Episode");
//        add("End of Life Planning");
//        add("Emergency Planning");
//        add("Advanced Directives");
//        add("Financial Data");
                add("Care Action Plan")
                add("Care Exchange")
                //        add("Issues / Incidents");
            }
        }

    companion object {
        private var utils: AppUtilities? = null
        val instance: AppUtilities?
            get() {
                if (utils == null) {
                    utils = AppUtilities()
                }
                return utils
            }

        fun isTablet(context: Context): Boolean {
            val xlarge = context.resources
                .configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == 4
            val large = context.resources
                .configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_LARGE
            return xlarge || large
        }

        val localIpAddress: String?
            get() {
                try {
                    val en =
                        NetworkInterface.getNetworkInterfaces()
                    while (en.hasMoreElements()) {
                        val intf = en.nextElement()
                        val enumIpAddr =
                            intf.inetAddresses
                        while (enumIpAddr.hasMoreElements()) {
                            val inetAddress = enumIpAddr.nextElement()
                            if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
                                return inetAddress.getHostAddress()
                            }
                        }
                    }
                } catch (ex: SocketException) {
                    ex.printStackTrace()
                }
                return null
            }

        val androidVersion: String
            get() {
                val manufacturer = Build.MANUFACTURER
                val model = Build.MODEL
                val release = Build.VERSION.RELEASE
                val sdkVersion = Build.VERSION.SDK_INT
                return "$manufacturer $model Android SDK $sdkVersion ($release)"
            }

        fun checkFileSize(filePathToUpload: String?): Boolean {
            val file = File(filePathToUpload)

            // Get length of file in bytes
            val fileSizeInBytes = file.length()
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            val fileSizeInKB = fileSizeInBytes / 1024
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            val fileSizeInMB = fileSizeInKB / 1024
            return if (fileSizeInMB > 150) {
                false
            } else true
        }

        fun getInstance(): Any {
            if (utils == null) {
                utils = AppUtilities()
            }
            return utils as AppUtilities
        }

        /**
         * @param activity current screen
         * @param view     Current focus view
         */
        fun hideKeyboard(activity: Activity, view: View?) {
            // Check if no view has focus:
            var view = view
            if (view == null) {
                view = activity.currentFocus
            }
            if (view != null) {
                val inputManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(
                    view.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }
        }

        var entryViaArray =
            arrayOf("Manual entry", "Linked device", "Flow from form")
        var entrySourceArray = arrayOf(
            "Care recipient",
            "Caregiver",
            "Clinician in person",
            "Clinician remote",
            "Other"
        )
        var entryPainLevelsArray = arrayOf(
            "0 - No pain",
            "1 - Minimal pain",
            "2 - Minimal pain",
            "3 - Minimal pain",
            "4 - Moderate pain",
            "5 - Moderate pain",
            "6 - Moderate pain",
            "7 - Severe pain",
            "8 - Severe pain",
            "9 - Severe pain",
            "10 - Worst pain imaginable"
        )
        var allergyTypeArray =
            arrayOf("Select", "Drug", "Environmental", "Food", "Other")
        var allergySeverityArray =
            arrayOf("Minimal", "Moderate", "Severe", "Life-threatening")
        var qualityFeedbackArray = arrayOf(
            "Very satisfied",
            "Somewhat satisfied",
            "Neutral",
            "Dissatisfied",
            "Very Dissatisfied"
        )
        var understandingFeedbackArray = arrayOf(
            "Extremely well",
            "Very well",
            "Somewhat well",
            "Not so well",
            "Not at all well"
        )
        var timeFeedbackArray = arrayOf(
            "Much shorter than expected",
            "Shorter than expected",
            "About what I expected",
            "Longer than expected",
            "Much longer than expected"
        )
        var frequencyArray = arrayOf(
            "Daily",
            "Two Times Per Day",
            "Three Times Per Day",
            "Four Times Per Day",
            "Six Times Per Day",
            "As Needed",
            "Every 1 Hour",
            "Every 2 Hours",
            "Every 3 Hours",
            "Every 4 Hours",
            "Every 6 Hours",
            "Every 8 Hours",
            "Every 12 Hours",
            "At Breakfast",
            "At bedtime",
            "Weekly",
            "Monthly",
            "Every Other Week",
            "Every Other Day",
            "As Needed(PRN)",
            "With Meals (3)"
        )

        /**
         * months array
         */
        val monthsArray = arrayOf(
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"
        )

        /**
         * years array
         */
        val accountTypeArray =
            arrayOf("Checking", "Saving")
    }
}