package com.techesolutions.view.activity.nextcloud.task;

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.*
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.nextcloud.events.EventsActivity
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils.getRootDirPath
import com.techesolutions.view.callbacks.ItemClickListener
import kotlinx.android.synthetic.main.activity_documents.*
import kotlinx.android.synthetic.main.activity_pdf_view.*
import kotlinx.android.synthetic.main.layout_header.*
import java.io.*
import java.util.*
import kotlin.collections.ArrayList


class TaskFoldersActivity : AppCompatActivity(), OnRemoteOperationListener,
    OnDatatransferProgressListener, ItemClickListener, View.OnClickListener{
    private val create_note_cmd = 0
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var file: String?=null
    private var user: String?=null
    var recyclerView: RecyclerView? = null
    var EVENT_DIR: String?=null
    var adapter: TaskCategoryAdapter? = null
    lateinit var dbTaskCategory: ArrayList<TasksCategory>
    private val progressDialog = CustomProgressDialog()
    private lateinit var FilePathStrings: Array<String>
    private lateinit var listFile: Array<File>
    var deletedFile:String?=null
    var downFolder:File?=null
    var from: String=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
        from = intent.getStringExtra("from")
        if(from.equals("events")) {
            headerTitle.text = "Events"
        }
        else{
            headerTitle.text = "Tasks"
        }
        dbTaskCategory= ArrayList()
        dbTaskCategory!!.clear()
        mHandler = Handler()
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        //EVENT_DIR = "/calendars/"+user+"/personal/"
        EVENT_DIR = "/calendars/"+user+"/"
        Log.i("data", url+", "+user+", "+pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
               pass!!
            )
        )
        downFolder = File(getRootDirPath(baseContext) + EVENT_DIR)
        dbTaskCategory.clear()
        startRefresh()

        back.setOnClickListener(this)
        calender!!.visibility = View.GONE
        addEvent!!.visibility = View.GONE
        addEvent.setOnClickListener(this)
        calender.setOnClickListener(this)
        refreshEvent!!.visibility = View.GONE
        refreshEvent.setOnClickListener(this)
    }

    private fun startRefresh() {
        showLoading("Please wait...")
        val refreshOperation = ReadCalanderCategoryRemoteOperation(user,FileUtils.PATH_SEPARATOR)
        refreshOperation.execute(mClient, this, mHandler)
    }


    private fun getStreamFromOtherSource(context: Context, contentUri: Uri): InputStream? {
        val res = context.applicationContext.contentResolver
        val uri = Uri.parse(contentUri.toString())
        val `is`: InputStream?
        `is` = try {
            res.openInputStream(uri)
        } catch (e: FileNotFoundException) {
            ByteArrayInputStream(ByteArray(0))
        }
        return `is`
    }
    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {

        if (!result!!.isSuccess()) {
            closeLoading()
        } else if (operation is ReadCalanderCategoryRemoteOperation) {
            onSuccessfulRefresh(operation as ReadCalanderCategoryRemoteOperation?, result)
        }

    }


    @SuppressLint("WrongConstant")
    private fun onSuccessfulRefresh(
        readContactsRemoteOperation: ReadCalanderCategoryRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        //mFilesAdapter.clear()
        val files: MutableList<RemoteFile> = ArrayList()
        for (obj in result.data) {
            files!!.add(obj as RemoteFile)
            Log.i("Calander data", files.toString())
        }
        if (files != null && files.size>0 ) {
            val it: Iterator<RemoteFile> = files.iterator()
            while (it.hasNext()) {

                    var title: String=it.next().remotePath.replace("/","")

                   // if(title.length >1 && ((!title.equals("outbox")) || (!title.equals("inbox")))) {
                        // dbTaskCategory!!.add(it.next().remotePath)
                if(title.length >1) {
                    if (title.equals("outbox") || title.equals("inbox")) {

                    } else {
                        dbTaskCategory!!.add(
                            TasksCategory(
                                title
                            )
                        )
                    }
                }
            }
           // mFilesAdapter.remove(mFilesAdapter.getItem(0))
            recyclerView = findViewById(R.id.recycler_view) as RecyclerView
            recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
            adapter = TaskCategoryAdapter(this@TaskFoldersActivity, dbTaskCategory,this)

            //now adding the adapter to recyclerview
            recyclerView?.adapter = adapter
            recyclerView?.adapter!!.notifyDataSetChanged()
            closeLoading()
        }
        else{
            closeLoading()
            Toast.makeText(this@TaskFoldersActivity,"No data available", Toast.LENGTH_SHORT).show()
        }

    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog!=null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }
    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileAbsoluteName: String?
    ) {
    }



    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                finish()
            }
            R.id.refreshEvent -> {
                if (downFolder != null && downFolder!!.exists() && downFolder!!.listFiles().size > 0) {
                    // downFolder!!.listFiles().de
                    if (downFolder!!.isDirectory()) {
                        val children: Array<String> = downFolder!!.list()
                        for (i in children.indices) {
                            File(downFolder, children[i]).delete()
                        }
                    }
                    //downFolder!!.delete()
                    dbTaskCategory!!.clear()
                    startRefresh()
                } else {
                    startRefresh()
                }
            }

        }
    }
     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
         super.onActivityResult(requestCode, resultCode, data)
         // Check which request we're responding to
         if (requestCode == 1) {
             // Make sure the request was successful
             if (resultCode == Activity.RESULT_OK) {
               startRefresh()
             }
         }
     }

    override fun onPositionClick(adapterPosition: Int) {
        if(from.equals("events")) {
            val intent =
                Intent(this, EventsActivity::class.java)
            intent.putExtra("type", dbTaskCategory[adapterPosition].title)
            intent.putExtra("from", from)
            startActivity(intent)
        }
        else{
            val intent =
                Intent(this, TaskActivity::class.java)
            intent.putExtra("type", dbTaskCategory[adapterPosition].title)
            intent.putExtra("from", from)
            startActivity(intent)
        }
    }

}
