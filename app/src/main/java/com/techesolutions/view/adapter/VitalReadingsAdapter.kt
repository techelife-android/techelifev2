package com.techesolutions.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.data.local.techewatch.ReadingsData
import com.techesolutions.utils.Utils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class VitalReadingsAdapter(
    private val context: Context,
    ReadingsData: List<ReadingsData>?,
    title: String
) :
    RecyclerView.Adapter<VitalReadingsAdapter.RecyclerViewHolders>() {
    private val readingsData: List<ReadingsData>?
    var inflater: LayoutInflater
    var title: String
    var formatterDate =
        SimpleDateFormat("MMM dd, yyyy | HH:mm")
    var formatDate = SimpleDateFormat("MM-dd-yyyy")
    var formatTime = SimpleDateFormat("HH:mm")
    var formatDate2 = SimpleDateFormat("yyyy-MM-dd")
    var currentFormat =
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    var formatterDate1 = SimpleDateFormat("MMM dd, yyyy")
    var date: String=""
    var convertedDate: String=""
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerViewHolders {
        val layoutView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.vital_readings_layout, null)
        val now = Date()
        val measurementDate = currentFormat.format(now)

//        date =
//          formatDate.format(formatterDate1.parse(measurementDate))
//        date = Utils.convertNewFormat(
//            date1,
//            context
//        )
        //Toast.makeText(context,date,Toast.LENGTH_SHORT).show()
        return RecyclerViewHolders(layoutView)
    }

    override fun onBindViewHolder(
        sectionViewHolder: RecyclerViewHolders,
        i: Int
    ) {
        val readingData: ReadingsData = readingsData!![i]
        try {
            sectionViewHolder.readingDateTV.setText(
                Utils.convertNewFormat(
                    readingData.date,
                    context
                )
            )
        } catch (e: ParseException) {
            if (title.equals("Activities", ignoreCase = true)) {
                sectionViewHolder.readingDateTV.setText(readingData.date)
            }
            e.printStackTrace()
        }
        sectionViewHolder.unitOneName.setText(readingData.unit1Name)
//        if(readingData.date.equals(date))
//             sectionViewHolder.current_readingsTV.visibility=View.VISIBLE
        sectionViewHolder.valueUnitOne.setText(readingData.unit1Value)
        sectionViewHolder.valueUnitOneName.setText(readingData.unit1Units)
        if (readingData.unit2Units.length === 0) {
            sectionViewHolder.unitTwoLayout.visibility = View.GONE
        } else {
            sectionViewHolder.unitTwoLayout.visibility = View.VISIBLE
            sectionViewHolder.unitTwoName.setText(readingData.unit2Name)
            sectionViewHolder.valueUnitTwo.setText(readingData.unit2Value)
            sectionViewHolder.valueUnitTwoName.setText(readingData.unit2Units)
        }
        if (readingData.unit3Name.length === 0) {
            sectionViewHolder.unitThreeLayout.visibility = View.GONE
        } else {
            sectionViewHolder.unitThreeLayout.visibility = View.VISIBLE
            sectionViewHolder.unitThreeName.setText(readingData.unit3Name)
            sectionViewHolder.valueUnitThree.setText(readingData.unit3Value)
            sectionViewHolder.valueUnitThreeName.setText(readingData.unit3Units)
        }
        if (readingData.unit4Units.length === 0) {
            sectionViewHolder.unitFourLayout.visibility = View.GONE
        } else {
            sectionViewHolder.unitFourLayout.visibility = View.VISIBLE
            sectionViewHolder.unitFourName.setText(readingData.unit4Name)
            sectionViewHolder.valueUnitFour.setText(readingData.unit4Value)
            sectionViewHolder.valueUnitFourName.setText(readingData.unit4Units)
        }
//        if (title.equals("Pulse Oximeter", ignoreCase = true) || title.equals(
//                "Teche Watch",
//                ignoreCase = true
//            )
//        ) {
//            sectionViewHolder.EdiBT.visibility = View.GONE
//        } else {
//            sectionViewHolder.EdiBT.visibility = View.VISIBLE
//        }
//        sectionViewHolder.EdiBT.setOnClickListener {
//            val value1: String = readingsData[i].getUnit1Value()
//            val value2: String = readingsData[i].unit2Value
//            val value3: String = readingsData[i].getUnit3Value()
//            val date: String = readingsData[i].getDateConverted()
//            val time: String = readingsData[i].getTimeConverted()
//            (context as iHealthDeviceControlsActivity).editReading(
//                value1,
//                value2,
//                value3,
//                date,
//                time,
//                i
//            )
//        }
    }

    override fun getItemCount(): Int {
        //TODO need to handle by arshdeep this place sometimes itemList null
        return readingsData?.size ?: 0
    }

    inner class RecyclerViewHolders(itemView: View) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var unitOneLayout: LinearLayout
        var unitTwoLayout: LinearLayout
        var unitThreeLayout: LinearLayout
        var unitFourLayout: LinearLayout
        //var EdiBT: LinearLayout
        var unitOneName: TextView
        var valueUnitOne: TextView
        var valueUnitOneName: TextView
        var unitTwoName: TextView
        var valueUnitTwo: TextView
        var valueUnitTwoName: TextView
        var unitThreeName: TextView
        var valueUnitThree: TextView
        var valueUnitThreeName: TextView
        var readingDateTV: TextView
        var current_readingsTV: TextView
        var unitFourName: TextView
        var valueUnitFour: TextView
        var valueUnitFourName: TextView
        override fun onClick(view: View) {
//            int pos = getAdapterPosition();
//            String value1 = readingsData.get(pos).getUnit1Value();
//            String value2 = readingsData.get(pos).unit2Value;
//            String value3 = readingsData.get(pos).getUnit3Value();
//            String date = readingsData.get(pos).getDateConverted();
//            String time = readingsData.get(pos).getTimeConverted();
//            ((iHealthDeviceControlsActivity) context).editReading(value1, value2, value3, date, time, pos);
        }

        init {
            unitOneLayout =
                itemView.findViewById<View>(R.id.unitOneLayout) as LinearLayout
            unitTwoLayout =
                itemView.findViewById<View>(R.id.unitTwoLayout) as LinearLayout
            unitThreeLayout =
                itemView.findViewById<View>(R.id.unitThreeLayout) as LinearLayout
            unitFourLayout =
               itemView.findViewById<View>(R.id.unitFourLayout) as LinearLayout
            unitOneName =
                itemView.findViewById<View>(R.id.unitOneName) as TextView
            valueUnitOne =
                itemView.findViewById<View>(R.id.valueUnitOne) as TextView
            valueUnitOneName =
                itemView.findViewById<View>(R.id.valueUnitOneName) as TextView
            unitTwoName =
                itemView.findViewById<View>(R.id.unitTwoName) as TextView
            valueUnitTwo =
                itemView.findViewById<View>(R.id.valueUnitTwo) as TextView
            valueUnitTwoName =
                itemView.findViewById<View>(R.id.valueUnitTwoName) as TextView
            unitThreeName =
                itemView.findViewById<View>(R.id.unitThreeName) as TextView
            valueUnitThree =
                itemView.findViewById<View>(R.id.valueUnitThree) as TextView
            valueUnitThreeName =
                itemView.findViewById<View>(R.id.valueUnitThreeName) as TextView
            readingDateTV =
                itemView.findViewById<View>(R.id.readingDateTV) as TextView
            current_readingsTV =
                itemView.findViewById<View>(R.id.current_readingsTV) as TextView
//            EdiBT =
//                itemView.findViewById<View>(R.id.EdiBT) as LinearLayout
            unitFourName =
                itemView.findViewById<View>(R.id.unitFourName) as TextView
            valueUnitFour =
                itemView.findViewById<View>(R.id.valueUnitFour) as TextView
            valueUnitFourName =
                itemView.findViewById<View>(R.id.valueUnitFourName) as TextView
            itemView.setOnClickListener(this)
        }
    }

    init {
        readingsData = ReadingsData
        this.title = title
        inflater = LayoutInflater.from(context)
    }
}
