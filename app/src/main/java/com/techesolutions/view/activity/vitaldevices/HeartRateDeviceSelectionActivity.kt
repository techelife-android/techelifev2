package com.techesolutions.view.activity.vitaldevices

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.techesolutions.R
import com.techesolutions.data.remote.model.dashboard.HeartRateSelectionData
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.adapter.HeartRateSelectionAdapter
import kotlinx.android.synthetic.main.activity_select_heartrate.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject

class HeartRateDeviceSelectionActivity : AppCompatActivity() {
    var title: String? = null
    var vitalName: String? = null
    var vitalNameLabels: JSONObject? = null
    var vitalUnitLabels: JSONObject? = null
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_select_heartrate)
        Utils.setDimensions(this)
        val i: Intent = getIntent()
        title = i.getStringExtra("title")
        vitalName = i.getStringExtra("vitalName")
        headerTitle!!.text= title
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }
        val vitals = ArrayList<HeartRateSelectionData>()
        vitals.add(
            HeartRateSelectionData(
                vitalNameLabels!!.getString("Activities"),
                "Activities",
                vitalName!!,
                 vitalUnitLabels!!.getString("BPM"),
                ""
            )
        )
        vitals.add(
            HeartRateSelectionData(
                vitalNameLabels!!.getString("Pulse_Oximetry"),
                "Pulse_Oximetry",
                vitalName!!,
                vitalUnitLabels!!.getString("BPM"),
                ""
            )
        )
        vitals.add(
            HeartRateSelectionData(
                vitalNameLabels!!.getString("Blood_Pressure"),
                "Blood_Pressure",
                vitalName!!,
                vitalUnitLabels!!.getString("BPM"),
                ""
            )
        )

        recycler_view!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val adapter = HeartRateSelectionAdapter(this@HeartRateDeviceSelectionActivity, vitals)

        //now adding the adapter to recyclerview
        recycler_view?.adapter = adapter
        back.setOnClickListener {
            finish() }
        /*   blood_pressure.setOnClickListener {
             val intent = Intent(this@HeartRateDeviceSelectionActivity, DeviceInstructionManualActivity::class.java)
             intent.putExtra("title", title)
             intent.putExtra("vitalName", vitalName)
             startActivity(intent)
             finish() }
         pulse_rate.setOnClickListener {
             val intent = Intent(this@HeartRateDeviceSelectionActivity, DeviceInstructionManualActivity::class.java)
             intent.putExtra("title", title)
             intent.putExtra("vitalName", vitalName)
             startActivity(intent)
             finish() }*/
    }
}
