package com.techesolutions.data.remote.repository.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.techesolutions.data.remote.model.forgetpassword.Forgot
import com.techesolutions.services.NetworkService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Author: Neelam Upadhyay
 * Created: 30/12/2020
 */
object ForgotPasswordRepository {
    val forgotPasswordResponse = MutableLiveData<Forgot>()

    fun getCode(email: String, app:String): MutableLiveData<Forgot> {

        val call = NetworkService.apiInterface.forgotPassword(email,app)

        call.enqueue(object: Callback<Forgot> {
            override fun onFailure(call: Call<Forgot>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<Forgot>,
                response: Response<Forgot>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()

//                val msg = data!!.message
                forgotPasswordResponse.postValue(data)
                //loginResponse.value = LoginResponse(data)
                    // loginResponse.value(response.body())
            }


        })

        return forgotPasswordResponse
    }

}