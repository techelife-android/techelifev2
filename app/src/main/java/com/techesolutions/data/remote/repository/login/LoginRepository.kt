package com.techesolutions.data.remote.repository.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.techesolutions.data.remote.model.login.LoginResponse
import com.techesolutions.services.NetworkService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Author: Neelam Upadhyay
 * Created: 30/12/2020
 */
object LoginRepository {
    val loginResponse = MutableLiveData<LoginResponse>()

    fun doLogin(email: String, password: String, apporogin: String, timezone: String, deviceid: String, deviceinfo: String, lang: String, devicetype: String, ipaddress: String): MutableLiveData<LoginResponse> {

        val call = NetworkService.apiInterface.doLogin(email,password,apporogin,timezone,deviceid,deviceinfo,lang,devicetype,ipaddress)

        call.enqueue(object: Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()

               // val msg = data!!.message
                loginResponse.postValue(data)
                //loginResponse.value = LoginResponse(data)
                    // loginResponse.value(response.body())
            }
        })

        return loginResponse
    }

}