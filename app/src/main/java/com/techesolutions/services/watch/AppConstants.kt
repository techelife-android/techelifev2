package com.techesolutions.services.watch

class AppConstants {
    val PATTERN_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm"

    val EXTRA_CONN_COUNT = "extra_conn_count"

    val PERMISSION_REQUEST_CODE = 1


    val REQUEST_CODE_PERMISSION = 120
    val REQUEST_CODE_PERMISSION_2 = 121
    val REQUEST_CODE_LOCATION_SETTINGS = 122

    val REQUEST_CODE_ENABLE_BT = 1001
}