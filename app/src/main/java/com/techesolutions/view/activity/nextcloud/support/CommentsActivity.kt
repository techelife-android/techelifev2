package com.techesolutions.view.activity.nextcloud.support

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.customControls.fontedVews.edittext.ATecheRegularEditText
import com.techesolutions.data.remote.model.support.CommentData
import com.techesolutions.services.NetworkService
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.callbacks.ItemClickListener
import kotlinx.android.synthetic.main.activity_comment.*
import kotlinx.android.synthetic.main.activity_comment.noDataText
import kotlinx.android.synthetic.main.activity_comment.tvSeacrh
import kotlinx.android.synthetic.main.activity_support.*
import kotlinx.android.synthetic.main.layout_header.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CommentsActivity : AppCompatActivity(), View.OnClickListener, ItemClickListener {
    private var progressDialog = CustomProgressDialog()
    var token: String? = null
    var subject: String? = null
    var ticket_no: String? = null
    var id: Int? = null
    var description: String? = null
    var created_date: String? = null
    var status: String? = null
    var recyclerView: RecyclerView? = null
    var commentAdapter: CommentsAdapter? = null
    var commentData = ArrayList<CommentData>()
    var format =
        SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    var formatDate =
        SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_comment)
        Utils.setDimensions(this)

        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        token = shared.getString(Constants.KEY_TOKEN, null)
        subject = intent.getStringExtra("subject")
        ticket_no = intent.getStringExtra("ticket_no")
        id = intent.getIntExtra("id", 0)
        description = intent.getStringExtra("description")
        created_date = intent.getStringExtra("date")
        status = intent.getStringExtra("status")
        tvTicket.text = ticket_no
        tvDescription.text = description
        tvSubject.text = subject
        tvStatus.text = status
        date.text = created_date
        setUpHeader()
        getCommentList()
        setListeners()
        //showLockScreen()
    }

    private fun setListeners() {
        // listening to search query text change
        tvSeacrh.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // filter recycler view when query submitted
                commentAdapter!!.filter!!.filter(query)
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                if (commentAdapter != null) commentAdapter!!.filter!!.filter(query)
                return false
            }
        })
    }

    @SuppressLint("WrongConstant")
    open fun setUpHeader() {
        val headerTitleTV =
            findViewById<View>(R.id.headerTitle) as TextView
        headerTitleTV.text = "Tickets"
        addEvent.visibility = View.VISIBLE
        addEvent.setOnClickListener(this)
        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }

        ivRefresh.setOnClickListener(this)


        recyclerView = findViewById(R.id.recycler_view) as RecyclerView

        //adding a layoutmanager
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

    }

    override fun onClick(v: View?) {

        when (v?.id) {

            R.id.addEvent -> {
                showAddCommentDialog(this@CommentsActivity)
            }
            R.id.ivRefresh -> {
                getCommentList()
            }
        }
    }

    private fun showAddCommentDialog(context: CommentsActivity) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.add_comment)

        val yesBtn = dialog.findViewById(R.id.save) as TextView
        val noBtn = dialog.findViewById(R.id.cancel) as TextView
        val etComment = dialog.findViewById(R.id.etComment) as ATecheRegularEditText

        yesBtn.setOnClickListener {
            if (TextUtils.isEmpty(etComment!!.text.toString())) {
                onErrorListener("Please enter Comment")
            } else {
                dialog.dismiss()
                addTicket(
                    etComment!!.text.toString()
                )

            }
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun addTicket(comment: String) {

        showLoading("Please wait")
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        if (userId != null && token != null) {
            val call = NetworkService.apiInterface.addComment(
                "Bearer $token",
                "mobile",
                comment,
                id!!
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    val stringResponse = response.body()?.string()
                    val jsonObj = JSONObject(stringResponse)
                    val success = jsonObj!!.getString("success")
                    if (success != null && success.toString().equals("true")) {
                        closeLoading()
                        getCommentList()
                    } else {
                        Toast.makeText(
                            this@CommentsActivity,
                            "Some error. Try again.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }


            })
        }

    }

    private fun getCommentList() {

        if (Utils.isOnline(this)) {
            showLoading("Fetching data...")
            commentData!!.clear()
            val call = NetworkService.apiInterface.getComments(
                "Bearer $token",
                "mobile",
                id!!
            )

            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    closeLoading()
                    Log.v("DEBUG : ", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    closeLoading()
                    if (response != null) {
                        val stringResponse = response.body()?.string()
                        val jsonObj = JSONObject(stringResponse)
                        val success = jsonObj!!.getString("success")
                        if (success != null && success.toString().equals("true")) {
                            setData(jsonObj)
                        } else {
                            Toast.makeText(
                                this@CommentsActivity,
                                jsonObj!!.getString("message"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            this@CommentsActivity,
                            "Something went wrong",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

            })

        }

    }

    private fun setData(jsonObj: JSONObject) {
        val jObj = jsonObj.getJSONObject("data")
        val dlist = jObj.getJSONArray("data")
        if(dlist.length()>0) {
            noDataText!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE
        for (i in 0 until dlist.length()) {
            val data = dlist.getJSONObject(i)

            commentData!!.add(
                CommentData(
                    data!!.getInt("ticket_msg_id"),
                    data!!.getInt("ticket_id"),
                    data!!.getString("message"),
                    data!!.getString("from_id"),
                    data!!.getString("to_id"),
                    data!!.getString("created_date"),
                    data!!.getString("from_name"),
                    data!!.getString("from_usertype"),
                    data!!.getString("to_name"),
                    data!!.getString("from_userimage")

                )
            )


            //adding some dummy data to the list

            commentAdapter = CommentsAdapter(this@CommentsActivity, commentData!!)
            //now adding the adapter to recyclerview
            recyclerView?.adapter = commentAdapter
        }
        }
        else{
            noDataText!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
        }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()
    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onPositionClick(adapterPosition: Int) {
        TODO("Not yet implemented")
    }

}