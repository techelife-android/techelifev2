package com.techesolutions.view.adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.data.remote.model.nextclouddashboard.NextCloudDataModel
import com.techesolutions.utils.Constants
import com.techesolutions.view.activity.DashboardActivity
import com.techesolutions.view.activity.nextcloud.assigneddevices.DevicesActivity
import com.techesolutions.view.activity.nextcloud.cameras.CameraActivity
import com.techesolutions.view.activity.nextcloud.contacts.ContactActivity
import com.techesolutions.view.activity.nextcloud.events.EventsActivity
import com.techesolutions.view.activity.nextcloud.files.FilesActivity
import com.techesolutions.view.activity.nextcloud.iot.IOTDoorReadingActivity
import com.techesolutions.view.activity.nextcloud.iot.IOTReadingActivity
import com.techesolutions.view.activity.nextcloud.notes.NotesCategoryActivity
import com.techesolutions.view.activity.nextcloud.support.SupportActivity
import com.techesolutions.view.activity.nextcloud.task.TaskActivity
import com.techesolutions.view.activity.nextcloud.task.TaskFoldersActivity
import com.techesolutions.view.activity.nextcloud.userinfo.UserInfoActivity
import com.techesolutions.view.activity.vitaldevices.TecheWatchActivity
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class NextCloudDashboardAdapter(
    context: Context,
    val userList: ArrayList<NextCloudDataModel>?
   ) : RecyclerView.Adapter<NextCloudDashboardAdapter.ViewHolder>() {
    var activity:Context = context
    var formatDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val now = Date()
    var dateValue = formatDate.format(now)

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NextCloudDashboardAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(
            R.layout.nextclod_dashboard_items,
            parent,
            false
        )
        return ViewHolder(v, activity, dateValue)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NextCloudDashboardAdapter.ViewHolder, position: Int) {
        userList?.get(position)?.let { holder.bindItems(it, activity) }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList?.size!!
    }

    //the class is hodling the list view
    class ViewHolder(
        itemView: View,
        context: Context,
        val dateValue: String
    ) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(vital: NextCloudDataModel, context: Context) {
            val textViewName = itemView.findViewById(R.id.activityName) as TextView
            val image = itemView.findViewById(R.id.activity_icon) as ImageView
            val progressDialog = CustomProgressDialog()
            textViewName.text = vital.name
            val longDescription = SpannableStringBuilder()
            var languageData = context.getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
                Constants.LANGUAGE_API_DATA,
                ""
            )
            var vitalUnitLabels: JSONObject? = null
            try {
                val jsonObject = JSONObject(languageData)
                val jsonArray = jsonObject.optJSONArray("config")
                for (i in 0 until jsonArray.length()) {
                    val jsonObject = jsonArray.getJSONObject(i)
                    val name = jsonObject.optString("name")
                    if (name.equals("dashboard")) {
                        vitalUnitLabels = jsonObject.getJSONObject("labels")
                    }
                }
            } catch (e: Exception) {
            }
            if (vital.label.equals("Vitals"))
                image.setImageResource(R.drawable.vitals)

            if (vital.label.equals("files"))
                image.setImageResource(R.drawable.file)

            if (vital.label.equals("notes"))
                image.setImageResource(R.drawable.notes)

            if (vital.label.equals("contacts"))
                image.setImageResource(R.drawable.contacts)

            if (vital.label.equals("events"))
                image.setImageResource(R.drawable.events)

            if (vital.label.equals("chat"))
                image.setImageResource(R.drawable.chat)

            if (vital.label.equals("task"))
                image.setImageResource(R.drawable.ic_tasks)

            if (vital.label.equals("iotDashbord"))
                image.setImageResource(R.drawable.ic_iot)

            if (vital.label.equals("camera"))
                image.setImageResource(R.drawable.ic_cameras)

            if (vital.label.equals("devices"))
                image.setImageResource(R.drawable.ic_assigned)

            if (vital.label.equals("savecred"))
                image.setImageResource(R.drawable.ic_save_user_info)

            if (vital.label.equals("support"))
                image.setImageResource(R.drawable.support)

            itemView.setOnClickListener(View.OnClickListener {
                if (vital.label.equals("Vitals")) {
                    val intent =
                        Intent(context, DashboardActivity::class.java)
                    intent.putExtra("from", "nextdashboard")
                    context.startActivity(intent)
                }
                if (vital.label.equals("notes")) {
                    val intent =
                        Intent(context, NotesCategoryActivity::class.java)
                    intent.putExtra("from", "nextdashboard")
                    context.startActivity(intent)
                }
                if (vital.label.equals("files")) {
                    val intent =
                        Intent(context, FilesActivity::class.java)
                    intent.putExtra("from", "nextdashboard")
                    context.startActivity(intent)
                }
                if (vital.label.equals("contacts")) {
                val previewIntent: Intent = Intent(context, ContactActivity::class.java)
                context.startActivity(previewIntent)
            }
                if (vital.label.equals("events")) {
                    val intent: Intent = Intent(context, TaskFoldersActivity::class.java)
                    intent.putExtra("from", "events")
                    context.startActivity(intent)
               /* val previewIntent: Intent = Intent(context, EventsActivity::class.java)
                context.startActivity(previewIntent)*/
            }
                if (vital.label.equals("task")) {
                    val intent: Intent = Intent(context, TaskFoldersActivity::class.java)
                    intent.putExtra("from", "task")
                    context.startActivity(intent)
                }
                if (vital.label.equals("camera")) {
                    val intent: Intent = Intent(context, CameraActivity::class.java)
                    context.startActivity(intent)
                }
                if (vital.label.equals("savecred")) {
                    val intent: Intent = Intent(context, UserInfoActivity::class.java)
                    context.startActivity(intent)
                }
                if (vital.label.equals("devices")) {
                    val intent: Intent = Intent(context, DevicesActivity::class.java)
                    context.startActivity(intent)
                }
                if (vital.label.equals("support")) {
                    val intent: Intent = Intent(context, SupportActivity::class.java)
                    context.startActivity(intent)
                }
                if (vital.label.equals("iotDashbord")) {
                    openDialog(context)
                }
            })
        }

        private fun openDialog(context: Context) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(true)
            dialog.setContentView(R.layout.iot_dialog)

            val doorReading= dialog.findViewById(R.id.doorReading) as TextView
            val iotReading= dialog.findViewById(R.id.iotReading) as TextView
            iotReading.setOnClickListener {
                var intent = Intent(context, IOTReadingActivity::class.java)
                context.startActivity(intent)
                dialog.dismiss()
            }

            doorReading.setOnClickListener {
                var intent = Intent(context, IOTDoorReadingActivity::class.java)
                context.startActivity(intent)
                dialog.dismiss()
            }

            dialog.show()
        }

    }

}