package com.techesolutions.view.activity.login


import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.graphics.Typeface
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import android.text.format.Formatter
import android.text.method.PasswordTransformationMethod
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.MySharedPreference
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.DashboardActivity
import com.techesolutions.view.activity.TimezoneSelectionActivity
import com.techesolutions.view.activity.nextcloud.NextCloudDashboardActivity
import com.techesolutions.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.activity_device_instruction_manual.view.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_header.*
import org.joda.time.DateTimeUtils
import org.joda.time.DateTimeZone
import org.json.JSONObject
import java.util.*


/**
 * Created by Neelam on 21-12-2020.
 */
class LoginActivity : AppCompatActivity(), View.OnClickListener {
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }
    private var loggedIn = false
    private var nextCloud = ""
    var showPassword: Boolean=false
    var userNameET: EditText? = null
    var passwordET: EditText? = null
    var emailLabel: TextView? = null
    var passwordLabel: TextView? = null
    var welcomeLabel: TextView? = null
    var nextBtn: TextView? = null
    var forgotPasswordTV: TextView? = null
    var note: TextView? = null
    var loginViewModel: LoginViewModel?=null
    private var languageData: String? = null
    var jsonValidations: JSONObject? = null
    private val progressDialog = CustomProgressDialog()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_login)
        overridePendingTransition(R.anim.act_pull_in_right, R.anim.act_push_out_left)
        back.visibility = View.GONE
        headerTitle.visibility = View.GONE
        forgotPasswordTV = findViewById<View>(R.id.forgot_pw) as TextView
        forgotPasswordTV?.setOnClickListener(this)
        userNameET = findViewById<View>(R.id.etEmail) as EditText
        passwordET = findViewById<View>(R.id.etPassword) as EditText
        nextBtn = findViewById<View>(R.id.btSignin) as TextView
        nextBtn!!.setOnClickListener(this)
        showPw!!.setOnClickListener(this)
        note = findViewById<View>(R.id.note) as TextView
        emailLabel = findViewById<View>(R.id.loginText) as TextView
        passwordLabel = findViewById<View>(R.id.loginPassword) as TextView
        welcomeLabel = findViewById<View>(R.id.heading) as TextView


        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        nextCloud = shared.getString(Constants.KEY_NEXT_CLOUD, "").toString()
        loggedIn = shared.getBoolean(Constants.KEY_LOGGEDIN, false)
        languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("login")) {
                    val jsonObjectLabels = jsonObject.getJSONObject("labels")
                    val longDescription = SpannableStringBuilder()
                    longDescription.append(jsonObjectLabels.getString("login_note").toString())
                    val start = longDescription.length
                    longDescription.append(
                        " " + jsonObjectLabels.getString("login_note_number").toString()
                    )
                    longDescription.setSpan(
                        ForegroundColorSpan(-0xff0000),
                        start,
                        longDescription.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    longDescription.setSpan(
                        StyleSpan(Typeface.BOLD),
                        start,
                        longDescription.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    note?.text = longDescription
                    emailLabel?.text = jsonObjectLabels.getString("email_login_id").toString()
                    passwordLabel?.text = jsonObjectLabels.getString("password").toString()
                    welcomeLabel?.text = jsonObjectLabels.getString("welcome_to_teche").toString()
                    val forgotPass = jsonObjectLabels.getString("forgot_password").toString()
                    val forgotStr = SpannableString(forgotPass)
                    forgotStr.setSpan(UnderlineSpan(), 0, forgotPass.length, 0)
                    forgotPasswordTV?.text = forgotStr
                    nextBtn?.text = jsonObjectLabels.getString("signin").toString()
                    jsonValidations = jsonObjectLabels.getJSONObject("validations")
                    Log.v("DEBUG : ", jsonObjectLabels.toString())

                }
            }

        } catch (e: Exception) {
        }
        if (loggedIn) {
            val username: String= shared.getString(Constants.KEY_USERNAME, "").toString()
            val password: String= shared.getString(Constants.KEY_PASSWORD, "").toString()
            userNameET!!.setText(username)
            passwordET!!.setText(password)
            //doLogin( username,password)
        }
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED  || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this, android.Manifest.permission.READ_CONTACTS
            )
            != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.BLUETOOTH_ADMIN
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@LoginActivity, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CALL_PHONE,
                    android.Manifest.permission.READ_CONTACTS,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.BLUETOOTH,
                    Manifest.permission.BLUETOOTH_ADMIN,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                MY_PERMISSIONS_REQUEST
            )
        }

    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btSignin -> {
                if (Utils.isOnline(this@LoginActivity)) {
                    setValidation()
                }
            }
            R.id.forgot_pw -> startActivity(
                Intent(
                    this@LoginActivity,
                    ForgotPasswordActivity::class.java
                )
            )
            R.id.showPw -> {
                showPassword = !showPassword
                if (showPassword == true) {
                    etPassword.transformationMethod = PasswordTransformationMethod()
                    showPw.setBackgroundResource(R.drawable.ic_pass_open)
                }
                else{
                    etPassword.transformationMethod = null
                    showPw.setBackgroundResource(R.drawable.ic_pass_hide)
                }
            }
        }
    }

    private fun setValidation() {

        if (TextUtils.isEmpty(userNameET!!.text.toString())) {
            onErrorListener(jsonValidations!!.getString("email_not_empty").toString())
        } else if (TextUtils.isEmpty(passwordET!!.text.toString())) {
            onErrorListener(jsonValidations!!.getString("passnotempty").toString())
        } else {
            //Toast.makeText(this, "Android version " +version + ","+ manufacturer+" "+model , Toast.LENGTH_LONG).show()
                doLogin( userNameET!!.text.toString(),
                    passwordET!!.text.toString())

        }

    }
    private fun doLogin(userName: String, password: String) {
        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        val dz = DateTimeZone.forID(TimeZone.getDefault().id)
        val timezone = dz.getNameKey(DateTimeUtils.currentTimeMillis())
        Log.i("timezone", timezone)
        val wifiManager: WifiManager = applicationContext.getSystemService(WIFI_SERVICE) as WifiManager
        val ipAddress: String = Formatter.formatIpAddress(
            wifiManager.getConnectionInfo().getIpAddress()
        )

        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        val id=Build.ID
        val version = Build.VERSION.SDK_INT
        val versionRelease = Build.VERSION.RELEASE
        val currentLanguage =  getSharedPreferences("TechePrefs", Context.MODE_PRIVATE).getString(
            Constants.KEY_LANGUAGE,
            ""
        ).toString()
        showLoading("Please wait...")
        loginViewModel!!.getUser(
            userName,
            password,
            "mobile",
            timezone,
            id,
            "Android version " + versionRelease + "," + manufacturer + " " + model,
            currentLanguage,
            "android",
            ipAddress
        )!!
            .observe(this, Observer { loginResponse ->
                if (loginResponse != null) {
                    val success = loginResponse.success;
                    Log.i("DEBUG : ", loginResponse.toString())
                    val token: String? = loginResponse.data?.token

                    val nextCloudEnable: String? = loginResponse.data?.nextcloud_access
                    // val nextCloudEnable: Boolean? = true
                    val userId: String? = loginResponse.data?.id
                    val verifiedTimezone: String? = loginResponse.data?.verifytimezone
                    // val jsonObject = loginResponse.data?.localize?.settings
                    //val settings = jsonObject.optJSONObject("settings")
                    val timezone: String? = loginResponse.data?.user?.timezone
                    val nextcloud_url: String? = loginResponse.data?.user?.nextcloud_url
                    val nextcloud_login: String? = loginResponse.data?.user?.nextcloud_username
                    val nextcloud_pw: String? = loginResponse.data?.user?.nextcloud_password
                    val unique_key: String? = loginResponse.data?.user?.unique_key
                    val dateformat: String? = loginResponse.data?.localize?.settings?.dateformat
                    val timeformat: String? = loginResponse.data?.localize?.settings?.timeformat
                    val weightin: String? = loginResponse.data?.localize?.settings?.weightin
                    val temperature_unit: String? =
                        loginResponse.data?.localize?.settings?.temperature_unit
                    val affiliationtimezone: String? =
                        loginResponse.data?.localize?.settings?.affiliationtimezone
                    val gson = Gson()
                    val timezones: String = gson.toJson(loginResponse.data?.localize?.timezones)
                    val nextcloud_features: String =
                        gson.toJson(loginResponse.data?.nextcloud_features)
                    if (success == true) {
                        MySharedPreference.setBooleanPreference(
                            this,
                            Constants.KEY_LOGGEDIN,
                            true
                        )
                            MySharedPreference.setPreference(
                                this, Constants.KEY_USERNAME,
                                userName
                            )


                            MySharedPreference.setPreference(
                                this, Constants.KEY_PASSWORD,
                                password
                            )

                        token?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_TOKEN,
                                it
                            )
                        }
                        userId?.let {
                            MySharedPreference.setPreference(
                                this, Constants.Login_User_ID,
                                it
                            )
                        }
                        nextCloudEnable?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_NEXT_CLOUD,
                                it
                            )
                        }
                        nextcloud_url?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_NEXT_CLOUD_URL,
                                it
                            )
                        }
                        nextcloud_login?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_NEXT_CLOUD_USER,
                                it
                            )
                        }
                        nextcloud_pw?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_NEXT_CLOUD_PASSWORD,
                                it
                            )
                        }
                        unique_key?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_UNIQUE,
                                it
                            )
                        }
                        verifiedTimezone?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_TIME_ZONE_VERIFIED,
                                it
                            )
                        }

                        timezones?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_TIME_ZONES,
                                it
                            )
                        }
                        nextcloud_features?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_NEXT_CLOUD_FEATURES,
                                it
                            )
                        }
                        timezone?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_USER_TIME_ZONE,
                                it
                            )
                        }
                        dateformat?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_USER_DATE_FORMAT,
                                it

                            )
                        }
                        timeformat?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_USER_TIME_FORMAT,
                                it
                            )
                        }
                        affiliationtimezone?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_USER_AFFILIATION_TIMEZONE,
                                it
                            )
                        }
                        temperature_unit?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_USER_TEMP_UNIT,
                                it
                            )
                        }
                        weightin?.let {
                            MySharedPreference.setPreference(
                                this, Constants.KEY_USER_WEIGHT_UNIT,
                                it
                            )
                        }

                        verifiedTimezone?.let { goToDashboard(it, nextCloudEnable) }
                    } else
                        Toast.makeText(this, loginResponse.message, Toast.LENGTH_SHORT).show()
                } else
                    Toast.makeText(this, "Email or password is wrong", Toast.LENGTH_SHORT)
                        .show()
            })
    }

        private fun goToDashboard(timezoneVerified: String, nextCloudEnable: String?) {
            Log.d("timezoneVerified",timezoneVerified)
            Log.d("nextCloudEnable",nextCloudEnable)
        closeLoading()
        if(timezoneVerified.equals("1"))
        {
            if (nextCloudEnable.equals("1")) {
                val intent = Intent(this@LoginActivity, NextCloudDashboardActivity::class.java)
                intent.putExtra("from", "splash1")
                startActivity(intent)
                finish();
            }
            else {
                val dashboardIntent = Intent(this@LoginActivity, DashboardActivity::class.java)
                dashboardIntent.putExtra("from", "login")
                startActivity(dashboardIntent)
                finish()
            }
        }
        else {
            val dashboardIntent = Intent(this@LoginActivity, TimezoneSelectionActivity::class.java)
            startActivity(dashboardIntent)
            finish()
        }
      /*  val dashboardIntent = Intent(this@LoginActivity, DashboardActivity::class.java)
        dashboardIntent.putExtra("from", "login")
        startActivity(dashboardIntent)
        finish()*/
    }

    companion object {
        private const val MY_PERMISSIONS_REQUEST = 1
        private const val KEY_NAME = "myTECHE Life"
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onResume() {
        super.onResume()
        if(loggedIn)
        {
            //Toast.makeText(this, "Android version " +version + ","+ manufacturer+" "+model , Toast.LENGTH_LONG).show()
            doLogin( userNameET!!.text.toString(),
                passwordET!!.text.toString())

        }

    }

}