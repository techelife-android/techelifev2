package com.techesolutions.data.remote.model.login

import com.google.gson.annotations.SerializedName

data class LoginResponse(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null



//	@field:SerializedName("error")
//   val error: String? = null

)

data class Data(

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("verifytimezone")
	val verifytimezone: String? = null,

	@field:SerializedName("nextcloud_access")
	val nextcloud_access: String? = null,

	@field:SerializedName("error")
    val error: String? = null,

	@field:SerializedName("localize")
	val localize: Localize? = null,

	@field:SerializedName("user")
	val user: User? = null,

	@field:SerializedName("nextcloud_features")
	val nextcloud_features: List<NextcloudFeaturesItem?>? = null
)
data class User(

	@field:SerializedName("timezone")
	val timezone: String? = null,

	@field:SerializedName("nextcloud_url")
	val nextcloud_url: String? = null,

	@field:SerializedName("nextcloud_username")
	val nextcloud_username: String? = null,

	@field:SerializedName("nextcloud_password")
	val nextcloud_password: String? = null,

	@field:SerializedName("unique_key")
	val unique_key: String? = null

)
data class Localize(

	@field:SerializedName("settings")
	val settings: Settings? = null,

	@field:SerializedName("timezones")
	val timezones: List<TimezonesItem?>? = null

)
data class Settings(

	@field:SerializedName("affiliationtimezone")
	val affiliationtimezone: String? = null,

	@field:SerializedName("dateformat")
	val dateformat: String? = null,

	@field:SerializedName("timeformat")
	val timeformat: String? = null,

	@field:SerializedName("weightin")
	val weightin: String? = null,

	@field:SerializedName("temperature_unit")
	val temperature_unit: String? = null
	)

data class TimezonesItem(

		@field:SerializedName("timezone")
		val timezone: String? = null,

		@field:SerializedName("timezone_text")
		val timezone_text: String? = null,

		@field:SerializedName("country")
		val country: String? = null
)

data class NextcloudFeaturesItem(

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("label")
		val label: String? = null,

		@field:SerializedName("enable")
		val enable: String? = null

)