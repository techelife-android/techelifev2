package com.techesolutions.data.remote.model.forgetpassword

import com.google.gson.annotations.SerializedName

data class CodeVerificationResponse(

	@field:SerializedName("success")
	val success: Boolean? = null,

	@field:SerializedName("message")
	val message: String? = null
)