package com.techesolutions.view.activity.vitaldevices

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.MySharedPreference
import com.techesolutions.utils.Utils
import kotlinx.android.synthetic.main.activity_device_instruction_manual.*
import org.json.JSONObject

class DeviceInstructionManualActivity : AppCompatActivity(), View.OnClickListener {
    var title: String? = null
    var vitalName: String? = null
    var subVitalName: String? = null
    var unit1: String? = null
    var unit2: String? = null
    var vitalUnitLabels: JSONObject? = null
    var vitalNameLabels: JSONObject? = null
    lateinit var dontShow: CheckBox

    private val progressDialog = CustomProgressDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        Utils.statusBarSetup(this)
        setContentView(R.layout.activity_device_instruction_manual)
        Utils.setDimensions(this)
       // ButterKnife.bind(this)
        val i: Intent = getIntent()
        title = i.getStringExtra("title")
        vitalName = i.getStringExtra("vitalName")
        subVitalName = i.getStringExtra("subVitalName")
        unit1 = i.getStringExtra("unitName1")
        unit2 = i.getStringExtra("unitName2")
//        val deviceModel = i.getStringExtra("deviceModel")
//        val brand = i.getStringExtra("brand")
        setUpHeader(title!!)
        val shared =
            getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val token: String? = shared.getString(Constants.KEY_TOKEN, null)
        val userId: String? = shared.getString(Constants.Login_User_ID, null)
        var languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if (name.equals("dashboard")) {
                    vitalUnitLabels = jsonObject.getJSONObject("labels")
                }
                if (name.equals("vitals")) {
                    vitalNameLabels = jsonObject.getJSONObject("labels")
                }
            }
        } catch (e: Exception) {
        }

        val instructionLay1 =findViewById<View>(R.id.instructionLay1) as LinearLayout
        val deviceNameHeader1 =findViewById<View>(R.id.deviceNameHeader) as TextView
        val instructionText1 =findViewById<View>(R.id.instructionText1) as TextView
        val instructionPoints1 =findViewById<View>(R.id.instructionPoints1) as TextView
        val letstart =findViewById<View>(R.id.letstart) as TextView
        val deviceImage =findViewById<View>(R.id.deviceImage) as ImageView
        val deviceImage1 =findViewById<View>(R.id.deviceImage1) as ImageView
        if (vitalName!!.equals("Activities")) {
            deviceImage!!.setImageResource(R.drawable.smartwatch)
            letstart!!.text=vitalNameLabels!!.getString("startActivityreading")!!
            instructionPoints1!!.text=vitalNameLabels!!.getString("activitystep")!!
        }
        else if (vitalName!!.equals("Blood_Pressure")) {
                deviceImage!!.setImageResource(R.drawable.bp)
            letstart!!.text=vitalNameLabels!!.getString("startBPreading")!!
            instructionPoints1!!.text=vitalNameLabels!!.getString("BPStep")!!

            } else if (vitalName!!.equals("Pulse_Oximetry")) {
                deviceImage!!.setImageResource(R.drawable.po)
            letstart!!.text=vitalNameLabels!!.getString("startPulseratereading")!!
            instructionPoints1!!.text=vitalNameLabels!!.getString("PulserateStep")!!

        }
        else if (vitalName!!.equals("Heart_Rate")) {
            deviceImage!!.setImageResource(R.drawable.po)
            letstart!!.text=vitalNameLabels!!.getString("startHeartratereading")!!
            instructionPoints1!!.text=vitalNameLabels!!.getString("Heartratestep")!!

        }
        else if (vitalName!!.equals("Blood_Sugar")) {
                deviceImage!!.setImageResource(R.drawable.glucometer)
            letstart!!.text=vitalNameLabels!!.getString("startBloodsugarreading")!!
            instructionPoints1!!.text=vitalNameLabels!!.getString("Bloodsugarstep")!!

        }
        else if(vitalName!!.equals("Weight")) {
                deviceImage!!.setImageResource(R.drawable.wscale)
            letstart!!.text=vitalNameLabels!!.getString("startbodyweightreading")!!
            instructionPoints1!!.text=vitalNameLabels!!.getString("bodyweightstep")!!

        }
        else if (vitalName!!.equals("Temperature")) {
            deviceImage!!.setImageResource(R.drawable.thermameter)
            letstart!!.text=vitalNameLabels!!.getString("startTempreading")!!
            instructionPoints1!!.text=vitalNameLabels!!.getString("tempstep")!!

        }

    }

    open fun setUpHeader(title: String) {
        val headerTitleTV =
            findViewById<View>(R.id.headerTitle) as TextView
        if (title.contains("Temperature")) {
            headerTitleTV.text = "Thermometer"
        } else if (title.contains("Weight")) {
            headerTitleTV.text = "Weight Scale"
        }
        else if (title.contains("Blood Sugar")) {
            headerTitleTV.text = "Glucometer"
        }
        else if (title.contains("Activities")) {
            headerTitleTV.text = "Activity Watch"
        }else {
            headerTitleTV.text = title
        }

        val backBtn =
            findViewById<View>(R.id.back) as ImageView
        backBtn.visibility = View.VISIBLE
        backBtn.setOnClickListener { finish() }
        val btDone =
            findViewById<View>(R.id.doneBT) as RelativeLayout
        btDone?.setOnClickListener(this)

    }
    override fun onClick(v: View?) {
        if (checkBox.isChecked) {
            MySharedPreference.setBooleanPreference(this, Constants.KEY_DONT_SHOW, true)
        }
        if (vitalName!!.equals("Temperature")) {
            var  i = Intent(this, TecheThermometerActivity::class.java)
            i.putExtra("title", title)
            i.putExtra("vitalName", vitalName)
            i.putExtra("unitName1", unit1)
            i.putExtra("unitName2", unit2)
            startActivity(i)
        }
       else if(vitalName!!.equals("Activities"))
        {
            var i = Intent(this, TecheWatchActivity::class.java)
            i.putExtra("title", title)
            i.putExtra("vitalName", vitalName)
            i.putExtra("unitName1", unit1)
            i.putExtra("unitName2", unit2)
            i.putExtra("subVitalName", subVitalName)
            startActivity(i)
        }
        else if(vitalName!!.equals("Pulse_Oximetry"))
        {
            var   i = Intent(this, TechePOActivity::class.java)
            i.putExtra("title", title)
            i.putExtra("vitalName", vitalName)
            i.putExtra("unitName1", unit1)
            i.putExtra("unitName2", unit2)
            i.putExtra("subVitalName", subVitalName)
            startActivity(i)
        }
        else if(vitalName!!.equals("Blood_Pressure"))
        {
            var   i = Intent(this, TecheBPActivity::class.java)
            i.putExtra("title", title)
            i.putExtra("vitalName", vitalName)
            i.putExtra("unitName1", unit1)
            i.putExtra("unitName2", unit2)
            i.putExtra("subVitalName", subVitalName)
            startActivity(i)
        }
        else if(vitalName!!.equals("Blood_Sugar"))
        {
            var   i = Intent(this, TecheBSActivity::class.java)
            i.putExtra("title", title)
            i.putExtra("vitalName", vitalName)
            i.putExtra("unitName1", unit1)
            i.putExtra("unitName2", unit2)
            startActivity(i)
        }
        else if(vitalName!!.equals("Weight")){
            var  i = Intent(this, TecheWSActivity::class.java)
            i.putExtra("title", title)
            i.putExtra("vitalName", vitalName)
            i.putExtra("unitName1", unit1)
            i.putExtra("unitName2", unit2)
            startActivity(i)
        }

    }

/*
    private fun startDiscovery() {
        progressDialog?.show(this,"Scanning Device")

        // Show progress dialog without Title
        progressDialog?.show(this)
        Handler(Looper.getMainLooper()).postDelayed({
            var i: Intent? = null
            i = Intent(this, ThermomaterReadingActivity::class.java)
            startActivity(i)
            finish()
            progressDialog?.dialog.dismiss()
        }, 4000)
    }*/
}