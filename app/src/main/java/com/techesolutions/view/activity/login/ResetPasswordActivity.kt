package com.techesolutions.view.activity.login

import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.viewmodel.ResetPasswordViewModel
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.layout_header.*
import org.json.JSONObject

/**
 * Created by Neelam on 01-01-2021.
 */
class ResetPasswordActivity : AppCompatActivity(), View.OnClickListener {
    var btConfirm: TextView? = null
    var newPassword: EditText? = null
    var confirmNewPassword: EditText? = null
    var back:ImageView?=null
    var reset_password_note: TextView? = null
    var heading: TextView? = null
    var loginPassword: TextView? = null
    var confirmPassword: TextView? = null
    lateinit var resetPasswordViewModel: ResetPasswordViewModel
    private val progressDialog = CustomProgressDialog()
    var hashcode: String = ""
    private var languageData: String? = null
    var jsonValidations: JSONObject?=null
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val window = window
        window.setFormat(PixelFormat.RGBA_8888)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_reset_password)
        headerTitle.visibility = View.GONE
        resetPasswordViewModel = ViewModelProvider(this).get(ResetPasswordViewModel::class.java)
        hashcode = intent.getStringExtra("hashcode")
        btConfirm = findViewById<View>(R.id.btSubmit) as TextView
        reset_password_note = findViewById<View>(R.id.reset_password_note) as TextView
        heading = findViewById<View>(R.id.heading) as TextView
        loginPassword = findViewById<View>(R.id.loginPassword) as TextView
        confirmPassword = findViewById<View>(R.id.confirmPassword) as TextView
        newPassword = findViewById<View>(R.id.etPassword) as EditText
        confirmNewPassword = findViewById<View>(R.id.etConfirmPassword) as EditText
        back=findViewById<View>(R.id.back) as ImageView
        back?.setVisibility(View.VISIBLE)
        back?.setOnClickListener(this)
        btConfirm?.setOnClickListener(this)
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        languageData = shared.getString(Constants.LANGUAGE_API_DATA, "")
        try {
            val jsonObject = JSONObject(languageData)
            val jsonArray = jsonObject.optJSONArray("config")
            for (i in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(i)
                val name = jsonObject.optString("name")
                if(name.equals("resetPassword")) {
                    val jsonObjectLabels = jsonObject.getJSONObject("labels")
                    reset_password_note?.text=jsonObjectLabels.getString("set_new_password_note").toString()
                    loginPassword?.text=jsonObjectLabels.getString("new_password").toString()
                    confirmPassword?.text=jsonObjectLabels.getString("confirm_new_password").toString()
                    heading?.text=jsonObjectLabels.getString("reset_password").toString()
                    btSubmit?.text=jsonObjectLabels.getString("submit").toString()
                    jsonValidations=jsonObjectLabels.getJSONObject("validations")
                    Log.v("DEBUG : ", jsonObjectLabels.toString())

                }
            }

        } catch (e: Exception) {
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btSubmit -> {
                setValidation()
            }
            R.id.back -> {
                finish()
            }
        }
    }

    private fun setValidation() {
        if (TextUtils.isEmpty(newPassword!!.text.toString())) {
            if(jsonValidations!=null && jsonValidations!!.length()>0)
                onErrorListener(jsonValidations!!.getString("email_not_empty").toString())
            else
            onErrorListener(getString(R.string.enter_new_password))
        }
        if (TextUtils.isEmpty(confirmNewPassword!!.text.toString())) {
            if(jsonValidations!=null && jsonValidations!!.length()>0)
                onErrorListener(jsonValidations!!.getString("email_not_empty").toString())
            else
            onErrorListener(getString(R.string.confirm_password))
        }
        else {
            if (Utils.isOnline(this@ResetPasswordActivity)) {
                showLoading("Please wait")
                resetPasswordViewModel.resetPassword(hashcode,"mobile",newPassword!!.text.toString(),confirmNewPassword!!.text.toString())!!
                    .observe(this, Observer { baseResponse ->

                        val success = baseResponse.success;
                        if (success == true) {
                            closeLoading()
                            startActivity(
                                Intent(
                                    this@ResetPasswordActivity,
                                    LoginActivity::class.java
                                )
                            )
                            finish()
                        } else {
                            closeLoading()
                            Toast.makeText(this, baseResponse.message, Toast.LENGTH_SHORT).show()
                        }
                    })
            }

        }
    }

    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }
    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

}