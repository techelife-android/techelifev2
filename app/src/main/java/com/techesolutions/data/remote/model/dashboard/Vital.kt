package com.techesolutions.data.remote.model.dashboard

data class Vital(
    val vitalName : String,
    val Title : String,
    val data1 : String,
    val data1val : String,
    val data2 : String,
    val data2val : String,
    val dateandtime : String,
    val takenfrom : String,
    val deviceType : String,
    val deviceId : String
)