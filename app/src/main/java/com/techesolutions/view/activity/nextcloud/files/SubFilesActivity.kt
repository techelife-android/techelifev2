package com.techesolutions.view.activity.nextcloud.files

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.network.OnDatatransferProgressListener
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.DownloadFileRemoteOperation
import com.owncloud.android.lib.resources.files.ReadFolderRemoteOperation
import com.owncloud.android.lib.resources.files.RemoveFileRemoteOperation
import com.owncloud.android.lib.resources.files.UploadFileRemoteOperation
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.FilePath.getPath
import com.techesolutions.view.activity.nextcloud.files.FilesActivity.Companion.getRootDirPath
import com.techesolutions.view.activity.nextcloud.files.contacts.FileContactActivity
import com.techesolutions.view.activity.nextcloud.files.preview.*
import com.techesolutions.view.callbacks.FileClickListener
import ezvcard.Ezvcard
import ezvcard.VCard
import ezvcard.property.Telephone
import java.io.File
import java.util.*
class SubFilesActivity() : Activity(), OnRemoteOperationListener, OnDatatransferProgressListener,
    FileClickListener {
    var list: ListView? = null
    var files: List<RemoteFile>? = null
    private var fileUri: Uri? = null
    private var filePath: String? = null
    private var type: String? = null
    private var path: String? = null
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var mFilesAdapter: SubFileAdapter? = null
    private val progressDialog: CustomProgressDialog? = CustomProgressDialog()
    private var subFolder: String? = null
    private var folderpath: String? = null

    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actvity_file)
        showLoading("Please wait...")
        subFolder = intent.getStringExtra("path")
        folderpath = intent.getStringExtra("path")
        Log.i("TAG", "subFolder: $subFolder")
        if (subFolder!!.endsWith("/")) {
            subFolder = subFolder!!.substring(0, subFolder!!.lastIndexOf("/"))
            val files = subFolder!!.split("/".toRegex()).toTypedArray()
            val f = files[files.size - 1]
            Log.i("TAG", "files: $f")
            (findViewById<View>(R.id.headerTitle) as TextView).text = f
        } else {
            val file = subFolder!!.substring(subFolder!!.lastIndexOf("/") + 1, subFolder!!.length)
            (findViewById<View>(R.id.headerTitle) as TextView).text = file
            Log.i("TAG", "textfiles: $file")
        }
        mHandler = Handler()

        val shared = getSharedPreferences("TechePrefs", MODE_PRIVATE)
        val url = shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        val user = shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass = shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        Log.i("data", "$url, $user, $pass")
        var serverUri: Uri? = null
        if (url!!.endsWith("/")) {
            serverUri = Uri.parse(url.substring(0, url.length - 1))
        } else {
            serverUri = Uri.parse(url)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, false)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user,
                pass
            )
        )
        //  files=new ArrayList<>();
        list = (findViewById<View>(R.id.list_view) as ListView)
        mFilesAdapter = SubFileAdapter(this, R.layout.file_folder_items, this)
        list!!.adapter = mFilesAdapter
        startRefresh()
        findViewById<View>(R.id.upload).visibility = View.VISIBLE
        findViewById<View>(R.id.refreshFiles).visibility = View.VISIBLE
        findViewById<View>(R.id.upload).setOnClickListener {
            showUploadDialog()
        }
        findViewById<View>(R.id.refreshFiles).setOnClickListener {
            startRefresh()
        }
        findViewById<View>(R.id.back).setOnClickListener { finish() }
    }

    private fun showLoading(msg: String) {
        progressDialog!!.show(this, msg)
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog.dialog.isShowing) progressDialog.dialog.dismiss()
    }

    public override fun onDestroy() {
        /* File upFolder = new File(getCacheDir(), getString(R.string.upload_folder_path));
        File upFile = upFolder.listFiles()[0];
        upFile.delete();
        upFolder.delete();*/
        super.onDestroy()
    }

    private fun startRefresh() {
        if (progressDialog != null && progressDialog.dialog.isShowing) {
            progressDialog.dialog.dismiss()
        }
        showLoading("Please wait...")
        val refreshOperation = ReadFolderRemoteOperation(subFolder)
        refreshOperation.execute(mClient, this, mHandler)
    }

    private fun startUpload(fileUri: String?, type: String?) {
        showLoading("Please wait...")
        val fileToUpload = File(fileUri)
        Log.i("remotePath", fileToUpload.name)
        val remotePath = folderpath + fileToUpload.name
        Log.i("remotePath", remotePath)
        val mimeType = type
        val timeStampLong = System.currentTimeMillis() / 1000
        val timeStamp = timeStampLong.toString()
        val uploadOperation =
            UploadFileRemoteOperation(fileToUpload.absolutePath, remotePath, mimeType, timeStamp)
        uploadOperation.addDataTransferProgressListener(this) //addDatatransferProgressListener(this);
        uploadOperation.execute(mClient, this, mHandler)
    }

    private fun startRemoteDeletion(path: String, fileid: String) {
        showLoading("Deleting file")
        /*    File upFolder = new File(getCacheDir(), getString(R.string.upload_folder_path));
        File fileToUpload = upFolder.listFiles()[0];
        String remotePath = FileUtils.PATH_SEPARATOR + fileToUpload.getName();*/
        val removeOperation = RemoveFileRemoteOperation(path)
        removeOperation.execute(mClient, this, mHandler)
    }

    private fun startDownload(remotePath: String) {
        val downFolder = File(cacheDir, getString(R.string.download_folder_path))
        downFolder.mkdir()
        // File upFolder = new File(getCacheDir(), getString(R.string.upload_folder_path));
        //File fileToUpload = upFolder.listFiles()[0];
        //String remotePath = FileUtils.PATH_SEPARATOR + fileToUpload.getName();
        val downloadOperation = DownloadFileRemoteOperation(remotePath, downFolder.absolutePath)
        downloadOperation.addDatatransferProgressListener(this)
        downloadOperation.execute(mClient, this, mHandler)
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>
    ) {
        if (!result.isSuccess) {
            Log.e(LOG_TAG, result.logMessage, result.exception)
        } else if (operation is ReadFolderRemoteOperation) {
            onSuccessfulRefresh(operation, result)
        } else if (operation is UploadFileRemoteOperation) {
            onSuccessfulUpload(operation, result)
        } else {
            closeLoading()
            startRefresh()
        }
    }

    private fun onSuccessfulRefresh(
        operation: ReadFolderRemoteOperation,
        result: RemoteOperationResult<*>
    ) {
        mFilesAdapter!!.clear()
        val files: MutableList<RemoteFile> = ArrayList()
        for (obj: Any in result.data) {
            files.add(obj as RemoteFile)
        }
        if (files != null) {
            val it: Iterator<RemoteFile> = files.iterator()
            while (it.hasNext()) {
                mFilesAdapter!!.add(it.next())
            }
            mFilesAdapter!!.remove(mFilesAdapter!!.getItem(0))
        }
        closeLoading()
        mFilesAdapter!!.notifyDataSetChanged()
    }

    private fun onSuccessfulUpload(
        operation: UploadFileRemoteOperation,
        result: RemoteOperationResult<*>
    ) {
        closeLoading()
        startRefresh()
    }

    override fun onTransferProgress(
        progressRate: Long,
        totalTransferredSoFar: Long,
        totalToTransfer: Long,
        fileName: String
    ) {
        val percentage =
            (if (totalToTransfer > 0) totalTransferredSoFar * 100 / totalToTransfer else 0)
        val upload = fileName.contains(getString(R.string.upload_folder_path))
        Log.d(LOG_TAG, "progressRate $percentage")
        mHandler!!.post {
            val progressView: TextView? = null
            if (upload) {
                // progressView = findViewById(R.id.upload_progress);
            } else {
                //progressView = findViewById(R.id.download_progress);
            }
            if (progressView != null) {
                progressView.text = java.lang.Long.toString(percentage) + "%"
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        when (requestCode) {
            PICKFILE_RESULT_CODE -> if (resultCode == -1) {
                fileUri = data.data
                filePath = fileUri!!.path
                val cR = contentResolver
                val mime = MimeTypeMap.getSingleton()
                //String type = mime.getExtensionFromMimeType(cR.getType(fileUri));
                type = cR.getType((fileUri)!!)

                //tvItemPath.setText(filePath);
                Log.i("data", type)
                path = getPath(baseContext, (fileUri)!!)
                Log.i("filename ", path)
                // String docFilePath = getFileNameByUri(getActivity(), fileuri);
                // System.out.println("ConnectToDoctorFragment.onActivityResult " + docFilePath);
                //filePathToUpload = path;
                //startUpload(path,type);
            }
        }
    }

    override fun onPositionClick(position: Int) {
        val p = mFilesAdapter!!.getItem(position)
        Log.i("mime type", p!!.mimeType)
        if (p.mimeType.equals("DIR", ignoreCase = true)) {
            val intent = Intent(this, SubFilesActivity::class.java)
            intent.putExtra("path", p.remotePath)
            startActivity(intent)
        } else if (p.mimeType.equals(
                "video/mp4",
                ignoreCase = true
            ) || p.mimeType.equals(
                "video/mpeg",
                ignoreCase = true
            ) || p.mimeType.equals("image/gif", ignoreCase = true)|| p.remotePath.contains(".mov", ignoreCase = true)
        ) {
            //startDownload(p.remotePath);
            val previewIntent = Intent(this, VideoPreviewActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            // previewIntent.putExtra("EXTRA_FILE", (Parcelable) p);
            previewIntent.putExtra(EXTRA_START_POSITION, 0)
            previewIntent.putExtra(EXTRA_AUTOPLAY, true)
            startActivity(previewIntent)
        } else if (p.mimeType.equals(
                "image/png",
                ignoreCase = true
            ) || p.mimeType.equals(
                "image/jpeg",
                ignoreCase = true
            ) || p.mimeType.equals(
                "image/jpg",
                ignoreCase = true
            ) || p.mimeType.equals(
                "image/heic",
                ignoreCase = true
            ) || p.mimeType.equals("image/heif", ignoreCase = true)
        ) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PreviewImageActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        } else if (p.mimeType.equals("application/pdf", ignoreCase = true)) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PdfViewActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.equals("text/vcard", ignoreCase = true)) {
            val previewIntent = Intent(this, FileContactActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.equals("text/markdown", ignoreCase = true)||p.mimeType.equals("text/plain", ignoreCase = true)) {
            val previewIntent = Intent(this, PreviewDocumentActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        /*else if (p.mimeType.equals(
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                ignoreCase = true
            ) || p.mimeType.equals("application/vnd.oasis.opendocument.text", ignoreCase = true)
        ) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        } else if (p.mimeType.equals(
                "application/xhtml+xml",
                ignoreCase = true
            ) || p.mimeType.equals(
                "application/xml",
                ignoreCase = true
            ) || p.mimeType.equals(
                "text/xml",
                ignoreCase = true
            ) || p.mimeType.equals(
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                ignoreCase = true
            ) || p.mimeType.equals(
                "text/csv",
                ignoreCase = true
            ) || p.mimeType.equals("application/vnd.ms-excel", ignoreCase = true)
            || p.mimeType.equals("application/octet-stream", ignoreCase = true)
        ) {
            // startDownload(p.remotePath);
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.equals("application/x-rar-compressed",ignoreCase = true)||p.mimeType.equals("application/zip",ignoreCase = true)|| p.remotePath.contains(".zip") || p.remotePath.contains(".rar")|| p.remotePath.contains(".ai",ignoreCase = true)|| p.remotePath.contains(".psd",ignoreCase = true)|| p.remotePath.contains(".svg",ignoreCase = true)) {
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(FilesActivity.EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FilesActivity.FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else if (p.mimeType.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation", ignoreCase = true)|| p.remotePath.contains(".ppt") || p.remotePath.contains(".pptx")) {
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(FilesActivity.EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FilesActivity.FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
        else {
            val previewIntent = Intent(this, PreviewDocumentActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }*/
        else {
            val previewIntent = Intent(this, PreviewExcelActivity::class.java)
            previewIntent.putExtra(EXTRA_STREAM_URL, p.remotePath)
            previewIntent.putExtra(FILE_ID, p.remoteId)
            startActivity(previewIntent)
        }
    }

    private fun readVCF(p: RemoteFile) {
        try {
            val downFolder = File(
                getRootDirPath(
                    applicationContext
                )
            )
            val downloadedFile = File(downFolder, p.remotePath)
            if (downloadedFile.exists()) {
                //showPdfFromFile(downloadedFile)
                val vcards = Ezvcard.parse(downloadedFile).all()
                for (vcard: VCard in vcards) {
                    Log.i("Name: ", vcard.formattedName.value)
                    for (tel: Telephone in vcard.telephoneNumbers) {
                        Log.i("Telephone numbers:", tel.types.toString() + ": " + tel.text)
                        //System.out.println(tel.getTypes() + ": " + tel.getText());
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // File file = new File( p.remotePath);
    }

    override fun onDeleteClick(position: Int) {
        showDeleteDialog(position)
    }

    private fun showDeleteDialog(position: Int) {
        val factory = LayoutInflater.from(this)
        val deleteDialogView = factory.inflate(R.layout.delete_dialog, null)
        val deleteDialog = AlertDialog.Builder(this).create()
        deleteDialog.setView(deleteDialogView)
        deleteDialogView.findViewById<View>(R.id.yes).setOnClickListener(
            View.OnClickListener { //your business logic
                deleteDialog.dismiss()
                val p = mFilesAdapter!!.getItem(position)
                val path = p!!.remotePath
                val fileid = p.remoteId
                startRemoteDeletion(path, fileid)
            })
        deleteDialogView.findViewById<View>(R.id.no)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    deleteDialog.dismiss()
                }
            })
        deleteDialog.show()
    }

    private fun showUploadDialog() {
        val factory = LayoutInflater.from(this)
        val uploadDialogView = factory.inflate(R.layout.upload_file_dialog, null)
        val uploadDialog = AlertDialog.Builder(this).create()
        uploadDialog.setView(uploadDialogView)
        uploadDialogView.findViewById<View>(R.id.spinner).visibility = View.GONE
        // uploadDialogView.findViewById(R.id.spinner_text).setVisibility(View.VISIBLE);
        uploadDialogView.findViewById<View>(R.id.llSpinner).visibility = View.VISIBLE
        val folderName = uploadDialogView.findViewById<TextView>(R.id.spinner_text)
        folderName.text = subFolder
        uploadDialogView.findViewById<View>(R.id.browse)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
//                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
//                chooseFile.setType("*/*");
//                chooseFile = Intent.createChooser(chooseFile, "Choose a file");
//                startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
//                Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, PICKFILE_RESULT_CODE);
                    val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
                    galleryIntent.type = "*/*"
                    startActivityForResult(galleryIntent, PICKFILE_RESULT_CODE)
                }
            })
        uploadDialogView.findViewById<View>(R.id.yes)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    //your business logic
                    uploadDialog.dismiss()
                    startUpload(path, type)
                }
            })
        uploadDialogView.findViewById<View>(R.id.no)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    uploadDialog.dismiss()
                }
            })
        uploadDialog.show()
    }

    companion object {
        private val LOG_TAG = SubFilesActivity::class.java.canonicalName
        val PICKFILE_RESULT_CODE = 1
        val EXTRA_STREAM_URL = "STREAM_URL"
        val FILE_ID = "FILE_ID"
        val FILE_MODIFIED = "FILE_MODIFIED"

        /** Key to receive a flag signaling if the video should be started immediately  */
        val EXTRA_AUTOPLAY = "AUTOPLAY"

        /** Key to receive the position of the playback where the video should be put at start  */
        val EXTRA_START_POSITION = "START_POSITION"
    }
}