package com.techesolutions.data.remote.model.timezones

class TimeZoneModel internal constructor(var name: String?, var short_name: String?, var country: String?, var checked: Boolean)
