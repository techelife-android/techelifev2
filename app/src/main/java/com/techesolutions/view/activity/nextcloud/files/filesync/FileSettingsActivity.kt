package com.techesolutions.view.activity.nextcloud.files.filesync;
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.techesolutions.R
import com.techesolutions.utils.Constants
import com.techesolutions.utils.MySharedPreference
import kotlinx.android.synthetic.main.actvity_file_settings.*
import kotlinx.android.synthetic.main.layout_header.*
import java.util.*


class  FileSettingsActivity : AppCompatActivity() {
    private var folderpath: String?=null
    private var file:String?=null
    var folders: List<RemoteFile>? = null
    private var instantUploadFolder: String? = null
    private var photoEnable: Boolean? = null
    private var videoEnable: Boolean? = null
    var shared: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.actvity_file_settings)
                headerTitle!!.text = "Settings"
        shared = getSharedPreferences("TechePrefs", MODE_PRIVATE)
        photoEnable = shared!!.getBoolean(Constants.KEY_PHOTO_UPLOAD, false)
        videoEnable = shared!!.getBoolean(Constants.KEY_VIDEO_UPLOAD, false)
        //videoEnable = true
        instantUploadFolder = shared!!.getString(Constants.KEY_REMOTE_FOLDER, "")
        videoSwitch.isChecked= videoEnable as Boolean
        photoSwitch.isChecked= photoEnable as Boolean
        back.setOnClickListener { finish() }
        //spinner = findViewById(R.id.spinner)
        folders=intent.getParcelableArrayListExtra("folderList")
        val intent: Intent = intent
        val args: Bundle = intent.getBundleExtra("BUNDLE")
        folders = args.getSerializable("folderList") as ArrayList<RemoteFile>?
        val categoryAdapter = InstantUplaodFolderAdapter(this, (folders as ArrayList<RemoteFile>?)!!)
        spinner.setAdapter(categoryAdapter)
        spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>?, arg1: View,
                                        position: Int, arg3: Long) {
                folderpath = folders!!.get(position).remotePath
                //folderpath = spinner.getSelectedItem().toString();
                Log.i("Path", "" + folderpath)
                folderpath?.let {
                    MySharedPreference.setPreference(this@FileSettingsActivity, Constants.KEY_REMOTE_FOLDER,
                            it
                    )
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        )
        var position=0
        for (i in folders!!.indices) {
            if (folders!![i].remotePath.equals(instantUploadFolder)) {
                Log.i("folders",""+i)
                position = i
            }
        }
        spinner!!.setSelection(position)
        photoSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            Log.i("isChecked", "" + isChecked)
            MySharedPreference.setBooleanPreference(this, Constants.KEY_PHOTO_UPLOAD, isChecked)
        })
        videoSwitch.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            Log.i("isChecked", "" + isChecked)
            MySharedPreference.setBooleanPreference(this, Constants.KEY_VIDEO_UPLOAD, isChecked)
        })

    }

}
