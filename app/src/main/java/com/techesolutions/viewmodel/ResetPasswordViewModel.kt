package com.techesolutions.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techesolutions.data.remote.model.forgetpassword.CodeVerificationResponse
import com.techesolutions.data.remote.model.forgetpassword.ResetPasswordResponse
import com.techesolutions.data.remote.repository.login.ResetPasswordRepository
import com.techesolutions.data.remote.repository.login.VerifyCodeRepository

/**
 * Author: Neelam Upadhyay
 * Created: 30/12/2020
 */
class ResetPasswordViewModel: ViewModel() {

    var resetPasswordLiveData: MutableLiveData<ResetPasswordResponse>? = null

    fun resetPassword( hashcode: String, apporigin: String,password: String,re_password: String): LiveData<ResetPasswordResponse>? {
        resetPasswordLiveData = ResetPasswordRepository.resetPassword(hashcode,apporigin,password,re_password)
        return resetPasswordLiveData
    }
}