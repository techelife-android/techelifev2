package com.techesolutions.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.techesolutions.data.remote.model.forgetpassword.Forgot
import com.techesolutions.data.remote.repository.login.ForgotPasswordRepository

/**
 * Author: Neelam Upadhyay
 * Created: 30/12/2020
 */
class ForgotPasswordViewModel: ViewModel() {

    var forgotPasswordLiveData: MutableLiveData<Forgot>? = null

    fun getCode(email: String, app: String): LiveData<Forgot>? {
        forgotPasswordLiveData = ForgotPasswordRepository.getCode(email,app)
        return forgotPasswordLiveData
    }
}