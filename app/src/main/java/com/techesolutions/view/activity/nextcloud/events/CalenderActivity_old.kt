package com.techesolutions.view.activity.nextcloud.events

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.owncloud.android.lib.common.OwnCloudClient
import com.owncloud.android.lib.common.OwnCloudClientFactory
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory
import com.owncloud.android.lib.common.accounts.AccountUtils
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener
import com.owncloud.android.lib.common.operations.RemoteOperation
import com.owncloud.android.lib.common.operations.RemoteOperationResult
import com.owncloud.android.lib.resources.files.DownloadEventsRemoteOperation
import com.owncloud.android.lib.resources.files.ReadCalanderRemoteOperation
import com.owncloud.android.lib.resources.files.RemoveEventRemoteOperation
import com.owncloud.android.lib.resources.files.UploadCalenderRemoteOperation
import com.owncloud.android.lib.resources.files.model.RemoteFile
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener
import com.techesolutions.BuildConfig
import com.techesolutions.R
import com.techesolutions.customControls.CustomProgressDialog
import com.techesolutions.utils.Constants
import com.techesolutions.utils.Utils
import com.techesolutions.view.activity.nextcloud.events.decorators.EventDecorator
import com.techesolutions.view.activity.nextcloud.events.decorators.MySelectorDecorator
import com.techesolutions.view.activity.nextcloud.events.decorators.OneDayDecorator
import com.techesolutions.view.activity.nextcloud.files.utils.FileUtils
import com.techesolutions.view.activity.nextcloud.notes.shared.model.NoteClickListener
import kotlinx.android.synthetic.main.activity_calender.*
import kotlinx.android.synthetic.main.activity_vital_details.*
import kotlinx.android.synthetic.main.layout_header.*
import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Property
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.io.*
import java.lang.ref.WeakReference
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors

class CalenderActivity_old : AppCompatActivity() , OnDateSelectedListener, View.OnClickListener,
     NoteClickListener, OnRemoteOperationListener {
    private var widget: MaterialCalendarView? = null
    private var mHandler: Handler? = null
    private var mClient: OwnCloudClient? = null
    private var file: String?=null
    var recyclerView: RecyclerView? = null
    var adapter: CalenderAdapter? = null
    lateinit var dbCalender: ArrayList<Events>
    lateinit var dbFilterCalender: ArrayList<Events>
    private val progressDialog = CustomProgressDialog()
    private lateinit var FilePathStrings: Array<String>
    private lateinit var listFile: Array<File>
    var EVENT_DIR: String?=null
    var user: String?=null
    val DEBUG = true //BuildConfig.DEBUG;
    var selectedDate: String=""
    var selectedStartDate: String=""
    var timestamp: String=""
    var deletedFile:String?=null
    var startDate: Date?=null
    private val FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    private val oneDayDecorator: OneDayDecorator = OneDayDecorator()
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calender)
        headerTitle.text = "Events"
        back.setOnClickListener(this)
        dbCalender= ArrayList()
        dbCalender!!.clear()
        dbFilterCalender= ArrayList()
        dbFilterCalender!!.clear()
        mHandler = Handler()
        val shared = getSharedPreferences("TechePrefs", Context.MODE_PRIVATE)
        val url: String?=shared.getString(Constants.KEY_NEXT_CLOUD_URL, "")
        user=shared.getString(Constants.KEY_NEXT_CLOUD_USER, "")
        val pass: String?=shared.getString(Constants.KEY_NEXT_CLOUD_PASSWORD, "")
        EVENT_DIR = "/calendars/"+user+"/personal/"
        Log.i("data", url + ", " + user + ", " + pass)
        var serverUri: Uri?=null
        if(url!!.endsWith("/")) {
            serverUri = Uri.parse(url!!.substring(0, url.length - 1))
        }
        else{
            serverUri = Uri.parse(url!!)
        }
        mClient = OwnCloudClientFactory.createOwnCloudClient(serverUri, this, true)
        mClient!!.setCredentials(
            OwnCloudCredentialsFactory.newBasicCredentials(
                user!!,
                pass!!
            )
        )
               // Gets the calendar from the view
        widget = findViewById(R.id.calendarView)
        widget!!.setOnDateChangedListener(this)
        //widget!!.setShowOtherDates(MaterialCalendarView.SHOW_ALL)

        val instance: LocalDate = LocalDate.now()
        widget!!.setSelectedDate(instance)

        val min: LocalDate =
            LocalDate.of(instance.getYear(), org.threeten.bp.Month.JANUARY, 1)
        val max: LocalDate =
            LocalDate.of(instance.getYear(), org.threeten.bp.Month.DECEMBER, 31)

        widget!!.state().edit().setMinimumDate(min).setMaximumDate(max).commit()

        widget!!.addDecorators(
            MySelectorDecorator(this),
            oneDayDecorator
        )

   // Extract the day from the text
        val calendar = Calendar.getInstance()
        val currentDate:Date=calendar.getTime()
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        // val sdf = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'")
        selectedStartDate = sdf.format(currentDate)
        //startRefresh()
        setData(selectedStartDate)
       // Toast.makeText(this, "Current Day: $selectedStartDate", Toast.LENGTH_SHORT).show()

        val sdfTimestamp = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'")
        timestamp = sdfTimestamp.format(currentDate)
        //Log.i("cal day", calendar[Calendar.DAY_OF_MONTH].toString())
        /*ApiSimulator(widget!!)
            .executeOnExecutor(
                Executors.newSingleThreadExecutor()
            )*/
        val task = MyAsyncTask(this)
        task.execute()
       /* for (i in 0..30) {
            *//* var eventDate: String? =
                 activity?.let { Utils.convertEventDateFormat(dbCal.stratTime, it) }*//*
            if(Utils.convertEventDateFormat(dbCalender[i].stratTime))
            // temp = temp.plusDays(5)
        }*/
    }
    override fun onClick(v: View?) {
        when (v?.id) {

            R.id.back -> {
                finish()
            }

            }
    }


    fun onErrorListener(`object`: Any?) {
        if (`object` != null) {
            Utils.showCustomToast(`object`.toString(), this)
        }
    }

    private fun closeLoading() {
        if (progressDialog != null && progressDialog?.dialog.isShowing())
            progressDialog?.dialog.dismiss()    }

    private fun showLoading(msg: String) {
        progressDialog?.show(this, msg)
    }

    override fun onRemoteOperationFinish(
        operation: RemoteOperation<*>?,
        result: RemoteOperationResult<*>?
    ) {
        if (!result!!.isSuccess()) {
            Toast.makeText(this, R.string.todo_operation_finished_in_fail, Toast.LENGTH_SHORT)
                .show()
            closeLoading()
        }
        else if (operation is ReadCalanderRemoteOperation) {
            onSuccessfulRefresh(operation as ReadCalanderRemoteOperation?, result)
        }
        else if(operation is UploadCalenderRemoteOperation)
        {
            Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                .show()
            onSuccessfulUpload(operation as UploadCalenderRemoteOperation?, result)

        }
        else if(operation is RemoveEventRemoteOperation)
        {
            Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                .show()
            closeLoading()
            /* Toast.makeText(this, R.string.todo_operation_finished_in_success, Toast.LENGTH_SHORT)
                 .show()*/
            val downFolder = File(FileUtils.getRootDirPath(baseContext) + EVENT_DIR)
            Log.i("download folder", downFolder.absolutePath)
            if(downFolder.exists()) {
                val downloadedFile = File(downFolder, deletedFile)
                if (downloadedFile.exists()) {
                    downloadedFile.delete()
                }
            }
            dbCalender.clear()
            dbFilterCalender.clear()
            if(downFolder.exists()) {
                listFile = downFolder.listFiles()
                if (listFile != null && listFile.size > 0) {
                    for (i in 0 until listFile.size) {
                        val downloadedFile = File(downFolder, listFile.get(i).name)
                        Log.i("file event", downloadedFile.toString())
                        downloadedFile?.let {
                            readICS(
                                selectedStartDate,
                                it
                            )
                        }
                    }

                }
            }
        }
        else {
            closeLoading()
            val downFolder = File(FileUtils.getRootDirPath(baseContext) + EVENT_DIR)
            Log.i("download folder", downFolder.absolutePath)
            dbCalender.clear()
            dbFilterCalender.clear()
            if(downFolder.exists()) {
                listFile = downFolder.listFiles()
                if (listFile != null && listFile.size > 0) {
                    for (i in 0 until listFile.size) {
                        val downloadedFile = File(downFolder, listFile.get(i).name)
                        Log.i("file event", downloadedFile.toString())
                        downloadedFile?.let {
                            readICS(
                                selectedStartDate,
                                it
                            )
                        }
                    }

                }
            }
            else{
                downFolder.mkdir()
            }
        }
    }
    private fun onSuccessfulRefresh(
        readContactsRemoteOperation: ReadCalanderRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        //mFilesAdapter.clear()
        val files: MutableList<RemoteFile> = ArrayList()
        for (obj in result.data) {
            files!!.add(obj as RemoteFile)
            Log.i("Calander data", files.toString())
        }
        if (files != null) {
            val it: Iterator<RemoteFile> = files.iterator()
            while (it.hasNext()) {
                startDownload(it.next().remotePath)
            }
        }
    }
    private fun startDownload(filename: String) {
        file=filename
        Log.i("Calander", "downloading")
        Log.i("Calander", "download path: " + filename)

        val downFolder = File(FileUtils.getRootDirPath(baseContext) + EVENT_DIR)
        // downFolder.mkdir()
        //val f = File("somedirname1/somedirname2/somefilename")
        if (!downFolder.parentFile.exists()) downFolder.parentFile.mkdirs()
        if (!downFolder.exists()) downFolder.mkdir()
        if(filename.length>2) {
            val downloadOperation = DownloadEventsRemoteOperation(user,filename, downFolder.absolutePath,"")
            downloadOperation.execute(mClient, this, mHandler)
        }
    }

    private fun onSuccessfulUpload(
        uploadContactRemoteOperation: UploadCalenderRemoteOperation?,
        result: RemoteOperationResult<*>
    ) {
        closeLoading()
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun setData(selectedStartDate: String) {
        dbCalender.clear()
        dbFilterCalender!!.clear()
        val downFolder = File(FileUtils.getRootDirPath(baseContext) + EVENT_DIR)
        Log.i("download folder", downFolder.absolutePath)
        if(downFolder.exists()) {
            listFile = downFolder.listFiles()
            if (listFile != null && listFile.size > 0) {
                for (i in 0 until listFile.size) {
                    val downloadedFile = File(downFolder, listFile.get(i).name)
                    Log.i("file event", downloadedFile.toString())
                    downloadedFile?.let {
                        readICS(
                            selectedStartDate,
                            it
                        )
                    }
                }

            }
        }
    }

    @SuppressLint("WrongConstant")
    private fun readICS(selectedStartDate: String, downloadedFile: File) {
        //dbCalender = ArrayList<Contacts>()
        Log.i("date", selectedStartDate)
        try {
            val fin = FileInputStream(downloadedFile)
            //Uri uri = Uri.fromFile(file);
            val uri =
                FileProvider.getUriForFile(
                    this@CalenderActivity_old,
                    BuildConfig.APPLICATION_ID + ".provider",
                    downloadedFile
                )

            //use ical4j to parse the event
            val cb = CalendarBuilder()
            val vcalendar: net.fortuna.ical4j.model.Calendar = cb.build(
                getStreamFromOtherSource(
                    this,
                    uri
                )
            )

            if(vcalendar!=null) {
                var title=""
                var description=""
                var sTime=""
                var eTime=""
                var eDueTime=""
                var timestamp=""
                var uid=""
                var timer=""
                val i = vcalendar!!.getComponents().iterator()
                while (i.hasNext()) {
                    val component = i.next() as Component
                    Log.i("calender", "Component [" + component.getName() + "]")
                    val j = component.getProperties().iterator()
                    while (j.hasNext()) {
                        val property = j.next() as Property

                        if(property.getName().equals("SUMMARY")) {
                            title = property.getValue()
                        }
                        if(property.getName().equals("DESCRIPTION")) {
                            description = property.getValue()
                        }
                        if(property.getName().equals("DTSTART")) {
                            sTime = property.getValue()
                        }
                        if(property.getName().equals("DTEND")) {
                            eTime = property.getValue()
                        }
                        if(property.getName().equals("DUE")) {
                            eDueTime = property.getValue()
                        }
                        if(property.getName().equals("DTSTAMP")) {
                            timestamp = property.getValue()
                        }
                        if(property.getName().equals("UID")) {
                            uid = property.getValue()
                        }
                        if(property.getName().equals("TRIGGER")) {
                            timer = property.getValue()
                        }

                    }
                    if(component.getName().equals("VEVENT")) {
                        dbCalender!!.add(
                            Events(
                                "VEVENT",
                                title,
                                description,
                                sTime,
                                eTime,
                                timestamp,
                                uid,
                                downloadedFile.name,
                                timer
                            )
                        )
                    }
                    if(component.getName().equals("VTODO") && eDueTime.length>1)
                    {
                        dbCalender!!.add(
                            Events(
                                "VTODO",
                                title,
                                description,
                                sTime,
                                eDueTime,
                                timestamp,
                                uid,
                                downloadedFile.name,
                                timer
                            )
                        )
                    }
                    var eventDate: String =
                        Utils.convertEventDate(sTime, this@CalenderActivity_old)
                    if(eventDate.equals(selectedStartDate))
                    {
                        dbFilterCalender!!.add(
                            Events(
                                component.getName(),
                                title,
                                description,
                                sTime,
                                eTime,
                                timestamp,
                                uid,
                                downloadedFile.name,
                                timer
                            )
                        )
                    }
                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.i("dbEvents", dbFilterCalender.toString())
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        recyclerView!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
       // adapter = CalenderAdapter(this@CalenderActivity_old, dbFilterCalender, this)

        //now adding the adapter to recyclerview
        recyclerView?.adapter = adapter
        recyclerView?.adapter!!.notifyDataSetChanged()

    }
    private fun getStreamFromOtherSource(context: Context, contentUri: Uri): InputStream? {
        val res = context.applicationContext.contentResolver
        val uri = Uri.parse(contentUri.toString())
        val `is`: InputStream?
        `is` = try {
            res.openInputStream(uri)
        } catch (e: FileNotFoundException) {
            ByteArrayInputStream(ByteArray(0))
        }
        return `is`
    }

    override fun onNoteClick(position: Int) {
        val intent: Intent = Intent(
            this@CalenderActivity_old,
            CreateEventActivity::class.java
        )
        intent.putExtra("action", "edit")
        intent.putExtra("from", "calender")
        intent.putExtra("title", dbFilterCalender[position].title)
        intent.putExtra("desc", dbFilterCalender[position].description)
        intent.putExtra("stime", dbFilterCalender[position].stratTime)
        intent.putExtra("etime", dbFilterCalender[position].endTime)
        intent.putExtra("filename", dbFilterCalender[position].filename)
        startActivityForResult(intent, 1)
    }

    override fun onNoteDeleteClick(position: Int) {
        showDeleteDialog(position)
    }

    override fun onNoteLongClick(position: Int, v: View?): Boolean {
        TODO("Not yet implemented")
    }
    private fun showDeleteDialog(position: Int) {
        val factory = LayoutInflater.from(this)
        val deleteDialogView = factory.inflate(R.layout.delete_dialog, null)
        val deleteDialog = AlertDialog.Builder(this).create()
        deleteDialog.setView(deleteDialogView)
        deleteDialogView.findViewById<View>(R.id.yes).setOnClickListener { //your business logic
            deleteDialog.dismiss()
            val p: Events = adapter!!.getItem(position)!!
            val path = "/"+p.filename
            startRemoteDeletion(path, p.filename)
        }
        deleteDialogView.findViewById<View>(R.id.no).setOnClickListener { deleteDialog.dismiss() }
        deleteDialog.show()
    }

    private fun startRemoteDeletion(path: String, filename: String) {
        showLoading("Deleting file")
        deletedFile=filename
        val removeOperation = RemoveEventRemoteOperation(user,"",path)
        removeOperation.execute(mClient, this, mHandler)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                startRefresh()
            }
        }
    }
    private fun startRefresh() {
        showLoading("Please wait...")
        val refreshOperation = ReadCalanderRemoteOperation(user,com.owncloud.android.lib.resources.files.FileUtils.PATH_SEPARATOR,"")
        refreshOperation.execute(mClient, this, mHandler)
    }

    override fun onDateSelected(
        widget: MaterialCalendarView,
        date: CalendarDay,
        selected: Boolean
    ) {
        oneDayDecorator.setDate(date.date)
        selectedStartDate= FORMATTER.format(date.date)
        widget.invalidateDecorators()
        setData(selectedStartDate)
    }


    private class ApiSimulator(widget: MaterialCalendarView) :
        AsyncTask<Void?, Void?, List<CalendarDay>>() {
         override fun doInBackground(vararg params: Void?): List<CalendarDay>? {
            try {
                Thread.sleep(2000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            var temp = LocalDate.now().minusMonths(1)
            val dates = ArrayList<CalendarDay>()
            for (i in 0..31) {
                val day = CalendarDay.from(temp)
                dates.add(day)
                temp = temp.plusDays(5)
            }
            return dates
        }

        override fun onPostExecute(@NonNull calendarDays: List<CalendarDay>) {
            super.onPostExecute(calendarDays)
           /* if (isFinishing()) {
                return
            }*/
            //widget!!.addDecorator(EventDecorator(Color.RED, calendarDays))
        }
    }

    companion object {
        class MyAsyncTask internal constructor(context: CalenderActivity_old) : AsyncTask<Void?, Void?, List<CalendarDay>>() {

            private var resp: String? = null
            private val activityReference: WeakReference<CalenderActivity_old> = WeakReference(context)


            override fun doInBackground(vararg params: Void?): List<CalendarDay>? {
                val activity = activityReference.get()
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                var temp = LocalDate.now()
                Log.i("dates",temp.toString())
                val dates = ArrayList<CalendarDay>()
               // for (i in 0..Integer.parseInt(temp.toString().substring(8,10))-1) {
                    for (i in 0..activity!!.dbCalender.size-1) {
                        var eventDate: String? =
                            activity?.let { Utils.convertEventEditOnlyDateFormat(activity!!.dbCalender[i].stratTime, it) }
                        //eventDate?.let { activity!!.setData(it) }
                       Log.i("dates",eventDate)
                        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
                        val sdate: Date? = inputFormat.parse(eventDate)

                 //   }
                    val day = CalendarDay.from(temp)
                    dates.add(day)
                    temp = temp.plusDays(5)
                }
                return dates
            }


            override fun onPostExecute(@NonNull calendarDays: List<CalendarDay>) {

                val activity = activityReference.get()
                if (activity == null || activity.isFinishing) return
                activity.widget!!.addDecorator(EventDecorator(Color.RED, calendarDays))
            }

        }
    }
}

